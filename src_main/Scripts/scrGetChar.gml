//
// scrGetChar(key)
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/
switch (argument0) {
  case vk_space: return " ";
  case ord("1"): if (keyboard_check(vk_shift)) return "!"; else return "1";
  case ord("2"): if (keyboard_check(vk_shift)) return "@"; else return "2";
  case ord("3"): if (keyboard_check(vk_shift)) return "#"; else return "3";
  case ord("4"): if (keyboard_check(vk_shift)) return "$"; else return "4";
  case ord("5"): if (keyboard_check(vk_shift)) return "%"; else return "5";
  case ord("6"): if (keyboard_check(vk_shift)) return "^"; else return "6";
  case ord("7"): if (keyboard_check(vk_shift)) return "&"; else return "7";
  case ord("8"): if (keyboard_check(vk_shift)) return "*"; else return "8";
  case ord("9"): if (keyboard_check(vk_shift)) return "("; else return "9";
  case ord("0"): if (keyboard_check(vk_shift)) return ")"; else return "0";
  case ord("A"): if (keyboard_check(vk_shift)) return "A"; else return "a";
  case ord("B"): if (keyboard_check(vk_shift)) return "B"; else return "b";
  case ord("C"): if (keyboard_check(vk_shift)) return "C"; else return "c";
  case ord("D"): if (keyboard_check(vk_shift)) return "D"; else return "d";
  case ord("E"): if (keyboard_check(vk_shift)) return "E"; else return "e";
  case ord("F"): if (keyboard_check(vk_shift)) return "F"; else return "f";
  case ord("G"): if (keyboard_check(vk_shift)) return "G"; else return "g";
  case ord("H"): if (keyboard_check(vk_shift)) return "H"; else return "h";
  case ord("I"): if (keyboard_check(vk_shift)) return "I"; else return "i";
  case ord("J"): if (keyboard_check(vk_shift)) return "J"; else return "j";
  case ord("K"): if (keyboard_check(vk_shift)) return "K"; else return "k";
  case ord("L"): if (keyboard_check(vk_shift)) return "L"; else return "l";
  case ord("M"): if (keyboard_check(vk_shift)) return "M"; else return "m";
  case ord("N"): if (keyboard_check(vk_shift)) return "N"; else return "n";
  case ord("O"): if (keyboard_check(vk_shift)) return "O"; else return "o";
  case ord("P"): if (keyboard_check(vk_shift)) return "P"; else return "p";
  case ord("Q"): if (keyboard_check(vk_shift)) return "Q"; else return "q";
  case ord("R"): if (keyboard_check(vk_shift)) return "R"; else return "r";
  case ord("S"): if (keyboard_check(vk_shift)) return "S"; else return "s";
  case ord("T"): if (keyboard_check(vk_shift)) return "T"; else return "t";
  case ord("U"): if (keyboard_check(vk_shift)) return "U"; else return "u";
  case ord("V"): if (keyboard_check(vk_shift)) return "V"; else return "v";
  case ord("W"): if (keyboard_check(vk_shift)) return "W"; else return "w";
  case ord("X"): if (keyboard_check(vk_shift)) return "X"; else return "x";
  case ord("Y"): if (keyboard_check(vk_shift)) return "Y"; else return "y";
  case ord("Z"): if (keyboard_check(vk_shift)) return "Z"; else return "z";
  case 186: if (keyboard_check(vk_shift)) return ":"; else return ";";
  case 187: if (keyboard_check(vk_shift)) return "+"; else return "=";
  case 188: if (keyboard_check(vk_shift)) return "<"; else return ",";
  case 189: if (keyboard_check(vk_shift)) return "_"; else return "-";
  case 190: if (keyboard_check(vk_shift)) return ">"; else return ".";
  case 191: if (keyboard_check(vk_shift)) return "?"; else return "/";
  case 192: if (keyboard_check(vk_shift)) return "~"; else return "`";
  case 219: if (keyboard_check(vk_shift)) return "{"; else return "[";
  case 220: if (keyboard_check(vk_shift)) return "|"; else return chr(92);
  case 221: if (keyboard_check(vk_shift)) return "}"; else return "]";
  case 222: if (keyboard_check(vk_shift)) return '"'; else return "'";
  case vk_numpad0: return "0";
  case vk_numpad1: return "1";
  case vk_numpad2: return "2";
  case vk_numpad3: return "3";
  case vk_numpad4: return "4";
  case vk_numpad5: return "5";
  case vk_numpad6: return "6";
  case vk_numpad7: return "7";
  case vk_numpad8: return "8";
  case vk_numpad9: return "9";
  case vk_decimal: return ".";
  case vk_add: return "+";
  case vk_subtract: return "-";
  case vk_multiply: return "*";
  case vk_divide: return "/";
  default: return "";
}
