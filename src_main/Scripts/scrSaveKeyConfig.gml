var file;
file = file_text_open_write("yasm_keys.cfg");
if (file) {
  file_text_write_string(file, "up=");
   file_text_write_string(file, string(global.keyUpVal));
   file_text_writeln(file);

  file_text_write_string(file, "down=");
   file_text_write_string(file, string(global.keyDownVal));
   file_text_writeln(file);

  file_text_write_string(file, "left=");
   file_text_write_string(file, string(global.keyLeftVal));
   file_text_writeln(file);

  file_text_write_string(file, "right=");
   file_text_write_string(file, string(global.keyRightVal));
   file_text_writeln(file);

  file_text_write_string(file, "jump=");
   file_text_write_string(file, string(global.keyJumpVal));
   file_text_writeln(file);

  file_text_write_string(file, "attack=");
   file_text_write_string(file, string(global.keyAttackVal));
   file_text_writeln(file);

  file_text_write_string(file, "item=");
   file_text_write_string(file, string(global.keyItemVal));
   file_text_writeln(file);

  file_text_write_string(file, "run=");
   file_text_write_string(file, string(global.keyRunVal));
   file_text_writeln(file);

  file_text_write_string(file, "bomb=");
   file_text_write_string(file, string(global.keyBombVal));
   file_text_writeln(file);

  file_text_write_string(file, "rope=");
   file_text_write_string(file, string(global.keyRopeVal));
   file_text_writeln(file);

  file_text_write_string(file, "pay=");
   file_text_write_string(file, string(global.keyPayVal));
   file_text_writeln(file);

  file_text_close(file);
}
