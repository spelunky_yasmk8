// scrUsePutItemOnGround(xVelMult, yVelNew)
// put item which player holds in his hands on the ground if player is ducking
// return `true` if item was put
if (scrPlayerIsDucking()) {
  holdItem.held = false;
  holdItem.resaleValue = 0;
  madeOffer = false;
  holdItem.safe = true;
  holdItem.alarm[2] = 10;
  if (facing == LEFT) {
    if (holdItem.heavy) holdItem.xVel = -4+xVel; else holdItem.xVel = -8+xVel;
  } else if (facing == RIGHT) {
    if (holdItem.heavy) holdItem.xVel = 4+xVel; else holdItem.xVel = 8+xVel;
  }
  holdItem.xVel *= argument0; //0.4;
  holdItem.yVel = argument1; //0.5;
  holdItem = 0;
  return true;
} else {
  return false;
}
