if (holdItem.type == "Damsel") {
  if (kDown) {
    // drop gently - damsel remains calm after stun
    holdItem.status = 2; // THROWN
    holdItem.calm = true; // prevents running after recover from stun
    holdItem.y -= 2;
    holdItem.counter = round(holdItem.stunMax/2); //holdItem.stunMax
  } else {
    // throw - damsel freaks out after stun
    holdItem.status = 2; // THROWN
    holdItem.counter = holdItem.stunMax;
    holdItem.y -= 4;
    if (holdItem.hp > 0) playSound(global.sndDamsel);
  }
}

holdItem.held = false;
madeOffer = false;
holdItem.resaleValue = 0;
holdItem.safe = true;
holdItem.alarm[2] = 10;

if (facing == LEFT) {
  if (holdItem.heavy) holdItem.xVel = -4+xVel; else holdItem.xVel = -8+xVel;
  if (collision_point(x-8, y, oSolid, 0, 0)) holdItem.x += 8; // prevent getting stuck in wall
} else if (facing == RIGHT) {
  if (holdItem.heavy) holdItem.xVel = 4+xVel; else holdItem.xVel = 8+xVel;
  if (collision_point(x+8, y, oSolid, 0, 0)) holdItem.x -= 8; // prevent getting stuck in wall
}
if (holdItem.heavy) holdItem.yVel = -2; else holdItem.yVel = -3;
if (kUp) { if (holdItem.heavy) holdItem.yVel = -4; else holdItem.yVel = -9; }
if (kDown) {
  if (platformCharacterIs(ON_GROUND)) {
    holdItem.y -= 2;
    holdItem.xVel *= 0.6;
    holdItem.yVel = 0.5;
  } else {
    holdItem.yVel = 3;
  }
} else if (!global.hasMitt) {
  if (facing == LEFT) {
    if (collision_point(x-8, y-10, oSolid, 0, 0)) {
      holdItem.yVel = 0;
      holdItem.xVel -= 1;
    }
  } else if (facing == RIGHT) {
    if (collision_point(x+8, y-10, oSolid, 0, 0)) {
      holdItem.yVel = 0;
      holdItem.xVel += 1;
    }
  }
}

if (global.hasMitt && !scrPlayerIsDucking()) {
  if (holdItem.xVel < 0) holdItem.xVel -= 6; else holdItem.xVel += 6;
  if (!kUp && !kDown) holdItem.yVel = -0.4; else if (kDown) holdItem.yVel = 6;
  holdItem.myGrav = 0.1;
}

if (holdItem.sprite_index == sBombBag ||
    holdItem.sprite_index == sBombBox ||
    holdItem.sprite_index == sRopePile)
{
    // do nothing
} else {
  playSound(global.sndThrow);
}

if (holdItem.sprite_index == sBombArmed) scrHoldItem(pickupItemType); else holdItem = 0;
