//
// scrUseItem()
//
// Use currently held item.  Must be called by oPlayer1.
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/
var n, obj, tx, ty;
var asleft, xofs, xsgn, xsbspr;

if (holdItem.sprite_index == sBomb) {
  holdItem.sprite_index = sBombArmed;
  holdItem.armed = true;
  holdItem.resaleValue = 0;
  with (holdItem) {
    alarm[0] = 80;
    image_speed = 0.2;
  }
} else if (holdItem.sprite_index == sRopeEnd) {
  if (!kDown && colTop) {
    // do nothing
  } else {
    holdItem.held = false;
    holdItem.armed = true;
    holdItem.resaleValue = 0;

    holdItem.px = x;
    holdItem.py = y;
    if (platformCharacterIs(ON_GROUND)) holdItem.startY = y; // YASM 1.7

    if (kDown) {
      if (facing == LEFT) obj = instance_create(x-12, y, oRopeThrow); else obj = instance_create(x+12, y, oRopeThrow);
      with (obj) {
        t = true;
        move_snap(16, 1);
        if (oPlayer1.x < x && !collision_point(oPlayer1.x+2, oPlayer1.y, oSolid, 0, 0)) {
               if (!collision_rectangle(x-8, y, x-7, y+16, oSolid, 0, 0)) x -= 8;
          else if (!collision_rectangle(x+7, y, x+8, y+16, oSolid, 0, 0)) x += 8;
          else t = false;
        } else if (!collision_point(oPlayer1.x-2, oPlayer1.y, oSolid, 0, 0)) {
               if (!collision_rectangle(x+7, y, x+8, y+16, oSolid, 0, 0)) x += 8;
          else if (!collision_rectangle(x-8, y, x-7, y+16, oSolid, 0, 0)) x -= 8;
          else t = false;
        }
        if (!t) {
          obj = oPlayer1.holdItem;
          obj = instance_create(obj.x, obj.y, oRopeThrow);
          if (oPlayer1.facing == 18) obj.xVel = -3.2; else obj.xVel = 3.2;
          obj.yVel = 0.5;
          instance_destroy();
        } else {
          instance_create(x, y, oRopeTop);
          armed = false;
          falling = true;
          xVel = 0;
          yVel = 0;
        }
      }
      with (holdItem) instance_destroy();
      holdItem = 0;
    } else {
      holdItem.x = x;
      holdItem.xVel = 0;
      holdItem.yVel = -12;
    }

    scrHoldItem(pickupItemType);
    playSound(global.sndThrow);
  }
} else if (holdItem.type == "Machete") {
  if (kDown && !whipping) scrUsePutItemOnGround(0.4, 0.5);
  if (!scrPlayerIsDucking() && !whipping) {
    image_speed = 1;
         if (global.isTunnelMan) sprite_index = sTunnelAttackL;
    else if (global.isDamsel) sprite_index = sDamselAttackL;
    else sprite_index = sAttackLeft;
    image_index = 0;
    whipping = true;
    holdItem.visible = false;
  }
} else if (holdItem.type == "Mattock") {
  if (kDown && !whipping) scrUsePutItemOnGround(0.4, 0.5);
  if (!scrPlayerIsDucking() && !whipping && platformCharacterIs(ON_GROUND)) {
    image_speed = 0.2;
         if (global.isTunnelMan) sprite_index = sTunnelAttackL;
    else if (global.isDamsel) sprite_index = sDamselAttackL;
    else sprite_index = sAttackLeft;
    image_index = 0;
    whipping = true;
    cantJump = 20;
    holdItem.visible = false;
  }
} else if (holdItem.type == "Pistol") {
  if (kDown) scrUsePutItemOnGround(0.4, 0.5);
  if (firing == 0 && !scrPlayerIsDucking()) {
    // fire gun
    if (facing == LEFT) {
      asleft = true;
      xofs = -9;
      xsgn = -1;
      xsbspr = oShotgunBlastLeft;
    } else {
      asleft = false;
      xofs = 8;
      xsgn = 1;
      xsbspr = oShotgunBlastRight;
    }
    instance_create(x+xofs, y+1, xsbspr);
    obj = instance_create(x+xofs, y-2, oBullet);
    obj.xVel = (xsgn*rand(6, 8))+xVel;
    if (asleft) {
      if (obj.xVel > -6) obj.xVel = -6;
    } else {
      if (obj.xVel < 6) obj.xVel = 6;
    }
    obj.yVel = 0;
    if (state != HANGING && state != CLIMBING) {
      yVel -= 1;
      xVel -= xsgn;
    }
    playSound(global.sndShotgun);
    firing = firingPistolMax;
  }
} else if (holdItem.type == "Shotgun" || holdItem.type == "Cheatgun") {
  if (kDown) scrUsePutItemOnGround(0.4, 0.5);
  if (firing == 0 && !scrPlayerIsDucking()) {
    if (!global.optSGAmmo || global.sgammo > 0 || holdItem.type == "Cheatgun" || isAshShotgun(holdItem)) {
      if (facing == LEFT) {
        asleft = true;
        xofs = -9;
        xsgn = -1;
        xsbspr = oShotgunBlastLeft;
      } else {
        asleft = false;
        xofs = 8;
        xsgn = 1;
        xsbspr = oShotgunBlastRight;
      }
      if (global.optSGAmmo && holdItem.type != "Cheatgun" && !isAshShotgun(holdItem)) global.sgammo -= 1;
      instance_create(x+xofs, y+1, xsbspr);
      repeat(6) {
        obj = instance_create(x+xofs, y-2, oBullet);
        obj.xVel = (xsgn*rand(6, 8))+xVel;
        if (asleft) {
          if (obj.xVel >= -6) obj.xVel = -6;
        } else {
          if (obj.xVel < 6) obj.xVel = 6;
        }
        obj.yVel = random(1)-random(1);
      }
      if (state != HANGING && state != CLIMBING && holdItem.type != "Cheatgun") {
        yVel -= 1;
        xVel -= 3*xsgn;
      }
      playSound(global.sndShotgun);
      if (holdItem.type == "Cheatgun") firing = 10; else firing = firingShotgunMax;
    } else if (global.optThrowEmptyShotgun) {
      if (!kDown) scrUseThrowItem();
    }
  }
} else if (holdItem.type == "Sceptre") {
  if (kDown) scrUsePutItemOnGround(0.4, 0.5);
  if (firing == 0 && !scrPlayerIsDucking()) {
    if (facing == LEFT) {
      asleft = true;
      xsgn = -1;
    } else {
      asleft = false;
      xsgn = 1;
    }
    xofs = 12*xsgn;
    repeat(3) {
      obj = instance_create(x+xofs, y+4, oPsychicCreateP);
      obj.xVel = xsgn*rand(1, 3);
      obj.yVel = -random(2);
    }
    obj = instance_create(x+xofs, y-2, oPsychicWaveP);
    obj.xVel = xsgn*6;
    playSound(global.sndPsychic);
    firing = firingPistolMax;
  }
} else if (holdItem.type == "Web Cannon") {
  if (kDown) scrUsePutItemOnGround(0.4, 0.5);
  if (firing == 0 && !scrPlayerIsDucking()) {
    if (facing == LEFT) {
      asleft = true;
      xsgn = -1;
      xsbspr = oShotgunBlastLeft;
    } else {
      asleft = false;
      xsgn = 1;
      xsbspr = oShotgunBlastRight;
    }
    xofs = 12*xsgn;

    instance_create(x+xofs, y, oShotgunBlastLeft);
    obj = instance_create(x+xofs, y-2, oWebBall);
    obj.xVel = (xsgn*rand(6, 8))+xVel;
    if (asleft) {
      if (obj.xVel >= -6) obj.xVel = -6;
    } else {
      if (obj.xVel < 6) obj.xVel = 6;
    }
    obj.yVel = 0;
    if (state != HANGING && state != CLIMBING) {
      yVel -= 1;
      xVel -= xsgn;
    }
    playSound(global.sndShotgun);
    firing = firingPistolMax;
  }
} else if (holdItem.type == "Teleporter") {
  if (kDown) {
    scrUsePutItemOnGround(0.4, 0.5);
  } else {
    if (kUp) {
      tx = x;
      ty = y-(16*rand(4, 8));
      while (ty < 16) ty += 16;
    } else if (firing == 0 && !scrPlayerIsDucking()) {
      if (facing == LEFT) {
        tx = x-(16*rand(4, 8));
        ty = y;
        if (tx < 8) tx = 8;
      } else if (facing == RIGHT) {
        tx = x+(16*rand(4, 8));
        ty = y;
        if (tx > room_width - 8) tx = room_width - 8;
      } else {
        // silence the linter
        tx = 0;
        ty = 0;
      }
    } else {
      // silence the linter
      tx = 0;
      ty = 0;
    }
    n = 0;
    while (collision_rectangle(tx-4, ty-4, tx+4, ty+4, oSolid, 0, 0) && n < 3 && ty > 16) {
      ty -= 16;
      n += 1;
    }
    repeat(3) instance_create(x-4+rand(0, 8), y-4+rand(0, 8), oFlareSpark);
    if (y < 8) y = 8;
    x = tx;
    y = ty;
    with (oBall) {
      x = oPlayer1.x;
      y = oPlayer1.y;
    }
    with (oChain) {
      x = oPlayer1.x;
      y = oPlayer1.y;
    }
    // state = STANDING;
    obj = instance_place(x, y, oEnemy);
    if (obj) {
      with (obj) {
        scrCreateBlood(oPlayer1.x, oPlayer1.y, 3);
        hp -= 99;
        instance_destroy();
      }
    }
    playSound(global.sndTeleport);
    with (oPlayer1) state = 16;
  }
} else if (holdItem.type == "Teleporter II") {
  scrUseTeleporter2();
} else if (holdItem.type == "Bow") {
  if (kDown) {
    scrUsePutItemOnGround(0.4, 0.5);
  } else if (firing == 0 && !scrPlayerIsDucking() && !bowArmed && global.arrows > 0) {
    bowArmed = true;
    playSound(global.sndBowPull);
  } else if (global.arrows <= 0) {
    global.message = "I'M OUT OF ARROWS!";
    global.message2 = "";
    global.messageTimer = 80;
  }
} else if (holdItem.type == "Block Item") {
  /*
  if (kDown) {
    if (scrUsePutItemOnGround(0.4, 0.5)) {
      crafting = false;
      with (oBlockPreview) instance_destroy();
    }
  }
  */
} else {
  // throw item
  scrUseThrowItem();
}

if (kDown && holdItem) { holdItem.x = x; holdItem.y = y; } //k8: was without "{}" -- bug?
if (holdItem == 0) pickupItemType = "";
