//
// scrCreateTileObj(tile, x, y)
//
// Creates an obj in the level loader.
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/

if (argument0 == "@")
{
    instance_create(argument1, argument2, oEntrance);
}
else if (argument0 == "X")
{
    instance_create(argument1, argument2, oExit);
}
else if (argument0 == "Р")
{
    instance_create(argument1, argument2, oXMarket);

    brick = 0;
    brick = collision_point(argument1-16, argument2, oSolid, 0, 0);
    if (brick)
        {
        if (brick.sprite_index == sLush)
            {
            instance_create(argument1, argument2, oLush);
            brick2 = instance_create(argument1, argument2+16, oLush);
            brick2.invincible = true;
            }
        else if (brick.sprite_index == sDark)
            {
            instance_create(argument1, argument2, oDark);
            brick2 = instance_create(argument1, argument2+16, oDark);
            brick2.invincible = true;
            }
        else if (brick.sprite_index == sTemple)
            {
            instance_create(argument1, argument2, oTemple);
            brick2 = instance_create(argument1, argument2+16, oTemple);
            brick2.invincible = true;
            }
        else if (brick.sprite_index == sGTemple)
            {
            instance_create(argument1, argument2, oGTemple);
            brick2 = instance_create(argument1, argument2+16, oGTemple);
            brick2.invincible = true;
            }
        else if (brick.sprite_index == sDesert)
            {
            instance_create(argument1, argument2, oSand);
            brick2 = instance_create(argument1, argument2+16, oSand);
            brick2.invincible = true;
            }
        else if (brick.sprite_index == sDesertNight)
            {
            instance_create(argument1, argument2, oSandDark);
            brick2 = instance_create(argument1, argument2+16, oSandDark);
            brick2.invincible = true;
            }
        else
            {
            instance_create(argument1, argument2, oBrick);
            brick2 = instance_create(argument1, argument2+16, oBrick);
            brick2.invincible = true;
            }
        }
    else
        {
        instance_create(argument1, argument2, oBrick);
        brick2 = instance_create(argument1, argument2+16, oBrick);
        brick2.invincible = true;
        }
}
else if (argument0 == "ж")
{
    instance_create(argument1, argument2, oRandomDoor);
}
else if (argument0 == "У")
{
    instance_create(argument1, argument2, oGoldExit);
}
else if (argument0 == "I")
{
    instance_create(argument1, argument2, oMsgSign);
}
else if (argument0 == "9")
{
    instance_create(argument1, argument2, oCheckpoint);
}
else if (argument0 == "1")
{
    obj = instance_create(argument1, argument2, oBrick);
    obj.sprite_index = sBrick;
}
else if (argument0 == "Х" or argument0 == "И" or argument0 == "К")
{
    smooth = instance_create(argument1, argument2, oBrickSmooth);
    if (argument0 == "И") smooth.sprite_index = sLushSmooth;
    if (argument0 == "К") smooth.sprite_index = sDarkSmooth;
}
else if (argument0 == "Ф")
{
    instance_create(argument1, argument2, oSign);
}
else if (argument0 == "2")
{
    obj = instance_create(argument1, argument2, oLush);
    obj.sprite_index = sLush;
}
else if (argument0 == "w" or argument0 == "э")
{
    instance_create(argument1, argument2, oWaterSwim);
    if (argument0 == "э") instance_create(argument1, argument2+16, oWaterSwim);
}
else if (argument0 == "3")
{
    obj = instance_create(argument1, argument2, oDark);
    obj.sprite_index = sDark;
}
else if (argument0 == "i")
{
    instance_create(argument1, argument2, oIce);
}
else if (argument0 == "ч")
{
    instance_create(argument1, argument2, oIceBlock);
}
else if (argument0 == "d")
{
    instance_create(argument1, argument2, oDarkFall);
}
else if (argument0 == "4")
{
    obj = instance_create(argument1, argument2, oTemple);
    obj.sprite_index = sTemple;
}
else if (argument0 == "l")
{
    instance_create(argument1, argument2, oLava);
}
else if (argument0 == "Q")
{
    instance_create(argument1, argument2, oAcid);
}
else if (argument0 == "L")
{
    instance_create(argument1, argument2, oLadderOrange);
}
else if (argument0 == "P")
{
    instance_create(argument1, argument2, oLadderTop);
}

else if (argument0 == "≤") // should work but doesn't look right in 8.1 ide
{
    instance_create(argument1, argument2, oSand);
}
else if (argument0 == "я")
{
    instance_create(argument1, argument2, oSandDark);
}
else if (argument0 == "Щ")
{
    instance_create(argument1, argument2, oShrub);
}
else if (argument0 == "Ъ")
{
    instance_create(argument1, argument2, oShrubDark);
}
else if (argument0 == "÷")
{
    obj = instance_create(argument1, argument2, oPalmDarkTop);
    obj = instance_create(argument1+16, argument2+32, oPalmDarkTrunk);
}
else if (argument0 == "щ")
{
    obj = instance_create(argument1, argument2, oPalmTop);
    obj = instance_create(argument1+16, argument2+32, oPalmTrunk);
}
else if (argument0 == "v" or argument0 == "t" or argument0 == "О")
{
     if (argument0 == "t") vine = instance_create(argument1, argument2, oVineTop);
     else vine = instance_create(argument1, argument2, oVine);

     if (argument0 == "О") instance_create(argument1, argument2, oMonkey);
}
else if (argument0 == "|" or argument0 == "x" or argument0 == "≥")
{
    tree = instance_create(argument1, argument2, oTree);
    if (argument0 == "x") tree.sprite_index = sTreeTop;
    if (argument0 == "≥")
    {
        tree.dead = true;
        tree.sprite_index = sTreeTopDead;
    }
}
else if (argument0 == ")" or argument0 == "▀")
{
    tree = instance_create(argument1, argument2, oLeaves);
    if (argument0 == "▀") tree.dead = true;
}
else if (argument0 == "q" or argument0 == "⌡")
{
    tree = instance_create(argument1, argument2, oTreeBranch);
    if (argument0 == "⌡") tree.dead = true;
}
else if (argument0 == "∙" or argument0 == "╥")
{
    grave = instance_create(argument1, argument2, oGrave);
    if (argument0 == "╥")
    {
        grave.sprite_index = sGraveAsh;
        item = 0;
        item = collision_rectangle(argument1-16, argument2, argument1, argument2+16, oItem, 0, 0);
        if (item)
        {
            item.x = argument1+8;
            if (item.type == "Dice" or item.type == "Crate" or item.type == "Chest") item.y = argument2+21;
            else if (item.type == "Bomb Box" or item.type == "Locked Chest") item.y = argument2+22;
            else if (item.type == "Gloves" or item.type == "Cape"  or item.type == "Jetpack" or item.type == "Mitt" or item.type == "Ankh" or item.type == "Crown" or item.type == "Kapala" or item.type == "Flare Crate") item.y = argument2+23;
            else if (item.type == "Lamp" or item.type == "Red Lamp" or item.type == "Gold Idol" or item.type == "Crystal Skull" or item.type == "Teleporter") item.y = argument2+27;
            else if (item.type == "Skull") item.y = argument2+29;
            else item.y = argument2+24;
        }
        else item = instance_create(argument1+8, argument2+24, oShotgun);
    }
}
else if (argument0 == "B")
{
    instance_create(argument1, argument2, oPushBlock);
}
else if (argument0 == "р")
{
    instance_create(argument1, argument2, oLamp);
}
else if (argument0 == "с")
{
    instance_create(argument1, argument2, oLampRed);
}
else if (argument0 == "н")
{
    instance_create(argument1, argument2, oTikiTorch);
}
else if (argument0 == "ц")
{
    instance_create(argument1, argument2, oAlienShipFloor);
}
else if (argument0 == "ю" or argument0 == "а" or argument0 == "Ю" or argument0 == "А")
{
    ship = instance_create(argument1, argument2, oAlienShip);
    if (argument0 != "ю") ship.sprite_index = sAlienWall;
}
else if (argument0 == "б")
{
    instance_create(argument1, argument2, oGTemple);
}
else if (argument0 == "─")
{
    instance_create(argument1, argument2, oGoldBlock);
}
else if (argument0 == "Ж")
{
    for (l = 0; l < 6; l += 1)
    {
        for (k = 0; k < 5; k += 1)
        {
            obj = instance_create(argument1+k*16, argument2+l*16, oXocBlock);
            if (k == 2 and l == 1) obj.treasure = "Diamond";
            if (k == 1 and l == 2) obj.treasure = "Sapphire";
            if (k == 3 and l == 2) obj.treasure = "Sapphire";
            if (k == 0 and l == 3) obj.treasure = "Emerald";
            if (k == 4 and l == 3) obj.treasure = "Emerald";
            if (k == 2 and l == 4) obj.treasure = "Ruby";
            tile_add(bgLadyXoc, k*16, l*16, 16, 16, argument1+k*16, argument2+l*16, 99);
        }
    }
}
else if (argument0 == "Z")
{
    instance_create(argument1, argument2, oMetalBlock);
}
else if (argument0 == "п")
{
    instance_create(argument1, argument2, oMetalDoor);
}
else if (argument0 == "ф")
{
    instance_create(argument1, argument2, oMoai);
    instance_create(argument1+16, argument2, oMoai2);
    instance_create(argument1+32, argument2, oMoai3);
    instance_create(argument1+16, argument2+16, oMoaiInside);
    tile_add(bgAlienShip3, 0, 0, 16, 16, argument1+16, argument2+16, 9001);
    tile_add(bgAlienShip3, 0, 0, 16, 16, argument1+16, argument2+32, 9001);
    tile_add(bgAlienShip3, 0, 0, 16, 16, argument1+16, argument2+48, 9001);
    global.MoaiX = argument1+24;
    global.MoaiY = argument2+24;
}
else if (argument0 == "╠")
{
    altarL = instance_create(argument1, argument2, oSacAltarLeft);
    altarR = instance_create(argument1+16, argument2, oSacAltarRight);
    tile_add(bgKaliBody, 0, 0, 64, 64, argument1-16, argument2-48, 9003);
    instance_create(argument1+16, argument2-64, oKaliHead);

    item1 = 0;
    item1 = collision_rectangle(argument1, argument2-16, argument1+16, argument2, oItem, 0, 0);
    if (item1)
    {
        if (item1.type == "Gold Idol" and item1.sprite_index == sCrystalSkull) item1.type = "Crystal Skull";
        altarL.gift1 = item1.type;
        altarR.gift1 = item1.type;
        item1.breakPieces = false;
        with (item1) instance_destroy();
    }

    item2 = 0;
    item2 = collision_rectangle(argument1+16, argument2-16, argument1+32, argument2, oItem, 0, 0);
    if (item2)
    {
        if (item2.type == "Gold Idol" and item2.sprite_index == sCrystalSkull) item2.type = "Crystal Skull";
        altarL.gift2 = item2.type;
        altarR.gift2 = item2.type;
        item2.breakPieces = false;
        with (item2) instance_destroy();
    }
}
else if (argument0 == "&")
{
    instance_create(argument1, argument2, oWeb);
}
else if (argument0 == "r")
{
    instance_create(argument1+8, argument2+12, oRock);
}
else if (argument0 == "j")
{
    instance_create(argument1+8, argument2+10, oJar);
}
else if (argument0 == "╧")
{
    instance_create(argument1+12, argument2+12, oArrow);
}
else if (argument0 == "k")
{
    instance_create(argument1, argument2, oBones);
    instance_create(argument1+12, argument2+12, oSkull);
}
else if (argument0 == "┘")
{
    instance_create(argument1, argument2, oFakeBones);
}
else if (argument0 == "b")
{
    instance_create(argument1, argument2, oBat);
}
else if (argument0 == "n")
{
    instance_create(argument1, argument2, oSnake);
}
else if (argument0 == "s")
{
    instance_create(argument1, argument2, oSpiderHang);
}
else if (argument0 == "S" or argument0 == "▌")
{
    baddy = instance_create(argument1, argument2, oGiantSpiderHang);
    if (argument0 == "▌")
    {
        baddy.pasteless = true;
        item = 0;
        item = collision_rectangle(argument1-16, argument2, argument1, argument2+16, oItem, 0, 0);
        if (item)
        {
            baddy.carries = item.type;
            if (item.type == "Locked Chest")
            {
                baddy.holds = item.holds;
            }
            if (item.type == "Gold Idol" and item.sprite_index == sCrystalSkull) baddy.carries = "Crystal Skull";
            item.breakPieces = false;
            with (item) instance_destroy();
        }
    }
}
else if (argument0 == "K")
{
    instance_create(argument1, argument2, oSkeleton);
}
else if (argument0 == "h")
{
    instance_create(argument1, argument2, oCaveman);
}
else if (argument0 == "о")
{
    instance_create(argument1, argument2, oIce);
    instance_create(argument1, argument2, oFrozenCaveman);
}
else if (argument0 == "!" or argument0 == "╫")
{
    obj = instance_create(argument1, argument2, oShopkeeper);
    obj.status = 4;
    if (argument0 == "╫") obj.hasGun = false;
}
else if (argument0 == "f")
{
    instance_create(argument1, argument2, oFrog);
}
else if (argument0 == "F")
{
    instance_create(argument1, argument2, oFireFrog);
}
else if (argument0 == "z")
{
    instance_create(argument1, argument2, oZombie);
}
else if (argument0 == "A" or argument0 == "Д")
{
    baddy = instance_create(argument1, argument2, oVampire);
    if (argument0 == "Д")
    {
        baddy.capeless = true;

        item = 0;
        item = collision_rectangle(argument1-16, argument2, argument1, argument2+16, oItem, 0, 0);
        if (item)
        {
            baddy.carries = item.type;
            if (item.type == "Locked Chest")
            {
                baddy.holds = item.holds;
            }
            if (item.type == "Gold Idol" and item.sprite_index == sCrystalSkull) baddy.carries = "Crystal Skull";
            item.breakPieces = false;
            with (item) instance_destroy();
        }
    }
}
else if (argument0 == "p")
{
    instance_create(argument1, argument2, oWaterSwim);
    instance_create(argument1+4, argument2+4, oPiranha);
}
else if (argument0 == "┴")
{
    instance_create(argument1, argument2, oWaterSwim);
    instance_create(argument1+4, argument2+4, oDeadFish);

}
else if (argument0 == "{" or argument0 == "Й")
{
    instance_create(argument1, argument2, oWaterSwim);
    instance_create(argument1+16, argument2, oWaterSwim);
    instance_create(argument1+32, argument2, oWaterSwim);
    instance_create(argument1+48, argument2, oWaterSwim);
    instance_create(argument1, argument2+16, oWaterSwim);
    instance_create(argument1+16, argument2+16, oWaterSwim);
    instance_create(argument1+32, argument2+16, oWaterSwim);
    instance_create(argument1+48, argument2+16, oWaterSwim);
    baddy = instance_create(argument1, argument2, oJaws);
    if (argument0 == "Й")
    {
        baddy.empty = true;
        item = 0;
        item = collision_rectangle(argument1-16, argument2, argument1, argument2+16, oItem, 0, 0);
        if (item)
        {
            if (!collision_point(argument1-16, argument2, oWaterSwim, 0, 0)) instance_create(argument1-16, argument2, oWaterSwim);
            baddy.carries = item.type;
            if (item.type == "Locked Chest")
            {
                baddy.holds = item.holds;
            }
            if (item.type == "Gold Idol" and item.sprite_index == sCrystalSkull) baddy.carries = "Crystal Skull";
            item.breakPieces = false;
            with (item) instance_destroy();
        }
    }
}
else if (argument0 == "M")
{
    instance_create(argument1-5, argument2, oManTrap);
}
/*
else if (argument0 == "ы")
{
    ManTrap = instance_create(argument1-5, argument2, oManTrap);
}
else if (argument0 == "З")
{
    instance_create(argument1-5, argument2, oTunnelManTrap);
}
else if (argument0 == "з")
{
    ManTrap = instance_create(argument1-5, argument2, oTunnelManTrap);
}
else if (argument0 == "Ш")
{
    instance_create(argument1-5, argument2, oVenusManTrap);
}
else if (argument0 == "ш")
{
    ManTrap = instance_create(argument1-5, argument2, oVenusManTrap);
}
*/
else if (argument0 == "m")
{
    instance_create(argument1, argument2, oMonkey);
}
else if (argument0 == "y")
{
    instance_create(argument1, argument2, oYeti);
}
else if (argument0 == "Y" or argument0 == "Ч")
{
    baddy = instance_create(argument1, argument2, oYetiKing);
    if (argument0 == "Ч")
    {
        baddy.ropeless = true;
        item = 0;
        item = collision_rectangle(argument1-16, argument2, argument1, argument2+16, oItem, 0, 0);
        if (item)
        {
            baddy.carries = item.type;
            if (item.type == "Locked Chest")
            {
                baddy.holds = item.holds;
            }
            if (item.type == "Gold Idol" and item.sprite_index == sCrystalSkull) baddy.carries = "Crystal Skull";
            item.breakPieces = false;
            with (item) instance_destroy();
        }
    }
}
else if (argument0 == "a")
{
    instance_create(argument1, argument2, oAlien);
}
else if (argument0 == "U")
{
    instance_create(argument1, argument2, oUFO);
}
else if (argument0 == "E" or argument0 == "N")
{
    baddy = instance_create(argument1, argument2, oAlienBoss);
    if (argument0 == "N")
    {
        item = 0;
        item = collision_rectangle(argument1-16, argument2, argument1, argument2+16, oItem, 0, 0);
        if (item)
        {
            baddy.carries = item.type;
            if (item.type == "Locked Chest")
            {
                baddy.holds = item.holds;
            }
            if (item.type == "Gold Idol" and item.sprite_index == sCrystalSkull) baddy.carries = "Crystal Skull";
            item.breakPieces = false;
            with (item) instance_destroy();
        }
    }
}
else if (argument0 == "H")
{
    instance_create(argument1, argument2, oHawkman);
}
else if (argument0 == "T" or argument0 == "┼")
{
    baddy = instance_create(argument1, argument2, oTombLord);
    if (argument0 == "┼")
    {
        item = 0;
        item = collision_rectangle(argument1-16, argument2, argument1, argument2+16, oItem, 0, 0);
        if (item)
        {
            baddy.carries = item.type;
            if (item.type == "Locked Chest")
            {
                baddy.holds = item.holds;
            }
            if (item.type == "Gold Idol" and item.sprite_index == sCrystalSkull) baddy.carries = "Crystal Skull";
            item.breakPieces = false;
            with (item) instance_destroy();
        }
        else baddy.hasSceptre = true;
    }
}
else if (argument0 == "П")
{
    baddy = instance_create(argument1, argument2, oMagmaMan);
    baddy.hp = 20;
    baddy.persist = true;
}
else if (argument0 == "в")
{
    instance_create(argument1, argument2, oOlmec);
}
/*
else if (argument0 == "║")
{
    instance_create(argument1, argument2, oSpelunker);
}
else if (argument0 == "▄")
{
    instance_create(argument1, argument2, oTunnelFoe);
}
*/
else if (argument0 == "у")
{
    instance_create(argument1, argument2, oScarab);
}
else if (argument0 == "╓")
{
    instance_create(argument1, argument2, oBarrierEmitter);
}
else if (argument0 == "╬")
{
    instance_create(argument1, argument2, oDeItemizer);
}
else if (argument0 == "^")
{
    instance_create(argument1, argument2, oSpikes);
}
else if (argument0 == "<")
{
    instance_create(argument1, argument2, oArrowTrapLeft);
}
else if (argument0 == ">")
{
    instance_create(argument1, argument2, oArrowTrapRight);
}
else if (argument0 == "╚")
{
    instance_create(argument1, argument2, oArrowTrapLeftLit);
}
else if (argument0 == "╩")
{
    instance_create(argument1, argument2, oArrowTrapRightLit);
}
else if (argument0 == "]" or argument0 == "╕" or argument0 == "[")
{
    if (argument0 == "]") instance_create(argument1, argument2, oSpearTrapTop);
    if (argument0 == "╕") instance_create(argument1, argument2, oSpearTrapLit);
    if (argument0 == "[") instance_create(argument1, argument2, oSpearTrapBottom);
    else instance_create(argument1, argument2+16, oSpearTrapBottom);
}
else if (argument0 == "_")
{
    instance_create(argument1, argument2, oSpringTrap);
}
else if (argument0 == "+")
{
    instance_create(argument1, argument2, oSmashTrap);
}
else if (argument0 == "╘")
{
    instance_create(argument1, argument2, oSmashTrapLit);
}
else if (argument0 == "≈")
{
    instance_create(argument1, argument2, oAltarLeft);
    instance_create(argument1+16, argument2, oAltarRight);
    item = 0;
    item = collision_rectangle(argument1, argument2-16, argument1+16, argument2, oItem, 0, 0);
    if (item)
    {
        item.x += 8;
        item.trigger = true;
    }
    else item = instance_create(argument1+16, argument2-4, oGoldIdol);
}
else if (argument0 == "╣")
{
    instance_create(argument1, argument2, oTrapBlock);
}
else if (argument0 == "e")
{
    instance_create(argument1, argument2, oBossBlock);
}
else if (argument0 == "ъ")
{
    instance_create(argument1, argument2-16, oDoor);

    brick = 0;
    brick = collision_point(argument1, argument2-16, oSolid, 0, 0);

    if (brick)
    {
        if (brick.sprite_index == sBrick) instance_create(argument1, argument2, oBrick);
        else if (brick.sprite_index == sLush) instance_create(argument1, argument2, oLush);
        else if (brick.sprite_index == sDark) instance_create(argument1, argument2, oDark);
        else if (brick.sprite_index == sGTemple) instance_create(argument1, argument2, oGTemple);
        else if (brick.sprite_index == sDesert) instance_create(argument1, argument2, oSand);
        else if (brick.sprite_index == sDesertNight) instance_create(argument1, argument2, oSandDark);
        else if (brick.sprite_index == sMetalBlock) instance_create(argument1, argument2, oMetalBlock);
        else instance_create(argument1, argument2, oTemple);
    }
    else
    {
        instance_create(argument1, argument2, oTemple);
        instance_create(argument1, argument2-16, oTemple);
    }
}
else if (argument0 == "┤")
{
    instance_create(argument1+16, argument2+16, oGiantTikiHead);
    tile_add(bgTiki, 0, 0, 32, 64, argument1, argument2+32, 9003);
    tile_add(bgTikiArms, 16*rand(0,2), 0, 16, 16, argument1+32, argument2+32, 9003);
    tile_add(bgTikiArms, 16*rand(0,2), 16, 16, 16, argument1-16, argument2+32, 9003);
}
else if (argument0 == " ")
{
    instance_create(argument1, argument2, oCeilingTrap);
}
else if (argument0 == "°")
{
    instance_create(argument1, argument2, oButtonTrap);
}
else if (argument0 == "╞")
{
    instance_create(argument1, argument2, oThwompTrap);
}
else if (argument0 == "В")
{
    instance_create(argument1, argument2, oThwompTrapLit);
}
else if (argument0 == "╦")
{
    instance_create(argument1+8, argument2+14, oGoldChunk);
}
else if (argument0 == "▒")
{
    instance_create(argument1+8, argument2+12, oGoldNugget);
}
else if (argument0 == ";")
{
    instance_create(argument1, argument2, oGold);
}
else if (argument0 == "└")
{
    instance_create(argument1, argument2, oGoldBig);
}
else if (argument0 == "$")
{
    instance_create(argument1+8, argument2+12, oGoldBar);
}
else if (argument0 == "*")
{
    instance_create(argument1+8, argument2+8, oGoldBars);
}
else if (argument0 == "#")
{
    item = instance_create(argument1+8, argument2+12, oGoldIdol);
}
else if (argument0 == "O" or argument0 == "⌠")
{
    idol = instance_create(argument1+8, argument2+12, oCrystalSkull);
    if (argument0 == "⌠") idol.cursed = true;
}
else if (argument0 == "▓")
{
    instance_create(argument1+8, argument2+14, oEmerald);
}
else if (argument0 == "5")
{
    instance_create(argument1+8, argument2+12, oEmeraldBig);
}
else if (argument0 == "╢")
{
    instance_create(argument1+8, argument2+14, oSapphire);
}
else if (argument0 == "6")
{
    instance_create(argument1+8, argument2+12, oSapphireBig);
}
else if (argument0 == "'")
{
    instance_create(argument1+8, argument2+14, oRuby);
}
else if (argument0 == "7")
{
    instance_create(argument1+8, argument2+12, oRubyBig);
}
else if (argument0 == "8")
{
    instance_create(argument1+8, argument2+12, oDiamond);
}
else if (argument0 == "c")
{
    instance_create(argument1+8, argument2+8, oChest);
}
else if (argument0 == "C")
{
    chest = instance_create(argument1+8, argument2+8, oCrate);
    treasure = 0;
    treasure = collision_rectangle(argument1-16, argument2, argument1, argument2+16, oItem, 0, 0);
    if (treasure)
    {
        chest.holds = treasure.type;
        if (treasure.type == "Gold Idol" and treasure.sprite_index == sCrystalSkull) chest.holds = "Crystal Skull";
        treasure.breakPieces = false;
        with (treasure) instance_destroy();
    }


}
else if (argument0 == "D")
{
    damsel = instance_create(argument1+8, argument2+8, oDamsel);
    damsel.forSale = false;
    damsel.cost = 0;
    damsel.trigger = false;
}
else if (argument0 == "╙")
{
    obj = instance_create(argument1+8, argument2+12, oBomb);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == ".")
{
    obj = instance_create(argument1+8, argument2+10, oBombBag);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == ":")
{
    obj = instance_create(argument1+8, argument2+8, oBombBox);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "u")
{
    obj = instance_create(argument1+8, argument2+10, oPaste);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "Ь")
{
    obj = instance_create(argument1+8, argument2+12, oRopeThrow);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "R")
{
    obj = instance_create(argument1+8, argument2+11, oRopePile);
    obj.cost = 0;
    obj.forSale = false;
}
/*
else if (argument0 == "Г")
{
    instance_create(argument1+8, argument2+12, oWaterGlass);
}
else if (argument0 == "г")
{
    instance_create(argument1+8, argument2+10, oBiteBag);
}
*/
else if (argument0 == "`")
{
    obj = instance_create(argument1+8, argument2+10, oParaPickup);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "o")
{
    obj = instance_create(argument1+8, argument2+10, oCompass);
    obj.cost = 0;
    obj.forSale = false;
}
/*
else if (argument0 == "┐")
{
    instance_create(argument1+8, argument2+10, oGrapplingHook);
}
*/
else if (argument0 == "/")
{
    obj = instance_create(argument1+8, argument2+13, oMachete);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "~")
{
    obj = instance_create(argument1+8, argument2+10, oSpringShoes);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "┬")
{
    obj = instance_create(argument1+8, argument2+10, oJordans);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "V")
{
    obj = instance_create(argument1+8, argument2+10, oSpikeShoes);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "}")
{
    obj = instance_create(argument1+8, argument2+12, oBow);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "-")
{
    obj = instance_create(argument1+8, argument2+12, oPistol);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "=")
{
    obj = instance_create(argument1+8, argument2+12, oShotgun);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "W")
{
    obj = instance_create(argument1+8, argument2+12, oWebCannon);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "%")
{
    obj = instance_create(argument1+8, argument2+10, oSpectacles);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "G")
{
    obj = instance_create(argument1+8, argument2+8, oGloves);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "g")
{
    obj = instance_create(argument1+8, argument2+8, oMitt);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "?")
{
    obj = instance_create(argument1+8, argument2+12, oTeleporter);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "(")
{
    obj = instance_create(argument1+8, argument2+10, oMattock);
    obj.cost = 0;
    obj.forSale = false;
}
/*
else if (argument0 == "Ё")
{
    instance_create(argument1+8, argument2+11, oShuriken);
}
else if (argument0 == "╗")
{
    instance_create(argument1+8, argument2+8, oNinjaSuit);
}
*/
else if (argument0 == "\")
{
    obj = instance_create(argument1+8, argument2+8, oCapePickup);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "J")
{
    obj = instance_create(argument1+8, argument2+8, oJetpack);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "│")
{
    instance_create(argument1+8, argument2+8, oDice);
}
else if (argument0 == "л")
{
    obj = instance_create(argument1+8, argument2+12, oLampItem);
    obj.trigger = false; // fix for YASM
}
else if (argument0 == "м")
{
    obj = instance_create(argument1+8, argument2+12, oLampRedItem);
    obj.trigger = false; // fix for YASM
}
else if (argument0 == "С")
{
    instance_create(argument1+8, argument2+12, oFlare);
}
else if (argument0 == "т")
{
    instance_create(argument1+8, argument2+8, oFlareCrate);
}
else if (argument0 == ",")
{
    instance_create(argument1, argument2, oThinIce);
}
/*
else if (argument0 == "ь")
{
    instance_create(argument1+8, argument2+8, oDeathMask);
}
*/
else if (argument0 == "ё")
{
    obj = instance_create(argument1+8, argument2+8, oKapala);
}
else if (argument0 == "╛")
{
    instance_create(argument1+8, argument2+11, oSceptre);
}
else if (argument0 == "е")
{
    instance_create(argument1+8, argument2+8, oAnkh);
}
else if (argument0 == "╝")
{
    instance_create(argument1+8, argument2+10, oUdjatEye);
}
else if (argument0 == "д")
{
    instance_create(argument1+8, argument2+8, oCrown);
}
else if (argument0 == "╔")
{
    instance_create(argument1+8, argument2+12, oKey);
}
else if (argument0 == "╒")
{
    chest = instance_create(argument1+8, argument2+8, oLockedChest);
    treasure = 0;
    treasure = collision_rectangle(argument1-16, argument2, argument1, argument2+16, oItem, 0, 0);
    if (treasure)
    {
        chest.holds = treasure.type;
        if (treasure.type == "Gold Idol" and treasure.sprite_index == sCrystalSkull) chest.holds = "Crystal Skull";
        treasure.breakPieces = false;
        with (treasure) instance_destroy();
    }
}
else if (argument0 == "╟")
{
    obj = instance_create(argument1+8, argument2+12, oBasketball);
    obj.cost = 0;
    obj.forSale = false;
}
else if (argument0 == "Л")
{
    brick = 0;
    brick = collision_point(argument1-16, argument2, oSolid, 0, 0);
    if (brick)
    {
        if (brick.sprite_index == sLush)
        {
            //instance_create(argument1, argument2-16, oLush);
            instance_create(argument1, argument2, oLush);
        }
        else if (brick.sprite_index == sDark)
        {
            //instance_create(argument1, argument2-16, oDark);
            instance_create(argument1, argument2, oDark);
        }
        else if (brick.sprite_index == sTemple)
        {
            //instance_create(argument1, argument2-16, oTemple);
            instance_create(argument1, argument2, oTemple);
        }
        else if (brick.sprite_index == sGTemple)
        {
            //instance_create(argument1, argument2-16, oGTemple);
            instance_create(argument1, argument2, oGTemple);
        }
        else if (brick.sprite_index == sDesert)
        {
            //instance_create(argument1, argument2-16, oSand);
            instance_create(argument1, argument2, oSand);
        }
        else if (brick.sprite_index == sDesertNight)
        {
            //instance_create(argument1, argument2-16, oSandDark);
            instance_create(argument1, argument2, oSandDark);
        }
        else
        {
            //instance_create(argument1, argument2-16, oBrick);
            instance_create(argument1, argument2, oBrick);
        }
    }
    else
    {
        //instance_create(argument1, argument2-16, oBrick);
        instance_create(argument1, argument2, oBrick);
    }

    treasure = 0;
    treasure = collision_rectangle(argument1, argument2-16, argument1+16, argument2, oItem, 0, 0);
    if (treasure)
    {
        treasure.x = argument1+8;
        if (treasure.type == "Dice" or treasure.type == "Crate" or treasure.type == "Chest") treasure.y = argument2+5;
        else if (treasure.type == "Bomb Box" or treasure.type == "Locked Chest") treasure.y = argument2+6;
        else if (treasure.type == "Gloves" or treasure.type == "Cape" or treasure.type == "Jetpack" or treasure.type == "Mitt" or treasure.type == "Ankh" or treasure.type == "Crown" or treasure.type == "Kapala" or treasure.type == "Flare Crate") treasure.y = argument2+7;
        else if (treasure.type == "Lamp" or treasure.type == "Red Lamp" or treasure.type == "Gold Idol" or treasure.type == "Crystal Skull" or treasure.type == "Teleporter") treasure.y = argument2+11;
        else if (treasure.type == "Skull") treasure.y = argument2+13;
        else treasure.y = argument2+8;
    }
}
else if (argument0 == "Н")
{
    secret = instance_create(argument1, argument2, oRandomItem);

    treasure = 0;
    treasure = collision_rectangle(argument1-16, argument2, argument1, argument2+16, oItem, 0, 0);
    if (treasure)
    {
        secret.item = treasure.type;
        if (treasure.type == "Locked Chest")
        {
            secret.holds = treasure.holds;
        }

        treasure.breakPieces = false;
        with (treasure) instance_destroy();
    }
}
else if (argument0 == "├")
{
    instance_create(argument1, argument2, oGhost);
}
else if (argument0 == "╤")
{
    instance_create(argument1, argument2, oDarkActive);
}
else if (argument0 == "х" or argument0 == "к" or argument0 == "й"
    or argument0 == "и")
{

    if (instance_exists(oBorder)) border = instance_find(oBorder, 0);
        else border = instance_create(argument1, argument2, oBorder);

    if (argument0 == "х") border.lush = true;
    else if (argument0 == "к") border.dark = true;
    else if (argument0 == "й") border.temple = true;
    else if (argument0 == "и") border.gold = true;
}
else if (argument0 =="Ы")
{
    obj = instance_create(argument1, argument2, oSurfaceBorder);
}
else if (argument0 =="Э")
{
    obj = instance_create(argument1, argument2, oSurfaceNightBorder);
}
else if (argument0 == "Я")
{
    instance_create(argument1, argument2, oPitMaker);
}
else if (argument0 == "©")
{
     global.scumBallAndChain = true;
}
else if (argument0 == "╡")
{
    instance_create(argument1, argument2, oGhostTimer);
}
else if (argument0 == "╖")
{
    global.isDamsel = false;
    global.isTunnelMan = false;
}
else if (argument0 == "■")
{
    global.isDamsel = true;
    global.isTunnelMan = false;
}
else if (argument0 == "·")
{
    global.isDamsel = false;
    global.isTunnelMan = true;
}
else if (argument0 == "ы" or argument0 == "з" or argument0 == "ш")
{
    obj = instance_create(argument1,argument2, oVendingMachine);
    if(argument0 == "з") obj.sprite_index = sVending2;
    else if(argument0 == "ш") obj.sprite_index = sVending3;

    item = 0;
    item = collision_rectangle(argument1,argument2,argument1-16,argument2+16,oItem,0,0);
    if (item)
    {

        with (item) scrSetVendingItem(type);
        item.forSale = true;
        item.forVending = true;
        item.canPickUp = false;
        //obj.item = 0;
        obj.item = item;
        item.owner = obj;
        if(argument0 == "з") item.cost = item.cost*1.5;
        else if(argument0 == "ш") item.cost = item.cost*2;
        item = 0;
    }
}
