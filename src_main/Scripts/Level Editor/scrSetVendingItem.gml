// set item price etc for vending machines
// called by scrCreateTileObj

shopName = string_upper(argument0);

switch (argument0)
{
    case "Rock": cost = 50; shopName = "ROCK"; break;
    case "Bomb": cost = 850; shopName = "BOMB"; break;
    case "Arrow": cost = 300; shopName = "ARROW"; break;
    case "Rope": cost = 850; shopName = "ROPE"; break;
    case "Chest": cost = 1500; shopName = "CHEST"; break;
    case "Bomb Bag": cost = 2500; shopName = "BAG OF 3 BOMBS"; break;
    case "Damsel":
    {
        cost = getKissValue() * 3;
        if (global.isDamsel) shopName = "SPELUNKER";
        else shopName = "DAMSEL";
        break;
    }
    case "Crate": cost = 10000; break;
    case "Flare": cost = 500; break;
    case "Jar": cost = 200; break;
    case "Skull":
    {
        cost = 100;
        if (rand(1,100) == 1) shopName = "YORIC";
        break;
    }
    case "Shotgun":
    {
        cost = 15000;
        if (rand(1,100) == 1) shopName = "BOOMSTICK";
        break;
    }
    case "Pistol": cost = 5000; break;
    case "Web Cannon": cost = 2000; break;
    case "Teleporter": cost = 10000; break;
    case "Teleporter II": cost = 15000; break;
    case "Cape": cost = 12000; shopName = "CAPE"; break;
    case "Machete": cost = 7000; break;
    case "Mattock": cost = 8000; break;
    case "Skull": cost = 20; break;
    case "Rope Pile": cost = 2500; shopName = "PILE OF ROPE"; break;
    case "Bomb Box": cost = 10000; shopName = "BOX OF 12 BOMBS"; break;
    case "Paste": cost = 3000; shopName = "BOMB PASTE"; break;
    case "Bastketball": cost = 750; shopName = "BASKETBALL"; break;
    case "Jetpack":
    {
        cost = 20000;
        if (rand(1,100) == 1) shopName = "JETBACK";
        break;
    }
    case "Compass": cost = 3000; shopName = "COMPASS"; break;
    case "Parachute": cost = 2000; shopName = "PARACHUTE"; break;
    case "Gloves": cost = 8000; shopName = "PAIR OF CLIMBING GLOVES"; break;
    case "Spectacles": cost = 8000; shopName = "PAIR OF SPECTACLES"; break;
    case "Spike Shoes": cost = 4000; shopName = "PAIR OF SPIKE SHOES"; break;
    case "Spring Shoes": cost = 5000; shopName = "PAIR OF SPRING SHOES"; break;
    case "Bow": cost = 1000; shopName = "BOW AND ARROWS"; break;
    case "Dice": cost = 2000; shopName = "DIE"; break;
    case "Locked Chest": cost = 10000; break;
    case "Key": cost = 3000; break;
    case "Udjat Eye": cost = 8000; break;
    case "Ankh":
    {
        cost = 50000;
        if (rand(1,100) == 1) shopName = "BUCKET OF IMMORTALITY";
        break;
    }
    case "Sceptre":
    {
        cost = 20000;
        if (rand(1,100) == 1) shopName = "DEATH RAY";
        break;
    }
    case "Crown":
    case "Hedjet":
    {
        cost = 9000;
        shopName = "HEDJET";
        break;
    }
    case "Gold Idol":
    {
        if (sprite_index == sCrystalSkull)
        {
            cost = 15000;
            shopName = "CRYSTAL SKULL";
        }
        else cost = 5000;
        break;
    }
    case "Mitt": cost = 4000; shopName = "PITCHER'S MITT"; break;
    case "Lamp": cost = 1000; break; // ed+ cost = 8000
    case "Red Lamp": cost = 1200; break; // ed+ plus cost = 15000
    case "Flare Crate": cost = 1500; break;
    case "Kapala": cost = 15000; break;
    case "Basketball": cost = 1000; shopName = "BASKETBALL"; break;
    case "Fish Bone": cost = 10; break;
    default: cost = 500; shopName = "THING";
}
