// scrMakeItem
// called by the object that creates this one (enemy/chest/crate/kali drops)

var obj;
obj = 0;
if (argument0 == "Udjat Eye") obj = instance_create(argument1, argument2, oUdjatEye);
else if (argument0 == "Rope") obj = instance_create(argument1, argument2, oRopeThrow);
else if (argument0 == "Bomb") obj = instance_create(argument1, argument2, oBomb);
else if (argument0 == "Basketball") obj = instance_create(argument1, argument2, oBasketball);
else if (argument0 == "Dice") obj = instance_create(argument1, argument2, oDice);
else if (argument0 == "Shotgun") obj = instance_create(argument1, argument2, oShotgun);
else if (argument0 == "Pistol") obj = instance_create(argument1, argument2, oPistol);
else if (argument0 == "Web Cannon") obj = instance_create(argument1, argument2, oWebCannon);
else if (argument0 == "Teleporter") obj = instance_create(argument1, argument2, oTeleporter);
else if (argument0 == "Teleporter II") obj = instance_create(argument1, argument2, oTeleporter2);
else if (argument0 == "Bow") obj = instance_create(argument1, argument2, oBow);
else if (argument0 == "Machete") obj = instance_create(argument1, argument2, oMachete);
else if (argument0 == "Sceptre") obj = instance_create(argument1, argument2, oSceptre);
else if (argument0 == "Mattock") obj = instance_create(argument1, argument2, oMattock);
else if (argument0 == "Rock") obj = instance_create(argument1, argument2, oRock);
else if (argument0 == "Jar") obj = instance_create(argument1, argument2, oJar);
else if (argument0 == "Skull") obj = instance_create(argument1, argument2, oSkull);
else if (argument0 == "Arrow") obj = instance_create(argument1, argument2, oArrow);
else if (argument0 == "Flare") obj = instance_create(argument1, argument2, oFlare);
else if (argument0 == "Gold Idol") obj = instance_create(argument1, argument2, oGoldIdol);
else if (argument0 == "Crystal Skull") obj = instance_create(argument1, argument2, oCrystalSkull);
else if (argument0 == "Lamp") obj = instance_create(argument1, argument2, oLampItem);
else if (argument0 == "Red Lamp") obj = instance_create(argument1, argument2, oLampRedItem);
else if (argument0 == "Chest") obj = instance_create(argument1, argument2, oChest);
else if (argument0 == "Locked Chest")
{
    obj = instance_create(argument1, argument2, oLockedChest);
    if (argument3) obj.holds = holds;
}
else if (argument0 == "Key") obj = instance_create(argument1, argument2, oKey);
else if (argument0 == "Crate")
{
    obj = instance_create(argument1, argument2, oCrate);
    if (argument3) obj.holds = holds;
}
else if (argument0 == "Flare Crate") obj = instance_create(argument1, argument2, oFlareCrate);
else if (argument0 == "Bomb Bag") obj = instance_create(argument1, argument2, oBombBag);
else if (argument0 == "Bomb Box") obj = instance_create(argument1, argument2, oBombBox);
else if (argument0 == "Paste") obj = instance_create(argument1, argument2, oPaste);
else if (argument0 == "Rope Pile") obj = instance_create(argument1, argument2, oRopePile);
else if (argument0 == "Parachute") obj = instance_create(argument1, argument2, oParaPickup);
else if (argument0 == "Compass") obj = instance_create(argument1, argument2, oCompass);
else if (argument0 == "Spring Shoes") obj = instance_create(argument1, argument2, oSpringShoes);
else if (argument0 == "Spike Shoes") obj = instance_create(argument1, argument2, oSpikeShoes);
else if (argument0 == "Jordans") obj = instance_create(argument1, argument2, oJordans);
else if (argument0 == "Spectacles") obj = instance_create(argument1, argument2, oSpectacles);
else if (argument0 == "Crown") obj = instance_create(argument1, argument2, oCrown);
else if (argument0 == "Kapala") obj = instance_create(argument1, argument2, oKapala);
else if (argument0 == "Ankh") obj = instance_create(argument1, argument2, oAnkh);
else if (argument0 == "Gloves") obj = instance_create(argument1, argument2, oGloves);
else if (argument0 == "Mitt") obj = instance_create(argument1, argument2, oMitt);
else if (argument0 == "Jetpack") obj = instance_create(argument1, argument2, oJetpack);
else if (argument0 == "Cape") obj = instance_create(argument1, argument2, oCapePickup);
else if (argument0 == "Damsel") obj = instance_create(argument1, argument2, oDamsel);

// treasure

else if (argument0 == "Gold Chunk") obj = instance_create(argument1, argument2, oGoldChunk);
else if (argument0 == "Gold Bar") obj = instance_create(argument1, argument2, oGoldBar);
else if (argument0 == "Gold Bars") obj = instance_create(argument1, argument2, oGoldBars);
else if (argument0 == "Emerald") obj = instance_create(argument1, argument2, oEmerald);
else if (argument0 == "Big Emerald") obj = instance_create(argument1, argument2, oEmeraldBig);
else if (argument0 == "Sapphire") obj = instance_create(argument1, argument2, oSapphire);
else if (argument0 == "Big Sapphire") obj = instance_create(argument1, argument2, oSapphireBig);
else if (argument0 == "Ruby") obj = instance_create(argument1, argument2, oRuby);
else if (argument0 == "Big Ruby") obj = instance_create(argument1, argument2, oRubyBig);
else if (argument0 == "Diamond") obj = instance_create(argument1, argument2, oDiamond);


if (instance_exists(obj))
{
    obj.cost = 0;
    obj.forSale = false;
}
