// scrLoadCheckpoint
// must be called by oPlayer1
// copied from TLS
if (oGame.moneyCount < global.money || oGame.drawStatus < 3) {
  oGame.drawStatus = 3;
  oGame.moneyCount = global.money;
} else {
  if (gamepad.startPressed) gamepad.startPressed = false;
  if (gamepad.itemPressed) gamepad.itemPressed = false;
  scrClearGlobals();
  global.customLevel = false;
  room_goto(rLoadLevel);
}
