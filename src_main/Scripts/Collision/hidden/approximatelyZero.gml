/*
Returns whether the argument is between -0.1 and 0.1
*/
return (argument0 > -0.1 && argument0 < 0.1);
