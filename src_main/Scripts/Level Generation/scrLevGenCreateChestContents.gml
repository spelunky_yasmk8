var ctx;
ctx = "";
if (rand(1, 12) == 1 and global.currLevel > 1) {
  ctx = "Bomb";
} else {
  repeat (rand(3, 4)) {
    switch (rand(1, 3)) {
      case 1: ctx += "e"; break; // emerald
      case 2: ctx += "s"; break; // sapphire
      case 3: ctx += "r"; break; // ruby
    }
  }
  if (rand(1, 4) == 1) {
    switch (rand(1, 3)) {
      case 1: ctx += "E"; break; // emerald
      case 2: ctx += "S"; break; // sapphire
      case 3: ctx += "R"; break; // ruby
    }
  }
  // also drop some shells (wow!)
  if (rand(1, 10) == 1) {
    // 7 shells
    ctx += "A"; // 7 shells
  } else if (rand(1, 6) == 1) {
    // some single shells
    repeat (rand(2, 4)) ctx += "a"; // 1 shell
  }
}
return ctx;
