if (global.optEnemyVariations && global.currLevel > 1) {
  var totdeath789;
  totdeath789 = global.levelDeaths[6]+global.levelDeaths[7]+global.levelDeaths[8];

  if (totdeath789 < 12) {
    if (rand(0, 100) >= 99-(global.currLevel-2)*8) return oCobra;
    return oSnake;
  }
  if (totdeath789 < 22) {
    if (rand(0, 100) >= 85-(global.currLevel-2)*8) return oCobra;
    return oSnake;
  }
  if (totdeath789 < 32) {
    if (rand(0, 100) >= 75-(global.currLevel-2)*8) return oCobra;
    return oSnake;
  }
  if (totdeath789 < 42) {
    if (rand(0, 100) >= 65-(global.currLevel-2)*8) return oCobra;
    return oSnake;
  }
  if (totdeath789 < 52) {
    if (rand(0, 100) >= 55-(global.currLevel-2)*8) return oCobra;
    return oSnake;
  }
  if (rand(0, 100) >= 42-(global.currLevel-2)*8) return oCobra;
}
return oSnake;
