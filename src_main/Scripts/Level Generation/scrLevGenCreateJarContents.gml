//WARNING: keep this in sync with oJar:destroy!
var ctx;
ctx = -666;

     if (rand(1, 3) == 1) ctx = oGoldChunk;
else if (/*global.optSGAmmo &&*/ rand(1, 4) == 1) ctx = oShellSingle;
else if (rand(1, 6) == 1) ctx = oGoldNugget;
else if (rand(1, 12) == 1) ctx = oEmeraldBig;
else if (rand(1, 12) == 1) ctx = oSapphireBig;
else if (rand(1, 12) == 1) ctx = oRubyBig;
else if (rand(1, 6) == 1) {
  ctx = oSpider;
} else if (rand(1, 12) == 1) {
  if (rand(1, 50) == 1) ctx = oCobra; else ctx = oSnake;
} else if (rand(1, 100) == 1) {
  ctx = oAlien;
} else if (rand(1, 300) == 1) {
  ctx = oBlob;
}

return ctx;
