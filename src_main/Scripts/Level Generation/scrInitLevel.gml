//
// scrInitLevel()
//
// Calls scrLevelGen(), scrRoomGen*(), and scrEntityGen() to build level.
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/
var i, j, k, obj;

scrSetSeed();

//global.currLevel = 16;
if (global.customLevel) {
  global.mapSprite = sMapDefault;
  global.mapTitle = "UNKNOWN LOCATION";
} else if (global.currLevel > 4 and global.currLevel < 9) {
  global.levelType = 1;
  global.mapSprite = sMapJungle;
  global.mapTitle = "The Jungle";
} else if (global.currLevel > 8 and global.currLevel < 13) {
  global.levelType = 2;
  global.mapSprite = sMapIce;
  global.mapTitle = "The Ice Caves";
} else if (global.currLevel > 12 and global.currLevel < 16) {
  global.levelType = 3;
  global.mapSprite = sMapTemple;
  global.mapTitle = "The Temple";
} else if (global.currLevel == 16) {
  global.levelType = 4;
  global.mapSprite = sMapTemple;
  global.mapTitle = "Olmec's Chamber";
  if (global.bizarrePlus) global.cityOfGold = true;
} else {
  global.levelType = 0;
  global.mapSprite = sMapMines;
  global.mapTitle = "The Mines";
}

if (global.currLevel <= 1 or
    global.currLevel == 5 or
    global.currLevel == 9 or
    global.currLevel == 13)
{
  global.hadDarkLevel = false;
}

scrSetGhostExtraTime(); // YASM 1.7

// DEBUG MODE //
/*
if (global.currLevel == 2) global.levelType = 4;
if (global.currLevel == 3) global.levelType = 2;
if (global.currLevel == 4) global.levelType = 3;
if (global.currLevel == 5) global.levelType = 4;
*/

global.startRoomX = 0;
global.startRoomY = 0;
global.endRoomX = 0;
global.endRoomY = 0;
oGame.levelGen = false;

// this is used to determine the path to the exit (generally no bombs required)
for (i = 0; i < 4; i += 1) {
  for (j = 0; j < 5; j += 1) {
    if (j < 4) global.roomPath[i, j] = 0;
    global.roomPathDisplay[i, j] = 0;
  }
}

// side walls
if (global.customLevel) {
  for (i = 0; i <= 42; i += 1) {
    for (j = 0; j <= 33; j += 1) {
      if (i*16 == 0 or
          i*16 == 656 or
          j*16 == 0 or
          j*16 >= 528)
      {
        obj = instance_position(i*16, j*16, oPit);
        if (!instance_exists(obj)) {
          if (global.levelType == 3) {
            if (global.cityOfGold) obj = instance_create(i*16, j*16, oGTemple);
            else { obj = instance_create(i*16, j*16, oTemple); obj.sprite_index = sTemple; }
          } else if (global.levelType == 2) {
            obj = instance_create(i*16, j*16, oDark);
            obj.sprite_index = sDark;
          } else if (global.levelType == 1) {
            obj = instance_create(i*16, j*16, oLush);
            obj.sprite_index = sLush;
          } else {
            obj = instance_create(i*16, j*16, oBrick);
            obj.sprite_index = sBrick;
          }
          obj.invincible = true;
          obj.ore = 0;
        }
      }
    }
  }
} else {
       if (global.levelType == 4) k = 54;
  else if (global.levelType == 2) k = 38;
  else if (global.lake) k = 41;
  else {
    k = 33;
    global.roomPathDisplay[0, 4] = -1;
    global.roomPathDisplay[1, 4] = -1;
    global.roomPathDisplay[2, 4] = -1;
    global.roomPathDisplay[3, 4] = -1;
  }
  writeln("level height: ", k, " tiles");
  for (i = 0; i <= 42; i += 1) {
    for (j = 0; j <= k; j += 1) {
      if (not isLevel()) {
        i = 999;
        j = 999;
      } else if (global.levelType == 2) {
        if (i*16 == 0 or
            i*16 == 656 or
            j*16 == 0)
        {
          obj = instance_create(i*16, j*16, oDark);
          obj.invincible = true;
          obj.ore = 0;
          obj.sprite_index = sDark;
        }
      } else if (global.levelType == 4) {
        if (i*16 == 0 or
            i*16 == 656 or
            j*16 == 0)
        {
          obj = instance_create(i*16, j*16, oTemple);
          obj.invincible = true;
          obj.ore = 0;
          if (not global.cityOfGold) obj.sprite_index = sTemple;
        }
      } else if (global.lake) {
        if (i*16 == 0 or
            i*16 == 656 or
            j*16 == 0 or
            j*16 >= 656)
        {
          obj = instance_create(i*16, j*16, oLush); obj.sprite_index = sLush;
          obj.invincible = true;
          obj.ore = 0;
        }
      } else if (i*16 == 0 or
                 i*16 == 656 or
                 j*16 == 0 or
                 j*16 >= 528)
      {
             if (global.levelType == 0) { obj = instance_create(i*16, j*16, oBrick); obj.sprite_index = sBrick; }
        else if (global.levelType == 1) { obj = instance_create(i*16, j*16, oLush); obj.sprite_index = sLush; }
        else { obj = instance_create(i*16, j*16, oTemple); if (not global.cityOfGold) obj.sprite_index = sTemple; }
        obj.invincible = true;
        obj.ore = 0;
      }
    }
  }

  // dark tiles @ bottomless pit
  if (global.levelType == 2) {
    for (i = 0; i <= 42; i += 1) {
      instance_create(i*16, 40*16, oDark);
      //instance_create(i*16, 35*16, oSpikes);
    }
  }
}


if (global.levelType == 3) background_index = bgTemple;

if (global.bizarre) {
  // bizarre mode - increased chance of shop
  if (global.currLevel > 1 and rand(1, global.currLevel+1) == 1) global.scumGenShop = true;
  if (isRoom("rOlmec2")) {
    // Make 2 layer floor
    for (i = 1; i < 41; i += 1) {
      obj = instance_create(i*16, 432, oTemple);
      if (global.bizarrePlus) { obj.sprite_index = sGTemple; obj.ore = 2; }
    }
    for (i = 1; i < 41; i += 1) {
      obj = instance_create(i*16, 448, oTemple);
      if (global.bizarrePlus) { obj.sprite_index = sGTemple; obj.ore = 2; }
    }
  }
}

global.temp1 = global.gameStart;
scrLevelGen();

global.cemetary = false;
if (global.levelType == 1 and rand(1, global.probCemetary) == 1) global.cemetary = true;
if (global.scumGenCemetary) global.cemetary = true;

if (global.bizarre) {
  global.roomStyle = 1;
} else {
  global.roomStyle = global.optRoomStyle;
  if (global.roomStyle < 0 || global.roomStyle > 1) {
    global.roomStyle = rand(0, 1);
  }
}

with (oRoom) {
  writeln("generating room at (", scrGetRoomX(x), ", ", scrGetRoomY(y), ")");
  if (global.levelType == 0) {
    // mines
    if (global.roomStyle == 0) scrRoomGen(); else scrRoomGenYASM();
  } else if (global.levelType == 1) {
    // lush
    if (global.blackMarket) scrRoomGenMarket();
    //if (global.roomStyle == 0) scrRoomGenMarket(); else scrRoomGenMarketBizarre();
    else if (global.roomStyle == 0) scrRoomGen2();
    else scrRoomGen2YASM();
  } else if (global.levelType == 2) {
    // ice
    if (global.yetiLair) scrRoomGenYeti(); else scrRoomGen3();
  } else if (global.levelType == 3) {
    // temple
    scrRoomGen4();
  } else {
    if (global.bizarre) scrRoomGen5Bizarre(); else scrRoomGen5(); // final boss
  }
}
writeln("room generation complete");

global.darkLevel = false;

// always dark?
if ((global.scumDarkness >= 2 or global.scumGenDark) and global.currLevel != 16 and !isRoom("rTutorial") and !isRoom("rTutorial2")) global.darkLevel = true;

//if (not global.hadDarkLevel and global.currLevel != 0 and global.levelType != 2 and global.currLevel != 16 and rand(1, 1) == 1)
if (global.scumDarkness != 0 and not global.hadDarkLevel and not global.noDarkLevel and
    global.currLevel != 0 and global.currLevel != 1 and global.levelType != 2 and
    global.currLevel != 16 and not global.customLevel and rand(1, global.probDarkLevel) == 1)
{
  global.darkLevel = true;
  global.hadDarkLevel = true;
}

if (global.blackMarket) global.darkLevel = false;

global.genUdjatEye = false;
if (not global.madeUdjatEye) {
       if (global.currLevel == 2 and rand(1, 3) == 1) global.genUdjatEye = true;
  else if (global.currLevel == 3 and rand(1, 2) == 1) global.genUdjatEye = true;
  else if (global.currLevel == 4) global.genUdjatEye = true;
}

global.genMarketEntrance = false;
if (not global.madeMarketEntrance) {
       if (global.currLevel == 5 and rand(1, 3) == 1) global.genMarketEntrance = true;
  else if (global.currLevel == 6 and rand(1, 2) == 1) global.genMarketEntrance = true;
  else if (global.currLevel == 7) global.genMarketEntrance = true;
}

////////////////////////////
// ENTITY / TREASURES
////////////////////////////
global.temp2 = global.gameStart;
//if (not isRoom("rTutorial") and not isRoom("rLoadLevel")) scrEntityGen();
if (not isRoom("rTutorial") and not isRoom("rTutorial2") and not isRoom("rLoadLevel")) {
  writeln("generating entities...");
  if (global.bizarre and !global.blackMarket and global.levelType != 4) scrEntityGenBizarre(); else scrEntityGen();
  writeln("entity generation complete");
}

if (instance_exists(oEntrance) and not global.customLevel) {
  oPlayer1.x = oEntrance.x+8;
  oPlayer1.y = oEntrance.y+8;
  writeln("player positioned at (", oPlayer1.x, ", ", oPlayer1.y, ")");
}

if (global.darkLevel or
    global.blackMarket or
    global.snakePit or
    global.cemetary or
    global.lake or
    global.yetiLair or
    global.alienCraft or
    global.sacrificePit or
    global.cityOfGold)
{
  if (not isRoom("rLoadLevel")) with (oPlayer1) alarm[0] = 10;
}

/*
if (global.levelType == 4) scrSetupWalls(864);
else if (global.lake) scrSetupWalls(656);
else scrSetupWalls(528);
*/
if (not isRoom("rIntro")) {
  writeln("fixing walls... (room height is ", room_height, ")");
  scrSetupWalls(room_height-16);
  writeln("walls fixed");
}

// add background details
if (global.graphicsHigh and !global.customLevel and !global.parallax) {
  repeat(20) {
    // bg = instance_create(16*rand(1, 42), 16*rand(1, 33), oCaveBG);
         if (global.levelType == 1 and rand(1, 3) < 3) tile_add(bgExtrasLush, 32*rand(0, 1), 0, 32, 32, 16*rand(1, 42), 16*rand(1, 33), 10002);
    else if (global.levelType == 2 and rand(1, 3) < 3) tile_add(bgExtrasIce, 32*rand(0, 1), 0, 32, 32, 16*rand(1, 42), 16*rand(1, 33), 10002);
    else if (global.levelType == 3 and rand(1, 3) < 3) tile_add(bgExtrasTemple, 32*rand(0, 1), 0, 32, 32, 16*rand(1, 42), 16*rand(1, 33), 10002);
    else tile_add(bgExtras, 32*rand(0, 1), 0, 32, 32, 16*rand(1, 42), 16*rand(1, 33), 10002);
  }
}

oGame.levelGen = true;

// generate angry shopkeeper at exit if murderer or thief
if ((global.murderer or global.thiefLevel > 0) and !global.customLevel) {
  with (oExit) {
    if (type == "Exit") {
      obj = instance_create(x, y, oShopkeeper);
      obj.style = "Bounty Hunter";
      obj.status = 4;
    }
  }
  // global.thiefLevel -= 1;
}

with (oTreasure) {
  if (collision_point(x, y, oSolid, 0, 0)) {
    obj = instance_place(x, y, oSolid);
    if (instance_exists(obj)) {
      if (obj.invincible) instance_destroy();
    }
  }
}

with (oWater) {
  if (sprite_index == sWaterTop or sprite_index == sLavaTop) scrCheckWaterTop();
}

global.temp3 = global.gameStart;
