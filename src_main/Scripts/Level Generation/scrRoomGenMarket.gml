//
// scrRoomGenMarket()
//
// Room generation for the Black Market, which is accessible from Area 2: Lush.
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/
var strTemp, roomPathAbove, n, i, j, strObs1, strObs2, strObs3, strObs4;
var tile, xpos, ypos, obj, block;
var tx, ty, b1, b2, m;

/*
Note:

ROOMS are 10x8 tile areas.

strTemp = "0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000";

OBSTACLES are 5x3 tile chunks that are randomized within rooms.

strObs = "00000
          00000
          00000";

The string representing a room or obstacle must be laid out unbroken:
*/
strTemp = "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000";

roomPath = global.roomPath[scrGetRoomX(x), scrGetRoomY(y)];
roomPathAbove = -1;
if (scrGetRoomY(y) != 0) roomPathAbove = global.roomPath[scrGetRoomX(x), scrGetRoomY(y-128)];

if (scrGetRoomX(x) == global.startRoomX and scrGetRoomY(y) == global.startRoomY) // start room
{
    if (roomPath == 2) n = rand(3,4);
    else n = rand(1,2);
    switch(n)
    {
        case 1: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 2: { strTemp = "1111111111"+
                            "2222222222"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        // hole
        case 3: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "2021111120";
                            break; }
        case 4: { strTemp = "1111111111"+
                            "2222222222"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "2021111120";
                            break; }
    }
}
else if (scrGetRoomX(x) == global.endRoomX and scrGetRoomY(y) == global.endRoomY) // end room
{
    if (roomPathAbove == 2) n = rand(1,2);
    else n = rand(3,4);
    switch(n)
    {
        case 1: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 2: { strTemp = "0000000000"+
                            "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 3: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 4: { strTemp = "1111111111"+
                            "2222222222"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
    }
}
else if (roomPath == 1)
{
    switch(rand(1,8))
    {
        // basic rooms
        case 1: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0050000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 2: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "5000050000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 3: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "5000050000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111"+
                            "1111111111";
                            break; }
        case 4: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0001111100"+
                            "0011111100"+
                            "1111111111";
                            break; }
        // spikes
        case 5: { strTemp = "1111111111"+
                            "V0000V0000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "1000000001"+
                            "1ssssssss1"+
                            "1111111111";
                            break; }
        // upper plats
        case 6: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "5000050000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        // water
        case 7: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "013wwww310"+
                            "013wwww310"+
                            "113wwww311"+
                            "1113333111"+
                            "1111111111";
                            break; }
        case 8: { strTemp = "0060000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "013wwww310"+
                            "113wwww311"+
                            "1113333111"+
                            "1111111111";
                            break; }
    }
}
else if (roomPath == 3)
{
    switch(rand(1,7))
    {
        // basic rooms
        case 1: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0050000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 2: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "5000050000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 3: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "5000050000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111"+
                            "1111111111";
                            break; }

        // upper plats
        case 4: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0002222220"+
                            "0011111110"+
                            "1111111111"+
                            "1111111111";
                            break; }
        case 5: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000221"+
                            "0000022111"+
                            "0000221111"+
                            "1111111111";
                            break; }
        // water
        case 6: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "013wwww310"+
                            "013wwww310"+
                            "113wwww311"+
                            "1113333111"+
                            "1111111111";
                            break; }
        case 7: { strTemp = "0000000000"+
                            "0060000000"+
                            "0000000000"+
                            "0000000000"+
                            "013wwww310"+
                            "113wwww311"+
                            "1113333111"+
                            "1111111111";
                            break; }
    }
}
else if (roomPath == 4 and x == 496) // shop
{
    strTemp = "0000000011"+
              ".....b0AlK"+
              "......bbbb"+
              ".........."+
              "1111111111"+
              "1111111111"+
              "1111111111"+
              "1111111111";
    shopType = "Ankh";
}
else if (roomPath == 4) // shop
{
    strTemp = ".........."+
              ".........."+
              "....22...."+
              "..2l00l2.."+
              "0.000000.0"+
              "0k000000k0"+
              "000iiiiK00"+
              "bbbbbbbbbb";

    shopType = "";

    n = rand(1,5);
    m = n;

    while (shopType == "")
    {
        if (n == 1) { if (not oGame.genSupplyShop) { shopType = "General"; oGame.genSupplyShop = true; } }
        else if (n == 2) { if (not oGame.genBombShop) { shopType = "Bomb"; oGame.genBombShop = true; } }
        else if (n == 3) { if (not oGame.genWeaponShop) { shopType = "Weapon"; oGame.genWeaponShop = true; } }
        else if (n == 4) { if (not oGame.genRareShop) { shopType = "Rare"; oGame.genRareShop = true; } }
        else if (n == 5) { if (not oGame.genClothingShop) { shopType = "Clothing"; oGame.genClothingShop = true; } }
        n += 1;
        if (n > 5) n = 1;
        if (n == m)
        {
            shopType = "General";
            break;
        }
    }
}
else if (roomPath == 5) // casino
{
    strTemp = "1111111111"+
              "1111111111"+
              "1111221111"+
              "112000lK11"+
              "01W0Q00b.."+
              "0k00000+0."+
              "00000zz+q."+
              "bbbbbbbbbb";
    shopType = "Craps";
}
else // drop
{
    if (roomPathAbove != 2) n = rand(1,6);
    else n = rand(1,5);
    switch(n)
    {
        case 1: { strTemp = "00G0000000"+
                            "00H1111000"+
                            "00G2222000"+
                            "00G0000000"+
                            "00G0000000"+
                            "00G0000022"+
                            "0000000211"+
                            "1111202111";
                            break; }
        case 2: { strTemp = "0000000G00"+
                            "0001111H00"+
                            "0002222G00"+
                            "0000000G00"+
                            "0000000G00"+
                            "2200000G00"+
                            "112T000000"+
                            "1111202111";
                            break; }
        case 3: { strTemp = "00000000G0"+
                            "60000011H0"+
                            "00000000G0"+
                            "00000000G0"+
                            "G0000000G0"+
                            "H1122000G0"+
                            "G0000000G0"+
                            "11100001H1";
                            break; }
        case 4: { strTemp = "0000000G00"+
                            "0001111H00"+
                            "0002222G00"+
                            "0000000G00"+
                            "0000000G00"+
                            "0000000000"+
                            "2000022222"+
                            "1000111111";
                            break; }
        case 5: { strTemp = "00G0000000"+
                            "00H1111000"+
                            "00G2222000"+
                            "00G0000000"+
                            "00G0000000"+
                            "0000000000"+
                            "2222200002"+
                            "1111110001";
                            break; }
        //
        case 6: { strTemp = "1111111111"+
                            "1111111111"+
                            "1200000021"+
                            "2000000002"+
                            "0000000000"+
                            "0220000220"+
                            "2112002112"+
                            "1111001111";
                            break; }
    }
}

if (global.scumTileDebug) scrRevealObstacle(strTemp); // In-Level obstacle debug (YASM 1.7.3)

// Add obstacles
for (i = 1; i < 81; i += 1)
{
    j = i;

    strObs1 = "00000";
    strObs2 = "00000";
    strObs3 = "00000";
    strObs4 = "00000";
    tile = string_char_at(strTemp, i);

    if (tile == "8")
    {
        n = rand(1,1);
        switch(n)
        {
            case 1: { strObs1 = "00900";
                      strObs2 = "01110";
                      strObs3 = "11111";
                      break; }
        }
    }
    else if (tile == "5") // ground
    {
        if (rand(1,8) == 1) n = rand(100,102);
        else n = rand(1,2);
        switch(n)
        {
            case 1: { strObs1 = "00000";
                      strObs2 = "00000";
                      strObs3 = "22222";
                      break; }
            case 2: { strObs1 = "00000";
                      strObs2 = "22222";
                      strObs3 = "11111";
                      break; }
            case 100: { strObs1 = "00000";
                        strObs2 = "00000";
                        strObs3 = "0T022";
                        break; }
            case 101: { strObs1 = "00000";
                        strObs2 = "00000";
                        strObs3 = "20T02";
                        break; }
            case 102: { strObs1 = "00000";
                        strObs2 = "00000";
                        strObs3 = "220T0";
                        break; }
        }
    }
    else if (tile == "6") // air
    {
        n = rand(1,4);
        switch(n)
        {
            case 1: { strObs1 = "11112";
                      strObs2 = "22220";
                      strObs3 = "00000";
                      break; }
            case 2: { strObs1 = "21111";
                      strObs2 = "02222";
                      strObs3 = "00000";
                      break; }
            case 3: { strObs1 = "22222";
                      strObs2 = "00000";
                      strObs3 = "00000";
                      break; }
            case 4: { strObs1 = "11111";
                      strObs2 = "21112";
                      strObs3 = "02120";
                      break; }
        }
    }
    else if (tile == "V") // vines
    {
        n = rand(1,3);
        switch(n)
        {
            case 1: { strObs1 = "L0L0L";
                      strObs2 = "L0L0L";
                      strObs3 = "L000L";
                      strObs4 = "L0000";
                      break; }
            case 2: { strObs1 = "L0L0L";
                      strObs2 = "L0L0L";
                      strObs3 = "L000L";
                      strObs4 = "0000L";
                      break; }
            case 3: { strObs1 = "0L0L0";
                      strObs2 = "0L0L0";
                      strObs3 = "0L0L0";
                      strObs4 = "000L0";
                      break; }
        }
    }

    if (tile == "5" or tile == "6" or tile == "8" or tile == "V")
    {
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs1, strTemp, j);
        j += 10;
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs2, strTemp, j);
        j += 10;
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs3, strTemp, j);
    }
    if (tile == "V")
    {
        j += 10;
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs4, strTemp, j);
    }
}

// Generate the tiles
for (j = 0; j < 8; j += 1)
{
    for (i = 1; i < 11; i += 1)
    {
        tile = string_char_at(strTemp, i+j*10);
        xpos = x + (i-1)*16;
        ypos = y + j*16;

        //debugging
        if (global.scumTileDebug)
        {
            if (tile != "0")
            {
                obj = instance_create(xpos, ypos, oTileDebug);
                obj.tileString = tile;
            }
        }

        if (tile == "1" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            instance_create(xpos, ypos, oLush);
        }
        else if (tile == "2" and rand(1,2) == 1 and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            instance_create(xpos, ypos, oLush);
        }
        if (tile == "t" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            instance_create(xpos, ypos, oTemple);
        }
        else if (tile == "3" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,2) == 1) instance_create(xpos, ypos, oWaterSwim);
            else instance_create(xpos, ypos, oLush);
        }
        else if (tile == "L") instance_create(xpos, ypos, oVine);
        else if (tile == "P") instance_create(xpos, ypos, oVineTop);
        else if (tile == "G") instance_create(xpos, ypos, oLadderOrange);
        else if (tile == "H") instance_create(xpos, ypos, oLadderTop);
        else if (tile == "7" and rand(1,3) == 1)
        {
            if (global.woodSpikes) instance_create(xpos, ypos, oSpikesWood);
            else instance_create(xpos, ypos, scrGenSpikeType());
        }
        else if (tile == "s")
        {
            if (global.woodSpikes) instance_create(xpos, ypos, oSpikesWood);
            else instance_create(xpos, ypos, scrGenSpikeType());
        }
        else if (tile == "4") instance_create(xpos, ypos, oPushBlock);
        else if (tile == "9")
        {
            block = instance_create(xpos, ypos+16, oLush);
            if (scrGetRoomX(x) == global.startRoomX and scrGetRoomY(y) == global.startRoomY)
                instance_create(xpos, ypos, oEntrance);
            else
            {
                instance_create(xpos, ypos, oExit);
                global.exitX = xpos+8;
                global.exitY = ypos+8;
                block.invincible = true;
                block.ore = 0;
            }
        }
        else if (tile == "c")
        {
            scrLevGenCreateChest(xpos, ypos);
        }
        else if (tile == "d")
        {
            instance_create(xpos, ypos, oWaterSwim);
            scrLevGenCreateChest(xpos, ypos);
        }
        else if (tile == "w")
        {
            instance_create(xpos, ypos, oWaterSwim);
        }
        else if (tile == "I")
        {
            instance_create(xpos+16, ypos+8, oGoldIdol);
        }
        else if (tile == "." and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            obj = instance_create(xpos, ypos, oLush);
            obj.shopWall = true;
        }
        else if (tile == "+")
        {
            obj = instance_create(xpos, ypos, oSolid);
            obj.sprite_index = sIceBlock;
            obj.shopWall = true;
        }
        else if (tile == "q")
        {
            //???: n = rand(1,6);
            obj = scrGenerateItem(xpos+8, ypos+8, 1);
            obj.inDiceHouse = true;
        }
        else if (tile == "Q")
        {
            if (shopType == "Craps")
            {
                tile_add(bgDiceSign, 0, 0, 48, 32, xpos, ypos, 9004);
            }
        }
        else if (tile == "W")
        {
            if (global.murderer or global.thiefLevel > 0)
            {
                if (global.isDamsel) tile_add(bgWanted, 32, 0, 32, 32, xpos, ypos, 9004);
                else if (global.isTunnelMan) tile_add(bgWanted, 64, 0, 32, 32, xpos, ypos, 9004);
                else tile_add(bgWanted, 0, 0, 32, 32, xpos, ypos, 9004);
            }
        }
        else if (tile == "b")
        {
            obj = instance_create(xpos, ypos, oBrickSmooth);
            obj.sprite_index = sLushSmooth;
            obj.shopWall = true;
        }
        else if (tile == "l")
        {
            obj = instance_create(xpos, ypos, oLamp);
        }
        else if (tile == "K")
        {
            //obj = instance_create(xpos, ypos, oShopkeeper);
            //obj.style = shopType;
            scrLevGenShopkeeper(xpos, ypos, shopType);
        }
        else if (tile == "k")
        {
            obj = instance_create(xpos, ypos, oSign);
            if (shopType == "General") obj.sprite_index = sSignGeneral;
            else if (shopType == "Bomb") obj.sprite_index = sSignBomb;
            else if (shopType == "Weapon") obj.sprite_index = sSignWeapon;
            else if (shopType == "Clothing") obj.sprite_index = sSignClothing;
            else if (shopType == "Rare") obj.sprite_index = sSignRare;
            else if (shopType == "Craps") obj.sprite_index = sSignCraps;
        }
        else if (tile == "i")
        {
            scrShopItemsGen(xpos, ypos);
        }
        else if (tile == "A")
        {
            obj = instance_create(xpos+8, ypos+8, oAnkh);
        }
        else if (tile == "z")
        {
            instance_create(xpos+8, ypos+8, oDice);
        }
        else if (tile == "B")
        {
            instance_create(xpos, ypos, oTrapBlock);
        }
        else if (tile == "p")
        {
            if (rand(1,2)) instance_create(xpos, ypos, oFakeBones);
            else scrLevGenCreateJar(xpos+8, ypos+10);
        }
        else if (tile == "T")
        {
            instance_create(xpos, ypos, oTree);
            n = 0;
            tx = xpos;
            ty = ypos-16;
            b1 = false;
            b2 = false;
            for (m = 0; m < 5; m += 1)
            {
                if (rand(0,m) > 2)
                {
                    break;
                }
                else
                {
                    if (not collision_point(tx, ty-16, oSolid, 0, 0) and
                        not collision_point(tx-16, ty-16, oSolid, 0, 0) and
                        not collision_point(tx+16, ty-16, oSolid, 0, 0))
                    {
                        instance_create(tx, ty, oTree);
                        if (m < 4)
                        {
                            if (rand(1,5) < 4 and not b1)
                            {
                                instance_create(tx+16, ty, oTreeBranch);
                                b1 = true;
                            }
                            else if (b1) b1 = false;
                            if (rand(1,5) < 4 and not b2)
                            {
                                instance_create(tx-16, ty, oTreeBranch);
                                b2 = true;
                            }
                            else if (b2) b2 = false;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                ty -= 16;
            }
            instance_create(tx-16, ty+16, oLeaves);
            instance_create(tx+16, ty+16, oLeaves);
        }
    }
}
