//
// scrRoomGen4()
//
// Room generation for Area 4, the Temple.
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/
var strTemp, n, i, j, l, k, strObs1, strObs2, strObs3;
var tile, xpos, ypos, obj, block;
var tx, ty, b1, b2, m;

/*
Note:

ROOMS are 10x8 tile areas.

strTemp = "0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000";

OBSTACLES are 5x3 tile chunks that are randomized within rooms.

strObs = "00000
          00000
          00000";

The string representing a room or obstacle must be laid out unbroken:
*/
strTemp = "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000";

roomPath = global.roomPath[scrGetRoomX(x), scrGetRoomY(y)];
if (scrGetRoomX(x) == global.startRoomX and scrGetRoomY(y) == global.startRoomY) // start room
{
    if (roomPath == 2) n = rand(2,2);
    else n = rand(1,1);
    switch(n)
    {
        case 1: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        // hole
        case 2: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "2000000002";
                            break; }
    }
}
else if (scrGetRoomX(x) == global.endRoomX and scrGetRoomY(y) == global.endRoomY) // end room
{
    n = rand(1,1);
    switch(n)
    {
        case 1: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000";
                            break; }
    }
}
else if (roomPath == 0 and rand(1,4) > 1) // side room
{
    if (global.cityOfGold)
    {
        n = rand(1,14);
        if (n == 12) n = 15;
    }
    else if (not oGame.altar and rand(1,12) == 1)
    {
        n = 16;
        oGame.altar = true;
    }
    else if (oGame.idol)
    {
        n = rand(1,11);
    }
    else
    {
        n = rand(1,12);
        if (n == 12) oGame.idol = true;
    }

    switch(n)
    {
        // upper plats
        case 1: { strTemp = "1111100000"+
                            "1111100000"+
                            "1111100000"+
                            "1111100000"+
                            "1111150000"+
                            "1111100000"+
                            "1111100000"+
                            "1111111111";
                            break; }
        case 2: { strTemp = "0000011111"+
                            "0000011111"+
                            "0000011111"+
                            "0000011111"+
                            "5000011111"+
                            "0000011111"+
                            "0000011111"+
                            "1111111111";
                            break; }
        // triangles
        case 3: { strTemp = "1100000000"+
                            "1110000000"+
                            "2111000000"+
                            "1111100000"+
                            "2211110000"+
                            "1111111000"+
                            "2221111100"+
                            "1111111111";
                            break; }
        case 4: { strTemp = "0000000011"+
                            "0000000111"+
                            "0000001112"+
                            "0000011111"+
                            "0000111122"+
                            "0001111111"+
                            "0011111222"+
                            "1111111111";
                            break; }
        case 5: { strTemp = "1111111111"+
                            "0000000000"+
                            "1111111000"+
                            "1111110000"+
                            "1111100000"+
                            "1111000000"+
                            "1110000000"+
                            "1100000011";
                            break; }
        case 6: { strTemp = "1111111111"+
                            "0000000000"+
                            "0001111111"+
                            "0000111111"+
                            "0000011111"+
                            "0000001111"+
                            "0000000111"+
                            "1100000011";
                            break; }
        case 7: { strTemp = "1111111111"+
                            "2000000002"+
                            "1101221011"+
                            "1100000011"+
                            "1101221011"+
                            "2000000002"+
                            "2001221002"+
                            "1100000011";
                            break; }
        case 8: { strTemp = "1111111111"+
                            "0002112000"+
                            "1100110011"+
                            "1110220111"+
                            "1100110011"+
                            "0201111020"+
                            "0002112000"+
                            "1111111111";
                            break; }
        case 9: { strTemp = "1111111111"+
                            "0000000000"+
                            "1101111011"+
                            "1101111011"+
                            "1001111001"+
                            "11wwwwww11"+
                            "11wwwwww11"+
                            "1111111111";
                            break; }
        // sun room
        case 10: {
            if (rand(1,2) == 1) strTemp = "1000000001"+
                                          "0000000000"+
                                          "1000000001"+
                                          "1000000001"+
                                          "1000000001"+
                                          "00T0000T00"+
                                          "0dddddddd0"+
                                          "1111111111";
            else strTemp = "1000000001"+
                           "0000000000"+
                           "1000000001"+
                           "1000000001"+
                           "1000000001"+
                           "00T0000T00"+
                           "0dddddddd0"+
                           "1111111111";
            break;
        }
        case 11: { strTemp = "1000000001"+
                             "0021111200"+
                             "1000000001"+
                             "1000000001"+
                             "1111001111"+
                             "1112002111"+
                             "1112002111"+
                             "1111001111";
                             break; }
        // idol
        case 12: { strTemp = "11CCCCCC11"+
                             "1100000011"+
                             "1100000011"+
                             "1D000000D1"+
                             "1000000001"+
                             "0000000000"+
                             "0000I00000"+
                             "1111111111";
                             break; }
        // treasure vaults
        case 13: { strTemp = "11ttttt011"+
                             "1111111011"+
                             "110ttttt11"+
                             "1101111111"+
                             "11ttttt011"+
                             "1111111011"+
                             "11cttttt11"+
                             "1111111111";
                             break; }
        case 14: { strTemp = "1111111111"+
                             "110ttttc11"+
                             "1101111111"+
                             "11ttttt011"+
                             "1111111011"+
                             "110ttttt11"+
                             "1101111111"+
                             "1100000011";
                             break; }
        case 15: { strTemp = "1111111111"+
                             "1111111111"+
                             "1111cc1111"+
                             "1101111011"+
                             "11c1111c11"+
                             "1111cc1111"+
                             "1111111111"+
                             "1111111111";
                             break; }
        // altars
        case 16: { strTemp = "2200000022"+
                             "0000000000"+
                             "0000000000"+
                             "0000000000"+
                             "0000000000"+
                             "0000x00000"+
                             "0221111220"+
                             "1111111111";
                             break; }
    }
}
else if (roomPath == 0 or roomPath == 1)
{
    if (global.cityOfGold) n = rand(1,12);
    else n = rand(1,10);
    switch(n)
    {
        // basic rooms
        case 1: { strTemp = "1000000001"+
                            "200r000002"+
                            "1000000001"+
                            "1000000001"+
                            "1100000011"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 2: { strTemp = "1000000000"+
                            "100r000000"+
                            "1000000000"+
                            "1000000000"+
                            "1100000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 3: { strTemp = "0000000001"+
                            "000r000001"+
                            "0000000001"+
                            "0000000001"+
                            "0000000011"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 4: { strTemp = "0000000001"+
                            "000r000001"+
                            "0000000001"+
                            "0000000001"+
                            "0000000011"+
                            "0000220000"+
                            "0001111000"+
                            "1111111111";
                            break; }
        case 5: { strTemp = "1100000011"+
                            "00L0000L00"+
                            "11Pr000P11"+
                            "11L0000L11"+
                            "11L0000L11"+
                            "02L0000L20"+
                            "0000000000"+
                            "1111111111";
                            break; }
        // hallways
        case 6: { strTemp = "1111111111"+
                            "1111111111"+
                            "1111111111"+
                            "1111111111"+
                            "1111111111"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 7: { strTemp = "1000000001"+
                            "000r000000"+
                            "1000000001"+
                            "1000000001"+
                            "1111111111"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 8: { strTemp = "120r000021"+
                            "0000000000"+
                            "1200000021"+
                            "1220LL0221"+
                            "1111PP1111"+
                            "0011LL1100"+
                            "0000LL0000"+
                            "1111111111";
                            break; }
        case 9: { strTemp = "1111111111"+
                            "2400000042"+
                            "1101111011"+
                            "1200000021"+
                            "1111111111"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        // lava
        case 10: { strTemp = "0000000000"+
                             "0000000000"+
                             "0000000000"+
                             "0000000000"+
                             "013wwww310"+
                             "013wwww310"+
                             "1113333111"+
                             "1111111111";
                             break; }
        case 11: { strTemp = "1000000001"+
                             "000r000000"+
                             "1000000001"+
                             "1000000001"+
                             "1111111111"+
                             "0000?00000"+
                             "0000000000"+
                             "1111111111";
                             break; }
        case 12: { strTemp = "1000000001"+
                             "0000000000"+
                             "1000?00001"+
                             "1000000001"+
                             "1111111111"+
                             "0000000000"+
                             "0000000000"+
                             "1111111111";
                             break; }
    }
}
else if (roomPath == 3)
{
    switch(rand(1,4))
    {
        // basic rooms
        case 1: { strTemp = "1000000001"+
                            "100r000001"+
                            "1000000001"+
                            "1000000001"+
                            "1100000011"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 2: { strTemp = "1000000000"+
                            "100r000000"+
                            "1000000000"+
                            "1000000000"+
                            "1100000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 3: { strTemp = "0000000001"+
                            "000r000001"+
                            "0000000001"+
                            "0000000001"+
                            "0000000011"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        // lava
        case 4: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "013wwww310"+
                            "013wwww310"+
                            "1113333111"+
                            "1111111111";
                            break; }
    }
}
else if (roomPath == 4) // shop
{
    strTemp = "1111111111"+
              "1111111111"+
              "1111221111"+
              "111l000211"+
              "...000W010"+
              "...00000k0"+
              "..Kiiii000"+
              "bbbbbbbbbb";

    if (oGame.damsel) n = rand(1,6);
    else n = rand(1,7);
    switch(n)
    {
        case 1: { shopType = "General"; break; }
        case 2: { shopType = "Bomb"; break; }
        case 3: { shopType = "Weapon"; break; }
        case 4: { shopType = "Rare"; break; }
        case 5: { shopType = "Clothing"; break; }
        case 6: { shopType = "Craps"; strTemp = "1111111111"+
            "1111111111"+
            "1111221111"+
            "11Kl000211"+
            "..bQ00W010"+
            ".0+00000k0"+
            ".q+uu00000"+
            "bbbbbbbbbb";
            break; }
        case 7: { shopType = "Kissing"; strTemp = "1111111111"+
            "1111111111"+
            "1111221111"+
            "111p000211"+
            "...000W010"+
            "...00000k0"+
            "..K00A0000"+
            "bbbbbbbbbb";
            oGame.damsel = true; break; }
    }
}
else if (roomPath == 5) // shop
{
    strTemp = "1111111111"+
              "1111111111"+
              "1111221111"+
              "112000l111"+
              "01W0000..."+
              "0k00000..."+
              "000iiiiK.."+
              "bbbbbbbbbb";

    if (oGame.damsel) n = rand(1,6);
    else n = rand(1,7);
    switch(n)
    {
        case 1: { shopType = "General"; break; }
        case 2: { shopType = "Bomb"; break; }
        case 3: { shopType = "Weapon"; break; }
        case 4: { shopType = "Rare"; break; }
        case 5: { shopType = "Clothing"; break; }
        case 6: { shopType = "Craps"; strTemp = "1111111111"+
            "1111111111"+
            "1111221111"+
            "112000lK11"+
            "01W0Q00b.."+
            "0k00000+0."+
            "00000uu+q."+
            "bbbbbbbbbb";
            break; }
        case 7: { shopType = "Kissing"; strTemp = "1111111111"+
            "1111111111"+
            "1111221111"+
            "112000p111"+
            "01W0000..."+
            "0k00000..."+
            "0000A00K.."+
            "bbbbbbbbbb";
            oGame.damsel = true; break; }
    }
}
else if (roomPath == 6) // Lady Xoc
{
    strTemp = "0000000000"+
              "00X0000000"+
              "0000000000"+
              "0000000000"+
              "0000000000"+
              "0000000000"+
              "0000000000"+
              "1111111111";
}
else if (roomPath == 7) // pit top
{
    strTemp = "0000000000"+
              "0000000000"+
              "0000000000"+
              "0000000000"+
              "0001001000"+
              "0011001100"+
              "0111;01110"+
              "111BBBB111";
}
else if (roomPath == 8) // pit
{
    strTemp = "1120000211"+
              "1120000211"+
              "1120000211"+
              "1120000211"+
              "1120000211"+
              "1120000211"+
              "1120000211"+
              "1120000211";
}
else if (roomPath == 9) // pit bottom
{
    strTemp = "1120000211"+
              "1120000211"+
              "1120000211"+
              "113wwww311"+
              "113wwww311"+
              "113wwww311"+
              "11RRRRRR11"+
              "1111111111";
}
else // drop
{
    switch(rand(1,8))
    {
        case 1: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0044404440"+
                            "1111101111";
                            break; }
        case 2: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0444000444"+
                            "1111000111";
                            break; }
        case 3: { strTemp = "0000000000"+
                            "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "1000000001";
                            break; }
        case 4: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "1000111111";
                            break; }
        case 5: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111110001";
                            break; }
        case 6: { strTemp = "0000000000"+
                            "0060000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0011122200"+
                            "0010000000"+
                            "1110111111";
                            break; }
        case 7: { strTemp = "0000000000"+
                            "0060000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0022211100"+
                            "0000000100"+
                            "1111110111";
                            break; }
        case 8: { strTemp = "0000000000"+
                            "0060000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0010110100"+
                            "0010000100"+
                            "1111001111";
                            break; }
    }
}

if (global.scumTileDebug) scrRevealObstacle(strTemp); // In-Level obstacle debug (YASM 1.7.3)

// Add obstacles
for (i = 1; i < 81; i += 1)
{
    j = i;

    strObs1 = "00000";
    strObs2 = "00000";
    strObs3 = "00000";
    tile = string_char_at(strTemp, i);

    if (tile == "8")
    {
        switch(rand(1,1))
        {
            case 1: { strObs1 = "00900";
                      strObs2 = "21112";
                      strObs3 = "21112";
                      break; }
        }
    }
    else if (tile == "5") // ground
    {
        switch(rand(1,8))
        {
            case 1: { strObs1 = "00000";
                      strObs2 = "02220";
                      strObs3 = "21112";
                      break; }
            case 2: { strObs1 = "00000";
                      strObs2 = "02020";
                      strObs3 = "21212";
                      break; }
            case 3: { strObs1 = "11100";
                      strObs2 = "11110";
                      strObs3 = "11111";
                      break; }
            case 4: { strObs1 = "00111";
                      strObs2 = "01111";
                      strObs3 = "11111";
                      break; }
            case 5: { strObs1 = "21112";
                      strObs2 = "22222";
                      strObs3 = "00000";
                      break; }
            case 6: { strObs1 = "00022";
                      strObs2 = "00011";
                      strObs3 = "00011";
                      break; }
            case 7: { strObs1 = "22000";
                      strObs2 = "11000";
                      strObs3 = "11000";
                      break; }
            case 8: { strObs1 = "00000";
                      strObs2 = "00000";
                      strObs3 = "00000";
                      break; }
        }
    }
    else if (tile == "6") // air
    {
        switch(rand(1,10))
        {
            case 1: { strObs1 = "11111";
                      strObs2 = "00000";
                      strObs3 = "00000";
                      break; }
            case 2: { strObs1 = "22222";
                      strObs2 = "00000";
                      strObs3 = "00000";
                      break; }
            case 3: { strObs1 = "22200";
                      strObs2 = "22200";
                      strObs3 = "00000";
                      break; }
            case 4: { strObs1 = "02220";
                      strObs2 = "02220";
                      strObs3 = "00000";
                      break; }
            case 5: { strObs1 = "00222";
                      strObs2 = "00222";
                      strObs3 = "00000";
                      break; }
            case 6: { strObs1 = "00000";
                      strObs2 = "01110";
                      strObs3 = "00000";
                      break; }
            case 7: { strObs1 = "00000";
                      strObs2 = "01110";
                      strObs3 = "02220";
                      break; }
            case 8: { strObs1 = "00000";
                      strObs2 = "02220";
                      strObs3 = "01110";
                      break; }
            case 9: { strObs1 = "00000";
                      strObs2 = "20100";
                      strObs3 = "00111";
                      break; }
            case 10: { strObs1 = "00000";
                       strObs2 = "00102";
                       strObs3 = "11100";
                       break; }
        }
    }
    else if (tile == "r") // air
    {
        switch(rand(1,10))
        {
            case 1: { strObs1 = "1111";
                      strObs2 = "0000";
                      strObs3 = "0000";
                      break; }
            case 2: { strObs1 = "2222";
                      strObs2 = "0000";
                      strObs3 = "0000";
                      break; }
            case 3: { strObs1 = "2220";
                      strObs2 = "2220";
                      strObs3 = "0000";
                      break; }
            case 4: { strObs1 = "0222";
                      strObs2 = "0222";
                      strObs3 = "0000";
                      break; }
            case 5: { strObs1 = "2222";
                      strObs2 = "0000";
                      strObs3 = "2222";
                      break; }
            case 6: { strObs1 = "0000";
                      strObs2 = "1111";
                      strObs3 = "0000";
                      break; }
            case 7: { strObs1 = "0000";
                      strObs2 = "1111";
                      strObs3 = "2222";
                      break; }
            case 8: { strObs1 = "0000";
                      strObs2 = "2222";
                      strObs3 = "1111";
                      break; }
            case 9: { strObs1 = "0000";
                      strObs2 = "0220";
                      strObs3 = "2112";
                      break; }
            case 10: { strObs1 = "0000";
                       strObs2 = "2002";
                       strObs3 = "1221";
                       break; }
        }
    }

    if (tile == "5" or tile == "6" or tile == "8")
    {
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs1, strTemp, j);
        j += 10;
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs2, strTemp, j);
        j += 10;
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs3, strTemp, j);
    }
    else if (tile == "r")
    {
        strTemp = string_delete(strTemp, j, 4);
        strTemp = string_insert(strObs1, strTemp, j);
        j += 10;
        strTemp = string_delete(strTemp, j, 4);
        strTemp = string_insert(strObs2, strTemp, j);
        j += 10;
        strTemp = string_delete(strTemp, j, 4);
        strTemp = string_insert(strObs3, strTemp, j);
    }
}

// Generate the tiles
for (j = 0; j < 8; j += 1)
{
    for (i = 1; i < 11; i += 1)
    {
        tile = string_char_at(strTemp, i+j*10);
        xpos = x + (i-1)*16;
        ypos = y + j*16;

        //debugging
        if (global.scumTileDebug)
        {
            if (tile != "0")
            {
                obj = instance_create(xpos, ypos, oTileDebug);
                obj.tileString = tile;
            }
        }

        if (tile == "1" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,100) == 1) instance_create(xpos, ypos, oLush);
            else if (rand(1,10) == 1) instance_create(xpos, ypos, oBlock);
            else instance_create(xpos, ypos, oTemple);
        }
        else if (tile == "2" and rand(1,2) == 1 and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,10) == 1) instance_create(xpos, ypos, oBlock);
            else instance_create(xpos, ypos, oTemple);
        }
        else if (tile == "3" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,2) == 1) instance_create(xpos, ypos, oLava);
            else instance_create(xpos, ypos, oTemple);
        }
        else if (tile == "R" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            block = instance_create(xpos, ypos, oTemple);
            with (block) treasure = "Big Ruby";
        }
        else if (tile == "L") instance_create(xpos, ypos, oLadderOrange);
        else if (tile == "P") instance_create(xpos, ypos, oLadderTop);
        else if (tile == "7" and rand(1,3) == 1)
        {
            if (global.woodSpikes) instance_create(xpos, ypos, oSpikesWood);
            else instance_create(xpos, ypos, scrGenSpikeType());
        }
        else if (tile == "4" and rand(1,4) == 1) instance_create(xpos, ypos, oPushBlock);
        else if (tile == "9")
        {
            block = instance_create(xpos, ypos+16, oTemple);
            if (scrGetRoomX(x) == global.startRoomX and scrGetRoomY(y) == global.startRoomY)
                instance_create(xpos, ypos, oEntrance);
            else
            {
                instance_create(xpos, ypos, oExit);
                global.exitX = xpos+8;
                global.exitY = ypos+8;
                block.invincible = true;
                block.ore = 0;
            }
        }
        else if (tile == "a")
        {
            scrLevGenCreateChest(xpos+8, ypos+8);
        }
        else if (tile == "c")
        {
            if (rand(1,2) == 1) scrLevGenCreateChest(xpos+8, ypos+8);
            else scrLevGenCreateCrate(xpos+8, ypos+8);
        }
        else if (tile == "t")
        {
            if (rand(1,120) == 1) instance_create(xpos+8, ypos+12, oRubyBig);
            else if (rand(1,80) == 1) instance_create(xpos+8, ypos+12, oSapphireBig);
            else if (rand(1,60) == 1) instance_create(xpos+8, ypos+12, oEmeraldBig);
            else instance_create(xpos+8, ypos+8, oGoldBars);
        }
        else if (tile == "x")
        {
            instance_create(xpos, ypos, oSacAltarLeft);
            instance_create(xpos+16, ypos, oSacAltarRight);
            tile_add(bgKaliBody, 0, 0, 64, 64, xpos-16, ypos-48, 10001);
            instance_create(xpos+16, ypos-80+16, oKaliHead);
        }
        else if (tile == "X")
        {
            for (l = 0; l < 6; l += 1)
            {
                for (k = 0; k < 5; k += 1)
                {
                    obj = instance_create(xpos+k*16, ypos+l*16, oXocBlock);
                    if (k == 2 and l == 1) obj.treasure = "Diamond";
                    if (k == 1 and l == 2) obj.treasure = "Sapphire";
                    if (k == 3 and l == 2) obj.treasure = "Sapphire";
                    if (k == 0 and l == 3) obj.treasure = "Emerald";
                    if (k == 4 and l == 3) obj.treasure = "Emerald";
                    if (k == 2 and l == 4) obj.treasure = "Ruby";
                    tile_add(bgLadyXoc, k*16, l*16, 16, 16, xpos+k*16, ypos+l*16, 99);
                }
            }
        }
        else if (tile == "I")
        {
            instance_create(xpos+16, ypos+12, oGoldIdol);
        }
        else if (tile == ";")
        {
            obj = instance_create(xpos+8, ypos+8, oDamsel);
            obj.cost = 0;
            obj.forSale = false;
            instance_create(xpos+16+8, ypos+12, oGoldIdol);
        }
        else if (tile == "B")
        {
            if (rand(1,1) == 1) instance_create(xpos, ypos, oTrapBlock);
        }
        else if (tile == "C")
        {
            instance_create(xpos, ypos, oCeilingTrap);
        }
        else if (tile == "D")
        {
            instance_create(xpos, ypos, oDoor);
            instance_create(xpos, ypos, oTempleFake);
            instance_create(xpos, ypos+16, oTempleFake);
        }
        else if (tile == "A")
        {
            obj = instance_create(xpos+8, ypos+8, oDamsel);
            obj.forSale = true;
            obj.status = 5;
        }
        else if (tile == "?")
        {
            instance_create(xpos, ypos, oTombLord);
        }
        else if (tile == "." and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            obj = instance_create(xpos, ypos, oTemple);
            obj.shopWall = true;
        }
        else if (tile == "Q")
        {
            if (shopType == "Craps")
            {
                tile_add(bgDiceSign, 0, 0, 48, 32, xpos, ypos, 9004);
            }
        }
        else if (tile == "q")
        {
            //???: n = rand(1,6);
            obj = scrGenerateItem(xpos+8, ypos+8, 1);
            obj.inDiceHouse = true;
        }
        else if (tile == "+")
        {
            obj = instance_create(xpos, ypos, oSolid);
            obj.sprite_index = sIceBlock;
            obj.shopWall = true;
        }
        else if (tile == "W")
        {
            if (global.murderer or global.thiefLevel > 0)
            {
                if (global.isDamsel) tile_add(bgWanted, 32, 0, 32, 32, xpos, ypos, 9004);
                else if (global.isTunnelMan) tile_add(bgWanted, 64, 0, 32, 32, xpos, ypos, 9004);
                else tile_add(bgWanted, 0, 0, 32, 32, xpos, ypos, 9004);
            }
        }
        else if (tile == "b")
        {
            obj = instance_create(xpos, ypos, oTemple);
            obj.shopWall = true;
        }
        else if (tile == "l")
        {
            instance_create(xpos, ypos, oLamp);
        }
        else if (tile == "p")
        {
            instance_create(xpos, ypos, oLampRed);
        }
        else if (tile == "K")
        {
            //obj = instance_create(xpos, ypos, oShopkeeper);
            //obj.style = shopType;
            scrLevGenShopkeeper(xpos, ypos, shopType);
        }
        else if (tile == "k")
        {
            obj = instance_create(xpos, ypos, oSign);
            if (shopType == "General") obj.sprite_index = sSignGeneral;
            else if (shopType == "Bomb") obj.sprite_index = sSignBomb;
            else if (shopType == "Weapon") obj.sprite_index = sSignWeapon;
            else if (shopType == "Clothing") obj.sprite_index = sSignClothing;
            else if (shopType == "Rare") obj.sprite_index = sSignRare;
            else if (shopType == "Craps") obj.sprite_index = sSignCraps;
            else if (shopType == "Kissing") obj.sprite_index = sSignKissing;
        }
        else if (tile == "i")
        {
            scrShopItemsGen(xpos, ypos);
        }

        else if (tile == "w")
        {
            instance_create(xpos, ypos, oLava);
        }
        else if (tile == "u")
        {
            instance_create(xpos+8, ypos+8, oDice);
        }
        else if (tile == "d")
        {
            instance_create(xpos, ypos, oLush);
        }
        else if (tile == "e")
        {
            if (rand(1,2) == 1) instance_create(xpos, ypos, oLush);
        }
        else if (tile == "T")
        {
            instance_create(xpos, ypos, oTree);
            n = 0;
            tx = xpos;
            ty = ypos-16;
            b1 = false;
            b2 = false;
            for (m = 0; m < 5; m += 1)
            {
                if (rand(0,m) > 2)
                {
                    break;
                }
                else
                {
                    if (not collision_point(tx, ty-16, oSolid, 0, 0) and
                        not collision_point(tx-16, ty-16, oSolid, 0, 0) and
                        not collision_point(tx+16, ty-16, oSolid, 0, 0))
                    {
                        instance_create(tx, ty, oTree);
                        if (m < 4)
                        {
                            if (rand(1,5) < 4 and not b1)
                            {
                                instance_create(tx+16, ty, oTreeBranch);
                                b1 = true;
                            }
                            else if (b1) b1 = false;
                            if (rand(1,5) < 4 and not b2)
                            {
                                instance_create(tx-16, ty, oTreeBranch);
                                b2 = true;
                            }
                            else if (b2) b2 = false;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                ty -= 16;
            }
            instance_create(tx-16, ty+16, oLeaves);
            instance_create(tx+16, ty+16, oLeaves);
        }
    }
}
