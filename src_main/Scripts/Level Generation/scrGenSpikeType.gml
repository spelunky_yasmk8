if (global.woodSpikes) {
  if (global.optSpikeVariations) {
    if (rand(0, 100) >= 76) return oSpikes;
  }
  return oSpikesWood;
} else {
  if (global.optSpikeVariations) {
    if (rand(0, 100) >= 76) return oSpikesWood;
  }
  return oSpikes;
}
