// scrLevGenShopkeeper(x, y, style)
var obj;
obj = instance_create(argument0, argument1, oShopkeeper);
//obj.shopkeeperName = scrGetName();
obj.style = /*shopType*/argument2;
if (/*shopType*/argument2 == "Craps") {
  // select sequence of next 9 prizes
  for (i = 0; i < 9; i += 1) {
    obj.prizes[i] = scrGeneratePrize();
    if (obj.prizes[i] == "Mattock") obj.prizeUses[i] = scrSetMattockUses();
  }
}
return obj; // just in case
