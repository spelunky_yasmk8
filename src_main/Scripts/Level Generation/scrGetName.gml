//
// scrGetName()
//
// Return a random name (for shopkeeper).
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/

switch (rand(1, 50)) {
  case 1: return "AHKMED";
  case 2: return "TERRY";
  case 3: return "SMITHY";
  case 4: return "KASIM";
  case 5: return "DOUG";
  case 6: return "BEN";
  case 7: return "KERT";
  case 8: return "DUKE";
  case 9: return "TOBY";
  case 10: return "GUERT";
  case 11: return "PANCHO";
  case 12: return "EARL";
  case 13: return "IVAN";
  case 14: return "OLLIE";
  case 15: return "IDO";
  case 16: return "BOB";
  case 17: return "RUDY";
  case 18: return "JIMBO";
  case 19: return "ERIC";
  case 20: return "WILLY";
  case 21: return "MATT";
  case 22: return "LAZLO";
  case 23: return "WANG";
  case 24: return "PETER";
  case 25: return "ANDY";
  case 26: return "DONG";
  case 27: return "LEMMY";
  case 28: return "OMAR";
  case 29: return "VADIM";
  case 30: return "TARN";
  case 31: return "SLASH";
  case 32: return "LANCE";
  case 33: return "ALEC";
  case 34: return "NOEL";
  case 35: return "KYLE";
  case 36: return "DEREK";
  case 37: return "RICH";
  case 38: return "JON";
  case 39: return "TOM";
  case 40: return "PHIL";
  case 41: return "CALVIN";
  case 42: return "HASSAN";
  case 43: return "PAUL";
  case 44: return "COLIN";
  case 45: return "IAN";
  case 46: return "EDDIE";
  case 47: return "JAKE";
  case 48: return "JOEY";
  case 49: return "JOSH";
  case 50: return "STEVE";
}
return "AHKMED";
