// scrLevGenJawsCrate()
// Select contents of the a crate dropped by Jaws during level generation
// Returns contents (string)
if (rand(1, 500) == 1) return "Jetpack";
if (rand(1, 200) == 1) return "Cape";
if (rand(1, 100) == 1) return "Shotgun";
if (rand(1, 100) == 1) return "Mattock";
if (rand(1, 100) == 1) return "Teleporter";
if (rand(1, 90) == 1) return "Gloves";
if (rand(1, 90) == 1) return "Spectacles";
if (rand(1, 80) == 1) return "Web Cannon";
if (rand(1, 80) == 1) return "Pistol";
if (rand(1, 80) == 1) return "Mitt";
if (rand(1, 60) == 1) return "Paste";
if (rand(1, 60) == 1) return "Spring Shoes";
if (rand(1, 60) == 1) return "Spike Shoes";
if (rand(1, 60) == 1) return "Machete";
if (rand(1, 40) == 1) return "Bomb Box";
if (rand(1, 40) == 1) return "Bow";
if (rand(1, 20) == 1) return "Compass";
if (rand(1, 10) == 1) return "Parachute";
if (rand(1, 2) == 1) return "Rope Pile";
return "Bomb Bag";
