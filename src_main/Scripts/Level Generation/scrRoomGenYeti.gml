//
// scrRoomGenYeti()
//
// Room generation for the Yeti Cave that appears randomly in Area 3: Ice.
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/
var strTemp, roomPathAbove, n, i, j, strObs1, strObs2, strObs3;
var tile, xpos, ypos, obj, block;

/*
Note:

ROOMS are 10x8 tile areas.

strTemp = "0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000";

OBSTACLES are 5x3 tile chunks that are randomized within rooms.

strObs = "00000
          00000
          00000";

The string representing a room or obstacle must be laid out unbroken:
*/
strTemp = "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000"+
          "0000000000";

roomPath = global.roomPath[scrGetRoomX(x), scrGetRoomY(y)];
roomPathAbove = -1;
if (scrGetRoomY(y) != 0) roomPathAbove = global.roomPath[scrGetRoomX(x), scrGetRoomY(y-128)];

if (scrGetRoomX(x) == global.startRoomX and scrGetRoomY(y) == global.startRoomY) // start room
{
    if (roomPath == 2) n = rand(3,4);
    else n = rand(1,2);
    switch(n)
    {
        case 1: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 2: { strTemp = "1111111111"+
                            "2222222222"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        // hole
        case 3: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "2021111120";
                            break; }
        case 4: { strTemp = "1111111111"+
                            "2222222222"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "2021111120";
                            break; }
    }
}
else if (scrGetRoomX(x) == global.endRoomX and scrGetRoomY(y) == global.endRoomY) // end room
{
    if (roomPathAbove == 2) n = rand(1,4);
    else n = rand(3,6);
    switch(n)
    {
        case 1: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 2: { strTemp = "0000000000"+
                            "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 3: { strTemp = "0000000000"+
                            "0010021110"+
                            "0010011110"+
                            "0011011112"+
                            "9012000000"+
                            "1111111110"+
                            "2111111120"+
                            "1111111111";
                            break; }
        case 4: { strTemp = "0000000000"+
                            "0111200100"+
                            "0111100100"+
                            "2111101100"+
                            "0000002109"+
                            "0111111111"+
                            "0211111112"+
                            "1111111111";
                            break; }
        // no drop
        case 5: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 6: { strTemp = "1111111111"+
                            "2222222222"+
                            "0000000000"+
                            "0000000000"+
                            "0008000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
    }
}
else if (roomPath == 0 and rand(1,4) < 4) // side room
{
    if (oGame.idol or scrGetRoomY(y) == 3)
        n = rand(1,2);
    else
    {
        n = rand(1,1);
        if (n == 10) oGame.idol = true;
        // else n = rand(1,9);
    }

    switch(n)
    {
        // upper plats
        case 1: { strTemp = "0000000000"+
                            "0010111100"+
                            "0000000000"+
                            "0001101000"+
                            "0050000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        // high walls
        case 2: { strTemp = "0000000000"+
                            "11tttttt11"+
                            "1200000021"+
                            "1200220021"+
                            "1200000021"+
                            "1200220021"+
                            "11ssssss11"+
                            "1111111111";
                            break; }
        // idols
        /* case 10: { strTemp = "2200000022"+
                                "0000B00000"+
                                "0000000000"+
                                "0000000000"+
                                "0000000000"+
                                "0000000000"+
                                "0000I00000"+
                                "1111A01111";
                                break; } */
    }
}
else if (roomPath == 0 or roomPath == 1) // main room
{
    if (not instance_exists(oYetiKing))
    {
        n = rand(1,10);
        if (y > 384) n = 10;
        else if (y > 256 and rand(1,2) == 1) n = 10;
        else if (y > 128 and rand(1,3) == 1) n = 10;
    }
    else n = rand(1,9);

    switch(n)
    {
        // basic rooms
        case 1: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0050000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 2: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "5000050000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 3: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0005000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111"+
                            "1111111111";
                            break; }
        /*case 4: { strTemp = "6000060000"+
                              "0000000000"+
                              "0006000000"+
                              "0000000000"+
                              "0000000000"+
                              "0002222200"+
                              "0011111100"+
                              "1111111111";
                              break; }*/
        case 4: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0002222200"+
                            "0011111100"+
                            "1111111111";
                            break; }
        case 5: { strTemp = "1111111111"+
                            "2222222222"+
                            "0000000000"+
                            "0000000000"+
                            "0050000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 6: { strTemp = "1111111111"+
                            "2111111112"+
                            "0222222220"+
                            "0000000000"+
                            "0050000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        // low ceiling
        case 7: { strTemp = "1111111111"+
                            "2111111112"+
                            "2111111112"+
                            "2111111112"+
                            "0111111110"+
                            "0222222220"+
                            "0000000000"+
                            "1111111111";
                            break; }
        // thin ice
        case 8: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "tttttttttt";
                            break; }
        case 9: { strTemp = "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "01tttttt10"+
                            "21ssssss12"+
                            "1111111111";
                            break; }
        // yeti king
        case 10: { strTemp = "iiiiiiiiii"+
                             "jiiiiiiiij"+
                             "0jjjjjjjj0"+
                             "0000000000"+
                             "0000000000"+
                             "0000Y00000"+
                             "00yy00yy00"+
                             "1111111111";
                             break; }
    }
}
else if (roomPath == 3) // main room
{
    switch(rand(1,9))
    {

        // basic rooms
        case 1: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0050000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 2: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "5000050000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 3: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "5000050000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111"+
                            "1111111111";
                            break; }
        case 4: { strTemp = "0000000000"+
                            "0000000000"+
                            "0006000000"+
                            "0000000000"+
                            "0000000000"+
                            "0001111100"+
                            "0011111100"+
                            "1111111111";
                            break; }
        // upper plats
        case 5: { strTemp = "0000000000"+
                            "0111111110"+
                            "0011111100"+
                            "0000000000"+
                            "5000050000"+
                            "0000000000"+
                            "0000000000"+
                            "1111111111";
                            break; }
        case 6: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0021111200"+
                            "0211111120"+
                            "2111111112"+
                            "1111111111";
                            break; }
        case 7: { strTemp = "1000000001"+
                            "1112002111"+
                            "1112002111"+
                            "0000000000"+
                            "0022222000"+
                            "1111111111"+
                            "1111111111"+
                            "1111111111";
                            break; }
        // thin ice
        case 8: { strTemp = "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "ssssssssss"+
                            "1111111111";
                            break; }
        case 9: { strTemp = "1000000001"+
                            "1111001111"+
                            "1111tt1111"+
                            "1120000211"+
                            "0000000000"+
                            "0000000000"+
                            "1110000111"+
                            "1111tt1111";
                            break; }
    }
}
else if (roomPath == 4) // shop
{

    strTemp = "1111111111"+
              "1111111111"+
              "1111221111"+
              "111l000211"+
              "...000W010"+
              "...00000k0"+
              "..K$$$$000"+
              "bbbbbbbbbb";

    switch(rand(1,7))
    {
        case 1: { shopType = "General"; break; }
        case 2: { shopType = "Bomb"; break; }
        case 3: { shopType = "Weapon"; break; }
        case 4: { shopType = "Rare"; break; }
        case 5: { shopType = "Clothing"; break; }
        case 6: { shopType = "Craps"; strTemp = "1111111111"+
            "1111111111"+
            "1111221111"+
            "11Kl000211"+
            "..bQ00W010"+
            ".0+00000k0"+
            ".q+dd00000"+
            "bbbbbbbbbb";
            break; }
        case 7: { shopType = "Kissing"; strTemp = "1111111111"+
            "1111111111"+
            "1111221111"+
            "111l000211"+
            "...000W010"+
            "...00000k0"+
            "..K00D0000"+
            "bbbbbbbbbb";
            oGame.damsel = true; break; }
    }
}

else if (roomPath == 5) // shop
{

    strTemp = "1111111111"+
              "1111111111"+
              "1111221111"+
              "112000l111"+
              "01W0000..."+
              "0k00000..."+
              "000$$$$K.."+
              "bbbbbbbbbb";

    switch(rand(1,7))
    {
        case 1: { shopType = "General"; break; }
        case 2: { shopType = "Bomb"; break; }
        case 3: { shopType = "Weapon"; break; }
        case 4: { shopType = "Rare"; break; }
        case 5: { shopType = "Clothing"; break; }
        case 6: { shopType = "Craps"; strTemp = "1111111111"+
            "1111111111"+
            "1111221111"+
            "112000lK11"+
            "01W0Q00b.."+
            "0k00000+0."+
            "00000dd+q."+
            "bbbbbbbbbb";
            break; }
        case 7: { shopType = "Kissing"; strTemp = "1111111111"+
            "1111111111"+
            "1111221111"+
            "112000l111"+
            "01W0000..."+
            "0k00000..."+
            "0000D00K.."+
            "bbbbbbbbbb";
            oGame.damsel = true; break; }
    }
}
else if (roomPath == 8) // snake pit
{
    switch(rand(1,1))
    {
        case 1: { strTemp = "1110000111"+
                            "11s0000s11"+
                            "1112002111"+
                            "11s0000s11"+
                            "1112002111"+
                            "11s0000s11"+
                            "1112002111"+
                            "11s0000s11";
                            break; }
    }
}
else if (roomPath == 9) // snake pit bottom
{
    switch(rand(1,1))
    {
        case 1: { strTemp = "1110000111"+
                            "11s0000s11"+
                            "1110000111"+
                            "1100S00011"+
                            "11S0110S11"+
                            "111STTS111"+
                            "1111111111"+
                            "1111111111";
                            break; }
    }
}
else // drop
{
    if (roomPath == 7) n = rand(4,12);
    else if (roomPathAbove != 2) n = rand(1,12);
    else n = rand(1,8);
    switch(n)
    {
        case 1: { strTemp = "0000000000"+
                            "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000";
                            break; }
        case 2: { strTemp = "0000000000"+
                            "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000050000"+
                            "0000000000"+
                            "0000000000"+
                            "1202111111";
                            break; }
        /*case 3: { strTemp = "0000000000"+
                              "6000060000"+
                              "0000000000"+
                              "0000000005"+
                              "0000000000"+
                              "0000000000"+
                              "0000000000"+
                              "1111112021";
                              break; }*/
        case 3: { strTemp = "0000000000"+
                            "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "5000000000"+
                            "0000000000"+
                            "0000000000"+
                            "1111112021";
                            break; }
        case 4: { strTemp = "0000000000"+
                            "6000060000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0220000220"+
                            "1112002111";
                            break; }
        case 5: { strTemp = "0000000000"+
                            "0000220000"+
                            "0000000000"+
                            "0020000200"+
                            "0112002110"+
                            "0111001110"+
                            "1200000021"+
                            "1111001111";
                            break; }
        case 6: { strTemp = "0000000000"+
                            "0060000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0011122200"+
                            "0210000000"+
                            "1110111111";
                            break; }
        case 7: { strTemp = "0000000000"+
                            "0060000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0022211100"+
                            "0000000120"+
                            "1111110111";
                            break; }
        case 8: { strTemp = "0000000000"+
                            "0060000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0020220200"+
                            "0010000100"+
                            "1111001111";
                            break; }
        case 9: { strTemp = "1111111111"+
                            "2222222222"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "0000000000"+
                            "1120000211";
                            break; }
        case 10: { strTemp = "1111111111"+
                             "2222111111"+
                             "0000022111"+
                             "0000000211"+
                             "0000000000"+
                             "2000000000"+
                             "0000000021"+
                             "1120000211";
                             break; }
        case 11: { strTemp = "1111111111"+
                             "1111112222"+
                             "1112200000"+
                             "1120000000"+
                             "0000000000"+
                             "0000000000"+
                             "1200000000"+
                             "1120000211";
                             break; }
        case 12: { strTemp = "1111111111"+
                             "2111111112"+
                             "0211111120"+
                             "0021111200"+
                             "0002112000"+
                             "0000220000"+
                             "0220000220"+
                             "1111001111";
                             break; }
    }
}

if (global.scumTileDebug) scrRevealObstacle(strTemp); // In-Level obstacle debug (YASM 1.7.3)

// Add obstacles
for (i = 1; i < 81; i += 1)
{
    j = i;

    strObs1 = "00000";
    strObs2 = "00000";
    strObs3 = "00000";
    tile = string_char_at(strTemp, i);

    if (tile == "8")
    {
        switch(rand(1,7))
        {
            case 1: { strObs1 = "00900";
                      strObs2 = "01110";
                      strObs3 = "11111";
                      break; }
            case 2: { strObs1 = "00900";
                      strObs2 = "02120";
                      strObs3 = "02120";
                      break; }
            case 3: { strObs1 = "00000";
                      strObs2 = "00000";
                      strObs3 = "92222";
                      break; }
            case 4: { strObs1 = "00000";
                      strObs2 = "00000";
                      strObs3 = "22229";
                      break; }
            case 5: { strObs1 = "00000";
                      strObs2 = "11001";
                      strObs3 = "19001";
                      break; }
            case 6: { strObs1 = "00000";
                      strObs2 = "10011";
                      strObs3 = "10091";
                      break; }
            case 7: { strObs1 = "00000";
                      strObs2 = "12021";
                      strObs3 = "12921";
                      break; }
        }
    }
    else if (tile == "5") // ground
    {
        switch(rand(1,16))
        {
            case 1: { strObs1 = "11111";
                      strObs2 = "00000";
                      strObs3 = "00000";
                      break; }
            case 2: { strObs1 = "00000";
                      strObs2 = "11110";
                      strObs3 = "00000";
                      break; }
            case 3: { strObs1 = "00000";
                      strObs2 = "01111";
                      strObs3 = "00000";
                      break; }
            case 4: { strObs1 = "00000";
                      strObs2 = "00000";
                      strObs3 = "11111";
                      break; }
            case 5: { strObs1 = "00000";
                      strObs2 = "20200";
                      strObs3 = "17177";
                      break; }
            case 6: { strObs1 = "00000";
                      strObs2 = "02020";
                      strObs3 = "71717";
                      break; }
            case 7: { strObs1 = "00000";
                      strObs2 = "00202";
                      strObs3 = "77171";
                      break; }
            case 8: { strObs1 = "00000";
                      strObs2 = "22200";
                      strObs3 = "11100";
                      break; }
            case 9: { strObs1 = "00000";
                      strObs2 = "02220";
                      strObs3 = "01110";
                      break; }
            case 10: { strObs1 = "00000";
                       strObs2 = "00222";
                       strObs3 = "00111";
                       break; }
            case 11: { strObs1 = "11100";
                       strObs2 = "22200";
                       strObs3 = "00000";
                       break; }
            case 12: { strObs1 = "01110";
                       strObs2 = "02220";
                       strObs3 = "00000";
                       break; }
            case 13: { strObs1 = "00111";
                       strObs2 = "00222";
                       strObs3 = "00000";
                       break; }
            case 14: { strObs1 = "00000";
                       strObs2 = "02220";
                       strObs3 = "21112";
                       break; }
            case 15: { strObs1 = "00000";
                       strObs2 = "20100";
                       strObs3 = "77117";
                       break; }
            case 16: { strObs1 = "00000";
                       strObs2 = "00102";
                       strObs3 = "71177";
                       break; }
        }
    }
    else if (tile == "6") // air
    {
        switch(rand(1,10))
        {
            case 1: { strObs1 = "11111";
                      strObs2 = "00000";
                      strObs3 = "00000";
                      break; }
            case 2: { strObs1 = "22222";
                      strObs2 = "00000";
                      strObs3 = "00000";
                      break; }
            case 3: { strObs1 = "11100";
                      strObs2 = "22200";
                      strObs3 = "00000";
                      break; }
            case 4: { strObs1 = "01110";
                      strObs2 = "02220";
                      strObs3 = "00000";
                      break; }
            case 5: { strObs1 = "00111";
                      strObs2 = "00222";
                      strObs3 = "00000";
                      break; }
            case 6: { strObs1 = "00000";
                      strObs2 = "01110";
                      strObs3 = "00000";
                      break; }
            case 7: { strObs1 = "00000";
                      strObs2 = "01110";
                      strObs3 = "02220";
                      break; }
            case 8: { strObs1 = "00000";
                      strObs2 = "02220";
                      strObs3 = "01110";
                      break; }
            case 9: { strObs1 = "00000";
                      strObs2 = "00222";
                      strObs3 = "00111";
                      break; }
            case 10: { strObs1 = "00000";
                       strObs2 = "22200";
                       strObs3 = "11100";
                       break; }
        }
    }

    if (tile == "5" or tile == "6" or tile == "8")
    {
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs1, strTemp, j);
        j += 10;
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs2, strTemp, j);
        j += 10;
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs3, strTemp, j);
    }
}

// Generate the tiles
for (j = 0; j < 8; j += 1)
{
    for (i = 1; i < 11; i += 1)
    {
        tile = string_char_at(strTemp, i+j*10);
        xpos = x + (i-1)*16;
        ypos = y + j*16;

        //debugging
        if (global.scumTileDebug)
        {
            if (tile != "0")
            {
                obj = instance_create(xpos, ypos, oTileDebug);
                obj.tileString = tile;
            }
        }

        if (tile == "1" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,6) == 1) instance_create(xpos, ypos, oDark);
            else instance_create(xpos, ypos, oIce);
        }
        if (tile == "2" and rand(1,2) == 1 and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,6) == 1) instance_create(xpos, ypos, oDark);
            else instance_create(xpos, ypos, oIce);
        }
        if (tile == "t" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            instance_create(xpos, ypos, oThinIce);
        }
        else if (tile == "L") instance_create(xpos, ypos, oLadderOrange);
        else if (tile == "P") instance_create(xpos, ypos, oLadderTop);
        else if (tile == "7" and rand(1,3) == 1)
        {
            if (global.woodSpikes) instance_create(xpos, ypos, oSpikesWood);
            else instance_create(xpos, ypos, scrGenSpikeType());
        }
        else if (tile == "4" and rand(1,4) == 1) instance_create(xpos, ypos, oPushBlock);
        else if (tile == "9")
        {
            block = instance_create(xpos, ypos+16, oDark);
            if (scrGetRoomX(x) == global.startRoomX and scrGetRoomY(y) == global.startRoomY)
                instance_create(xpos, ypos, oEntrance);
            else
            {
                instance_create(xpos, ypos, oExit);
                global.exitX = xpos+8;
                global.exitY = ypos+8;
                block.invincible = true;
                block.ore = 0;
            }
        }
        else if (tile == "A")
        {
            instance_create(xpos, ypos, oAltarLeft);
            instance_create(xpos+16, ypos, oAltarRight);
        }
        else if (tile == "a")
        {
            scrLevGenCreateChest(xpos, ypos);
        }
        else if (tile == "I")
        {
            instance_create(xpos+16, ypos+8, oGoldIdol);
        }
        else if (tile == "B")
        {
            instance_create(xpos+16, ypos+16, oGiantTikiHead); //ypos+12
        }
        else if (tile == "." and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            obj = instance_create(xpos, ypos, oDark);
            obj.shopWall = true;
        }
        else if (tile == "Q")
        {
            if (shopType == "Craps")
            {
                tile_add(bgDiceSign, 0, 0, 48, 32, xpos, ypos, 9004);
            }
        }
        else if (tile == "q")
        {
            //???: n = rand(1,6);
            obj = scrGenerateItem(xpos+8, ypos+8, 1);
            obj.inDiceHouse = true;
        }
        else if (tile == "+")
        {
            obj = instance_create(xpos, ypos, oSolid);
            obj.sprite_index = sIceBlock;
            obj.shopWall = true;
        }
        else if (tile == "W")
        {
            if (global.murderer or global.thiefLevel > 0)
            {
                if (global.isDamsel) tile_add(bgWanted, 32, 0, 32, 32, xpos, ypos, 9004);
                else if (global.isTunnelMan) tile_add(bgWanted, 64, 0, 32, 32, xpos, ypos, 9004);
                else tile_add(bgWanted, 0, 0, 32, 32, xpos, ypos, 9004);
            }
        }
        else if (tile == "b")
        {
            obj = instance_create(xpos, ypos, oBrickSmooth);
            obj.sprite_index = sDarkSmooth;
            obj.shopWall = true;
        }
        else if (tile == "l")
        {
            if (oGame.damsel) instance_create(xpos, ypos, oLampRed);
            else instance_create(xpos, ypos, oLamp);
        }
        else if (tile == "K")
        {
            //obj = instance_create(xpos, ypos, oShopkeeper);
            //obj.style = shopType;
            scrLevGenShopkeeper(xpos, ypos, shopType);
        }
        else if (tile == "k")
        {
            obj = instance_create(xpos, ypos, oSign);
            if (shopType == "General") obj.sprite_index = sSignGeneral;
            else if (shopType == "Bomb") obj.sprite_index = sSignBomb;
            else if (shopType == "Weapon") obj.sprite_index = sSignWeapon;
            else if (shopType == "Clothing") obj.sprite_index = sSignClothing;
            else if (shopType == "Rare") obj.sprite_index = sSignRare;
            else if (shopType == "Craps") obj.sprite_index = sSignCraps;
            else if (shopType == "Kissing") obj.sprite_index = sSignKissing;
        }
        else if (tile == "$")
        {
            scrShopItemsGen(xpos, ypos);
        }
        else if (tile == "d")
        {
            instance_create(xpos+8, ypos+8, oDice);
        }
        else if (tile == "D")
        {
            obj = instance_create(xpos+8, ypos+8, oDamsel);
            obj.forSale = true;
            obj.status = 5;
        }
        else if (tile == "s")
        {
            if (global.woodSpikes) instance_create(xpos, ypos, oSpikesWood);
            else instance_create(xpos, ypos, scrGenSpikeType());
        }
        else if (tile == "S")
        {
            var stt;
            stt = scrGenGetSnakeType();
            instance_create(xpos, ypos, stt);
        }
        else if (tile == "T")
        {
            instance_create(xpos+8, ypos+8, oRubyBig);
        }
        else if (tile == "i")
        {
            instance_create(xpos, ypos, oIce);
        }
        else if (tile == "j" and rand(1,2) == 1)
        {
            instance_create(xpos, ypos, oIce);
        }
        else if (tile == "Y")
        {
            instance_create(xpos, ypos, oYetiKing);
        }
        else if (tile == "y")
        {
            instance_create(xpos, ypos, oYeti);
        }
    }
}
