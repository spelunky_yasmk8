if (global.optEnemyVariations) {
  var totdeath789;
  totdeath789 = global.levelDeaths[6]+global.levelDeaths[7]+global.levelDeaths[8];

  if (totdeath789 < 22) {
    if (rand(0, 100) >= 99-(global.currLevel-4)*8) return oGreenFrog;
    return oFrog;
  }
  if (totdeath789 < 32) {
    if (rand(0, 100) >= 85-(global.currLevel-4)*8) return oGreenFrog;
    return oFrog;
  }
  if (totdeath789 < 42) {
    if (rand(0, 100) >= 75-(global.currLevel-4)*8) return oGreenFrog;
    return oFrog;
  }
  if (totdeath789 < 52) {
    if (rand(0, 100) >= 65-(global.currLevel-4)*8) return oGreenFrog;
    return oFrog;
  }
  if (totdeath789 < 62) {
    if (rand(0, 100) >= 55-(global.currLevel-4)*8) return oGreenFrog;
    return oFrog;
  }
  if (rand(0, 100) >= 50-(global.currLevel-4)*8) return oGreenFrog;
}
return oFrog;
