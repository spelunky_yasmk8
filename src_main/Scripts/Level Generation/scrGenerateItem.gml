// scrGenerateItem(x, y, setType)
//
// Generate an item at (x, y).
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/
var obj;
var objx, objy;
obj = 0;
objx = argument0;
objy = argument1;

if (argument2 == 0) {
  // Crate Set
       if (rand(1, 500) == 1) obj = instance_create(objx, objy, oJetpack);
  else if (rand(1, 200) == 1) obj = instance_create(objx, objy, oCapePickup);
  else if (rand(1, 100) == 1) obj = instance_create(objx, objy, oShotgun);
  else if (rand(1, 100) == 1 and !global.isTunnelMan) obj = instance_create(objx, objy, oMattock);
  else if (rand(1, 100) == 1) obj = instance_create(objx, objy, oTeleporter);
  else if (rand(1, 90) == 1) obj = instance_create(objx, objy, oGloves);
  else if (rand(1, 90) == 1) obj = instance_create(objx, objy, oSpectacles);
  else if (rand(1, 80) == 1) obj = instance_create(objx, objy, oWebCannon);
  else if (rand(1, 80) == 1) obj = instance_create(objx, objy, oPistol);
  else if (rand(1, 80) == 1) obj = instance_create(objx, objy, oMitt);
  else if (rand(1, 60) == 1) obj = instance_create(objx, objy, oPaste);
  else if (rand(1, 60) == 1) obj = instance_create(objx, objy, oSpringShoes);
  else if (rand(1, 60) == 1) obj = instance_create(objx, objy, oSpikeShoes);
  else if (rand(1, 60) == 1) obj = instance_create(objx, objy, oMachete);
  else if (rand(1, 40) == 1) obj = instance_create(objx, objy, oBombBox);
  else if (rand(1, 40) == 1) obj = instance_create(objx, objy, oBow);
  else if (rand(1, 20) == 1) obj = instance_create(objx, objy, oCompass);
  else if (rand(1, 10) == 1) obj = instance_create(objx, objy, oParaPickup);
  else obj = instance_create(objx, objy, oRopePile);
  obj.cost = 0;
  obj.forSale = false;
} else if (argument2 == 1) {
  // High End Set -- craps game prizes
       if (rand(1, 40) == 1) obj = instance_create(objx, objy, oJetpack);
  else if (rand(1, 25) == 1) obj = instance_create(objx, objy, oCapePickup);
  else if (rand(1, 20) == 1) obj = instance_create(objx, objy, oShotgun);
  else if (rand(1, 10) == 1) obj = instance_create(objx, objy, oGloves);
  else if (rand(1, 10) == 1) obj = instance_create(objx, objy, oTeleporter);
  else if (rand(1, 8) == 1 and !global.isTunnelMan) obj = instance_create(objx, objy, oMattock);
  else if (rand(1, 8) == 1) obj = instance_create(objx, objy, oPaste);
  else if (rand(1, 8) == 1) obj = instance_create(objx, objy, oSpringShoes);
  else if (rand(1, 8) == 1) obj = instance_create(objx, objy, oSpikeShoes);
  else if (rand(1, 8) == 1) obj = instance_create(objx, objy, oCompass);
  else if (rand(1, 8) == 1) obj = instance_create(objx, objy, oPistol);
  else if (rand(1, 8) == 1) obj = instance_create(objx, objy, oMachete);
  else obj = instance_create(objx, objy, oBombBox);
} else if (argument2 == 2) {
  // Underground Set (items hidden inside solid walls, see oBrick etc)
  switch (rand(0, 18)) {
    case 0: obj = instance_create(objx, objy-2, oJetpack); break;
    case 1: obj = instance_create(objx, objy, oCapePickup); break;
    case 2: obj = instance_create(objx, objy, oShotgun); break;
    case 3:
      if (global.isTunnelMan) obj = instance_create(objx, objy, oRopePile);
      else obj = instance_create(objx, objy, oMattock);
      break;
    case 4: obj = instance_create(objx, objy+3, oTeleporter); break;
    case 5: obj = instance_create(objx, objy-1, oGloves); break;
    case 6: obj = instance_create(objx, objy, oSpectacles); break;
    case 7: obj = instance_create(objx-2, objy, oWebCannon); break;
    case 8: obj = instance_create(objx, objy, oPistol); break;
    case 9: obj = instance_create(objx, objy-1, oMitt); break;
    case 10: obj = instance_create(objx, objy, oPaste); break;
    case 11: obj = instance_create(objx, objy, oSpringShoes); break;
    case 12: obj = instance_create(objx, objy, oSpikeShoes); break;
    case 13: obj = instance_create(objx, objy, oMachete); break;
    case 14: obj = instance_create(objx, objy-2, oBombBox); break;
    case 15: obj = instance_create(objx, objy, oBow); break;
    case 16: obj = instance_create(objx, objy, oCompass); break;
    case 17: obj = instance_create(objx, objy, oParaPickup); break;
    case 18: obj = instance_create(objx, objy, oRopePile); break;
  }
  obj.cost = 0;
  obj.forSale = false;
}

return obj;
