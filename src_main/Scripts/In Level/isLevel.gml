//
// isLevel()
//
// Checks if you are in a level.
//
return
  (isRoom("rTutorial") or
   isRoom("rTutorial2") or
   isRoom("rLoadLevel") or
   isRoom("rLevel") or
   isRoom("rLevel2") or
   isRoom("rLevel3") or
   isRoom("rOlmec"));
