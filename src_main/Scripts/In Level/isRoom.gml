//
// isRoom(name)
//
// Check current room with name (e.g. "rLevel")
//

if (room_get_name(room) == argument0) return true;
else if (room_get_name(room) == "rOlmec2" and argument0 == "rOlmec") return true;
else return false;
