//
// isInShop(x, y)
//
// Is this point (x, y) in a shop?
//
var tx, ty;

if (isLevel()) {
  tx = argument0;
  ty = argument1;
  //if scrGetRoomX(tx) == -1 or scrGetRoomY(ty) == -1 return false;
  if (global.roomPath[scrGetRoomX(tx), scrGetRoomY(ty)] == 4 or
      global.roomPath[scrGetRoomX(tx), scrGetRoomY(ty)] == 5 or
      collision_point(tx, ty, oShop, 0, 0)) return true;
}
return false;
