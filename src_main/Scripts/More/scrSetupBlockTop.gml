// Does the same thing as scrSetupWalls(), but for just one block.
// Called from oSolid.Destroy
// Adds decorations to walls, changes their sprites depending on placement.

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/

if (object_index == oBrick or object_index == oHardBlock)
{
    down = false;
    if (y >= argument0 or collision_point(x, y+16, oBrick, 0, 0) or collision_point(x, y+16, oLush, 0, 0)) { down = true; }

    sprite_index = sCaveUp;
    if (global.graphicsHigh)
    {
        if (rand(1,3) < 3) tile_add(bgCaveTop, 0, 0, 16, 16, x, y-16, 3);
        else tile_add(bgCaveTop, 16, 0, 16, 16, x, y-16, 3);
    }

    if (not down)
    {
        sprite_index = sCaveUp2;
    }
}
else if (object_index == oLush)
{

    down = false;
    if (y >= argument0 or collision_point(x, y+16, oLush, 0, 0)) { down = true; }

    sprite_index = sLushUpBare; //sLushUp;

    /*
    if (global.graphicsHigh)
    {
        if (rand(1,8) == 1) tile_add(bgCaveTop2, 32, 0, 16, 16, x, y-16, 3);
        else if (rand(1,3) < 3) tile_add(bgCaveTop2, 0, 0, 16, 16, x, y-16, 3);
        else tile_add(bgCaveTop2, 16, 0, 16, 16, x, y-16, 3);
    }
    */

    if (not down)
    {
        if (sprite_index == sLushUp or sprite_index == sLushUp2 or sprite_index == sLushUp3)
        {
            sprite_index = sLushUpBareTop;
        }
        else sprite_index = sLushUpBare2; //sLushUp2;
    }
}
else if (object_index == oDark)
{

    down = false;
    if (y >= argument0 or collision_point(x, y+16, oDark, 0, 0)) { down = true; }


    sprite_index = sDarkUp;
    if (global.graphicsHigh)
    {
        if (rand(1,3) < 3) tile_add(bgCaveTop3, 0, 0, 16, 16, x, y-16, 3);
        else tile_add(bgCaveTop3, 16, 0, 16, 16, x, y-16, 3);
    }

    if (not down)
    {
        sprite_index = sDarkUp2;
    }
}
else if (object_index == oIce)
{
    up = false;
    down = false;
    left = false;
    right = false;

    if (collision_point(x, y+16, oIce, 0, 0)) { down = true; }
    if (collision_point(x-16, y, oIce, 0, 0)) { left = true; }
    if (collision_point(x+16, y, oIce, 0, 0)) { right = true; }

    sprite_index = sIceUp;

    if (not down)
    {
        sprite_index = sIceUp2;
    }
    if (not left)
    {
        if (not down) sprite_index = sIceUDL;
        else sprite_index = sIceUL;
    }
    if (not right)
    {
        if (not down) sprite_index = sIceUDR;
    }
    if (not left and not right and down) sprite_index = sIceULR;
    if (not up and not down and not left and not right)
    {
        sprite_index = sIceBlock;
    }
}
else if (object_index == oTemple)
{
    up = false;
    down = false;
    left = false;
    right = false;

    if (y >= argument0 or collision_point(x, y+16, oTemple, 0, 0) or collision_point(x, y+16, oTempleFake, 0, 0)) { down = true; }
    if (collision_point(x-16, y, oTemple, 0, 0) or collision_point(x-16, y, oTempleFake, 0, 0)) { left = true; }
    if (collision_point(x+16, y, oTemple, 0, 0) or collision_point(x+16, y, oTempleFake, 0, 0)) { right = true; }

    if (global.cityOfGold)
    {
        if (not up)
        {
            sprite_index = sGTempleUp;
            if (global.graphicsHigh)
            {
                if (rand(1,4) == 1) tile_add(bgCaveTop4, 0, 0, 16, 16, x, y-16, 3);
                else if (rand(1,4) == 1) tile_add(bgCaveTop4, 16, 0, 16, 16, x, y-16, 3);
            }
            if (not left and not right)
            {
            if (not down) sprite_index = sGTempleUp6;
            else sprite_index = sGTempleUp5;
            }
            else if (not left)
            {
            if (not down) sprite_index = sGTempleUp7;
            else sprite_index = sGTempleUp3;
            }
            else if (not right)
            {
            if (not down) sprite_index = sGTempleUp8;
            else sprite_index = sGTempleUp4;
            }
            else if (left and right and not down)
            {
            sprite_index = sGTempleUp2;
            }
        }
        else if (not down)
        {
            sprite_index = sGTempleDown;
        }

    }
    else
    {
        if (not up)
        {
            sprite_index = sTempleUp;
            if (global.graphicsHigh)
            {
                if (rand(1,4) == 1) tile_add(bgCaveTop4, 0, 0, 16, 16, x, y-16, 3);
                else if (rand(1,4) == 1) tile_add(bgCaveTop4, 16, 0, 16, 16, x, y-16, 3);
            }
            if (not left and not right)
            {
            if (not down) sprite_index = sTempleUp6;
            else sprite_index = sTempleUp5;
            }
            else if (not left)
            {
            if (not down) sprite_index = sTempleUp7;
            else sprite_index = sTempleUp3;
            }
            else if (not right)
            {
            if (not down) sprite_index = sTempleUp8;
            else sprite_index = sTempleUp4;
            }
            else if (left and right and not down)
            {
            sprite_index = sTempleUp2;
            }
        }
        else if (not down)
        {
            sprite_index = sTempleDown;
        }
    }
}
