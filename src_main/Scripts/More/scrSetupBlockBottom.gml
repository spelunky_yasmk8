// Called by a solid when the block below it has been destroyed.
// Does the same thing as scrSetupWalls(), but for just one block.
// Called from oSolid.Destroy.
// Adds decorations to walls, changes their sprites depending on placement.

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/

if (object_index == oBrick or object_index == oHardBlock)
{
    up = false;
    if (y == 0 or collision_point(x, y-16, oBrick, 0, 0) or collision_point(x, y-16, oLush, 0, 0)) { up = true; }
    if (not up) sprite_index = sCaveUp2;
    else sprite_index = sBrickDown;
}
else if (object_index == oLush)
{

    up = false;
    if (y == 0 or collision_point(x, y-16, oLush, 0, 0)) { up = true; }


    if (not up)
    {
        if (sprite_index == sLushUp or sprite_index == sLushUp2 or sprite_index == sLushUp3)
        {
            sprite_index = sLushUpBareBottom;
        }
        else sprite_index = sLushUpBare2; //sLushUp2;
    }
    else sprite_index = sLushDownBare; //sLushDown;

    /*
    if (global.graphicsHigh)
    {
        if (rand(1,12) == 1) tile_add(bgCaveTop2, 48, 0, 16, 16, x, y+16, 3);
        else if (rand(1,12) == 1) tile_add(bgCaveTop2, 64, 0, 16, 16, x, y+16, 3);
    }
    */
}
else if (object_index == oDark)
{
    up = false;
    if (y == 0 or collision_point(x, y-16, oDark, 0, 0)) { up = true; }
    if (not up) sprite_index = sDarkUp2;
    else sprite_index = sDarkDown;
}
else if (object_index == oIce)
{
    up = false;
    down = false;
    left = false;
    right = false;

    if (collision_point(x, y-16, oIce, 0, 0)) { up = true; }
    if (collision_point(x-16, y, oIce, 0, 0)) { left = true; }
    if (collision_point(x+16, y, oIce, 0, 0)) { right = true; }

    if (not up) sprite_index = sIceUp2;
    else sprite_index = sIceDown;

    if (rand(1,20) == 1) instance_create(x, y+16, oIceBottom);

    if (not left)
    {
        if (not up) sprite_index = sIceUDL;
        else sprite_index = sIceDL;
    }
    if (not right)
    {
        if (not up) sprite_index = sIceUDR;
        else sprite_index = sIceDR;
    }

    if (not left and not right and up) sprite_index = sIceDLR;
    if (not up and not left and not right)
    {
        sprite_index = sIceBlock;
    }
}
else if (object_index == oTemple)
{
    up = false;
    down = false;
    left = false;
    right = false;

    if (y == 0 or collision_point(x, y-16, oTemple, 0, 0) or collision_point(x, y-16, oTempleFake, 0, 0)) { up = true; }
    if (collision_point(x-16, y, oTemple, 0, 0) or collision_point(x-16, y, oTempleFake, 0, 0)) { left = true; }
    if (collision_point(x+16, y, oTemple, 0, 0) or collision_point(x+16, y, oTempleFake, 0, 0)) { right = true; }

    if (global.cityOfGold)
    {
        if (not up)
        {
            sprite_index = sGTempleUp;

            if (not left and not right)
            {
                if (not down) sprite_index = sGTempleUp6;
                else sprite_index = sGTempleUp5;
            }
            else if (not left)
            {
                if (not down) sprite_index = sGTempleUp7;
                else sprite_index = sGTempleUp3;
            }
            else if (not right)
            {
                if (not down) sprite_index = sGTempleUp8;
                else sprite_index = sGTempleUp4;
            }
            else if (left and right and not down)
            {
                sprite_index = sGTempleUp2;
            }
        }
        else if (not down)
        {
            sprite_index = sGTempleDown;
        }

    }
    else
    {
        if (not up)
        {
            sprite_index = sTempleUp;

            if (not left and not right)
            {
            if (not down) sprite_index = sTempleUp6;
            else sprite_index = sTempleUp5;
            }
            else if (not left)
            {
            if (not down) sprite_index = sTempleUp7;
            else sprite_index = sTempleUp3;
            }
            else if (not right)
            {
            if (not down) sprite_index = sTempleUp8;
            else sprite_index = sTempleUp4;
            }
            else if (left and right and not down)
            {
            sprite_index = sTempleUp2;
            }
        }
        else if (not down)
        {
            sprite_index = sTempleDown;
        }
    }
}
