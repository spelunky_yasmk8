// called by Destroy event of blocks that can contain ore (oBrick, oLush, etc)
var i, gold;

switch (argument0) {
  case 1: //sGold
    for (i = 0; i < 3; i += 1) {
      gold = instance_create(x+8+rand(0,4)-rand(0,4), y+8+rand(0,4)-rand(0,4), oGoldChunk);
      gold.xVel = rand(0,3) - rand(0,3);
      gold.yVel = rand(2,4) * 1;
    }
    break;
  case 2: // sGoldBig
    for (i = 0; i < 3; i += 1) {
      gold = instance_create(x+8+rand(0,4)-rand(0,4), y+8+rand(0,4)-rand(0,4), oGoldChunk);
      gold.xVel = rand(0,3) - rand(0,3);
      gold.yVel = rand(2,4) * 1;
    }
    gold = instance_create(x+8+rand(0,4)-rand(0,4), y+8+rand(0,4)-rand(0,4), oGoldNugget);
    gold.xVel = rand(0,3) - rand(0,3);
    gold.yVel = rand(2,4) * 1;
    break;
}
