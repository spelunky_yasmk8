// Called by oSolid.Destroy.

if (object_index == oIce)
{
    up = false;
    down = false;
    left = false;
    right = false;

    if (collision_point(x, y-16, oIce, 0, 0)) { up = true; }
    if (collision_point(x, y+16, oIce, 0, 0)) { down = true; }
    if (collision_point(x+16, y, oIce, 0, 0)) { right = true; }

    if (not up)
    {
        sprite_index = sIceUp;
    }
    if (not down)
    {
        if (not up) sprite_index = sIceUp2;
        else sprite_index = sIceDown;
    }
    if (not left)
    {
        if (not up and not down) sprite_index = sIceUDL;
        else if (not up) sprite_index = sIceUL;
        else if (not down) sprite_index = sIceDL;
        else sprite_index = sIceLeft;
    }
    if (not right)
    {
        if (not up and not down) sprite_index = sIceUDR;
        else if (not up) sprite_index = sIceUR;
        else if (not down) sprite_index = sIceDR;
        else sprite_index = sIceRight;
    }
    if (not up and not left and not right and down) sprite_index = sIceULR;
    if (not down and not left and not right and up) sprite_index = sIceDLR;
    if (up and down and not left and not right) sprite_index = sIceLR;
    if (not up and not down and not left and not right)
    {
        sprite_index = sIceBlock;
    }
}
else if (object_index == oTemple)
{
    up = false;
    down = false;
    left = false;
    right = false;

    if (y == 0 or collision_point(x, y-16, oTemple, 0, 0) or collision_point(x, y-16, oTempleFake, 0, 0)) { up = true; }
    if (y >= argument0 or collision_point(x, y+16, oTemple, 0, 0) or collision_point(x, y+16, oTempleFake, 0, 0)) { down = true; }
    if (collision_point(x+16, y, oTemple, 0, 0) or collision_point(x+16, y, oTempleFake, 0, 0)) { right = true; }

    if (global.cityOfGold)
    {
        if (not up)
        {
            sprite_index = sGTempleUp;

            if (not left and not right)
            {
            if (not down) sprite_index = sGTempleUp6;
            else sprite_index = sGTempleUp5;
            }
            else if (not left)
            {
            if (not down) sprite_index = sGTempleUp7;
            else sprite_index = sGTempleUp3;
            }
            else if (not right)
            {
            if (not down) sprite_index = sGTempleUp8;
            else sprite_index = sGTempleUp4;
            }
            else if (left and right and not down)
            {
            sprite_index = sGTempleUp2;
            }
        }
        else if (not down)
        {
            sprite_index = sGTempleDown;
        }

    }
    else
    {
        if (not up)
        {
            sprite_index = sTempleUp;

            if (not left and not right)
            {
            if (not down) sprite_index = sTempleUp6;
            else sprite_index = sTempleUp5;
            }
            else if (not left)
            {
            if (not down) sprite_index = sTempleUp7;
            else sprite_index = sTempleUp3;
            }
            else if (not right)
            {
            if (not down) sprite_index = sTempleUp8;
            else sprite_index = sTempleUp4;
            }
            else if (left and right and not down)
            {
            sprite_index = sTempleUp2;
            }
        }
        else if (not down)
        {
            sprite_index = sTempleDown;
        }
    }
}
