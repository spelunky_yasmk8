// used in bizarre mode
// decorates room to reflect habitat of its owner
// called by scrEntityGenBizarre
// argument0, argument1 = x, y of top left corner of room (160x128)
// argument2 = enemy type

if (isInShop(argument0, argument1) or global.cityOfGold) exit;

var xpos, ypos, i, j, obj, newSolid, newEnemy;

if (argument2 == "Yeti King") {
  for (j = 0; j < 8; j += 1) {
    for (i = 1; i < 11; i += 1) {
      xpos = argument0 + (i-1)*16;
      ypos = argument1 + j*16;
      obj = instance_position(xpos+8, ypos+8, oDrawnSprite);
      newSolid = 0;
      newEnemy = 0;
      if (instance_exists(obj)) {
        // solids
        if (obj.object_index == oBrick or obj.object_index == oBlock or obj.object_index == oLush or obj.object_index == oTemple) {
          if ((ypos >= argument1 + 112 or ypos < argument1+16) and rand(1, 4) != 4) {
            newSolid = oDark;
          } else if (rand(1, 6) != 6) {
            if (rand(1, 50) == 1) newSolid = oFrozenCaveman; else newSolid = oIce;
          }
        } else if (obj.object_index == oPushBlock) {
          newSolid = oIceBlock;
        } else if (obj.object_index == oCaveman or obj.object_index == oHawkman or obj.object_index == oManTrap) {
          newEnemy = oYeti;
        }
        // create/destroy
        if (newSolid != 0) {
          if (!obj.invincible) {
            with (obj) {
              cleanDeath = true;
              scrRemoveBlockFrills();
              instance_destroy();
            }
            instance_create(xpos, ypos, newSolid);
            newEnemy = 0;
          }
        }
        if (newEnemy != 0) {
          instance_create(xpos, ypos, newEnemy);
          with (obj) instance_destroy();
        }
      }
    }
  }
} else if (argument2 == "Alien Boss") {
  for (j = 0; j < 8; j += 1)
  {
    for (i = 1; i < 11; i += 1) {
      xpos = argument0 + (i-1)*16;
      ypos = argument1 + j*16;
      obj = instance_position(xpos+8, ypos+8, oDrawnSprite);
      newSolid = 0;
      newEnemy = 0;
      if (instance_exists(obj)) {
        // solids
        if ((obj.object_index == oBrick or obj.object_index == oBlock or obj.object_index == oLush or obj.object_index == oTemple) and rand(1, 5) != 5) {
          if (rand(1, 6) == 1) newSolid = oIce; else newSolid = oDark;
        } else if (obj.object_index == oPushBlock) {
          newSolid = oIceBlock;
        } else if (obj.object_index == oCaveman or obj.object_index == oHawkman or obj.object_index == oManTrap) {
          newEnemy = oAlien;
        } else if (obj.object_index == oBat or obj.object_index == oSpiderHang) {
          newEnemy = oUFO;
        }
        // create/destroy
        if (newSolid != 0) {
          if (!obj.invincible) {
            with (obj) {
              cleanDeath = true;
              scrRemoveBlockFrills();
              instance_destroy();
            }
            instance_create(xpos, ypos, newSolid);
            newEnemy = 0;
          }
        }
        if (newEnemy != 0) {
          instance_create(xpos, ypos, newEnemy);
          with (obj) instance_destroy();
        }
      }
    }
  }
} else if (argument2 == "Tomb Lord") {
  for (j = 0; j < 8; j += 1) {
    for (i = 1; i < 11; i += 1) {
      xpos = argument0 + (i-1)*16;
      ypos = argument1 + j*16;
      obj = instance_position(xpos+8, ypos+8, oDrawnSprite);
      newSolid = 0;
      newEnemy = 0;
      if (instance_exists(obj)) {
        // solids
        if ((obj.object_index == oBrick or obj.object_index == oIce or obj.object_index == oLush or obj.object_index == oDark) and rand(1, 6) != 6) {
          newSolid = oTemple;
        } else if (obj.object_index == oIceBlock) {
          newSolid = oPushBlock;
        } else if (obj.object_index == oManTrap or obj.object_index == oYeti) {
          newEnemy = oSkeleton;
        } else if (obj.object_index == oUFO) {
          newEnemy = choose(oBat, oSpiderHang);
        }
        // create/destroy
        if (newSolid != 0) {
          if (!obj.invincible) {
            with (obj) {
              cleanDeath = true;
              scrRemoveBlockFrills();
              instance_destroy();
            }
            instance_create(xpos, ypos, newSolid);
            newEnemy = 0;
          }
        }
        if (newEnemy != 0) {
          instance_create(xpos, ypos, newEnemy);
          with (obj) instance_destroy();
        }
      }
    }
  }
} else if (argument2 == "Vampire") {
  for (j = 0; j < 8; j += 1) {
    for (i = 1; i < 11; i += 1) {
      xpos = argument0 + (i-1)*16;
      ypos = argument1 + j*16;
      obj = instance_position(xpos+8, ypos+8, oDrawnSprite);
      newSolid = 0;
      newEnemy = 0;
      if (instance_exists(obj)) {
        // solids
        if ((obj.object_index == oBrick or obj.object_index == oLush or
             obj.object_index == oDark or obj.object_index == oIce or obj.object_index == oTemple) and
             !collision_rectangle(xpos-16, ypos-32, xpos+31, ypos-1, oSolid, 0, 0) and
             !collision_rectangle(xpos, ypos-16, xpos+15, ypos-1, oEnemy, 0, 0) and
             !collision_point(xpos+8, ypos-8, oExit, 0, 0) and
             rand(1, 2) == 1)
        {
          instance_create(xpos, ypos-16, oGrave);
          if (not collision_point(xpos+8, ypos+8, oTreasure, 0, 0)) {
                 if (rand(1, 2) == 1) instance_create(x+8, y+8, oGoldNugget);
            else if (rand(1, 4) == 1) instance_create(x+8, y+8, oSapphireBig);
            else if (rand(1, 6) == 1) instance_create(x+8, y+8, oEmeraldBig);
            else if (rand(1, 8) == 1) instance_create(x+8, y+8, oRubyBig);
          }
          if (obj.object_index == oIce) newSolid = oDark;
        }
             if ((obj.object_index == oTemple or obj.object_index == oBrick) and rand(1, 2) == 1) newSolid = oLush;
        else if (obj.object_index == oManTrap or obj.object_index == oYeti) newEnemy = choose(oSkeleton, oZombie);
        else if (obj.object_index == oUFO) newEnemy = choose(oBat, oSpiderHang);
        // create/destroy
        if (newSolid != 0) {
          if (!obj.invincible) {
            with (obj) {
              cleanDeath = true;
              scrRemoveBlockFrills();
              instance_destroy();
            }
            instance_create(xpos, ypos, newSolid);
            newEnemy = 0;
          }
        }
        if (newEnemy != 0) {
          instance_create(xpos, ypos, newEnemy);
          with (obj) instance_destroy();
        }
      }
    }
  }
} else if (argument2 == "Magma Man") {
  for (j = 0; j < 8; j += 1) {
    for (i = 1; i < 11; i += 1) {
      xpos = argument0 + (i-1)*16;
      ypos = argument1 + j*16;
      obj = instance_position(xpos+8, ypos+8, oDrawnSprite);
      newSolid = 0;
      newEnemy = 0;
      if (instance_exists(obj)) {
        // solids
        if (obj.object_index == oLush) {
          if (collision_point(x, y-16, oLava, 0, 0)) newSolid = oBrick; else if (rand(1, 5) != 5) newSolid = oBrick;
        }
        else if (obj.object_index == oIce) newSolid = oDark;
        else if (obj.object_index == oIceBlock) newSolid = oPushBlock;
        else if (obj.object_index == oWaterSwim and !global.lake) newSolid = oLava;
        else if (obj.object_index == oYeti) newEnemy = oSkeleton;
        else if (obj.object_index == oUFO) newEnemy = choose(oBat, oSpiderHang);
        // create/destroy
        if (newSolid != 0) {
          with (obj) {
            cleanDeath = true;
            scrRemoveBlockFrills();
            instance_destroy();
          }
          instance_create(xpos, ypos, newSolid);
          newEnemy = 0;
        }
        if (newEnemy != 0) {
          instance_create(xpos, ypos, newEnemy);
          with (obj) instance_destroy();
        }
      } else {
        if (instance_position(xpos-16, ypos, oSolid) and
            instance_position(xpos-16, ypos+16, oSolid) and
            instance_position(xpos, ypos+16, oSolid) and
            instance_position(xpos+16, ypos+16, oSolid) and
            instance_position(xpos+16, ypos, oSolid) and
            !collision_point(xpos, ypos-16, oSolid, 0, 0))
        {
          instance_create(xpos, ypos, oLava);
        }
      }
    }
  }
}
