// used in bizarre mode
// randomizes blocks in transition rooms
// called by oTransition
var obj, newObj, madeEnemy;
obj = 0;
newObj = 0;
madeEnemy = false;

if (argument0) {
  // user defines block type
  with (oSolid) {
    obj = argument0;
    if (obj != object_index) {
      newObj = instance_create(x, y, obj);
      if (instance_exists(newObj)) {
        cleanDeath = true;
        instance_destroy();
      }
    }
  }
} else {
  // random blocks/details
  with (oSolid) {
    if (y != 192) {
      if (rand(1, 20) == 1) {
        switch (rand(1, 6)) {
          case 1: obj = oBrick; break;
          case 2: obj = oLush; break;
          case 3: obj = oDark; break;
          case 4: obj = oIce; break;
          case 5: obj = oTemple; break;
          case 6: obj = oBlock; break;
        }
        if (obj != object_index) {
          newObj = instance_create(x, y, obj);
          if (obj == oIce) newObj.sprite_index = sIceBlock;
          if (instance_exists(newObj)) {
            cleanDeath = true;
            instance_destroy();
          }
        }
      } else if (!madeEnemy and (y == 16 or y == 208) and (x > 0 and x < 304)) {
        if (rand(1, 40) == 1) {
          madeEnemy = true;
          switch (rand(1, 11)) {
            case 1: newObj = instance_create(x, y, oWeb); break;
            case 2:
              if (global.woodSpikes) newObj = instance_create(x, y, oSpikesWood); else newObj = instance_create(x, y, scrGenSpikeType());
              break;
            case 3: newObj = instance_create(x, y, oDrawnSprite); newObj.sprite_index = sFrogLeft; newObj.image_speed = 0.3; break;
            case 4: newObj = instance_create(x, y, oDrawnSprite); newObj.sprite_index = sLavaTop; newObj.image_speed = 0.4; break;
            case 5: newObj = instance_create(x, y, oDrawnSprite); newObj.sprite_index = sSnakeWalkL; newObj.image_speed = 0.3; break;
            case 6: newObj = instance_create(x, y, oDrawnSprite); newObj.sprite_index = sSkeletonCreateL; newObj.image_speed = 0; break;
            case 7: newObj = instance_create(x, y, oDrawnSprite); newObj.sprite_index = sVampireBatHang; break;
            case 8: newObj = instance_create(x, y, oDrawnSprite); newObj.sprite_index = sCavemanDeadL; break;
            case 9: newObj = instance_create(x, y, oDrawnSprite); newObj.sprite_index = sFireFrogLeft; newObj.image_speed = 0.3; break;
            case 10: newObj = instance_create(x, y, oDrawnSprite); newObj.sprite_index = sZombieLeft; newObj.image_speed = 0.3; break;
            case 11: newObj = instance_create(x+8, y+8, oDrawnSprite); newObj.sprite_index = sChest; break;
          }
          if (instance_exists(newObj)) newObj.depth = 9001;
          cleanDeath = true;
          instance_destroy();
        }
      }
    }
  }
}
