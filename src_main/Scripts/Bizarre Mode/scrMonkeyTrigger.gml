// called from oMonkey
// monkey triggers trap by throwing idol
var trap, dist;

if (other.trigger and not isRoom("rLoadLevel")) {
  if (global.levelType == 0) {
    trap = instance_nearest(x, y-64, oGiantTikiHead);
    with (trap) { alarm[0] = 100; monkey = true; }
    scrShake(100);
    other.trigger = false;
  } else if (global.levelType == 1) {
    /*
    if (global.cemetary and not global.ghostExists) {
      if (oPlayer1.x > room_width / 2) instance_create(view_xview[0]+view_wview[0]+8, view_yview[0]+floor(view_hview[0] / 2), oGhost);
      else instance_create(view_xview[0]-32,  view_yview[0]+floor(view_hview[0] / 2), oGhost);
      global.ghostExists = true;
    }
    */
    with (oTrapBlock) {
      dist = distance_to_object(other); // monkey
      if (dist < 90) dying = true;
    }
  } else if (global.levelType == 3) {
    if (instance_exists(oCeilingTrap)) {
      with (oCeilingTrap) {
        status = 1;
        yVel = 0.5;
      }
      scrShake(20);
      trap = instance_nearest(x-64, y-64, oDoor);
      if (trap) {
        trap.status = 1;
        trap.yVel = 1;
      }
      trap = instance_nearest(x+64, y-64, oDoor);
      if (trap) {
        trap.status = 1;
        trap.yVel = 1;
      }
    } else {
      with (oTrapBlock) {
        dist = distance_to_object(other); // monkey
        if (dist < 90) instance_destroy();
        playSound(global.sndThump);
        scrShake(10);
      }
    }
    other.trigger = false;
  }
} else if (other.trigger and isRoom("rLoadLevel")) {
  // from TLS, for Editor Plus compatibility
  trap = instance_nearest(x, y-64, oGiantTikiHead);
  if (instance_exists(trap)) {
    with (trap) {
      if (!sprung) {
        alarm[0] = 100;
        sprung = true;
        scrShake(100);
      }
    }
  }
  /*
  if (other.cursed and not global.ghostExists) {
    if (oPlayer1.x > room_width / 2) instance_create(view_xview[0]+view_wview[0]+8, view_yview[0]+floor(view_hview[0] / 2), oGhost);
    else instance_create(view_xview[0]-32,  view_yview[0]+floor(view_hview[0] / 2), oGhost);
    global.ghostExists = true;
    other.cursed = false;
  }
  */

  with (oTrapBlock) {
    if (distance_to_object(other) < 96) dying = true;
  }

  with (oCeilingTrap) {
    if (!sprung) {
      status = 1;
      yVel = 0.5;
      sprung = true;
    }
  }

  trap = instance_nearest(x-64, y-64, oDoor);
  if (instance_exists(trap)) {
    if (!trap.sprung) {
      trap.status = 1;
      trap.yVel = 1;
      trap.sprung = true;
      scrShake(20);
      playSound(global.sndClick);
    }
  }
  trap = instance_nearest(x+64, y-64, oDoor);
  if (instance_exists(trap)) {
    if (!trap.sprung) {
      trap.status = 1;
      trap.yVel = 1;
      trap.sprung = true;
      scrShake(20);
      playSound(global.sndClick);
    }
  }

  other.cursed = false;
  other.trigger = false;
}
