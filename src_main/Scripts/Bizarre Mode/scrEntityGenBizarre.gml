//
// scrEntityGen()
//
// Generates enemies, traps, and treasure.
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/
var n, obj, sol;

// Note: depth of trees, statues is 9005

global.LockedChest = false;
global.Key = false;
global.lockedChestChance = 8;

global.giantSpider = false;
global.genGiantSpider = false;
if (global.levelType == 0)
{
    if (rand(1, 4) == 1) global.genGiantSpider = true;
}
else
{
    if (rand(1, 10) == 1) global.genGiantSpider = true;
}

global.genYetiKing = false;
if (!global.yetiLair)
{
    if (global.levelType == 2)
    {
        if (rand(1, 4) == 1) global.genYetiKing = true;
    }
    else
    {
        if (rand(1, 10) == 1) global.genYetiKing = true;
    }
}

global.genAlienBoss = false;
if (!global.alienCraft)
{
    if (global.levelType == 2)
    {
        if (rand(1, 6) == 1) global.genAlienBoss = true;
    }
    else
    {
        if (rand(1, 10) == 1) global.genAlienBoss = true;
    }
}

global.TombLord = false;
global.totalTombLords = 0;
global.gaveSceptre = false;
global.genTombLord = false;
if (global.currLevel == 13) global.genTombLord = true;
else if (global.levelType == 3)
{
    if (rand(1, 4) == 1) global.genTombLord = true;
}
else
{
    if (rand(1, 10) == 1) global.genTombLord = true;
}

instance_create(0, 0, oBizarre); // holds entity probability values

global.genGoldEntrance = false;
if (global.currLevel == 14) global.genGoldEntrance = true;
global.madeGoldEntrance = false;

if (global.cemetary)
{
    global.ashGrave = false;
    with (oLush)
    {
        // generate graves
        if (not collision_point(x, y-16, oSolid, 0, 0) and
            not collision_point(x, y-16, oEntrance, 0, 0) and
            not collision_point(x, y-16, oExit, 0, 0) and
            rand(1, 20) == 1 and
            x != 160 and x != 176 and x != 320 and x != 336 and x != 480 and x != 496)
        {
            obj = instance_create(x, y-16, oGrave);
            if (not global.ashGrave and rand(1, 30) == 1)
            {
                obj.sprite_index = sGraveAsh;
                obj = instance_create(x+8, y+8, oShotgun);
                obj.cost = 0;
                obj.forSale = false;
                obj.ashShotgun = true;
                global.ashGrave = true;
            }
            else if (not collision_point(x+8, y+8, oTreasure, 0, 0))
            {
                     if (rand(1, 2) == 1) instance_create(x+8, y+8, oGoldNugget);
                else if (rand(1, 4) == 1) instance_create(x+8, y+8, oSapphireBig);
                else if (rand(1, 6) == 1) instance_create(x+8, y+8, oEmeraldBig);
                else if (rand(1, 8) == 1) instance_create(x+8, y+8, oRubyBig);
            }
        }
    }
}

with (oSolid)
{
    if (not isInShop(x, y) and y > 16)
    {
        if (type != "Tree" and type != "Altar" and y != 0 and
                 not collision_rectangle(x, y-32, x+15, y-1, oSolid, false, true) and
                 not collision_rectangle(x, y-16, x+15, y-1, oEnemy, 0, 0) and
                 not collision_point(x, y-16, oLava, 0, 0) and
                 (not collision_point(x-16, y, oSolid, 0, 0) or not collision_point(x+16, y, oSolid, 0, 0)) and
                 collision_point(x, y+16, oSolid, 0, 0) and
                 not collision_point(x, y, oXMarket, 0, 0) and
                 point_distance(x, y, oEntrance.x, oEntrance.y) > 64)
        {
            if (global.levelType == 1 and global.darkLevel and not collision_point(x, y-32, oWater, 0, 0) and rand(1, 20) == 1)
            {
                instance_create(x, y-32, oTikiTorch);
            }
            else if (rand(1, ceil(oBizarre.spearTrap/global.trapMult)) == 1 and
                     x != 160 and x != 176 and x != 320 and x != 336 and x != 480 and x != 496 and
                     !invincible and type != "Xoc Block")
            {
                sol = 0;
                if (collision_point(x, y-16, oSolid, 0, 0))
                {
                    sol = instance_nearest(x, y-16, oSolid);
                }
                if (global.levelType != 1 and global.levelType != 3)
                {
                    if (!collision_rectangle(x-16, y-32, x+31, y-1, oSolid, 0, 0))
                    {
                        if (instance_exists(sol)) with (sol) { cleanDeath = true; instance_destroy(); }
                        instance_create(x, y, oSpearTrapBottom);
                        if (global.darkLevel) instance_create(x, y-16, oSpearTrapLit);
                        else instance_create(x, y-16, oSpearTrapTop);
                        cleanDeath = true;
                        instance_destroy();
                    }
                }
                else
                {
                    if (instance_exists(sol)) with (sol) { cleanDeath = true; instance_destroy(); }
                    instance_create(x, y, oSpearTrapBottom);
                    if (global.darkLevel) instance_create(x, y-16, oSpearTrapLit);
                    else instance_create(x, y-16, oSpearTrapTop);
                    cleanDeath = true;
                    instance_destroy();
                }

            }
        }

        if (y > 32 and not collision_point(x, y-16, oSolid, 0, 0) and global.genGoldEntrance and not global.madeGoldEntrance)
        {
            if (rand(1, global.goldChance) == 1)
            {
                instance_create(x, y-16, oGoldDoor);
                invincible = true;
                global.madeGoldEntrance = true;
            }
            else global.goldChance -= 1;
        }

        if (type != "Altar")
        {
            if (global.cemetary) scrTreasureGenBizarre(10);
            else scrTreasureGenBizarre();
        }

        // enemies
        if (scrGetRoomX(x) != global.startRoomX or scrGetRoomY(y-16) != global.startRoomY)
        {
            if (y < room_height - 64 and
                not collision_point(x, y+16, oSolid, 0, 0) and not collision_point(x, y+32, oSolid, 0, 0) and
                not collision_point(x, y+16, oWater, 0, 0) and not collision_point(x, y+32, oWater, 0, 0) and
                not collision_point(x, y+16, oEnemy, 0, 0))
            {
                if (global.genGiantSpider and
                    not global.giantSpider and
                    y < room_height - 64 and
                    not collision_point(x+16, y+16, oSolid, 0, 0) and
                    not collision_point(x+16, y+32, oSolid, 0, 0) and
                    rand(1, ceil(oBizarre.giantSpider/global.eMult)) == 1)
                {
                    obj = instance_create(x, y+16, oGiantSpiderHang);
                    if (global.hasStickyBombs and rand(1, 2) == 1) { obj.pasteless = true; obj.carries = "Crate"; }
                    if (global.enemyMult >= 2 and rand(1, ceil(200/global.eMult)) == 1) global.giantSpider = false;
                    else global.giantSpider = true;
                }
                else if (global.levelType == 0 and global.darkLevel and rand(1, 60) == 1) instance_create(x, y+16, oLamp);
                else if (global.darkLevel and rand(1, 40) == 1) instance_create(x, y+16, oScarab);
                else if (rand(1, ceil(oBizarre.floater/global.eMult)) == 1)
                {
                    obj = instance_create(x, y+16, oFloater);
                    switch (rand(1, 6)) {
                        case 4:
                        case 5: obj.carries = "Big Sapphire"; break;
                        case 6: obj.carries = "Big Ruby"; break;
                        default: obj.carries = "Big Emerald";
                    }
                    if (rand(1, 20-global.currLevel) == 1) { obj.explosive = true; obj.sprite_index = sFloaterEvil; }
                }
                else if (rand(1, ceil(oBizarre.bat/global.eMult)) == 1) instance_create(x, y+16, oBat);
                else if (rand(1, ceil(oBizarre.spider/global.eMult)) == 1 and !collision_point(x+8, y+24, oLadder, 0, 0)) instance_create(x, y+16, oSpiderHang);
                else if (rand(1, ceil(oBizarre.greenSpider/global.eMult)) == 1 and !collision_point(x+8, y+24, oLadder, 0, 0)) instance_create(x, y+16, oGreenSpiderHang);
                else if (y > 16 and y < room_height - 48 and not collision_point(x, y-16, oSolid, 0, 0))
                {
                    if (rand(1, ceil(oBizarre.ufo/global.eMult)) == 1) instance_create(x, y-16, oUFO);
                }
            }

            if (y > 16 and y < room_height - 32 and
                not collision_point(x, y-16, oSolid, 0, 0) and
                not collision_point(x+8, y-8, oEnemy, 0, 0) and
                not collision_point(x+8, y-1, oSpikes, 0, 0) and
                not collision_point(x+8, y-1, oSpikesWood, 0, 0) and
                point_distance(x, y, oEntrance.x, oEntrance.y) > 64)
            {
                if ((rand(1, ceil(oBizarre.springTrap/global.trapMult)) == 1 and
                    not collision_rectangle(x, y-64, x+15, y-1, oSolid, 0, 0) and
                    distance_to_object(oExit) > 64) and
                    (sprite_index == sDark or sprite_index == sLush or sprite_index == sBrick or
                    sprite_index == sTemple or sprite_index == sGTemple))
                {
                  if (global.levelType == 2) instance_create(x, y-16, oSpringTrap); else instance_create(x, y-16, oSpringTrapTan);
                }
            }


            if (y > 16 and not collision_point(x, y-16, oSolid, 0, 0) and
                not collision_point(x, y, oEnemy, 0, 0) and
                not collision_point(x, y, oSpikes, 0, 0) and
                not collision_point(x, y, oSpikesWood, 0, 0))
            {

                if (not collision_point(x, y-16, oWater, 0, 0))
                {
                    if (global.cemetary)
                    {
                        if (rand(1, ceil(30/global.eMult)) == 1) instance_create(x, y-16, oZombie);
                        else if (rand(1, ceil(160/global.eMult)) == 1) instance_create(x, y-16, oVampire);
                    }

                    if (global.blackMarket and (y mod 128 == 0)) n = 0; // to prevent mantraps from spawning near shopkeepers in black market
                    else n = 1;

                    if (rand(1, ceil(oBizarre.snake/global.eMult)) == 1) instance_create(x, y-16, oSnake);
                    else if (rand(1, ceil(oBizarre.cobra/global.eMult)) == 1) instance_create(x, y-16, oCobra);
                    else if (rand(1, ceil(oBizarre.caveman/global.eMult)) == 1) instance_create(x, y-16, oCaveman);
                    else if (rand(1, ceil(oBizarre.manTrap/global.eMult)) == n) instance_create(x, y-16, oManTrap);
                    else if (rand(1, ceil(oBizarre.hawkman/global.eMult)) == 1) instance_create(x, y-16, oHawkman);
                    else if (rand(1, ceil(oBizarre.frog/global.eMult)) == 1) instance_create(x, y-16, oFrog);
                    else if (rand(1, ceil(oBizarre.blob/global.eMult)) == 1) instance_create(x, y-16, oBlob);
                    else if (rand(1, ceil(oBizarre.greenFrog/global.eMult)) == 1) instance_create(x, y-16, oGreenFrog);
                    else if (rand(1, ceil(oBizarre.yeti/global.eMult)) == 1) instance_create(x, y-16, oYeti);
                    else if (rand(1, ceil(oBizarre.fireFrog/global.eMult)) == 1) instance_create(x, y-16, oFireFrog);
                    else if (rand(1, ceil(oBizarre.zombie/global.eMult)) == 1) instance_create(x, y-16, oZombie);
                    else if (rand(1, ceil(oBizarre.vampire/global.eMult)) == 1)
                    {
                        obj = instance_create(x, y-16, oVampire);
                        if (global.hasCape and rand(1, 2) == 1)
                        {
                            obj.capeless = true;
                            if (rand(1, 100) == 1) obj.carries = "Crystal Skull"; else obj.carries = "Crate";
                        }
                        if (!global.cemetary) scrDecorate(16+scrGetRoomX(x+8)*160, 16+scrGetRoomY(y-8)*128, "Vampire");
                    }
                    else if (rand(1, ceil(oBizarre.alien/global.eMult)) == 1) instance_create(x, y-16, oAlien);
                    else if (rand(1, ceil(oBizarre.monkey/global.eMult)) == 1 and !global.blackMarket)
                    {
                        obj = instance_create(x, y-16, oMonkey);
                        obj.status = 0; // IDLE
                    }
                    /*
                    else if (rand(1, ceil(oBizarre.outlaw/global.eMult)) == 1)
                    {
                        obj = instance_create(x, y-16, oShopkeeper)
                        obj.status = 4 // PATROL
                        obj.outlaw = true // no thief/murderer status for player when killed
                        obj.hp = 10
                        if rand(1, 4) == 1 obj.hasGun = true
                        else obj.hasGun = false
                    }
                    */
                    else if (rand(1, ceil(oBizarre.magmaMan/global.eMult)) == 1)
                    {
                        obj = instance_create(x, y-16, oMagmaMan);
                        obj.persist = true;
                        obj.hp = 20;
                        if (global.levelType != 3) scrDecorate(16+scrGetRoomX(x+8)*160, 16+scrGetRoomY(y-8)*128, "Magma Man");
                    }
                    else if (rand(1, ceil(oBizarre.man/global.eMult)) == 1)
                    {
                        instance_create(x, y-16, choose(oManLink, oManCrown, oManWizard, oManHelmet, oManBomber, oManBlob, oManSanta));
                    }
                }
                else if (rand(1, ceil(oBizarre.frog/global.eMult)) == 1) instance_create(x, y-16, oFrog);
                else if (rand(1, ceil(oBizarre.greenFrog/global.eMult)) == 1) instance_create(x, y-16, oGreenFrog);
            }

            if (y > 16 and not collision_point(x, y-16, oSolid, 0, 0))
            {
                if (global.genTombLord and
                    not global.TombLord and
                    not collision_rectangle(x, y-32, x+31, y-1, oSolid, 0, 0) and
                    not collision_point(x, y-32, oWater, 0, 0) and
                    rand(1, ceil(oBizarre.tombLord/global.eMult)) == 1)
                {
                    obj = instance_create(x, y-32, oTombLord);
                    global.totalTombLords += 1;

                    if (global.currLevel == 13)
                    {
                        if (!global.gaveSceptre)
                        {
                            if (rand(1, global.enemyMult) == 1)
                            {
                                obj.hasSceptre = true;
                                global.gaveSceptre = true;
                            }
                        }

                        if (rand(1, global.enemyMult) == 1 or global.totalTombLords >= global.enemyMult) global.TombLord = true; // don't make any more tomb lords
                        if (global.TombLord == true and !global.gaveSceptre)
                        {
                            obj.hasSceptre = true;
                            global.gaveSceptre = true;
                        }
                    }
                    else if (rand(1, global.enemyMult) == 1 or global.totalTombLords >= global.enemyMult) global.TombLord = true;

                    if (!obj.hasSceptre) // give something else to carry
                    {
                        if (global.currLevel != 13 and rand(1, 400) == 1)
                        {
                            obj.carries = "Sceptre";
                            obj.hasSceptre = true;
                        }
                        if (not obj.hasSceptre)
                        {
                            if (rand(1, 40) == 1) obj.carries = "Diamond";
                            else if (rand(1, 20) == 1) obj.carries = "Big Ruby";
                            else if (rand(1, 18) == 1) obj.carries = "Big Sapphire";
                            else if (rand(1, 16) == 1) obj.carries = "Big Emerald";
                            else if (rand(1, 14) == 1) obj.carries = "Gold Bars";
                            else if (rand(1, 12) == 1) obj.carries = "Gold Bar";
                            else obj.carries = "Crate";
                        }
                    }
                    if (global.levelType != 3) scrDecorate(16+scrGetRoomX(x+8)*160, 16+scrGetRoomY(y-16)*128, "Tomb Lord");

                }
                else if (global.genYetiKing and
                    not collision_rectangle(x-16, y-32, x+31, y-1, oSolid, 0, 0) and
                    not collision_point(x, y-32, oWater, 0, 0) and
                    rand(1, ceil(oBizarre.yetiKing/global.eMult)) == 1)
                {
                    obj = instance_create(x, y-32, oYetiKing);
                    if (rand(1, 3) == 1)
                    {
                        obj.carries = "Crate";
                        obj.ropeless = true;
                    }
                    if (global.levelType != 2) scrDecorate(16+scrGetRoomX(x+16)*160, 16+scrGetRoomY(y-16)*128, "Yeti King");
                    if (rand(1, ceil(100/global.eMult)) == 1) global.genYetiKing = true; else global.genYetiKing = false;
                }
                else if (global.genAlienBoss and
                    not collision_rectangle(x-16, y-32, x+31, y-1, oSolid, 0, 0) and
                    collision_point(x+16, y, oSolid, 0, 0) and
                    not collision_point(x, y-32, oWater, 0, 0) and
                    rand(1, ceil(oBizarre.alienBoss/global.eMult)) == 1)
                {
                    obj = instance_create(x, y-32, oAlienBoss);
                    if (x < 320) obj.facing = 1; // right
                    if (rand(1, 300) == 1) obj.carries = "Jetpack";
                    else if (rand(1, 2) == 1) obj.carries = "Crate";
                    else obj.carries = "";
                    if (global.levelType != 2) scrDecorate(16+scrGetRoomX(x+16)*160, 16+scrGetRoomY(y-16)*128, "Alien Boss");
                    if (rand(1, ceil(200/global.eMult)) == 1) global.genAlienBoss = true; else global.genAlienBoss = false;
                }
                else if (rand(1, ceil(oBizarre.smashTrap/global.eMult)) == 1 and !collision_point(x+8, y-8, oEnemy, 0, 0))
                {
                    if (global.darkLevel) instance_create(x, y-16, oSmashTrapLit);
                    else instance_create(x, y-16, oSmashTrap);
                }
            }

            if (y >=16 and y < room_height-64 and
                !collision_rectangle(x, y+16, x+15, y+64, oSolid, 0, 0) and
                !collision_rectangle(x, y+16, x+15, y+64, oLadder, 0, 0) and
                !collision_rectangle(x-16, y+32, x+31, y+47, oSolid, 0, 0) and
                !collision_point(x, y+16, oWater, 0, 0))
            {
                if (rand(1, ceil(oBizarre.thwompTrap/global.trapMult)) == 1 and
                    !collision_point(x+8, y-8, oEnemy, 0, 0))
                {
                    if (global.darkLevel) instance_create(x, y+16, oThwompTrapLit); else instance_create(x, y+16, oThwompTrap);
                }
            }
        }
    }
}

// force generate tomb lord
if (global.currLevel == 13 and global.genTombLord and !global.TombLord)
{
    if (instance_exists(oExit))
    {
        obj = instance_create(oExit.x, oExit.y-16, oTombLord);
        global.totalTombLords += 1;
        obj.hasSceptre = true;
        global.gaveSceptre = true;
    }
}

with (oBlock)
{
    if (not isInShop(x, y))
    {
        n = point_distance(x, y, oEntrance.x, oEntrance.y);
        if (rand(1, ceil(oBizarre.arrowTrap/global.trapMult)) == 1 and not
            (y == oEntrance.y and n < 144) and
            n > 48)
        {
            if (collision_point(x+16, y, oSolid, 0, 0) and not
                collision_rectangle(x-32, y, x-1, y+15, oSolid, 0, 0))
            {
                if (global.darkLevel) instance_create(x, y, oArrowTrapLeftLit);
                else instance_create(x, y, oArrowTrapLeft);
                cleanDeath = true;
                instance_destroy();
            }
            else if (collision_point(x-16, y, oSolid, 0, 0) and not
                collision_rectangle(x+16, y, x+48, y+15, oSolid, 0, 0))
            {
                if (global.darkLevel) instance_create(x, y, oArrowTrapRightLit);
                else instance_create(x, y, oArrowTrapRight);
                cleanDeath = true;
                instance_destroy();
            }
        }
        else if (rand(1, 20) == 1 and collision_point(x, y+16, oSolid, 0, 0))
        {
            cleanDeath = true;
            if (rand(1, 50) == 1) instance_change(oFrozenCaveman, true); else instance_change(oIceBlock, true);
        }
    }
}

if (global.trapMult >= 2)
{
    with (oBrick)
    {
        if (not isInShop(x, y))
        {
            n = point_distance(x, y, oEntrance.x, oEntrance.y);
            if (rand(1, ceil(oBizarre.arrowTrap2/global.trapMult)) == 1 and
                x >= 16 and x < room_width -16 and not
                (y == oEntrance.y and n < 144) and
                n > 48)
            {
                if (collision_point(x+16, y, oSolid, 0, 0) and not
                    collision_rectangle(x-32, y, x-1, y+15, oSolid, 0, 0))
                {
                    if (global.darkLevel) instance_create(x, y, oArrowTrapLeftLit);
                    else instance_create(x, y, oArrowTrapLeft);
                    cleanDeath = true;
                    instance_destroy();
                }
                else if (collision_point(x-16, y, oSolid, 0, 0) and not
                    collision_rectangle(x+16, y, x+48, y+15, oSolid, 0, 0))
                {
                    if (global.darkLevel) instance_create(x, y, oArrowTrapRightLit);
                    else instance_create(x, y, oArrowTrapRight);
                    cleanDeath = true;
                    instance_destroy();
                }
            }
        }
    }

    with (oLush)
    {
        if (not isInShop(x, y))
        {
            n = point_distance(x, y, oEntrance.x, oEntrance.y);
            if (rand(1, ceil(oBizarre.arrowTrap2/global.trapMult)) == 1 and
                x >= 16 and x < room_width -16 and not
                (y == oEntrance.y and n < 144) and
                n > 48)
            {
                if (collision_point(x+16, y, oSolid, 0, 0) and not
                    collision_rectangle(x-32, y, x-1, y+15, oSolid, 0, 0))
                {
                    if (global.darkLevel) instance_create(x, y, oArrowTrapLeftLit);
                    else instance_create(x, y, oArrowTrapLeft);
                    cleanDeath = true;
                    instance_destroy();
                }
                else if (collision_point(x-16, y, oSolid, 0, 0) and not
                    collision_rectangle(x+16, y, x+48, y+15, oSolid, 0, 0))
                {
                    if (global.darkLevel) instance_create(x, y, oArrowTrapRightLit);
                    else instance_create(x, y, oArrowTrapRight);
                    cleanDeath = true;
                    instance_destroy();
                }
            }
        }
    }

    with (oTemple)
    {
        if (not isInShop(x, y))
        {
            n = point_distance(x, y, oEntrance.x, oEntrance.y);
            if (rand(1, ceil(oBizarre.arrowTrap2/global.trapMult)) == 1 and
                x >= 16 and x < room_width -16 and not
                (y == oEntrance.y and n < 144) and
                n > 48)
            {
                if (collision_point(x+16, y, oSolid, 0, 0) and not
                    collision_rectangle(x-32, y, x-1, y+15, oSolid, 0, 0))
                {
                    if (global.darkLevel) instance_create(x, y, oArrowTrapLeftLit);
                    else instance_create(x, y, oArrowTrapLeft);
                    cleanDeath = true;
                    instance_destroy();
                }
                else if (collision_point(x-16, y, oSolid, 0, 0) and not
                    collision_rectangle(x+16, y, x+48, y+15, oSolid, 0, 0))
                {
                    if (global.darkLevel) instance_create(x, y, oArrowTrapRightLit);
                    else instance_create(x, y, oArrowTrapRight);
                    cleanDeath = true;
                    instance_destroy();
                }
            }
        }
    }
}

with (oVine)
{
    if (rand(1, ceil(oBizarre.monkeyVine/global.eMult)) == 1) instance_create(x, y, oMonkey);
}

with (oWater)
{
    if (not collision_point(x, y, oSolid, 0, 0) and type != "Lava")
    {
        if (rand(1, ceil(oBizarre.fish/global.eMult)) == 1)
        {
            if (global.cemetary or rand(1, 10) == 1) instance_create(x+4, y+4, oDeadFish);
            else instance_create(x+4, y+4, oPiranha);
        }
    }
}

with (oLava)
{
    if (not collision_line(x+8, y, x+8, y-64, oSolid, 0, 0) and
        not collision_point(x, y-16, oLava, 0, 0))
    {
        if (rand(1, 20) == 1) instance_create(x, y, oLavaTosser);
    }
}

with (oPushBlock)
{
    if (rand(1, 20) == 1)
    {
        cleanDeath = true;
        if (rand(1, 50) == 50) instance_change(oFrozenCaveman, true); else instance_change(oIceBlock, true);
    }
}

with (oBizarre) instance_destroy();


// ************************************************************************
// now generate items that are area-specific

if (global.levelType == 0)
{
    // force generate chest
    if (global.genUdjatEye and not global.LockedChest)
    {
        with (oExit)
        {
            if (not collision_point(x-8, y, oSolid, 0, 0) and
                not collision_point(x-8, y+15, oTreasure, 0, 0) and
                not collision_point(x-8, y+8, oChest, 0, 0) and
                not collision_point(x-8, y+8, oSpikes, 0, 0) and
                not collision_point(x-8, y+8, oSpikesWood, 0, 0))
            {
                instance_create(x-8, y+8, oLockedChest);
                global.LockedChest = true;
                break;
            }
            else if (not collision_point(x+8, y, oSolid, 0, 0) and
                     not collision_point(x+8, y+15, oTreasure, 0, 0) and
                     not collision_point(x+8, y+8, oChest, 0, 0) and
                     not collision_point(x+8, y+8, oSpikes, 0, 0) and
                     not collision_point(x+8, y+8, oSpikesWood, 0, 0))
            {
                instance_create(x+16+8, y+8, oLockedChest);
                global.LockedChest = true;
                break;
            }
            else
            {
                instance_create(x+8, y+8, oLockedChest);
                global.LockedChest = true;
                break;
            }
        }
    }

    // generate key if locked chest has been generated
    if (instance_exists(oLockedChest))
    {
        n = 1;
        while (n < 8 and global.Key == false)
        {
            with (oTreasure)
            {
                if (rand(1, 8) <= 1 and not collision_point(x, y, oSolid, 0, 0) and global.Key == false)
                {
                    if (type == "Gold Bars") instance_create(x, y+4, oKey);
                    else instance_create(x, y, oKey);
                    global.Key = true;
                    instance_destroy();
                    break;
                }
            }
            n += 1;
        }
        if (not global.Key)
        {
            with (oTreasure)
            {
                if (not collision_point(x, y, oSolid, 0, 0))
                {
                    if (type == "Gold Bars") instance_create(x, y+4, oKey);
                    else instance_create(x, y, oKey);
                    global.Key = true;
                    instance_destroy();
                    break;
                }
            }
        }
    }

    if (global.Key) global.madeUdjatEye = true;

}
else if (global.levelType == 1)
{


    with (oSolid)
    {
        if (x >= 16 and x < room_width-16 and y >= 16 and y < room_height-16)
        {
            // bg
            if (rand(1, 100) == 1 and not collision_point(x, y-16, oSolid, 0, 0)) tile_add(bgTrees, 0, 0, 16, 48, x, y-32, 9005);

            if (not isInShop(x, y))
            {
                if (y > 32 and collision_point(x, y-16, oSolid, 0, 0) and global.genMarketEntrance and not global.madeMarketEntrance)
                {
                    obj = instance_place(x, y-16, oSolid);
                    if (obj.type != "Tree" and type != "Altar" and not obj.invincible and rand(1, global.marketChance) <= 1)
                    {
                        instance_create(x, y-16, oXMarket);
                        invincible = true;
                        global.madeMarketEntrance = true;
                    }
                    else global.marketChance -= 1;
                }
            }
        }
    }


    // force market entrance
    if (global.genMarketEntrance and not global.madeMarketEntrance)
    {
        with (oSolid)
        {
            if (y > 32 and collision_point(x, y-16, oSolid, 0, 0))
            {
                obj = instance_place(x, y-16, oSolid);
                if (obj.type != "Tree" and type != "Altar" and not obj.invincible)
                {
                    instance_create(x, y-16, oXMarket);
                    invincible = true;
                    global.madeMarketEntrance = true;
                }
            }
        }
    }
}
else if (global.levelType == 2)
{
    // nothing
}
else if (global.levelType == 3)
{
    with (oSolid)
    {
        // bg
        if (rand(1, 100) == 1 and not collision_point(x, y-16, oSolid, 0, 0)) tile_add(bgStatues, 0, 0, 16, 48, x, y-32, 9005);
    }

    // force generate gold door
    if (global.genGoldEntrance and not global.madeGoldEntrance)
    {
        with (oSolid)
        {
            if (y > 32 and not collision_point(x, y-16, oSolid, 0, 0))
            {
                instance_create(x, y-16, oGoldDoor);
                invincible = true;
                global.madeGoldEntrance = true;
                break;
            }
        }
    }
}

// add box of flares to dark level
if (global.darkLevel)
{
    with (oEntrance)
    {
        if (not collision_point(x-16, y, oSolid, 0, 0))
        {
            instance_create(x-16+8, y+8, oFlareCrate);
        }
        else if (not collision_point(x+16, y, oSolid, 0, 0))
        {
            instance_create(x+16+8, y+8, oFlareCrate);
        }
        else
        {
            instance_create(x+8, y+8, oFlareCrate);
        }
    }
}

global.cleanSolids = false;
