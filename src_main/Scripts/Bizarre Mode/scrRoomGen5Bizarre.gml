//
// scrRoomGen5()
//
// Room generation for the final boss level (which is only partially randomized).
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/
var strTemp, i, j, strObs1, strObs2, strObs3;
var tile, xpos, ypos, obj, block;

/*
Note:

ROOMS are 10x8 tile areas.

strTemp = "0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000";

OBSTACLES are 5x3 tile chunks that are randomized within rooms.

strObs = "00000
          00000
          00000";

The string representing a room or obstacle must be laid out unbroken:
*/
strTemp = "00000000000000000000000000000000000000000000000000000000000000000000000000000000";

roomPath = global.roomPath[scrGetRoomX(x), scrGetRoomY(y)];
if (y < 352)
{
    if (y < 144)
    {
        switch(rand(1,6))
        {
            case 1: { strTemp = "00000000006000000000000000000000000000000000000000000006000000000000000000000000"; break; }
            case 2: { strTemp = "00000000000000060000000000000000000000000000000000000000000000000000000000000000"; break; }
            case 3: { strTemp = "00000000006000000000000000000000000000000000000000000006000000000000000000000000"; break; }
            case 4: { strTemp = "00000000006000060000000000000000000000000000000000000000000000000000000000000000"; break; }
            case 5: { strTemp = "00000000000000000000000000000000000000000000000000600006000000000000000000000000"; break; }
            case 6: { strTemp = "00000000000000060000000000000000000000000000000000000000000000000000000000000000"; break; }
        }
    }
    else
    {
        switch(rand(1,6))
        {
            case 1: { strTemp = "00000000005000060000000000000000000000000000000000000000000000000000000000000000"; break; }
            case 2: { strTemp = "00000000000000050000000000000000000000000000000000000000000000000000000000000000"; break; }
            case 3: { strTemp = "00000000005000000000000000000000000000000000000000000000000000000000000000000000"; break; }
            case 4: { strTemp = "00000000006000050000000000000000000000000000000000000000000000000000000000000000"; break; }
            case 5: { strTemp = "00000000000000060000000000000000000000000000000000000000000000000000000000000000"; break; }
            case 6: { strTemp = "00000000000000050000000000000000000000000000000000000000000000000000000000000000"; break; }
        }
    }
}
else
{
/*
    switch(rand(1,6))
    {
        case 1: { strTemp = "1111111111111111111111111111111111T111111111111111111111111111111111111111111111"; break; }
        case 2: { strTemp = "11111111111222111111122211T11111111111111111111111111111111111111111111111111111"; break; }
        case 3: { strTemp = "1111111111111111111111111111111112222111111222211111111111111111T111111111111111"; break; }
        case 4: { strTemp = "11111111111111112221111111222111111111111111111111111111111111111111111111111111"; break; }
        case 5: { strTemp = "1111111111111111111111111111111111111111111111111112T211111112221111111111111111"; break; }
        case 6: { strTemp = "1111111111111T111111111111111111111111111111111111111111222111111122211111111111"; break; }
    }
*/
    switch(rand(1,6))
    {
        case 1: { strTemp = "1212?T112222111?T11?1121211221?111T11111122212211122221112222111?T1111111??11111"; break; }
        case 2: { strTemp = "11?111111112221111?1122211T1111?11111222211221111?112211T22211?111122111111??111"; break; }
        case 3: { strTemp = "11112222111221111122221111122211122221111112222?T11?111122111111T1111?111111?111"; break; }
        case 4: { strTemp = "11212121221?111122211?T1?12221111?22222111222111?21211?11122111?111212?11111?111"; break; }
        case 5: { strTemp = "11?111T11212121211T11122221111111112211122111?T?1112T212111?T2121111?111???11111"; break; }
        case 6: { strTemp = "11222112221?1T112221221?111111222?T1111?1222111222111221222111221122211?111???1?"; break; }
    }
}

if (global.scumTileDebug) scrRevealObstacle(strTemp); // In-Level obstacle debug (YASM 1.7.3)

// Add obstacles
for (i = 1; i < 81; i += 1)
{
    j = i;

    strObs1 = "00000";
    strObs2 = "00000";
    strObs3 = "00000";
    tile = string_char_at(strTemp, i);

    if (tile == "8")
    {
        switch(rand(1,1))
        {
            case 1: { strObs1 = "00900"; strObs2 = "21112"; strObs3 = "21112"; break; }
        }
    }
    else if (tile == "5") // air with alien boss
    {
        switch(rand(1,6))
        {
            case 1: { strObs1 = "0A00?"; strObs2 = "0000?"; strObs3 = "111??"; break; }
            case 2: { strObs1 = "00A00"; strObs2 = "00002"; strObs3 = "??11?"; break; }
            case 3: { strObs1 = "?0A00"; strObs2 = "?0000"; strObs3 = "?111?"; break; }
            case 4: { strObs1 = "0A00?"; strObs2 = "00002"; strObs3 = "111??"; break; }
            case 5: { strObs1 = "000A0"; strObs2 = "20000"; strObs3 = "1?111"; break; }
            case 6: { strObs1 = "A000?"; strObs2 = "00022"; strObs3 = "11???"; break; }
        }
        /*
        switch(rand(1,8))
        {
            case 1: { strObs1 = "00000"; strObs2 = "02220"; strObs3 = "2???2"; break; }
            case 2: { strObs1 = "00000"; strObs2 = "02020"; strObs3 = "212?2"; break; }
            case 3: { strObs1 = "1??00"; strObs2 = "???10"; strObs3 = "1???1"; break; }
            case 4: { strObs1 = "001??"; strObs2 = "0?1?1"; strObs3 = "???11"; break; }
            case 5: { strObs1 = "2??12"; strObs2 = "22222"; strObs3 = "00000"; break; }
            case 6: { strObs1 = "00022"; strObs2 = "000?1"; strObs3 = "000??"; break; }
            case 7: { strObs1 = "22000"; strObs2 = "1?000"; strObs3 = "1?000"; break; }
            case 8: { strObs1 = "00000"; strObs2 = "00000"; strObs3 = "00000"; break; }
        }
        */
    }
    else if (tile == "6") // air
    {
        switch(rand(1,6))
        {
            case 1: { strObs1 = "0TTT0"; strObs2 = "2???2"; strObs3 = "02220"; break; }
            case 2: { strObs1 = "0000T"; strObs2 = "0TTT?"; strObs3 = "2????"; break; }
            case 3: { strObs1 = "T0000"; strObs2 = "?TTT0"; strObs3 = "????2"; break; }
            case 4: { strObs1 = "?TT00"; strObs2 = "????2"; strObs3 = "?2200"; break; }
            case 5: { strObs1 = "0TTT?"; strObs2 = "2????"; strObs3 = "0022?"; break; }
            case 6: { strObs1 = "2???2"; strObs2 = "TTTTT"; strObs3 = "?????"; break; }
        }
    }

    if (tile == "5" or tile == "6" or tile == "8")
    {
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs1, strTemp, j);
        j += 10;
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs2, strTemp, j);
        j += 10;
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs3, strTemp, j);
    }
}

// Generate the tiles
for (j = 0; j < 8; j += 1)
{
    for (i = 1; i < 11; i += 1)
    {
        tile = string_char_at(strTemp, i+j*10);
        xpos = x + (i-1)*16;
        ypos = y + j*16;

        //debugging
        if (global.scumTileDebug)
        {
            if (tile != "0")
            {
                obj = instance_create(xpos, ypos, oTileDebug);
                obj.tileString = tile;
            }
        }

        if (tile == "1" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (global.bizarrePlus)
            {
                if (rand(1,12) == 1) instance_create(xpos, ypos, oBlock);
                else
                {
                    obj = instance_create(xpos, ypos, oTemple);
                    obj.sprite_index = sGTemple;
                }
            }
            else
            {
                if (rand(1,12) == 1) instance_create(xpos, ypos, oBlock);
                else
                {
                    instance_create(xpos, ypos, oTemple);
                }
            }
        }
        else if (tile == "2" and rand(1,2) == 1 and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,14) == 1)
            {
                if (global.bizarrePlus) instance_create(xpos, ypos, oBlock);
                else instance_create(xpos, ypos, oBlock);
            }
            else
            {
                obj = instance_create(xpos, ypos, oTemple);
                if (global.bizarrePlus) obj.sprite_index = sGTemple;
            }
        }
        else if (tile == "L") instance_create(xpos, ypos, oVine);
        else if (tile == "P") instance_create(xpos, ypos, oVineTop);
        else if (tile == "7" and rand(1,3) == 1)
        {
            if (global.woodSpikes) instance_create(xpos, ypos, oSpikesWood);
            else instance_create(xpos, ypos, scrGenSpikeType());
        }
        else if (tile == "4" and rand(1,4) == 1)
        {
            obj = instance_create(xpos, ypos, oPushBlock);
            //if (global.bizarrePlus) obj.sprite_index = sGoldBlock;
        }
        else if (tile == "9")
        {
            if (scrGetRoomX(x) == global.startRoomX and scrGetRoomY(y) == global.startRoomY)
                instance_create(xpos, ypos, oEntrance);
            else
            {
                instance_create(xpos, ypos, oExit);
                global.exitX = xpos+8;
                global.exitY = ypos+8;
            }
            block = instance_create(xpos, ypos+16, oTemple);
            block.invincible = true;
            if (global.bizarrePlus) block.sprite_index = sGTemple;
        }
        else if (tile == "a")
        {
            if (rand(1,1) == 1) instance_create(xpos+8, ypos+8, oChest);
        }
        else if (tile == "T")
        {
            if (rand(1,15) == 1) instance_create(xpos+8, ypos+8, oChest);
            else if (rand(1,6) == 1) instance_create(xpos+8, ypos+8, oGoldBars);
            else if (rand(1,6) == 1) instance_create(xpos+8, ypos+12, oEmeraldBig);
            else if (rand(1,8) == 1) instance_create(xpos+8, ypos+12, oSapphireBig);
            else if (rand(1,10) == 1) instance_create(xpos+8, ypos+12, oRubyBig);
            else if (rand(1,10) == 1) instance_create(xpos+8, ypos+8, oCrate);
            else if (rand(1,10) == 1)
            {
                obj = instance_create(xpos, ypos, oBlock);
                //if (global.bizarrePlus) obj.sprite_index = sGoldBlock;
            }
            else
            {
                obj = instance_create(xpos, ypos, oTemple);
                if (global.bizarrePlus) obj.sprite_index = sGTemple;
            }
        }
        else if (tile == "t")
        {
            instance_create(xpos, ypos, oThwompTrap);
        }
        else if (tile == "?")
        {
            if (rand(1,2) == 1)
            {
                obj = instance_create(xpos, ypos, oTemple);
                if (global.bizarrePlus) obj.sprite_index = sGTemple;
            }
            else instance_create(xpos, ypos, choose(oBrick, oLush, oLush, oDark, oIce, oGTemple, oBlock));
        }
        else if (tile == "I")
        {
            if (rand(1,1) == 1) instance_create(xpos+16, ypos, oGoldIdol);
        }
        else if (tile == "C")
        {
            instance_create(xpos, ypos, oCeilingTrap);
        }
        else if (tile == "D")
        {
            instance_create(xpos, ypos, oTempleFake);
            instance_create(xpos, ypos+16, oTempleFake);
            instance_create(xpos, ypos, oDoor);
        }
        else if (tile == "w")
        {
            instance_create(xpos, ypos, oWaterSwim);
        }
        else if (tile == "A")
        {
            if (not instance_exists(oAlienBoss))
            {
                obj = instance_create(xpos, ypos, oAlienBoss);
                obj.hp = 15;
                if (x < 320) obj.facing = 1; // right
                if (rand(1,4) > 1) obj.carries = "Bomb Box";
                else obj.carries = "Crate";
                if (global.levelType != 2) scrDecorate(16+scrGetRoomX(x+16)*160, 16+scrGetRoomY(y-16)*128, "Alien Boss");
                if (rand(1,ceil(200/global.eMult)) == 1) global.genAlienBoss = true; else global.genAlienBoss = false;
            }
            else
            {
                if (rand(1,6) == 1) instance_create(xpos+8, ypos+24, oCrate);
                else if (rand(1,4) == 1) instance_create(xpos+8, ypos+24, oChest);
                else if (rand(1,3) == 1) instance_create(xpos+8, ypos+16+12, choose(oEmeraldBig, oSapphireBig, oRubyBig));
                else instance_create(xpos, ypos+16, oAlien);
            }
        }


    }
}
