//
// scrRoomGen1()
//
// Room generation for Area 1, the Mines.
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/
var strTemp, roomPathAbove, n, i, j, strObs1, strObs2, strObs3;
var tile, xpos, ypos, obj, block;

/*
Note:

ROOMS are 10x8 tile areas.

strTemp = "0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000
           0000000000";

OBSTACLES are 5x3 tile chunks that are randomized within rooms.

strObs = "00000
          00000
          00000";

The string representing a room or obstacle must be laid out unbroken:
*/
strTemp = "00000000000000000000000000000000000000000000000000000000000000000000000000000000";

roomPath = global.roomPath[scrGetRoomX(x), scrGetRoomY(y)];
roomPathAbove = -1;
shopType = "General";
if (scrGetRoomY(y) != 0) roomPathAbove = global.roomPath[scrGetRoomX(x), scrGetRoomY(y-128)];

if (scrGetRoomX(x) == global.startRoomX and scrGetRoomY(y) == global.startRoomY) // start room
{
    if (roomPath == 2) n = rand(5,8);
    else n = rand(1,4);
    switch(n)
    {
        case 1: { strTemp = choose("60000600000000000000000000000000000000000008000000000000000000000000001111111111",
            "0000000222L090000000P111000000L111200000L221112000000211200000021111201111111111"); break; }


        case 2: { strTemp = choose("11111111112222222222000000000000000000000008000000000000000000000000001111111111",
            "2220000000000000090L000000111P000002111L000211122L000211200002111120001111111111"); break; }
        case 3: { strTemp = choose("00000000000008000000000000000000L000000000P111111000L111111000L00111111111111111",
            "00000002200211120000002220022000000021100000000220009020000001111727201111111111"); break; }
        case 4: { strTemp = choose("0000000000008000000000000000000000000L000111111P000111111L001111100L001111111111",
            "21112222220222000000000000002000000900000200212000000021200007711111701111111111"); break; }
        // hole
        case 5: { strTemp = choose("60000600000000000000000000000000000000000008000000000000000000000000002021111120",
            "111111111122222222220000000000L022200000P111110000L022210900L0000111101112022211"); break; }
        case 6: { strTemp = choose("11111111112222222222000000000000000000000008000000000000000000000000002021111120",
            "111111111122222222220000000000000002220L000011111P009012220L011110000L1122202111"); break; }
        case 7: { strTemp = choose("00000000000008000000000000000000L000000000P111111000L111111000L00011111111101111",
            "22222222220000000000000900000000111021000012002100011102111001200002101111001111"); break; }
        case 8: { strTemp = choose("0000000000008000000000000000000000000L000111111P000111111L001111000L001111011111",
            "21111221120222200220000000000000020900000001111000021122112000400004001112002111"); break; }
    }
}
else if (scrGetRoomX(x) == global.endRoomX and scrGetRoomY(y) == global.endRoomY) // end room
{
    if (roomPathAbove == 2) n = rand(1,4);
    else n = rand(3,6);
    switch(n)
    {
        case 1: { strTemp = choose("00000000006000060000000000000000000000000008000000000000000000000000001111111111",
            "00000000000000222000000211100000011112000011221100000000040001100001101120900211"); break; }
        case 2: { strTemp = choose("00000000000000000000000000000000000000000008000000000000000000000000001111111111",
            "00000000000000000000000100000000012000000001100100090400110021111022011111201101"); break; }
        case 3: { strTemp = choose("00000000000010021110001001111000110111129012000000111111111021111111201111111111",
            "00000000000000000000000000100000000210000010011000001100409010220111121011021111"); break; }
        case 4: { strTemp = choose("00000000000111200100011110010021111011000000002109011111111102111111121111111111",
            "00000000000220090000000011100000002120000001111100000211120000111111100021111120"); break; }
        // no drop
        case 5: { strTemp = choose("60000600000000000000000000000000000000000008000000000000000000000000001111111111",
            "11111111110000000000000800000000000000000000000000021111111001111111101111111111"); break; }
        case 6: { strTemp = choose("11111111110222222220000000000000000000000008000000000000000000000000001111111111",
            "11111111110211111120022222222000000000000008000000220000000022000000001111111111"); break; }
    }
}
else if (roomPath == 0) // side room
{
    if (global.currLevel > 1 and not oGame.altar and rand(1,12) == 1) //rand(1,16) == 1)
    {
        n = 11;
        oGame.altar = true;
    }
    else if (oGame.idol or scrGetRoomY(y) == 3)
    {
        n = rand(1,9);
    }
    else
    {
        n = rand(1,10);
        if (n == 10) oGame.idol = true;
        // else n = rand(1,9);
    }

    switch(n)
    {
        // upper plats
        case 1: { strTemp = choose("00000000000010111100000000000000011010000050000000000000000000000000001111111111",
            "00000000000010111100000000000000011010000050000000000000000000000000001111111111",
            "00000000000210211220000000000000021012000050000000000000000000000000001111111111",
            "00000000000222222220000000000000022020000050000000000000000000000000001111111111"); break; }
        // high walls
        case 2: { strTemp = choose("110000000040L600000011P000000011L000000011L5000000110000000011000000001111111111",
            "120600000040000000001100000000120L000000111P500000111L000000112L0000001111111111"); break; }
        case 3: { strTemp = choose("00000000110060000L040000000P110000000L110050000L11000000001100000000111111111111",
            "006000002100000000040000000011000000L021050000P111000000L111000000L2111111111111"); break; }
        case 4: { strTemp = choose("110000000040L600000011P000000011L000000011L0000000110000000011000000001112222111",
            "22200002220000000000100000000110L002200211P221120212L222220212L00000001112222211"); break; }
        case 5: { strTemp = choose("00000000110060000L040000000P110000000L110000000L11000000001100000000111112222111",
            "2220000222000000000010000000012002200L012021122P112022222L210000000L211122222111"); break; }
        case 6: { strTemp = choose("11111111110221111220002111120000022220000002222000002111120002211112201111111111",
            "11111111110112112110022011022000002200000020220200021211212001111111101111111111"); break; }
        case 7: { strTemp = choose("11111111111112222111112000021111102201111120000211111022011111200002111112222111",
            "11111111111122222211120000002111022220111200000021111200211112000000211112222111",
            "111111111111tttttt1111t0v00t1111t0000t1111t0000t1111tG$$0t1111tttttt111111111111", // these 2 are sealed treasure rooms
            "111111111111tttttt1121t00v0t1121t0000t1221t0000t1211tG$$0t1211tttttt111111111111"); break; }
        case 8: { strTemp = choose("11111111110000000000110000001111222222111111111111112222221122000000221100000011",
            "11111111110002222000100000000112002200211102112011122111122120112211021000000001"); break; }
        case 9: { strTemp = choose("121111112100L2112L0011P1111P1111L2112L1111L1111L1111L1221L1100L0000L001111221111",
            "121122112100L0000L0011P1111P1111L2112L1112L1111L2111L2112L1100L2112L001211111121",
            "121111112100L2120L0011P1111P1112L0000L2112L2000L211211221P110000000L001111221111",
            "12111111210021221200100200200110000000011100000011120211202100000000001111221111",
            "12111111210L000000001P122221011L111111011L212212011L110011010L000000001111221111",
            "12111111210000110000100002001112120001111020000021100021201100200020001111221111"); break; }
        // idols
        case 10: { strTemp = "22000000220000B0000000000000000000000000000000000000000000000000I000001111A01111"; break; }
        // altars
        case 11: { strTemp = "220000002200000000000000000000000000000000000000000000x0000002211112201111111111"; break; }
    }
}
else if (roomPath == 0 or roomPath == 1) // main room
{
    switch(rand(1,13))
    {

        // basic rooms
        case 1: { strTemp = "60000600000000000000000000000000000000000050000000000000000000000000001111111111"; break; }
        case 2: { strTemp = "60000600000000000000000000000000000000005000050000000000000000000000001111111111"; break; }
        case 3: { strTemp = choose("60000600000000000000000000000000050000000000000000000000000011111111111111111111",
            "60000600000000000000000000000060000600000000000000000000000000000000001111111111"); break; }
        case 4: { strTemp = choose("60000600000000000000000000000000000000000000000000000222220000111111001111111111",
            "00000000000600000000000000000000000000000000000000002000000002200000001111111111"); break; }
        case 5: { strTemp = "11111111112222222222000000000000000000000050000000000000000000000000001111111111"; break; }
        case 6: { strTemp = "11111111112111111112022222222000000000000050000000000000000000000000001111111111"; break; }
        // low ceiling
        case 7: { strTemp = choose("11111111112111111112211111111221111111120111111110022222222000000000001111111111",
            "11111111112111111112211112111221121011120110202210022000002000000000001111111111",
            "11111111112111111112211111111221221211120200202110000000022000211200001111111111",
            "11111111112111111112211112111221120022120222200020000000200002222112201111111111"); break; }
        // ladders
        case 8: {
            if (rand(1,2) == 1) strTemp = "1111111111000000000L111111111P000000000L5000050000000000000000000000001111111111";
            else strTemp = "1111111111L000000000P111111111L0000000005000050000000000000000000000001111111111"; break;
        }
        case 9: { strTemp = choose("000000000000L0000L0000P1111P0000L0000L0000P1111P0000L1111L0000L1111L001111111111",
            "000000000000L022200000P111100000L111110000L111110000L111111000L11111101111111111",
            "00000000000002220L000001111P000011111L000011111L000111111L000211111L001111111111",
            "000000000000000220000L002120000P111111000L11111200002111111000211111201111111111",
            "0000000000000022200000002120L000011111P000211111L0011111120002111112001111111111",
            "00000000000L000000L00P112211P00L112200L00L111111P000221111L000221111L01111111111"); break; }
        // upper plats
        case 10: { strTemp = choose("00000000000111111110001111110000000000005000050000000000000000000000001111111111",
            "00000000000111111110002222220000000000000050000000000000000000000000001111111111"); break; }
        case 11: { strTemp = choose("00000000000000000000000000000000000000000021111200021111112021111111121111111111",
            "00000000000000000000000200200000112212000021111100011111111021111111121111111111"); break; }
        // treasure below
        case 12: { strTemp = choose("2222222222000000000000000000L001111111P001050000L011000000L010000000L01111111111",
            "222222222200000000L000222221P001111111L001050000L0110000000010000000001111111111", // new
            "222222222200000000000L000000000P111111100L500000100L000000110L000000011111111111",
            "22222222220L000000000P122222000L111111100L50000010000000001100000000011111111111"); break; } // new
        // lava/water
        case 13: { strTemp = choose("00000000000000000000000000000000000000000NHyyyyN3003HyyyyH30333HHHH3333333333333",
            "000000000000000002000002000000000000000003HyHHyH3003HyyyyH30333HHHH33NNN333N3N33",
            "00000000000000000N00000N00000000000000000NHyyyyH3003HyyyyH30333HHHH3333NNN33NN33",
            "00000000000000000000000000000000000000000NJyyyyJ100NJyyyyJ10111JJJJ1111111111111",
            "000000000000000002000002000000000000000001JyJJyJ1001JyyyyJ10111JJJJ1111111111111",
            "000000000000000001000001000000000000000001JyyyyJ1001JyyyyJ10111JJJJ1111111111111",
            "00000000000000000000000000000000000000000N^wwwwN3003^wwww^30333^^^^3333333333333",
            "000000000000000002000002000000000000000003^w^^w^3003^wwww^30333^^^^33NNN333N3N33",
            "00000000000000000N00000N00000000000000000N^wwww^3003^wwww^30333^^^^3333NNN33NN33",
            "00000000000000000000000000000000000000000NZwwwwZ100NZwwwwZ10111ZZZZ1111111111111",
            "000000000000000002000002000000000000000001ZwZZwZ1001ZwwwwZ10111ZZZZ1111111111111",
            "000000000000000001000001000000000000000001ZwwwwZ1001ZwwwwZ10111ZZZZ1111111111111"); break; }

    }
}
else if (roomPath == 3) // main room
{
    switch(rand(1,8))
    {

        // basic rooms
        case 1: { strTemp = "00000000000000000000000000000000000000000050000000000000000000000000001111111111"; break; }
        case 2: { strTemp = "00000000000000000000000000000000000000005000050000000000000000000000001111111111"; break; }
        case 3: { strTemp = "00000000000000000000000000000050000500000000000000000000000011111111111111111111"; break; }
        case 4: { strTemp = choose("00000000000000000000000600000000000000000000000000000111110000111111001111111111",
            "00000000000000000000020600002000000000000000000000000211120000111111001111111111"); break; }
        // upper plats
        case 5: { strTemp = choose("00000000000111111110001111110000000000005000050000000000000000000000001111111111",
            "00000000000211111120002222220000000000005000050000000000000000000000001111111111"); break; }
        case 6: { strTemp = choose("00000000000000000000000000000000000000000021111200021111112021111111121111111111",
            "00000000000000000000000022000000021120000021111200021111112021111111121111111111"); break; }
        case 7: { strTemp = choose("10000000011112002111111200211100000000000022222000111111111111111111111111111111",
            "10000000011112002111111100111100220022000000000000110000001111222222111111111111"); break; }
        // treasure below
        case 8: { strTemp = choose("0000000000000000000000000000L001111111P001050000L011000000L010000000L01111111111",
            "000000000000022000L000211221P001111111L001050000L0110000000010000000001111111111", // new
            "000000000000000000000L000000000P111111100L500000100L000000110L000000011111111111",
            "00000000000L000220000P122112000L111111100L50000010000000001100000000011111111111"); break; // new
        }
    }
}
else if (roomPath == 4) // shop
{
    strTemp = "111111111111111111111111221111111l000211...000W010...00000k0..Kiiii000bbbbbbbbbb";
    n = rand(1,7);
    //n = 6;
    switch(n)
    {
        case 1: { shopType = "General"; break; }
        case 2: { shopType = "Bomb"; break; }
        case 3: { shopType = "Weapon"; break; }
        case 4: { shopType = "Rare"; break; }
        case 5: { shopType = "Clothing"; break; }
        case 6: { shopType = "Craps"; strTemp = "11111111111111111111111122111111Kl000211..bQ00W010.0+00000k0.q+dd00000bbbbbbbbbb"; break; }
        case 7: { shopType = "Kissing"; strTemp = "111111111111111111111111221111111l000211...000W010...00000k0..K00D0000bbbbbbbbbb"; oGame.damsel = true; break; }
    }
}
else if (roomPath == 5) // shop
{
    strTemp = "111111111111111111111111221111112000l11101W0000...0k00000...000iiiiK..bbbbbbbbbb";
    n = rand(1,7);
    //n = 6;
    switch(n)
    {
        case 1: { shopType = "General"; break; }
        case 2: { shopType = "Bomb"; break; }
        case 3: { shopType = "Weapon"; break; }
        case 4: { shopType = "Rare"; break; }
        case 5: { shopType = "Clothing"; break; }
        case 6: { shopType = "Craps"; strTemp = "111111111111111111111111221111112000lK1101W0Q00b..0k00000+0.00000dd+q.bbbbbbbbbb"; break; }
        case 7: { shopType = "Kissing"; strTemp = "111111111111111111111111221111112000l11101W0000...0k00000...0000D00K..bbbbbbbbbb"; oGame.damsel = true; break; }
    }
}
else if (roomPath == 8) // snake pit
{
    switch(rand(1,1))
    {
        case 1: { strTemp = "111000011111s0000s11111200211111s0000s11111200211111s0000s11111200211111s0000s11"; break; }
    }
}
else if (roomPath == 9) // snake pit bottom
{
    switch(rand(1,1))
    {
        case 1: { strTemp = "111000011111s0000s1111100001111100S0001111S0110S11111STTS1111111M111111111111111"; break; }
    }
}
else // drop
{
    if (roomPath == 7) n = rand(4,12); // top of pit
    else if (roomPathAbove != 2) n = rand(1,12); // any
    else n = rand(1,8); // need open ceiling
    switch(n)
    {
        case 1: { strTemp = choose("00000000006000060000000000000000000000006000060000000000000000000000000000000000",
            "00000000000060000000000000000000000000000000600000000000000000000000000000000000"); break; }
        case 2: { strTemp = choose("00000000006000060000000000000000000000000000050000000000000000000000001202111111",
            "00000000000000000000022202220000000000000000250000020200000000000000001202111111"); break; }
        case 3: { strTemp = choose("00000000006000060000000000000000000000005000000000000000000000000000001111112021",
            "00000000000002220000000000022000000000005000020000000000022000000000001111112021"); break; }
        case 4: { strTemp = choose("00000000006000060000000000000000000000000000000000000000000002200002201112002111",
            "00000000000000000000002222220000110011000000000000002000020002120021201111001111"); break; }
        case 5: { strTemp = choose("00000000000000220000000000000000200002000112002110011100111012000000211111001111",
            "00000000000021111200000000000000200002000212002120011100111011000000111112001111"); break; }
        case 6: { strTemp = choose("00000000000060000000000000000000000000000000000000001112220002100000001110111111",
            "00000000000022000000000002220000010111000001000000001112220002100000001110211111"); break; }
        case 7: { strTemp = choose("00000000000060000000000000000000000000000000000000002221110000000001201111110111",
            "00000000000000002200002220000000111010000000001000002221110000000001201111120111"); break; }
        case 8: { strTemp = choose("00000000000060000000000000000000000000000000000000002022020000100001001111001111",
            "00000000000002002000000100100000110011000012002100011100111001200002101111001111"); break; }
        case 9: { strTemp = choose("11111111112222222222000000000000000000000000000000000000000000000000001120000211",
            "11111111112111111112021111112000211112000002222000000000000002000000201120000211"); break; }
        case 10: { strTemp = choose("11111111112222111111000002211100000002110000000000200000000000000000211120000211",
            "11111111110111111111021111111100212111110002021110000000200002000000111120000111"); break; }
        case 11: { strTemp = choose("11111111111111112222111220000011200000000000000000000000000012000000001120000211",
            "11111111111111111110111111112011111221000111200200000200000012000000201120000211"); break; }
        case 12: { strTemp = choose("11111111112111111112021111112000211112000002112000000022000002200002201111001111",
            "11111111112112222112012000021001002200100102112110000000000002200002201111001111"); break; }
    }
}

if (global.scumTileDebug) scrRevealObstacle(strTemp); // In-Level obstacle debug (YASM 1.7.3)

// Add obstacles
for (i = 1; i < 81; i += 1)
{
    j = i;

    strObs1 = "00000";
    strObs2 = "00000";
    strObs3 = "00000";
    tile = string_char_at(strTemp, i);

    //if (tile == "8") { strObs1 = "00000"; strObs2 = "00000"; strObs3 = "00900" }

    if (tile == "8") // entrance/exit
    {
        switch(rand(1,12))
        {
            case 1: { strObs1 = "00900"; strObs2 = "01110"; strObs3 = "11111"; break; }
            case 2: { strObs1 = "00900"; strObs2 = "02120"; strObs3 = "02120"; break; }
            case 3: { strObs1 = "00000"; strObs2 = "00000"; strObs3 = "92222"; break; }
            case 4: { strObs1 = "00000"; strObs2 = "00000"; strObs3 = "22229"; break; }
            case 5: { strObs1 = "00000"; strObs2 = "11001"; strObs3 = "19001"; break; }
            case 6: { strObs1 = "00000"; strObs2 = "10011"; strObs3 = "10091"; break; }
            case 7: { strObs1 = "11111"; strObs2 = "10001"; strObs3 = "40094"; break; }
            case 8: { strObs1 = "00000"; strObs2 = "12021"; strObs3 = "12921"; break; }
            case 9: { strObs1 = "00900"; strObs2 = "03330"; strObs3 = "33333"; break; }
            case 10: { strObs1 = "00000"; strObs2 = "33003"; strObs3 = "39003"; break; }
            case 11: { strObs1 = "00000"; strObs2 = "N00N3"; strObs3 = "N0093"; break; }
            case 12: { strObs1 = "N3333"; strObs2 = "30003"; strObs3 = "40094"; break; }
        }
    }
    else if (tile == "5") // ground
    {
        switch(rand(1,19))
        {
            case 1: { strObs1 = "11111"; strObs2 = "00000"; strObs3 = "00000"; break; }
            case 2: { strObs1 = "00000"; strObs2 = "11110"; strObs3 = "00000"; break; }
            case 3: { strObs1 = "00000"; strObs2 = "01111"; strObs3 = "00000"; break; }
            case 4: { strObs1 = "00000"; strObs2 = "00000"; strObs3 = "11111"; break; }
            case 5: { strObs1 = "00000"; strObs2 = "20200"; strObs3 = "17177"; break; }
            case 6: { strObs1 = "00000"; strObs2 = "02020"; strObs3 = "71717"; break; }
            case 7: { strObs1 = "00000"; strObs2 = "00202"; strObs3 = "77171"; break; }
            case 8: { strObs1 = "00000"; strObs2 = "22200"; strObs3 = "11100"; break; }
            case 9: { strObs1 = "00000"; strObs2 = "02220"; strObs3 = "01110"; break; }
            case 10: { strObs1 = "00000"; strObs2 = "00222"; strObs3 = "00111"; break; }
            case 11: { strObs1 = "11100"; strObs2 = "22200"; strObs3 = "00000"; break; }
            case 12: { strObs1 = "01110"; strObs2 = "02220"; strObs3 = "00000"; break; }
            case 13: { strObs1 = "00111"; strObs2 = "00222"; strObs3 = "00000"; break; }
            case 14: { strObs1 = "00000"; strObs2 = "02220"; strObs3 = "21112"; break; }
            case 15: { strObs1 = "00000"; strObs2 = "20100"; strObs3 = "77117"; break; }
            case 16: { strObs1 = "00000"; strObs2 = "00102"; strObs3 = "71177"; break; }
            case 17: { strObs1 = "NN3NN"; strObs2 = "00000"; strObs3 = "00000"; break; }
            case 18: { strObs1 = "00000"; strObs2 = "N33N0"; strObs3 = "00000"; break; }
            case 19: { strObs1 = "00000"; strObs2 = "0N33N"; strObs3 = "00000"; break; }
        }
    }
    else if (tile == "6") // air
    {
        switch(rand(1,12))
        {
            case 1: { strObs1 = "11111"; strObs2 = "00000"; strObs3 = "00000"; break; }
            case 2: { strObs1 = "22222"; strObs2 = "00000"; strObs3 = "00000"; break; }
            case 3: { strObs1 = "11100"; strObs2 = "22200"; strObs3 = "00000"; break; }
            case 4: { strObs1 = "01110"; strObs2 = "02220"; strObs3 = "00000"; break; }
            case 5: { strObs1 = "00111"; strObs2 = "00222"; strObs3 = "00000"; break; }
            case 6: { strObs1 = "00000"; strObs2 = "01110"; strObs3 = "00000"; break; }
            case 7: { strObs1 = "00000"; strObs2 = "01110"; strObs3 = "02220"; break; }
            case 8: { strObs1 = "00000"; strObs2 = "02220"; strObs3 = "01110"; break; }
            case 9: { strObs1 = "00000"; strObs2 = "00220"; strObs3 = "01111"; break; }
            case 10: { strObs1 = "00000"; strObs2 = "22200"; strObs3 = "11100"; break; }
            case 11: { strObs1 = "NNNNN"; strObs2 = "00000"; strObs3 = "00000"; break; }
            case 12: { strObs1 = "00000"; strObs2 = "0NNN0"; strObs3 = "00000"; break; }
        }
    }

    if (tile == "5" or tile == "6" or tile == "8")
    {
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs1, strTemp, j);
        j += 10;
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs2, strTemp, j);
        j += 10;
        strTemp = string_delete(strTemp, j, 5);
        strTemp = string_insert(strObs3, strTemp, j);
    }
}

// Generate the tiles
for (j = 0; j < 8; j += 1)
{
    for (i = 1; i < 11; i += 1)
    {
        tile = string_char_at(strTemp, i+j*10);
        xpos = x + (i-1)*16;
        ypos = y + j*16;

        //debugging
        if (global.scumTileDebug)
        {
            if (tile != "0")
            {
                obj = instance_create(xpos, ypos, oTileDebug);
                obj.tileString = tile;
            }
        }

        if (tile == "1" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,10) == 1) instance_create(xpos, ypos, oBlock);
            else
            {
                instance_create(xpos, ypos, oBrick);
            }
        }
        else if (tile == "2" and rand(1,2) == 1 and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,10) == 1) instance_create(xpos, ypos, oBlock);
            else
            {
                instance_create(xpos, ypos, oBrick);
            }
        }
        else if (tile == "3" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,12) == 1) instance_create(xpos, ypos, oBlock);
            else if (rand(1,15) == 1) instance_create(xpos, ypos, oLush);
            else
            {
                instance_create(xpos, ypos, oTemple);
            }
        }
        else if (tile == "t" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            instance_create(xpos, ypos, oTemple);
        }
        else if (tile == "L") instance_create(xpos, ypos, oLadderOrange);
        else if (tile == "P") instance_create(xpos, ypos, oLadderTop);
        else if (tile == "7")
        {
            var tc;
            if (global.bizarrePlus and rand(1,3) == 1) tc = 2;
            else tc = 3;

            if (rand(1,ceil(tc/global.trapMult)) == 1)
            {
                if (global.woodSpikes) instance_create(xpos, ypos, oSpikesWood);
                else instance_create(xpos, ypos, scrGenSpikeType());
            }
        }
        else if (tile == "4" and rand(1,4) == 1) instance_create(xpos, ypos, oPushBlock);
        else if (tile == "9")
        {
            block = instance_create(xpos, ypos+16, oBrick);
            if (scrGetRoomX(x) == global.startRoomX and scrGetRoomY(y) == global.startRoomY)
                instance_create(xpos, ypos, oEntrance);
            else
            {
                instance_create(xpos, ypos, oExit);
                global.exitX = xpos+8;
                global.exitY = ypos+8;
                block.invincible = true;
            }
        }
        else if (tile == "A")
        {
            instance_create(xpos, ypos, oAltarLeft);
            instance_create(xpos+16, ypos, oAltarRight);
        }
        else if (tile == "x")
        {
            instance_create(xpos, ypos, oSacAltarLeft);
            instance_create(xpos+16, ypos, oSacAltarRight);
            tile_add(bgKaliBody, 0, 0, 64, 64, xpos-16, ypos-48, 10001);
            instance_create(xpos+16, ypos-80+16, oKaliHead);
        }
        else if (tile == "a")
        {
            instance_create(xpos, ypos, oChest);
        }
        else if (tile == "I")
        {
            instance_create(xpos+16, ypos+12, oGoldIdol);
        }
        else if (tile == "B")
        {
            instance_create(xpos+16, ypos+16, oGiantTikiHead); //ypos+16
            tile_add(bgTiki, 0, 0, 32, 64, xpos, ypos+32, 10001);
            tile_add(bgTikiArms, 16*rand(0,2), 0, 16, 16, xpos+32, ypos+32, 10001);
            tile_add(bgTikiArms, 16*rand(0,2), 16, 16, 16, xpos-16, ypos+32, 10001);
        }
        else if (tile == "Q")
        {
            if (shopType == "Craps")
            {
                tile_add(bgDiceSign, 0, 0, 48, 32, xpos, ypos, 9004);
            }
        }
        else if (tile == "q")
        {
            //???: n = rand(1,6);
            obj = scrGenerateItem(xpos+8, ypos+8, 1);
            obj.inDiceHouse = true;
        }
        else if (tile == "+")
        {
            obj = instance_create(xpos, ypos, oSolid);
            obj.sprite_index = sIceBlock;
            obj.shopWall = true;
        }
        else if (tile == "W")
        {
            if (global.murderer or global.thiefLevel > 0)
            {
                if (global.isDamsel) tile_add(bgWanted, 32, 0, 32, 32, xpos, ypos, 9004);
                else if (global.isTunnelMan) tile_add(bgWanted, 64, 0, 32, 32, xpos, ypos, 9004);
                else tile_add(bgWanted, 0, 0, 32, 32, xpos, ypos, 9004);
            }
        }
        else if (tile == "." and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,10) == 1) obj = instance_create(xpos, ypos, oBlock);
            else
            {
                obj = instance_create(xpos, ypos, oBrick);
            }
            obj.shopWall = true;
        }
        else if (tile == "b")
        {
            obj = instance_create(xpos, ypos, oBrickSmooth);
            obj.shopWall = true;
        }
        else if (tile == "l")
        {
            if (oGame.damsel) instance_create(xpos, ypos, oLampRed);
            else instance_create(xpos, ypos, oLamp);
        }
        else if (tile == "K")
        {
            //obj = instance_create(xpos, ypos, oShopkeeper);
            //obj.style = shopType;
            scrLevGenShopkeeper(xpos, ypos, shopType);
        }
        else if (tile == "k")
        {
            obj = instance_create(xpos, ypos, oSign);
            if (shopType == "General") obj.sprite_index = sSignGeneral;
            else if (shopType == "Bomb") obj.sprite_index = sSignBomb;
            else if (shopType == "Weapon") obj.sprite_index = sSignWeapon;
            else if (shopType == "Clothing") obj.sprite_index = sSignClothing;
            else if (shopType == "Rare") obj.sprite_index = sSignRare;
            else if (shopType == "Craps") obj.sprite_index = sSignCraps;
            else if (shopType == "Kissing") obj.sprite_index = sSignKissing;
        }
        else if (tile == "i")
        {
            scrShopItemsGen(xpos, ypos);
        }
        else if (tile == "d")
        {
            instance_create(xpos+8, ypos+8, oDice);
        }
        else if (tile == "D")
        {
            obj = instance_create(xpos+8, ypos+8, oDamsel);
            obj.forSale = true;
            obj.status = 5;
        }
        else if (tile == "s")
        {
            if (rand(1,10) == 1) instance_create(xpos, ypos, oSnake);
            else if (rand(1,2) == 1) instance_create(xpos, ypos, oBrick);
        }
        else if (tile == "S")
        {
            instance_create(xpos, ypos, oSnake);
        }
        else if (tile == "T")
        {
            instance_create(xpos+8, ypos+8, oRubyBig);
        }
        else if (tile == "M") // mattock hidden in bottom of snake pit
        {
            instance_create(xpos, ypos, oBrick);
            if (global.isTunnelMan)
            {
                obj = instance_create(xpos+8, ypos+6, oRopePile);
                obj.cost = 0;
                obj.forSale = false;
                obj = instance_create(xpos+24, ypos+6, oRopePile);
            }
            else obj = instance_create(xpos+8, ypos+8, oMattock);
            obj.cost = 0;
            obj.forSale = false;
        }
        else if (tile == "H" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,2) == 1) instance_create(xpos, ypos, oLava);
            else instance_create(xpos, ypos, oTemple);
        }
        else if (tile == "^" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,2) == 1) instance_create(xpos, ypos, oWaterSwim);
            else instance_create(xpos, ypos, oTemple);
        }
        else if (tile == "y")
        {
            instance_create(xpos, ypos, oLava);
        }
        else if (tile == "J" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,2) == 1) instance_create(xpos, ypos, oLava);
            else instance_create(xpos, ypos, oBrick);
        }
        else if (tile == "Z" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,2) == 1) instance_create(xpos, ypos, oWaterSwim);
            else instance_create(xpos, ypos, oBrick);
        }
        else if (tile == "N" and not collision_point(xpos, ypos, oSolid, 0, 0))
        {
            if (rand(1,3) == 1) instance_create(xpos, ypos, oTemple);
            else
            {
                instance_create(xpos, ypos, oBrick);
            }
        }
        else if (tile == "w")
        {
            instance_create(xpos, ypos, oWaterSwim);
        }
        else if (tile == "G") // treasure room guard
        {
            switch(rand(1,3))
            {
                case 1:
                {
                    /*
                    obj = instance_create(xpos, ypos, oShopkeeper);
                    obj.outlaw = true;
                    obj.status = 4;
                    obj.hasGun = choose(true, false);
                    obj.hp = choose(10, 15, 20);
                    */

                    repeat(2)
                    {
                        if (rand(1,2) == 1) instance_create(xpos, ypos, oZombie);
                        else instance_create(xpos, ypos, oSkeleton);
                    }
                    break;
                }
                case 2:
                {
                    obj = instance_create(xpos, ypos-16, oTombLord);
                    if (rand(1,2) == 1) obj.carries = "Crate";
                    if (rand(1,3) == 1) instance_create(xpos, ypos, oScarab);
                    break;
                }
                case 3:
                {
                    repeat(3)
                    {
                        obj = instance_create(xpos, ypos, oFloater);
                        if (rand(1,2) == 1)
                        {
                            obj.explosive = true;
                            obj.sprite_index = sFloaterEvil;
                        }
                    }
                    if (rand(1,3) == 1) instance_create(xpos, ypos, oScarab);
                    break;
                }
            }
        }
        else if (tile == "v") // vampire treasure guard (attached to ceiling)
        {
            if (rand(1,4) == 1)
            {
                obj = instance_create(xpos, ypos, oVampire);
                obj.hp = 4;
                obj.status = 6; // HANG
                obj.capeless = true;
                if (rand(1,4) == 1) obj.carries = "Crate";
                else if (rand(1,4) == 1) obj.carries = "Bomb Bag";
                else obj.carries = "Rope Pile";
            }
        }
        else if (tile == "$") // treasure room loot
        {
            switch(rand(1,6))
            {
                case 1:
                case 2:
                case 3: instance_create(xpos+8, ypos+8, oChest); break;
                case 4:
                case 5: instance_create(xpos+8, ypos+8, oCrate); break;
                case 6:
                {
                    instance_create(xpos+8, ypos+10, choose(oEmeraldBig, oSapphireBig, oRubyBig, oDiamond));
                    break;
                }
            }
        }
    }
}
