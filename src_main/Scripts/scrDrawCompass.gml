// scrDrawCompass
// called from scrDrawHUD

if (isRoom("rOlmec")) {
  global.exitX = 648;
  global.exitY = 552;
} else if (isRoom("rOlmec2")) {
  global.exitX = 648;
  global.exitY = 424;
}

if (global.customLevel) {
  // multiple compass arrows
  var cc, sa, n;
  for (n = 0; n < 10; n += 1) {
    cc = global.compassCol[n];
    sa = 1; // compass arrow sprite alpha (arrows appear behind other HUD elements now, no need to make transparent)
    if (global.levelExitX[n] > 0 and global.levelExitY[n] > 0) {
      if (global.levelExitY[n] > view_yview[0]+232) {
             if (global.levelExitX[n] < view_xview[0]+8) draw_sprite_ext(sCompassD, -1, 8, 232, -1, 1, 0, cc, sa); // lower left corner
        else if (global.levelExitX[n] > view_xview[0]+320-8) draw_sprite_ext(sCompassD, -1, 312, 232, 1, 1, 0, cc, sa); // lower right corner
        else {
          // bottom edge
          //if (global.messageTimer > 0) sa = 0.8;
          draw_sprite_ext(sCompassR, -1, global.levelExitX[n]-view_xview[0]-8, 232, 1, 1, 270, cc, sa); // bottom edge
        }
      } else if (global.levelExitY[n] < view_yview[0]) {
             if (global.levelExitX[n] < view_xview[0]+8) draw_sprite_ext(sCompassD, -1, 8, 8, -1, -1, 0, cc, sa); // upper left corner
        else if (global.levelExitX[n] > view_xview[0]+320-8) draw_sprite_ext(sCompassD, -1, 312, 8, 1, -1, 0, cc, sa); // upper right corner
        else draw_sprite_ext(sCompassR, -1, global.levelExitX[n]-view_xview[0], 8, 1, 1, 90, cc, sa); // top edge
      }
      else if (global.levelExitX[n] < view_xview[0]+8) draw_sprite_ext(sCompassR, -1, 8, global.levelExitY[n]-view_yview[0], -1, 1, 0, cc, sa); // left edge
      else if (global.levelExitX[n] > view_xview[0]+320-8) draw_sprite_ext(sCompassR, -1, 312, global.levelExitY[n]-view_yview[0], 1, 1, 0, cc, sa); // right edge
    }
  }
} else {
  // original compass
  if (global.exitY > view_yview[0]+240) {
    if (global.exitX < view_xview[0]) {
           if (global.messageTimer > 0) draw_sprite(sCompassSmallLL, -1, 0, 224);
      else draw_sprite(sCompassLL, -1, 0, 224);
    } else if (global.exitX > view_xview[0]+320-16) {
      if (global.messageTimer > 0) draw_sprite(sCompassSmallLR, -1, 304, 224);
      else draw_sprite(sCompassLR, -1, 304, 224);
    } else {
      if (global.messageTimer > 0) draw_sprite(sCompassSmallDown, -1, global.exitX-view_xview[0], 224);
      else draw_sprite(sCompassDown, -1, global.exitX-view_xview[0], 224);
    }
  } else if (global.exitX < view_xview[0]) {
    if (global.messageTimer > 0) draw_sprite(sCompassSmallLeft, -1, 0, global.exitY-view_yview[0]);
    else draw_sprite(sCompassLeft, -1, 0, global.exitY-view_yview[0]);
  } else if (global.exitX > view_xview[0]+320-16) {
    if (global.messageTimer > 0) draw_sprite(sCompassSmallRight, -1, 304, global.exitY-view_yview[0]);
    else draw_sprite(sCompassRight, -1, 304, global.exitY-view_yview[0]);
  }
}
