/**
 * The game's step event.
 */
var ST_CLIMBING;
ST_CLIMBING = 14; //k8: i'm sorry; this defined in `characterCreateEvent()`, but not here
var cplr, i;

oGame.players[0] = noone; // players (used when "with( )" structures will not work)
oGame.players_length = 0;
with (oCharacter) {
  // necessary to reset the "viscid" movement from a moving solid
  viscidMovementOk = 1;
  // store the characters in the oGame.players variable
  oGame.players[oGame.players_length] = id;
  oGame.players_length += 1;
}

// since we are not using GM's hspeed and vspeed variables, we need to add in decimal
// support ourselves (so 0.25 will only move 1 pixel every 4 steps, for example)
oGame.time += 1;
//we don't want the time to grow too large
if (oGame.time > 100000000) oGame.time = 0;

// moves all of the solids so that none of them collide with the character
with (oMovingSolid) {
  // applies the acceleration
  xVel += xAcc;
  yVel += yAcc;
  // approximates the "active" variables
  if (approximatelyZero(xVel)) xVel = 0;
  if (approximatelyZero(yVel)) yVel = 0;
  if (approximatelyZero(xAcc)) xAcc = 0;
  if (approximatelyZero(yAcc)) yAcc = 0;
  // moves the solid, pushes the character, carries the character, and stops if the character will be crushed by another solid
  mstXPrev = x;
  mstYPrev = y;
  // change the decimal arguments to integer variables with relation to time
  xVelFrac = frac(abs(xVel));
  yVelFrac = frac(abs(yVel));
  xVelInteger = 0;
  yVelInteger = 0;
  if (xVelFrac != 0) {
    if (round(1/xVelFrac) != 0) xVelInteger = (oGame.time mod round(1/xVelFrac) == 0);
  }
  if (yVelFrac != 0) {
    if (round(1/yVelFrac) != 0) yVelInteger = (oGame.time mod round(1/yVelFrac) == 0);
  }
  xVelInteger += floor(abs(xVel));
  yVelInteger += floor(abs(yVel));
  if (xVel < 0) xVelInteger *= -1;
  if (yVel < 0) yVelInteger *= -1;
  xVelInteger = round(xVelInteger);
  yVelInteger = round(yVelInteger);
  // calculate the collision bounds of the character -- we'll need it later
  with (oCharacter) calculateCollisionBounds();
  solidIsNearPlayers = 0; // whether the solid is near either of the players
  //determine if the solid is close to a player
  for (i = 0; i < oGame.players_length; i += 1) {
    cplr = oGame.players[i];
    if (isCollisionRectangle(
         x-abs(xVelInteger)-sprite_xoffset-2,
         y-abs(yVelInteger)-sprite_yoffset-2,
         x+sprite_width+abs(xVelInteger)-sprite_xoffset+2,
         y+sprite_height+abs(yVelInteger)-sprite_yoffset+2,
         cplr.lb,
         cplr.tb,
         cplr.rb,
         cplr.bb))
    {
      solidIsNearPlayers = 1;
      break;
    }
  }

  if (solidIsNearPlayers) {
    if (xVelInteger != 0) {
      // solid is moving horizontally
      var xdir;
      if (xVelInteger > 0) xdir = 1; else xdir = -1;
      breakNow = 0; // whether we should break out of the movement loop because the character is stuck
      while (!breakNow) {
        if (xVelInteger > 0) {
          if (x >= mstXPrev+xVelInteger) break;
        } else {
          if (x <= mstXPrev+xVelInteger) break;
        }
        for (i = 0; i < oGame.players_length; i += 1) {
          // climbing - added in YASM 1.7
          cplr = oGame.players[i];
          if (cplr.state != 14) {
            if (viscidTop && isCollisionCharacterTop(1, cplr) && (cplr.viscidMovementOk == 1 || cplr.viscidMovementOk == 2)) {
              with (cplr) {
                if (xVelInteger > 0) {
                  if (isCollisionRight(1) == 0) { x += 1; viscidMovementOk = 2; }
                } else {
                  if (isCollisionLeft(1) == 0) { x -= 1; viscidMovementOk = 2; }
                }
              }
            } else {
              var coll;
              if (xVelInteger > 0) coll = isCollisionCharacterRight(1, cplr); else coll = isCollisionCharacterLeft(1, cplr);
              if (coll) {
                with (cplr) {
                  if (xVelInteger > 0) collision = isCollisionRight(1); else collision = isCollisionLeft(1);
                }
                if (cplr.collision) { breakNow = 1; break; }
                cplr.x += xdir;
              }
            }
            if (breakNow) break;
          }
        }
        x += xdir;
      }
    }

    if (yVelInteger != 0) {
      // solid is moving vertically
      var ydir;
      if (yVelInteger > 0) ydir = 1; else ydir = -1;
      breakNow = 0; // whether we should break out of the movement loop because the character is stuck
      while (!breakNow) {
        if (yVelInteger > 0) {
          if (y >= mstYPrev+yVelInteger) break;
        } else {
          if (y <= mstYPrev+yVelInteger) break;
        }
        for (i = 0; i < oGame.players_length; i += 1) {
          cplr = oGame.players[i];
          if (yVelInteger > 0) {
            if (viscidTop && isCollisionCharacterTop(2, cplr)) {
              // since we do not want to include the solid that is pulling the character down,
              // we must alter the position of the solid to get around this dilemma
              y += 5;
              with (cplr) { if (isCollisionBottom(1) == 0) y += 1; }
              y -= 5;
            } else if (isCollisionCharacterBottom(1, cplr)) {
              with (cplr) collision = isCollisionBottom(1);
              if (cplr.collision) { breakNow = 1; break; }
              cplr.y += 1;
            }
          } else {
            // push the character up regardless of the viscid properties of the solid top
            if (isCollisionCharacterTop(1, cplr)) {
              with (cplr) collision = isCollisionTop(1);
              if (cplr.collision) { breakNow = 1; break; }
              cplr.y -= 1;
            }
            if (isCollisionCharacterBottom(1, cplr)) {
              // variable jumping causes the character to get stuck to the bottom of a moving solid
              // that is moving faster than 1 pixel per step upwards, so we need this code
              if (cplr.jumpTime < cplr.jumpTimeTotal) {
                cplr.yVel = -2;
                cplr.jumpTime = cplr.jumpTimeTotal;
              }
            }
          }
          /////////////
          if (breakNow) break;
        }
        y += ydir;
      }
    }
    with (oCharacter) { if (viscidMovementOk == 2) viscidMovementOk = 0; }
  } else {
    // solid is not near players
    x += xVelInteger;
    y += yVelInteger;
  }
  /*
  if (place_meeting(x, y+1, oItem)) {
    yVel = 0;
    myGrav = 0;
    //break;
  } else {
    myGrav = 1;
  }
  */
}
// finished oMovingSolid code

// accelerates the oMoveableSolid objects downwards
with (oMoveableSolid) {
  if (x > view_xview[0]-16 && x < view_xview[0]+view_wview[0] &&
      y > view_yview[0]-16 && y < view_yview[0]+view_hview[0])
  {
    yMPrev = y;
    yVel += myGrav;
    if (yVel > 8) yVel = 8;
    //yVel += oGame.moveableSolidGrav;
    // moves the moveable solid down
    for (y = y; y < yMPrev+yVel; y += 1) {
      // if there is a collision with a solid or the character one pixel below the moveable solid, we want it to stop
      // is there a (precise) collision
      if (place_meeting(x, y+1, oSolid)) { // or isCollisionCharacterBottom(2))
        if (yVel > myGrav) playSound(global.sndThud);
        yVel = 0;
        break;
      }
      //scrMoveableSolidRecurseDrop();
      /*
      obj = 0;
      obj = instance_place(x, y-1, oEnemy);
      if (obj) obj.y += 1;
      obj = 0;
      obj = instance_place(x, y-1, oItem);
      if (obj) obj.y += 1;
      obj = 0;
      obj = instance_place(x, y-1, oTreasure);
      if (obj) obj.y += 1;
      obj = 0;
      */
    }
  }
}
