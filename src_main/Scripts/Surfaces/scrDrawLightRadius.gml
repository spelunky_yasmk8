// called from scrSurface* scripts (oScreen)

if (instance_exists(oLampRed)) {
  with (oPlayer1) {
    distToLamp = distance_to_object(oLampRed);
    if (distToLamp <= 96) draw_set_color(make_color_rgb(255-distToLamp, 120-(96-distToLamp), 120-(96-distToLamp)));
  }
}
if (instance_exists(oLampRedItem)) {
  with (oPlayer1) {
    distToLamp = distance_to_object(oLampRedItem);
    if (distToLamp <= 96) draw_set_color(make_color_rgb(255-distToLamp, 120-(96-distToLamp), 120-(96-distToLamp)));
  }
}
draw_circle(oPlayer1.x-view_xview[0], oPlayer1.y-view_yview[0], 96-64*oLevel.darkness, false);

if (global.scumDarkness == 3) {
  // always light player
  with (oPlayer1) draw_circle(x-view_xview[0], y-view_yview[0], 96, false);
}

with (oFlare) draw_circle(x-view_xview[0], y-view_yview[0], 96, false);
with (oFlareCrate) draw_circle(x-view_xview[0], y-view_yview[0], 96, false);
with (oLamp) draw_circle((x+8)-view_xview[0], (y+8)-view_yview[0], 96, false);
with (oLampItem) draw_circle(x-view_xview[0], (y-4)-view_yview[0], 96, false);
with (oArrowTrapLeftLit) draw_circle((x+8)-view_xview[0], (y+8)-view_yview[0], 32, false);
with (oArrowTrapRightLit) draw_circle((x+8)-view_xview[0], (y+8)-view_yview[0], 32, false);
with (oTikiTorch) draw_circle((x+8)-view_xview[0], (y+8)-view_yview[0], 32, false);
with (oFireFrog) draw_circle((x+8)-view_xview[0], (y+8)-view_yview[0], 32, false);
with (oMagma) draw_circle((x+8)-view_xview[0], (y+8)-view_yview[0], 32, false);
with (oMagmaMan) draw_circle((x+8)-view_xview[0], (y+8)-view_yview[0], 32, false);
with (oFireball) draw_circle((x+8)-view_xview[0], (y+8)-view_yview[0], 32, false);
with (oSpearTrapLit) draw_circle((x+8)-view_xview[0], (y+8)-view_yview[0], 32, false);
with (oSmashTrapLit) draw_circle((x+8)-view_xview[0], (y+8)-view_yview[0], 32, false);
with (oThwompTrapLit) draw_circle((x+8)-view_xview[0], (y+8)-view_yview[0], 32, false);
with (oExplosion) draw_circle(x-view_xview[0], y-view_yview[0], 96, false);
with (oLava) draw_circle((x+8)-view_xview[0], (y+8)-view_yview[0], 32, false);
with (oScarab) draw_circle((x+8)-view_xview[0], (y+8)-view_yview[0], 16, false);
with (oGhost) draw_circle((x+16)-view_xview[0], (y+16)-view_yview[0], 64, false);
// YASM 1.7.3
/*k8: ??? is obj?
with (oObjectLabel) {
  if (instance_exists(obj)) {
    draw_set_color(c_white);
    draw_set_alpha(0.66);
    draw_roundrect(obj.bbox_left-1-view_xview[0], obj.bbox_top-1-view_yview[0], obj.bbox_right+1-view_xview[0], obj.bbox_bottom+1-view_yview[0], true);
    draw_set_alpha(1);
    //draw_set_alpha(max(0, alpha-0.5));
    draw_rectangle(x1-view_xview[0], y1-view_yview[0], x2-view_xview[0], y2-view_yview[0], false);
  }
}
*/
