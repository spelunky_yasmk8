// scrDrawHelpPage()
// called by oHelpPages
//draw_set_color(c_white);
//draw_rectangle(view_xview[0]+10, view_yview[0]+16, view_xview[0]+310, view_yview[0]+234, 0);
var i, tx, ty, tty, drawCount, helptext, ghost, txoff, ll;

draw_set_color(c_black);
if (isRoom("rHighscores") or isRoom("rTitle")) draw_set_alpha(0.9); else draw_set_alpha(0.8);
//draw_rectangle(view_xview[0]+12, view_yview[0]+18, view_xview[0]+308, view_yview[0]+232, 0);
draw_rectangle(0, 0, 320, 240, false);
draw_set_alpha(1);
draw_set_color(c_white);
draw_set_font(global.myFontSmall);

if (helpPage == helpPageConfig) {
  // config
  if (uiDrawCurItem > uiItemCount-1) uiDrawCurItem = uiItemCount-1;
  if (uiDrawCurItem < 0) uiDrawCurItem = 0;
  if (uiDrawCurItem < uiDrawPageTop) uiDrawPageTop = uiDrawCurItem;
  while (uiDrawPageTop+uiItemsOnPage-1 < uiDrawCurItem) uiDrawPageTop += 1; // yep, i'm ashamed
  // draw it
  helptext = "";
  tx = 16;
  tty = 2;
  draw_text(tx, tty, "CONFIGURATION (PRESS 'S' TO SAVE)"); tty += 10;
  if (ycfgMessageTime > 0) {
    draw_set_color(c_yellow);
    draw_text((320-string_length(ycfgMessage)*8-4), tty, ycfgMessage);
  }
  tty += 10;
  drawCount = 0;
  if (uiDrawPageTop > 0) {
    draw_set_color(c_olive);
    //draw_text(4, tty, "*");
    draw_sprite(sPageUp, 0, 4, tty-1);
  }
  for (i = 0; i < uiItemCount; i += 1) {
    if (i < uiDrawPageTop) continue;
    ty = tty+(i-uiDrawPageTop)*8;
    drawCount += 1;
    if (drawCount > uiItemsOnPage) {
      draw_set_color(c_olive);
      draw_set_alpha(1);
      //draw_text(4, ty-8, "*");
      draw_sprite(sPageDown, 0, 4, ty-8);
      break;
    }
    if (uiDisabled[i]) {
      draw_set_color(c_dkgray);
      draw_text(tx+4*8, ty, uiText[i]);
      draw_set_color(c_white);
      continue;
    } else {
      if (i == uiDrawCurItem) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      //draw_set_color(c_white);
      draw_text(tx+4*8, ty, uiText[i]);
    }
    if (i == uiDrawCurItem) helptext = uiInfoText[i];
    if (uiType[i] == "checkbox") {
      draw_text(tx, ty, "[ ]");
      if (uiCurValue[i]) {
        draw_set_color(c_red);
        draw_text(tx+8, ty, "x");
        if (i == uiDrawCurItem) draw_set_color(c_yellow); else draw_set_color(c_white);
      }
    } else if (uiType[i] == "updown") {
      draw_set_color(c_red);
      if (uiVarName[i] == "scumDarkness") {
        var dark;
        dark = "DEFAULT";
             if (global.scumDarkness == 0) dark = "NEVER";
        else if (global.scumDarkness == 2) dark = "ALWAYS";
        draw_text(tx+4*8+string_length(uiText[i])*8, ty, dark);
      } else if (uiVarName[i] == "scumGhost") {
        var g;
        g = global.scumGhost;
        ghost = "INSTANT";
        if (g != 666) {
               if (g < 1) ghost = "NEVER";
          else if (g <= 10) ghost = "10 SEC";
          else if (g <= 30) ghost = "30 SEC";
          else if (g <= 60) ghost = "60 SEC";
          else if (g <= 90) ghost = "90 SEC";
          else if (g <= 120) ghost = "2 MIN";
          else if (g <= 180) ghost = "3 MIN";
          else if (g <= 300) ghost = "5 MIN";
          else if (g <= 600) ghost = "10 MIN";
          else if (g <= 900) ghost = "15 MIN";
          else ghost = "20 MIN";
        }
        draw_text(tx+4*8+string_length(uiText[i])*8, ty, ghost);
      } else if (uiVarName[i] == "optRoomStyle") {
        draw_set_color(c_red);
        if (uiCurValue[i] == 0)
          draw_text(tx+4*8+string_length(uiText[i])*8, ty, "CLASSIC");
        else if (uiCurValue[i] == 1)
          draw_text(tx+4*8+string_length(uiText[i])*8, ty, "BIZARRE");
        else
          draw_text(tx+4*8+string_length(uiText[i])*8, ty, "RANDOM");
      } else {
        draw_set_color(c_red);
        draw_text(tx+4*8+string_length(uiText[i])*8, ty, string(uiCurValue[i]));
      }
    }
  }
  draw_set_color(c_yellow);
  draw_set_alpha(1);
  ty = tty+(uiItemsOnPage+1)*8;
  while (string_length(helptext) > 0) {
    var pos;
    pos = string_pos("#", helptext);
    if (pos < 1) pos = string_length(helptext)+1;
    draw_text(tx, ty, string_copy(helptext, 1, pos-1));
    helptext = string_delete(helptext, 1, pos);
    ty += 8;
  }
  draw_set_color(c_white);
} else if (helpPage > 0) {
  tx = 16;
  txoff = 0; // text x pos offset (for multi-color lines)
  ty = 8;
  draw_text(tx, ty, "HELP (PAGE "+string(helpPage)+" OF "+string(helpPageMax)+")");
  ty += 24;
  switch (helpPage) {
    case 1:
      draw_set_color(c_yellow);
      draw_text(tx, ty, "INVENTORY BASICS"); ty += 16;
      draw_set_color(c_white);
      draw_text(tx, ty, "Press"); txoff += 48;
      draw_set_color(c_lime); draw_text(tx+txoff, ty, "SWITCH"); txoff += 56;
      draw_set_color(c_white); draw_text(tx+txoff, ty, "to cycle through items."); ty += 16;
      draw_set_color(c_silver);
      draw_text(tx, ty, "KEYBOARD: " + scrGetKey(global.keyItemVal)); ty += 8;
      if (global.gamepadOn) draw_text(tx, ty, "GAMEPAD: " + global.joyItemLabel);
      ty += 56;
      draw_set_color(c_white);
      draw_sprite(sHelpSprite1, -1, 64, 96);
      break;
    case 2:
      draw_set_color(c_yellow);
      draw_text(tx, ty, "SELLING TO SHOPKEEPERS"); ty += 16;
      draw_set_color(c_white);
      draw_text(tx, ty, "Press"); txoff += 48;
      draw_set_color(c_lime); draw_text(tx+txoff, ty, "PAY"); txoff += 32;
      draw_set_color(c_white); draw_text(tx+txoff, ty, "to offer your currently"); ty += 8;
      draw_text(tx, ty, "held item to the shopkeeper."); ty += 16;
      draw_text(tx, ty, "If the shopkeeper is interested, "); ty += 8;
      txoff = 0;
      draw_text(tx, ty, "press"); txoff += 48;
      draw_set_color(c_lime); draw_text(tx+txoff, ty, "PAY"); txoff += 32;
      draw_set_color(c_white); draw_text(tx+txoff, ty, "again to complete the sale."); ty += 72;
      draw_sprite(sHelpSell, -1, 112, 100);
      draw_text(tx, ty, "Purchasing goods from the shopkeeper"); ty += 8;
      draw_text(tx, ty, "will increase the funds he has"); ty += 8;
      draw_text(tx, ty, "available to buy your unwanted stuff."); ty += 8;
      break;
    case 3:
      draw_set_color(c_yellow);
      draw_text(tx, ty, "CONTROL REFERENCE"); ty += 16;
      draw_set_color(c_silver);
      draw_text(tx, ty, "Command");
      draw_text(tx+104, ty, "Keyboard");

      if (global.gamepadOn) {
        draw_text(tx+208, ty, "Gamepad");
        ll = 264;
      } else {
        ll = 184;
      }

      draw_set_color(c_dkgray);
      draw_set_alpha(1);
      for (i = 1; i <= 13; i += 1) draw_line(16, 62+i*8, 16+ll, 62+i*8);
      draw_set_alpha(1);

      ty += 16;
      i = 0;
      draw_set_color(c_white);
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, "UP"); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, "DOWN"); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, "LEFT"); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, "RIGHT"); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, "JUMP"); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, "ACTION/USE"); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, "SWITCH ITEM"); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, "RUN"); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, "BOMB"); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, "ROPE"); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, "PAY/SELL"); ty += 8; i += 1;
      draw_set_color(c_gray); draw_set_alpha(1);
      draw_text(tx, ty, "PAUSE/MENU"); ty += 8; i += 1;
      draw_text(tx, ty, "SCREENSHOT");

      tx += 104;
      ty = 64;
      i = 0;
      if (!kcfgWantKey) i = 666;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, scrGetKey(global.keyUpVal)); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, scrGetKey(global.keyDownVal)); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, scrGetKey(global.keyLeftVal)); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, scrGetKey(global.keyRightVal)); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, scrGetKey(global.keyJumpVal)); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, scrGetKey(global.keyAttackVal)); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, scrGetKey(global.keyItemVal)); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, scrGetKey(global.keyRunVal)); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, scrGetKey(global.keyBombVal)); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, scrGetKey(global.keyRopeVal)); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_text(tx, ty, scrGetKey(global.keyPayVal)); ty += 8; i += 1;
      if (kcfgCurItem == i) { draw_set_color(c_yellow); draw_set_alpha(glowAlpha); } else { draw_set_color(c_white); draw_set_alpha(1); }
      draw_set_color(c_white); draw_set_alpha(1);
      draw_text(tx, ty, "ESCAPE"); ty += 8;
      draw_text(tx, ty, "F9");

      ll = 192; // row seperator
      if (global.gamepadOn) {
        ll = 288;
        tx += 104;
        ty = 64;
        draw_text(tx, ty, "UP"); ty += 8;
        draw_text(tx, ty, "DOWN"); ty += 8;
        draw_text(tx, ty, "LEFT"); ty += 8;
        draw_text(tx, ty, "RIGHT"); ty += 8;
        draw_text(tx, ty, global.joyJumpLabel); ty += 8;
        draw_text(tx, ty, global.joyAttackLabel); ty += 8;
        draw_text(tx, ty, global.joyItemLabel); ty += 8;
        draw_text(tx, ty, global.joyRunLabel); ty += 8;
        draw_text(tx, ty, global.joyBombLabel); ty += 8;
        draw_text(tx, ty, global.joyRopeLabel); ty += 8;
        draw_text(tx, ty, global.joyPayLabel); ty += 8;
        draw_text(tx, ty, global.joyStartLabel);
      }

      tx = 16;
      ty += 24;
      draw_text(tx, ty, "Press 'O' to enter Setup."); ty += 8;
      break;
  }
  draw_set_color(c_yellow);
  draw_text(8, 232, "PRESS SWITCH/LEFT/RIGHT TO CHANGE PAGE");
} else if (helpPage == -1) {
  // map
  draw_set_font(global.myFont);
  draw_set_color(c_white);
  draw_text(136, 8, "MAP");
  draw_set_color(c_silver);
  draw_set_font(global.myFontSmall);
  draw_text(mapTitleX, 24, global.mapTitle);
  draw_set_color(c_white);
  draw_sprite(global.mapSprite, -1, mapX, mapY);
  if (global.mapSprite != sMapDefault) {
    if (global.mapIcon1x >= 0 and global.mapIcon1y >= 0) draw_sprite(global.mapIcon1, -1, mapX+global.mapIcon1x, mapY+global.mapIcon1y);
    if (global.mapIcon2x >= 0 and global.mapIcon2y >= 0) draw_sprite(global.mapIcon2, -1, mapX+global.mapIcon2x, mapY+global.mapIcon2y);
    if (global.mapIcon3x >= 0 and global.mapIcon3y >= 0) draw_sprite(global.mapIcon3, -1, mapX+global.mapIcon3x, mapY+global.mapIcon3y);
  }
  draw_set_color(c_yellow);
  draw_text(8, 232, "PRESS SWITCH/LEFT/RIGHT TO CHANGE PAGE");
  draw_set_color(c_white);
} else {
  draw_set_color(c_yellow);
  draw_text(32, 232, "PRESS ITEM SWITCH FOR HELP PAGE");
}
