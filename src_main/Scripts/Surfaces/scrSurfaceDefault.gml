// called by oScreen > Begin Step
var n, s, m, s2, m2, strLen, obj;

// parallax scrolling
if (global.parallax) {
  if (isLevel()) {
    background_x[0] = view_xview[0]*0.4;
    background_y[0] = view_yview[0]*0.4;
  }
}

if (!surface_exists(pSurf)) pSurf = surface_create(screen_w, screen_h);
if (!surface_exists(screen)) screen = surface_create(screen_w, screen_h);
if (!surface_exists(darkSurf)) darkSurf = surface_create(screen_w, screen_h);

if (checkBombPressed()) {
  if (paused && global.plife > 0 && isLevel() && helpPage == 0) {
    if (!instance_exists(oTextPage)) {
      instance_activate_all();
      paused = false;
      with (oPlayer1) {
        if (facing == 18) xVel = -3; else xVel = 3;
        yVel = -6;
        global.plife = -99;
        if (isRealLevel()) global.miscDeaths[12] += 1;
      }
      scrSetMusicVolume(global.musicVol);
      if (!global.hasAnkh) stopAllMusic();
    }
  }
} else if (checkRopePressed()) {
  if (paused) {
    if (!instance_exists(oTextPage)) game_end();
  }
} else if (checkStartPressed()) {
  if (!paused && canPause) {
    if (instance_exists(oPlayer1)) {
      if (!oPlayer1.dead) {
        scrDevScreenshot();

        surface_set_target(pSurf);
        screen_redraw();

        if (!global.gahara) scrSetMusicVolume(-1);

        px = oPlayer1.x;
        py = oPlayer1.y;

        if (instance_exists(oTextPage)) {
          instance_deactivate_all(true);
          instance_activate_object(oTextPage);
          instance_activate_object(oGamepad);
        } else {
          if (global.darkLevel) draw_set_alpha(1); else draw_set_alpha(pauseAlpha);
          draw_set_color(c_black);
          draw_rectangle(0, 0, screen_w*screen_scale, screen_h*screen_scale, false);
          draw_set_alpha(1);

          obj = instance_create(x, y, oHelpPages);
               if (helpJump > 0 || helpJump == -1) { obj.helpPage = helpJump; helpJump = 0; }
          else if (!isLevel()) { obj.helpPage = 3; obj.hideInventory = true; } // start help at controls reference
          instance_deactivate_all(true);
          instance_activate_object(oHelpPages);
          instance_activate_object(oGamepad);
        }
        paused = true;
      }
    }
  } else {
    instance_activate_all();
    if (!global.gahara) scrSetMusicVolume(global.musicVol);
    if (instance_exists(oHelpPages)) with (oHelpPages) instance_destroy();
    if (instance_exists(oTextPage)) with (oTextPage) instance_destroy();
    paused = false;
  }
}

surface_reset_target();
draw_clear(0);
if (paused) {
  if (instance_exists(oTextPage)) {
    surface_reset_target();
    draw_clear(0);
    surface_set_target(pSurf);
  } else {
    surface_reset_target();
    draw_clear(0);
    surface_set_target(pSurf);

    draw_set_font(global.myFont);
    draw_set_color(c_white);
    draw_text(112, 200+8, "PAUSED");
    draw_set_font(global.myFontSmall);
    if (isLevel()) {
      drawStats = true;
      with (oHelpPages) { if (helpPage != 0) other.drawStats = false; }
      if (drawStats) {
        n = 24; // 40;
        /*
        if (global.currLevel < 1) scrCenterText("TUTORIAL CAVE", global.myFontSmall, n-24, c_white);
        else if (isRoom("rLoadLevel")) scrCenterText(global.customLevelName + " BY " + global.customLevelAuthor, global.myFontSmall, n-24, c_white);
        else scrCenterText("LEVEL " + string(global.currLevel), global.myFontSmall, n-24, c_white);
        */
             if (global.currLevel < 1) draw_text(70, n-16, "TUTORIAL CAVE");
        else if (isRoom("rLoadLevel")) draw_text(70, n-16, global.customLevelName+" by "+global.customLevelAuthor);
        else draw_text(70, n-16, "LEVEL: "+string(global.currLevel));

             if (global.scumMetric) draw_text(70, n-8, "DEPTH: "+string((174.8*(global.currLevel-1)+(py+8)*0.34)*0.3048) + " METRES");
        else draw_text(70, n-8, "DEPTH: " + string(174.8*(global.currLevel-1)+(py+8)*0.34) + " FEET");
        draw_text(70, n, "MONEY: "+string(global.money));
        draw_text(70, n+8, "KILLS: "+string(global.kills));
        s = global.xtime;
        s = floor(s/1000);
        m = s div 60;
        s = s mod 60;
        s = string(s);
        while (string_length(s) < 2) s = "0"+s;
        s2 = global.time;
        s2 = floor(s2/1000);
        m2 = s2 div 60;
        m2 = m2 mod 60;
        s2 = string(s);
        while (string_length(s2) < 2) s2 = "0"+s2;
        draw_text(70, n+16, "TIME:  "+string(m)+":"+s+" / "+string(m2)+":"+s2);
        draw_text(70, n+24, "SAVES: "+string(global.damsels));
        draw_text(60, n+24+4*8, "PRESS 'O' TO ENTER SETUP.");
      }
    }
    scrDrawPauseFooter();
  }
  surface_reset_target();
  draw_set_blend_mode_ext(bm_one, bm_zero); // According to ChevyRay, this should fix the black box glitch
  draw_surface_stretched(screen, screen_x, screen_y, screen_w*screen_scale, screen_h*screen_scale);
  draw_surface_stretched(pBack, screen_x, screen_y, screen_w*screen_scale, screen_h*screen_scale);
  draw_set_blend_mode(bm_normal); // According to ChevyRay, this should fix the black box glitch
  draw_surface_stretched(pSurf, screen_x, screen_y, screen_w*screen_scale, screen_h*screen_scale);
} else {
  if (isRoom("rTitle")) {
    surface_set_target(screen);
    draw_set_alpha(oTitle.darkness);
    draw_set_color(c_black);
    if (oTitle.darkness > 0) draw_rectangle(0, 0, screen_w, screen_h, false);
    /*
    if (oTitle.state == 1) {
      draw_set_font(global.myFontSmall);
      draw_set_color(c_white);
      draw_text(88, 48, "DEREK YU PRESENTS");
    }
    */
    draw_set_alpha(1);
    draw_set_blend_mode_ext(bm_src_color, bm_one);
    draw_set_color(c_black);
    draw_rectangle(0, 0, screen_w, screen_h, 0);
    draw_set_blend_mode(bm_normal);
    surface_reset_target();
  } else if (isLevel() && instance_exists(oPlayer1)) {
    if (global.darkLevel && !oPlayer1.dead) {
      surface_set_target(darkSurf);
      draw_set_color(c_black);
      draw_rectangle(0, 0, screen_w, screen_h, false);
      draw_set_color(make_color_rgb(255-255*oLevel.darkness, 255-255*oLevel.darkness, 255));
      scrDrawLightRadius();
      surface_set_target(screen);
      draw_set_blend_mode_ext(bm_dest_color, bm_zero);
      draw_set_alpha(1);
      draw_surface(darkSurf, 0, 0);
      draw_set_blend_mode(bm_normal);
    }
    surface_set_target(screen);
    scrDrawHUD();
    if (global.messageTimer > 0) {
      draw_set_font(global.myFontSmall);
      draw_set_color(c_white);
      strLen = string_length(global.message)*8;
      n = 320-strLen;
      n = ceil(n / 2);
      draw_text(n, 216, string(global.message));

      if (!isRoom("rTutorial")) draw_set_color(c_yellow);
      strLen = string_length(global.message2)*8;
      n = 320-strLen;
      n = ceil(n / 2);
      draw_text(n, 224, string(global.message2));

      global.messageTimer -= 1;
    }
    draw_set_blend_mode_ext(bm_src_color, bm_one);
    draw_set_color(c_black);
    draw_rectangle(0, 0, screen_w, screen_h, 0);
    draw_set_blend_mode(bm_normal);
    surface_reset_target();
  }
  draw_set_blend_mode_ext(bm_one, bm_zero); // According to ChevyRay, this should fix the black box glitch
  draw_surface_stretched(screen, screen_x, screen_y, screen_w*screen_scale, screen_h*screen_scale);
  draw_set_blend_mode(bm_normal); // According to ChevyRay, this should fix the black box glitch
}
/*
if (oPlayer1.holdItem != 0) {
  if (oPlayer1.holdItem.type == "Damsel") {
    draw_set_font(global.myFont);
    draw_set_color(c_white);
    draw_text(1, 100, "DROPPED:"+string(oPlayer1.holdItem.damselDropped));
  }
}
*/
if (global.ddamselDebug) {
  if (instance_exists(oDamsel)) {
    var dms;
    dms = instance_find(oDamsel, 0);
    draw_set_font(global.myFont);
    draw_set_color(c_white);
    with (dms) draw_text(1, 100, "DROPPED:"+string(damselDropped));
  }

  draw_set_font(global.myFont);
  draw_set_color(c_white);
  draw_text(1, 120, "DDOUBLE:"+string(global.xdamseldouble));

  draw_set_font(global.myFont);
  draw_set_color(c_white);
  draw_text(1, 140, "XDAMSELS:"+string(global.xdamsels));
}

if (global.k8objTest >= 0) {
  draw_set_font(global.myFont);
  draw_set_color(c_white);
  draw_text(1, 100, "K8OBJTEST0:"+string(global.k8objTest));
  draw_text(1, 120, "K8OBJTEST1:"+string(global.k8objTest1));
  draw_text(1, 140, "K8OBJTEST2:"+string(global.k8objTest2));
}

if (global.optVSync) screen_wait_vsync(); //k8
screen_refresh();
