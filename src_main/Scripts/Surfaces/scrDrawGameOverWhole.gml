// called by oGame > Draw

if (isRealLevel() or isRoom("rTutorial") or isRoom("rTutorial2")) {
  if (drawStatus > 0) scrCenterScoreWhole("GAME OVER", global.myFont, 96, c_red);
  if (drawStatus > 1) scrCenterScoreWhole("FINAL SCORE:", global.myFontSmall, 144, c_yellow);
  if (drawStatus > 2) {
    scrCenterScoreWhole("$"+string(moneyCount), global.myFont, 160, c_white);
    if (global.gamepadOn) {
      scrCenterScoreWhole("PRESS " + global.joyAttackLabel + " FOR HIGH SCORES.", global.myFontSmall, 256, c_yellow);
    } else {
      scrCenterScoreWhole("PRESS " + scrGetKey(global.keyAttackVal) + " FOR HIGH SCORES.", global.myFontSmall, 256, c_yellow);
    }
  }
} else {
  // custom level, test level, generated level
  if (global.customLevel) {
    // allow retry at checkpoint
    if (drawStatus > 0) scrCenterScoreWhole("YOU DIED", global.myFont, 96, c_yellow);
    if (drawStatus > 1) scrCenterScoreWhole("CURRENT SCORE:", global.myFontSmall, 144, c_yellow);
    if (drawStatus > 2) {
      scrCenterScoreWhole("$" + string(moneyCount), global.myFont, 160, c_white);
      if (global.gamepadOn) {
        if (global.testLevel != "") scrCenterScoreWhole("PRESS " + global.joyItemLabel + " TO EDIT LEVEL.", global.myFontSmall, 304, c_yellow);
        else scrCenterScoreWhole("PRESS " + global.joyItemLabel + " TO LOAD ANOTHER LEVEL.", global.myFontSmall, 304, c_yellow);
        scrCenterScoreWhole("PRESS " + global.joyAttackLabel + " TO TRY AGAIN.", global.myFontSmall, 256, c_lime);
      } else {
        if (global.testLevel != "") scrCenterScoreWhole("PRESS " + scrGetKey(global.keyItemVal) + " TO EDIT LEVEL.", global.myFontSmall, 304, c_yellow);
        else scrCenterScoreWhole("PRESS " + scrGetKey(global.keyItemVal) + " TO LOAD ANOTHER LEVEL.", global.myFontSmall, 304, c_yellow);
        scrCenterScoreWhole("PRESS " + scrGetKey(global.keyAttackVal) + " TO TRY AGAIN.", global.myFontSmall, 256, c_lime);
      }
    }
  } else {
    // no retry available
    if (drawStatus > 0) scrCenterScoreWhole("GAME OVER", global.myFont, 96, c_red);
    if (drawStatus > 1) scrCenterScoreWhole("FINAL SCORE:", global.myFontSmall, 144, c_yellow);
    if (drawStatus > 2) {
      scrCenterScoreWhole("$" + string(moneyCount), global.myFont, 160, c_white);
      if (global.gamepadOn) {
        scrCenterScoreWhole("PRESS " + global.joyAttackLabel + " FOR MENU.", global.myFontSmall, 256, c_yellow);
      } else {
        scrCenterScoreWhole("PRESS " + scrGetKey(global.keyAttackVal) + " FOR MENU.", global.myFontSmall, 256, c_yellow);
      }
    }
  }
}
