// scrCenterScoreWhole(string, font, ypos, color)
// used to center text on "game over" screen for Whole View levels
var str, strLen, fontWidth, n;
str = argument0;
if (argument1 == global.myFont) fontWidth = 32; else fontWidth = 16;
draw_set_font(argument1);
draw_set_color(argument3);
strLen = string_length(str)*fontWidth;
n = 672 - strLen;
n = ceil(n / 2);
draw_text_transformed(view_xview[0]+n, view_yview[0]+argument2, str, 2, 2, 0);
