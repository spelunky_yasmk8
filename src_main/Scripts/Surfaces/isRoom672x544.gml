// returns if room size is 672x544
// called by oScreen > Begin Step

return
  (isRoom("rLevel") or
   isRoom("rLoadLevel") or
   isRoom("rLevelEditor"));
