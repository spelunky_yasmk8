// scrSetMapPos
// for 'real' levels, sets player icon and exit icon positions for the world maps

var mx, my, px, py;
mx = -1;
my = -1;
px = oScreen.px / 16;
py = oScreen.py / 16;

// set position of player icon
if (global.currLevel == 1) { mx = 81; my = 22; }
else if (global.currLevel == 2) { mx = 113; my = 63; }
else if (global.currLevel == 3) { mx = 197; my = 86; }
else if (global.currLevel == 4) { mx = 133; my = 109; }
else if (global.currLevel == 5) { mx = 181; my = 22; }
else if (global.currLevel == 6) { mx = 126; my = 64; }
else if (global.currLevel == 7) { mx = 158; my = 112; }
else if (global.currLevel == 8) { mx = 66; my = 80; }
else if (global.currLevel == 9) { mx = 30; my = 26; }
else if (global.currLevel == 10) { mx = 88; my = 54; }
else if (global.currLevel == 11) { mx = 148; my = 81; }
else if (global.currLevel == 12) { mx = 210; my = 205; }
else if (global.currLevel == 13) { mx = 66; my = 17; }
else if (global.currLevel == 14) { mx = 146; my = 17; }
else if (global.currLevel == 15) { mx = 82; my = 77; }
else if (global.currLevel == 16) { mx = 178; my = 81; }

global.mapIcon1x = mx + px;
global.mapIcon1y = my + py;

// set position of exit door icon
if (global.hasCompass)
{
    global.mapIcon2 = sMapRedDot;
    global.mapIcon2x = mx + global.exitX / 16;
    global.mapIcon2y = my + global.exitY / 16;
}
else
{
    global.mapIcon2 = sInvisible;
    global.mapIcon2x = -1;
    global.mapIcon2y = -1;
}
