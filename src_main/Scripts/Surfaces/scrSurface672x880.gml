// called by oScreen > Begin Step
var n, s, m, str, s2, m2, str2, strLen;

if (not surface_exists(pSurf880)) pSurf880 = surface_create(672, 880);
if (not surface_exists(screen880)) screen880 = surface_create(672, 880);

if (checkBombPressed())
{
    if (paused and global.plife > 0 and isLevel() and helpPage == 0)
    {
        instance_activate_all();
        paused = false;
        with (oPlayer1)
        {
            if (facing == 18) xVel = -3;
            else xVel = 3;
            yVel = -6;
            global.plife = -99;
            if (isRealLevel()) global.miscDeaths[12] += 1;
        }

        scrSetMusicVolume(global.musicVol);

        if (not global.hasAnkh) stopAllMusic();
    }
}
else if (checkRopePressed())
{
    if (paused)
        game_end();
}
else if (checkStartPressed())
{
    if (not paused and canPause)
    {
        if (instance_exists(oPlayer1))
        {
            if (not oPlayer1.dead)
            {
                surface_set_target(pSurf880);
                screen_redraw();

                if (global.darkLevel) draw_set_alpha(1);
                else draw_set_alpha(pauseAlpha);
                draw_set_color(c_black);
                draw_rectangle(0, 0, 672, 880, false);
                draw_set_alpha(1);

                if (not global.gahara) scrSetMusicVolume(-1);

                px = oPlayer1.x;
                py = oPlayer1.y;
                instance_deactivate_all(true);
                instance_activate_object(oGamepad);
                paused = true;
            }
        }
    }
    else
    {
        instance_activate_all();
        if (not global.gahara) scrSetMusicVolume(global.musicVol);
        paused = false;
    }
}

surface_reset_target();
draw_clear(0);
if (paused)
{
    surface_set_target(pSurf880);

    draw_set_font(global.myFont);
    draw_set_color(c_white);
    draw_text_transformed(256, 432, "PAUSED",2,2,0);
    draw_set_font(global.myFontSmall);

    n = 160;
    //if (global.currLevel < 1) draw_text(40, n-24, "TUTORIAL CAVE");
    if (isRoom("rLoadLevel")) draw_text_transformed(144, n-32, "LEVEL: " + global.customLevelName + " BY " + global.customLevelAuthor,2,2,0);
    else draw_text_transformed(144, n-32, "LEVEL " + string(global.currLevel),2,2,0);
    if (global.scumMetric) draw_text_transformed(144, n-16, "DEPTH: " + string((174.8*(global.currLevel-1)+(py+8)*0.34)*0.3048) + " METRES",2,2,0);
    else draw_text_transformed(144, n-16, "DEPTH: " + string(174.8*(global.currLevel-1)+(py+8)*0.34) + " FEET",2,2,0);
    n += 16;
    draw_text_transformed(144, n, "MONEY: " + string(global.money),2,2,0);
    draw_text_transformed(144, n+16, "KILLS: " + string(global.kills),2,2,0);
    s = global.xtime;
    s = floor(s / 1000);
    m = 0;
    while (s > 59)
    {
        s -= 60;
        m += 1;
    }
    if (s < 10) str = "0" + string(s);
    else str = string(s);
    s2 = global.time;
    s2 = floor(s2 / 1000);
    m2 = 0;
    while (s2 > 59)
    {
        s2 -= 60;
        m2 += 1;
    }
    if (s2 < 10) str2 = "0" + string(s2);
    else str2 = string(s2);
    draw_text_transformed(144, n+32, "TIME:  " + string(m) + ":" + str + " / " + string(m2) + ":" + str2,2,2,0);
    draw_text_transformed(144, n+48, "SAVES: " + string(global.damsels),2,2,0);
    if (global.gamepadOn) draw_text_transformed(80, 480, "START-RETURN  BOMB-DIE  ROPE-QUIT",2,2,0);
    else draw_text_transformed(112, 480, "ESC-RETURN  F1-DIE  F10-QUIT",2,2,0);

    //draw_set_color(c_red)
    //draw_text_transformed(88, 352, "SORRY! INVENTORY MENU DISABLED", 2, 2, 0)
    draw_set_color(c_white);

    surface_reset_target();
    draw_surface(pSurf880,screen_x,screen_y);
}
else
{
    if (isLevel() and instance_exists(oPlayer1))
    {
        surface_set_target(screen880);
        scrDrawHUD();
        if (global.messageTimer > 0)
        {
            draw_set_font(global.myFontSmall);
            draw_set_color(c_white);
            strLen = string_length(global.message)*16;
            n = 672 - strLen;
            n = ceil(n / 2);
            draw_text_transformed(n, 880-48, string(global.message),2,2,0);

            if (not isRoom("rTutorial")) draw_set_color(c_yellow);
            strLen = string_length(global.message2)*16;
            n = 672 - strLen;
            n = ceil(n / 2);
            draw_text_transformed(n, 880-32, string(global.message2),2,2,0);

            global.messageTimer -= 1;
        }
        draw_set_blend_mode_ext(bm_src_color,bm_one);
        draw_set_color(c_black);
        draw_rectangle(0, 0, 672,880, 0);
        draw_set_blend_mode(bm_normal);
        surface_reset_target();
    }
    draw_set_blend_mode_ext(bm_one, bm_zero); // According to ChevyRay, this should fix the black box glitch
    if (global.softfullscreen == 1) draw_surface_stretched(screen880,screen_x,screen_y,672,880);
    else draw_surface(screen880,screen_x,screen_y);
    draw_set_blend_mode(bm_normal); // According to ChevyRay, this should fix the black box glitch
}

if (global.optVSync) screen_wait_vsync(); //k8
screen_refresh();
