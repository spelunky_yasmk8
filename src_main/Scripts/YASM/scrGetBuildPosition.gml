// called by oPlayer1 (scrCraftBlock)
// show/hide/move crafting preview position

if (holdItem)
{
    if (holdItem.type == "Block Item")
    {

        var block, checkPrev;
        if (instance_exists(oBlockPreview))
        {
            block = instance_nearest(x, y, oBlockPreview); // the preview block, not the held item
            checkPrev = true; // check to see if we moved the preview
        }
        else
        {
            block = instance_create(x, y, oBlockPreview);
            block.sprite = holdItem.sprite;
            block.blockType = holdItem.blockType;
            checkPrev = false;
        }

        var snap;
        if (instance_exists(oSnapPlayer)) snap = instance_nearest(x, y, oSnapPlayer);
        else
        {
            snap = instance_create(x, y, oSnapPlayer);
            snap.x = x-8;
            snap.y = y-8;
            with (snap) move_snap(16, 16);
        }

        with (block) move_snap(16, 16);

        var prevx, prevy;
        prevx = block.x;
        prevy = block.y;

        if (abs(xVel) > 4 or abs(yVel) > 4 or
            state == JUMPING or
            state == FALLING or
            state == DUCKTOHANG)
        {
            with (block) { state = 0; buildCounter = 0; x = oPlayer1.x-8; y = oPlayer1.y-8; } // hide block preview
        }
        else if (state == DUCKING)
        {
            if (block.state < 2) // < BUILDING
            {
                if (block.state < 1) block.state = 1; // HIDDEN to IDLE

                if (facing == LEFT)
                {
                    if (canBuild(-1, 1, snap)) movePreview(-1, 1, block, snap);
                    else if (canBuild(-1, 0, snap)) movePreview(-1, 0, block, snap);
                }
                else if (facing == RIGHT)
                {
                    if (canBuild(1, 1, snap)) movePreview(1, 1, block, snap);
                    else if (canBuild(1, 0, snap)) movePreview(1, 0, block, snap);
                }
            }

            if (collision_point(x, y, oBlockPreview, 0, 0)) block.state = 0;

            with (block) move_snap(16,16);
        }
        else if (state == HANGING)
        {
            if (block.state < 2) // < BUILDING
            {
                if (block.state < 1) block.state = 1; // HIDDEN to IDLE

                if (facing == LEFT)
                {
                    if (kDown)
                    {
                        if (kRight and canBuild(1, 1, snap)) movePreview(1, 1, block, snap);
                        else if (canBuild(0, 1, snap)) movePreview(0, 1, block, snap);
                        else if (canBuild(-1, 1, snap)) movePreview(-1, 1, block, snap);
                    }
                    else if (kUp)
                    {
                        if (kRight)
                        {
                            if (canBuild(1, -1, snap)) movePreview(1, -1, block, snap);
                            else if (canBuild(1, 0, snap)) movePreview(1, 0, block, snap);
                        }
                    }
                    else
                    {
                        if (kRight and canBuild(1, 0, snap)) movePreview(1, 0, block, snap);
                        else if (canBuild(-1, 1, snap)) movePreview(-1, 1, block, snap);
                        else if (canBuild(0, 1, snap)) movePreview(0, 1, block, snap);
                    }
                }
                else if (facing == RIGHT)
                {
                    if (kDown)
                    {
                        if (kLeft and canBuild(-1, 1, snap)) movePreview(-1, 1, block, snap);
                        else if (canBuild(0, 1, snap)) movePreview(0, 1, block, snap);
                        else if (canBuild(1, 1, snap)) movePreview(1, 1, block, snap);
                    }
                    else if (kUp)
                    {
                        if (kLeft)
                        {
                            if (canBuild(-1, -1, snap)) movePreview(-1, -1, block, snap);
                            else if (canBuild(-1, 0, snap)) movePreview(-1, 0, block, snap);
                        }
                    }
                    else
                    {
                        if (kLeft and canBuild(-1, 0, snap)) movePreview(-1, 0, block, snap);
                        else if (canBuild(1, 1, snap)) movePreview(1, 1, block, snap);
                        else if (canBuild(0, 1, snap)) movePreview(0, 1, block, snap);
                    }
                }
            }

            if (collision_point(x, y, oBlockPreview, 0, 0)) block.state = 0;

            with (block) move_snap(16,16);
        }
        else
        {
            if (block.state < 2) // < BUILDING
            {
                if (block.state < 1) block.state = 1; // HIDDEN to IDLE

                if (facing == LEFT)
                {
                    if (kUp)
                    {
                        if (kLeft and canBuild(-1, -1, snap)) movePreview(-1, -1, block, snap);
                        else if (canBuild(0, -1, snap)) movePreview(0, -1, block, snap);
                        //else if (canBuild(-1, -1, snap) and state != CLIMBING) movePreview(-1, -1, block, snap);
                        else if (canBuild(-1, -1, snap)) movePreview(-1, -1, block, snap);
                    }
                    else if (kDown)
                    {
                        if (canBuild(0, 1, snap)) movePreview(0, 1, block, snap);
                        else if (canBuild(0, 0, snap)) movePreview(0, 0, block, snap);
                    }
                    else
                    {
                        //if (canBuild(-1, 0, snap) and state != CLIMBING) movePreview(-1, 0, block, snap);
                        if (canBuild(-1, 0, snap)) movePreview(-1, 0, block, snap);
                    }
                }
                else if (facing == RIGHT)
                {
                    if (kUp)
                    {
                        if (kRight and canBuild(1, -1, snap)) movePreview(1, -1, block, snap);
                        else if (canBuild(0, -1, snap)) movePreview(0, -1, block, snap);
                        //else if (canBuild(1, -1, snap) and state != CLIMBING) movePreview(1, -1, block, snap);
                        else if (canBuild(1, -1, snap)) movePreview(1, -1, block, snap);
                    }
                    else if (kDown)
                    {
                        if (canBuild(0, 1, snap)) movePreview(0, -1, block, snap);
                        else if (canBuild(0, 0, snap)) movePreview(0, 0, block, snap);
                    }
                    else
                    {
                        //if (canBuild(1, 0, snap) and state != CLIMBING) movePreview(1, 0, block, snap);
                        if (canBuild(1, 0, snap)) movePreview(1, 0, block, snap);
                    }
                }
            }

            if (collision_point(x, y, oBlockPreview, 0, 0)) block.state = 0;

            with (block) move_snap(16,16);
        }
    }
    else
    {
        if (instance_exists(oBlockPreview)) with (oBlockPreview) instance_destroy();
        if (instance_exists(oSnapPlayer)) with (oSnapPlayer) instance_destroy();
    }
}
