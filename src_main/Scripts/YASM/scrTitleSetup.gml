// changes walls/decorations in title and high scores room to match current gamemode
// called from oTitle.Create, and oHighScores.Create
var n;

if (isRoom("rTitle") or isRoom("rHighscores")) {
  if (global.gameMode == 1) {
    if (global.bizarrePlusTitle) {
      // bizarre mode plus
        with (oBrick) {
          if (rand(1, 60) == 1) instance_change(oIce, false); else instance_change(oDark, false);
        }
        with (oDark) {
          n = rand(1, 100);
               if (n < 4) sprite_index = sDarkGoldBig;
          else if (n < 10) sprite_index = sDarkGold;
          else sprite_index = sDark;
        }
        with (oHardBlock) {
          instance_change(oDark, false);
          sprite_index = sDark;
        }
        with (oBrickSmooth) sprite_index = sDarkSmooth;
        with (oPushBlock) sprite_index = sIceBlock;
        instance_create(48, 128, oCarl);
    } else  {
      // bizarre mode
      with (oBrick) {
        if (rand(1, 60) == 1) instance_change(oBlock, false); else instance_change(oLush, false);
      }
      with (oLush) {
        n = rand(1, 100);
             if (n < 4) sprite_index = sLushGoldBig;
        else if (n < 10) sprite_index = sLushGold;
        else sprite_index = sLush;
      }
      with (oHardBlock) {
        instance_change(oLush, false);
        sprite_index = sLush;
      }
      with (oBrickSmooth) sprite_index = sLushSmooth;

      instance_create(432, 160, oTikiTorch);
      instance_create(496, 160, oTikiTorch);
      instance_create(48, 128, oFrogCritter);
    }
  } else {
    // classic mode
    //body was commented, and this: if (rand(1, 80) == 1) instance_change(oBlock, true); else instance_change(oBrick, false);
    //with (oBrick) instance_change(oIce, false);
    with (oBrick) instance_change(oBrick, false);

    with (oBrick) {
      n = rand(1, 100);
           if (n < 4) sprite_index = sBrickGoldBig;
      else if (n < 10) sprite_index = sBrickGold;
      else if (n < 20) sprite_index = sBrick2;
      else sprite_index = sBrick;
    }
    with (oHardBlock) {
      instance_change(oBrick, false);
      if (rand(1, 10) == 1) sprite_index = sBrick2; else sprite_index = sBrick;
    }
    // if (isRoom("rTitle")) instance_create(400, 224, oCopy);
  }
}
