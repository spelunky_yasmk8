// scrNudgeItem(x, y)
// called by oItem child
// Nudge oItem away from argument0(x), argument1(y) (usually player)
// oDamsel, oJar, oCrate, oFlareCrate, oChest, oSkull have their own melee weapon collision events for opening/breaking/crying

if (global.nudge && !nudged) {
  if (not collision_point(x, y, oSolid, 0, 0)) {
    if (forSale || forVending || trigger) {
      if (!trigger) yVel -= 1;
    } else {
      if (heavy) {
        yVel -= 1;
             if (argument0 < x) xVel += rand(5, 8)*0.1;
        else if (argument0 > x) xVel -= rand(5, 8)*0.1;
      } else if (type == "Arrow" && abs(xVel) > 0) {
               if (abs(xVel) < 4) xVel = -xVel;
          else if (xVel < 0) xVel = rand(3, 5);
          else if (xVel > 0) xVel = -rand(3, 5);
          yVel = -yVel;
      } else if (!stuck and !sticky) {
        yVel -= choose(1.5,2);
             if (argument0 < x) xVel += rand(10, 15)*0.1;
        else if (argument0 > x) xVel -= rand(10, 15)*0.1;
        if (type == "Basketball") {
          if (abs(yVel) < 4) yVel -= 5;
          xVel = xVel*4;
        }
      }
    }
    nudged = true;
    alarm[9] = 10;
  }
}
