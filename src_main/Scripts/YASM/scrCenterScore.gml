// scrCenterScore(string, font, ypos, color)
// used to center text on "game over" screen
var str, fontWidth, strLen, n;

str = argument0;
if (argument1 == global.myFont) fontWidth = 16; else fontWidth = 8;
draw_set_font(argument1);
draw_set_color(argument3);
strLen = string_length(str)*fontWidth;
n = 320 - strLen;
n = ceil(n / 2);
draw_text(view_xview[0]+n, view_yview[0]+argument2, str);
