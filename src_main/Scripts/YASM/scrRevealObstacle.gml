// scrRevealObstacle
// called from scrRoomGen* scripts
var i, j, xpos, ypos, tile, obj, strTemp;
strTemp = argument0;

for (j = 0; j < 8; j += 1) {
  for (i = 1; i < 11; i += 1) {
    tile = string_char_at(strTemp, i+j*10);
    xpos = x+(i-1)*16;
    ypos = y+j*16;
    if (tile == "5" or tile == "6" or tile == "8" or tile == "V" or tile == "F") {
      obj = instance_create(xpos, ypos, oObstacleDebug);
           if (tile == "5") obj.col = c_orange; // ground
      else if (tile == "6") obj.col = c_teal; // air
      else if (tile == "V") { obj.col = c_lime; obj.h = 64; } // vines
      else if (tile == "F") { obj.col = c_blue; obj.l = 48; obj.h = 48; } // ice/falling platforms
    } else if (tile == "r" and global.levelType == 3) {
      obj = instance_create(xpos, ypos, oObstacleDebug);
      obj.col = c_yellow; // air (temple)
      obj.l = 64;
      obj.h = 48;
    }
  }
}
