// we need to store `global.hasStickyBombs` somewhere
// also, don't forget to reset it too
if (global.hasStickyBombs) {
  global.stickyBombsActive = !global.stickyBombsActive;
} else {
  global.stickyBombsActive = true;
}
