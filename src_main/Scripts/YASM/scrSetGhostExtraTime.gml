// scrSetGhostExtraTime
// called from scrInitLevel
// randomizes time until ghost appears once time limit is reached
// global.ghostRandom (true/false) if extra time is randomized - loaded from yasm.cfg. if false, use 30 second default
// global.ghostExtraTime (time in seconds * 1000) for currently generated level

if (global.ghostRandom)
{
    var tMin, tMax, tTime;
    tMin = rand(10,30); // min(global.plife*10, 30);
    tMax = max(tMin+rand(10,30), min(global.scumGhost, 300)); // 5 minutes max
    tTime = rand(tMin, tMax);
    if (tTime <= 0) tTime = round(tMax/2);
    global.ghostExtraTime = tTime*1000;

}
else global.ghostExtraTime = 30000; // use 30 second default
