var file;

file = file_text_open_read("yasm_gamepadlabels.cfg");
if (file) {
  global.joyJumpLabel = string_upper(file_text_read_string(file));
  file_text_readln(file);
  global.joyAttackLabel = string_upper(file_text_read_string(file));
  file_text_readln(file);
  global.joyItemLabel = string_upper(file_text_read_string(file));
  file_text_readln(file);
  global.joyRunLabel = string_upper(file_text_read_string(file));
  file_text_readln(file);
  global.joyBombLabel = string_upper(file_text_read_string(file));
  file_text_readln(file);
  global.joyRopeLabel = string_upper(file_text_read_string(file));
  file_text_readln(file);
  global.joyPayLabel = string_upper(file_text_read_string(file));
  file_text_readln(file);
  global.joyStartLabel = string_upper(file_text_read_string(file));
  file_text_close(file);
}
