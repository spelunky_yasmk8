// called by scrGetBuildPosition

var obj, obj2;
obj = argument2;
obj2 = argument3;
if (instance_exists(obj) and instance_exists(obj2))
{
    obj.x = obj2.x+argument0*16;
    obj.y = obj2.y+argument1*16;
}
