// argument0 = start x coordinate
// argument1 = start y coordinate
// argument2 = nearest object index to look for
// argument3 = distance object needs to be within to return true

var near;
near = instance_nearest(argument0, argument1, argument2);
if (instance_exists(near))
{
    if (point_distance(argument0, argument1, near.x, near.y) <= argument3) return true;
}

return false;
