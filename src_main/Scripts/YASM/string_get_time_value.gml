/*
string_get_time_value(datetime)
datetime: valid datetime from a function like date_create_datetime()
Returns time as a 6 digit string (HHMMSS)
*/

var hh, mm, ss;
hh = string(date_get_hour(argument0));
if (string_length(hh) == 1) hh = "0" + hh;
mm = string(date_get_minute(argument0));
if (string_length(mm) == 1) mm = "0" + mm;
ss = string(date_get_second(argument0));
if (string_length(ss) == 1) ss = "0" + ss;
return hh+mm+ss;
