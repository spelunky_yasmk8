draw_set_font(global.myFontSmall);
draw_set_color(c_white);
if (isLevel())
{
    if (helpPage == 0)
    {
        if (global.gamepadOn) draw_text(24, 224, "START-RETURN  BOMB-DIE  ROPE-QUIT");
        else draw_text(32, 224, "ESC-RETURN  BOMB-DIE  F10-QUIT");
    }
    else
    {
        if (global.gamepadOn) draw_text(64, 224, "START-RETURN  ROPE-QUIT");
        else draw_text(64, 224, "ESC-RETURN    F10-QUIT");
    }
}
else
{
    if (helpPage == 0)
    {
        if (global.gamepadOn) draw_text(64, 224, "START-RETURN  ROPE-QUIT");
        else draw_text(80, 224, "ESC-RETURN  F10-QUIT");
    }
    else
    {
        if (global.gamepadOn) draw_text(64, 224, "START-RETURN  ROPE-QUIT");
        else draw_text(80, 224, "ESC-RETURN  F10-QUIT");
    }
}
