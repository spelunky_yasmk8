// combines scrSprayRubble and Spelunky Natural rubble blending
// argument0 = how many pieces
// argument1 = sprite 1 (ex sRubble)
// argument2 = sprite 2 (ex sRubbleSmall)

var rubble;
repeat (argument0)
{
    if (rand(1,3) == 1)
    {
        rubble = instance_create(x+8+rand(0,8)-rand(0,8), y+8+rand(0,8)-rand(0,8), oRubble);
        rubble.sprite_index = argument2;
        rubble.image_blend = image_blend;
    }
    else
    {
        rubble = instance_create(x+8+rand(0,8)-rand(0,8), y+8+rand(0,8)-rand(0,8), oRubbleSmall);
        rubble.sprite_index = argument1;
        rubble.image_blend = image_blend;
    }
}
