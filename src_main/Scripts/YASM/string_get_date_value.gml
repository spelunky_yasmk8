/*
string_get_date_value(date)
date: valid date from a function like date_create_date()
Returns date as an 8 digit string (YYYYMMDD) that can be sorted
*/

var ye, mo, da;
ye = string(date_get_year(argument0));
mo = string(date_get_month(argument0));
if (string_length(mo) == 1) mo = "0" + mo;
da = string(date_get_day(argument0));
if (string_length(da) == 1) da = "0" + da;
return ye+mo+da;
