// called by oPlayer.Step (Action, bottom)

scrGetBuildPosition();
if (instance_exists(oBlockPreview))
{
    if (kAttackPressed)
    {
        with (oBlockPreview)
        {
            if (state == 0)
            {
                pressTimer = 0;
                event_user(1); // throw
            }
            else if (state == 1) state = 2; // start crafting
        }
    }
    else if (kAttack) // increment crafting progress
    {
        with (oBlockPreview)
        {
            if (pressTimer >= 0)
            {
                pressTimer += 1;
                if (state == 1) state = 2;
            }
            event_user(0);

        }
    }
    else if (kAttackReleased) // cancel crafting
    {
        with (oBlockPreview)
        {
            if (pressTimer >= 0 and pressTimer < pressLimit) event_user(1);

            pressTimer = 0;
            buildCounter = 0;
            state = 1;


        }
    }
}
