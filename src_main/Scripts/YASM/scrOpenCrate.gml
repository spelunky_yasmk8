// scrOpenCrate();
// argument0 = id of crate to open
// for use with feature that allows whip/mattock/machete to open crates
if (instance_exists(argument0)) {
  if (!collision_point(argument0.x, argument0.y, oSolid, 0, 0)) {
    with (argument0) {
      if (isRealLevel()) global.totalCrates += 1;

      var obj, ctx;
      obj = -1;

      if (holds != "") {
        obj = scrMakeItem(holds, x, y);
      } else if (isRoom("rTutorial")) {
        obj = instance_create(x, y, oBombBag);
      } else if (variable_local_exists("contents")) {
        ctx = contents;
        if (ctx < 0) ctx = scrLevGenCreateCrateContents();
        if ((ctx == oShells || ctx == oShellSingle) && !global.optSGAmmo) ctx = oBombBag;
        if (ctx == oTeleporter2 && !global.bizarre) ctx = oBombBag;
        if (ctx == oJordans && !global.bizarre) ctx = oBombBag;
        if (ctx == oMattock && global.isTunnelMan) ctx = oBombBag;
        if (ctx == oBombBag && global.bizarre && global.bizarrePlus) ctx = oBombBox; // bizarre mode new game +
        obj = instance_create(x, y, ctx);
      }

      if (obj >= 0) {
        with (obj) {
          if (variable_local_exists("cost")) cost = 0;
          if (variable_local_exists("forSale")) forSale = 0;
          if (variable_local_exists("type")) {
            if (type == "Shotgun") {
              if (rand(1, 42) == 1) ashShotgun = true;
            }
          }
        }
      }
      playSound(global.sndPickup);

      if (oPlayer1.holdItem == self.id) {
        oPlayer1.holdItem = 0;
        oPlayer1.pickupItemType = "";
      }

      oPlayer1.kAttackPressed = false;

      instance_create(x, y, oPoof);
      instance_destroy();
    }
  }
}
