if (is_real(argument0) && is_real(argument1)) return (argument0 == argument1);
else if (is_string(argument0) && is_string(argument1)) return (argument0 == argument1);
else return -1;
