// called from scrInit
var file, str, name, value;

// defaults
global.skipIntro = false;
global.useDoorWithButton = true;
global.weaponsOpenContainers = true;
global.scumClimbSpeed = 3;
global.scumFallDamage = 1;
global.scumSpringShoesReduceFallDamage = true;
global.scumBallAndChain = false;
global.scumFlipHold = true;
global.scumUnlocked = false;
global.scumDarkness = 1;
global.scumGhost = 120;
global.ghostRandom = false;
global.useFrozenRegion = true;
global.trapMult = 1;
global.enemyMult = 1;
global.scumHud = true;
global.parallax = false;
global.scumMetric = true;
global.nudge = true;
global.enemyBreakWeb = true;
global.woodSpikes = false;
global.boulderChaos = false;
global.toggleRunAnywhere = true;
global.naturalSwim = true;
// starting stats for Spelunker and Damsel
global.scumStartLife = 4;
global.scumStartBombs = 4;
global.scumStartRope = 4;
global.scumStartMoney = 0;
// starting stats for Tunnel Man
global.scumTMLife = 2;
global.scumTMBombs = 0;
global.scumTMRope = 0;
global.scumTMMoney = 0;

global.optSGAmmo = true; // shotgun using ammo (and jars can drop ammo, and... you got the idea)
global.optImmTransition = true; // second "attack" does immediate transition
global.optVSync = false;
global.optThrowEmptyShotgun = true;
global.startWithKapala = false;


file = file_text_open_read("yasm.cfg");
if (file) {
  // read "key=value" pairs
  while (not file_text_eof(file)) {
    str = file_text_read_string(file);
    file_text_readln(file);
    name = k8strkey(str);
    value = k8strvalue(str);
         if (name == "skipIntro") { global.skipIntro = (value == "tan" or value == "true"); }
    else if (name == "useDoorWithButton") { global.useDoorWithButton = (value == "tan" or value == "true"); }
    else if (name == "weaponsOpenContainers") { global.weaponsOpenContainers = (value == "tan" or value == "true"); }
    else if (name == "scumClimbSpeed") { global.scumClimbSpeed = real(value); }
    else if (name == "scumFallDamage") { global.scumFallDamage = real(value); }
    else if (name == "scumSpringShoesReduceFallDamage") { global.scumSpringShoesReduceFallDamage = (value == "tan" or value == "true"); }
    else if (name == "scumBallAndChain") { global.scumBallAndChain = (value == "tan" or value == "true"); }
    else if (name == "scumFlipHold") { global.scumFlipHold = (value == "tan" or value == "true"); }
    else if (name == "scumUnlocked") { global.scumUnlocked = (value == "tan" or value == "true"); }
    else if (name == "scumDarkness") { global.scumDarkness = real(value); }
    else if (name == "scumGhost") { global.scumGhost = real(value); }
    else if (name == "ghostRandom") { global.ghostRandom = (value == "tan" or value == "true"); }
    else if (name == "useFrozenRegion") { global.useFrozenRegion = (value == "tan" or value == "true"); }
    else if (name == "trapMult") { global.trapMult = real(value); }
    else if (name == "enemyMult") { global.enemyMult = real(value); }
    else if (name == "scumHud") { global.scumHud = (value == "tan" or value == "true"); }
    else if (name == "parallax") { global.parallax = (value == "tan" or value == "true"); }
    else if (name == "scumMetric") { global.scumMetric = (value == "tan" or value == "true"); }
    else if (name == "nudge") { global.nudge = (value == "tan" or value == "true"); }
    else if (name == "enemyBreakWeb") { global.enemyBreakWeb = (value == "tan" or value == "true"); }
    else if (name == "woodSpikes") { global.woodSpikes = (value == "tan" or value == "true"); }
    else if (name == "boulderChaos") { global.boulderChaos = (value == "tan" or value == "true"); }
    else if (name == "toggleRunAnywhere") { global.toggleRunAnywhere = (value == "tan" or value == "true"); }
    else if (name == "naturalSwim") { global.naturalSwim = (value == "tan" or value == "true"); }
    else if (name == "SGAmmo") { global.optSGAmmo = (value == "tan" or value == "true"); global.optSGAmmoOrig = global.optSGAmmo; }
    else if (name == "ImmTransition") { global.optImmTransition = (value == "tan" or value == "true"); }
    else if (name == "VSync") { global.optVSync = (value == "tan" or value == "true"); }
    else if (name == "throwEmptyShotgun") { global.optThrowEmptyShotgun = (value == "tan" or value == "true"); }
    else if (name == "startWithKapala") { global.startWithKapala = (value == "tan" or value == "true"); }
    else if (name == "doubleKiss") { global.optDoubleKiss = (value == "tan" or value == "true"); }
    else if (name == "enemyVariations") { global.optEnemyVariations = (value == "tan" or value == "true"); }
    else if (name == "roomStyle") { global.optRoomStyle = real(value); }
    else if (name == "spikeVariations") { global.optSpikeVariations = (value == "tan" or value == "true"); }
    else if (name == "shopkeeperIdiots") { global.optShopkeeperIdiots = (value == "tan" or value == "true"); }
    else if (name == "initialHearts") { global.scumStartLife = real(value); }
    else if (name == "initialRopes") { global.scumStartRope = real(value); }
    else if (name == "initialBombs") { global.scumStartBombs = real(value); }
    else if (name == "initialMoney") { global.scumStartMoney = real(value); }
    else if (name == "initialHeartsTunnelMan") { global.scumTMLife = real(value); }
    else if (name == "initialRopesTunnelMan") { global.scumTMRope = real(value); }
    else if (name == "initialBombsTunnelMan") { global.scumTMBombs = real(value); }
    else if (name == "initialMoneyTunnelMan") { global.scumTMMoney = real(value); }
    else if (name == "graphicsHigh") { global.graphicsHigh = (value == "tan" or value == "true"); }
    else if (name == "downToRun") { global.downToRun = (value == "tan" or value == "true"); }
    else if (name == "musicVol") { global.musicVol = real(value); }
    else if (name == "soundVol") { global.soundVol = real(value); }
    else if (name == "seedComputer") { global.optSeedComputer = (value == "tan" or value == "true"); }
    else if (name == "musicEnabled") { global.musicEnabled = (value == "tan" or value == "true"); }
    else if (name == "soundEnabled") { global.soundEnabled = (value == "tan" or value == "true"); }
  }
  file_text_close(file);
  // sanitize options
  if (global.scumClimbSpeed < 1) global.scumClimbSpeed = 1;
  else if (global.scumClimbSpeed > 3) global.scumClimbSpeed = 3;

  if (global.scumFallDamage < 1) global.scumFallDamage = 1;
  else if (global.scumFallDamage > 10) global.scumFallDamage = 10;

  if (global.scumDarkness < 0) global.scumDarkness = 0;
  else if (global.scumDarkness > 2) global.scumDarkness = 2;

  if (global.scumGhost < 0) global.scumGhost = 0;
  else if (global.scumGhost > 1200) global.scumGhost = 1200;

  if (global.enemyMult < 1) global.enemyMult = 1;
  else if (global.enemyMult > 10) global.enemyMult = 10;

  if (global.trapMult < 1) global.trapMult = 1;
  if (global.trapMult > 10) global.trapMult = 10;

  if (global.scumStartLife < 1) global.scumStartLife = 1;
  else if (global.scumStartLife > 42) global.scumStartLife = 42; // arbitrary limit

  if (global.scumStartRope < 0) global.scumStartRope = 0;
  else if (global.scumStartRope > 42) global.scumStartRope = 42; // arbitrary limit

  if (global.scumStartBombs < 0) global.scumStartBombs = 0;
  else if (global.scumStartBombs > 42) global.scumStartBombs = 42; // arbitrary limit

  if (global.scumStartMoney < 0) global.scumStartMoney = 0;
  else if (global.scumStartMoney > 666666) global.scumStartMoney = 666666; // arbitrary limit

  if (global.scumTMLife < 1) global.scumTMLife = 1;
  else if (global.scumTMLife > 42) global.scumTMLife = 42; // arbitrary limit

  if (global.scumTMRope < 0) global.scumTMRope = 0;
  else if (global.scumTMRope > 42) global.scumTMRope = 42; // arbitrary limit

  if (global.scumTMBombs < 0) global.scumTMBombs = 0;
  else if (global.scumTMBombs > 42) global.scumTMBombs = 42; // arbitrary limit

  if (global.scumTMMoney < 0) global.scumTMMoney = 0;
  else if (global.scumTMMoney > 666666) global.scumTMMoney = 666666; // arbitrary limit
} else {
  global.cfgExists = false; // tell player to use config program on title screen
}
global.initBC = global.scumBallAndChain;
