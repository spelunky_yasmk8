/*
called by scrInit
by TyrOvC from Spelunky 1.2.3 Translation Release

/sprites/ directory
Sprites is the portable network graphics (.png) format. Any image in this directory will replace any GameMaker sprite with the same name.
Example:
sQuitSign.png would replace the sQuitSign sprite.
*/
var i, tempsprite, spritemap, filepointer, spritename, spriteindex;
//Load Sprites
//First, need to map sprite_index ids to sprite names so that we can replace them based on filename
tempsprite = sprite_create_from_screen(0, 0, 1, 1, false, false, 0, 0); // make a new sprite and use it to get the highest sprite_index value
sprite_delete(tempsprite);
spritemap = ds_map_create();
for (i = 0; i < tempsprite; i += 1) {
  if (sprite_exists(i)) ds_map_add(spritemap, sprite_get_name(i), i);
}

filepointer = file_find_first("sprites\*.png", false); // added *.png mask YASM 1.8
//file_find_first("languages\"+languageid+"\sprites\*", false);

//filepointer=file_find_next();
while (filepointer != "") {
  spritename = filename_change_ext(filepointer, "");
  spriteindex = ds_map_find_value(spritemap, spritename);
  if (sprite_exists(spriteindex)) {
    sprite_replace(spriteindex, "sprites\"+filepointer, sprite_get_number(spriteindex), false, false, sprite_get_xoffset(spriteindex), sprite_get_yoffset(spriteindex));
  }
  filepointer = file_find_next();
}
file_find_close();
ds_map_destroy(spritemap);
