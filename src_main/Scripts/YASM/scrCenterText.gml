// identical to scrCenterScore but without view_xview etc - for inventory screen

// scrCenterScore(string, font, ypos, color)
// used to center text on "game over" screen
var str, strLen, nn, fontWidth;
str = argument0;
if (argument1 == global.myFont) fontWidth = 16; else fontWidth = 8;
draw_set_font(argument1);
draw_set_color(argument3);
strLen = string_length(str)*fontWidth;
nn = 320 - strLen;
nn = ceil(nn / 2);
draw_text(nn, argument2, str);
