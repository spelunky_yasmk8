// called from scrUseItem
// Teleporter Mark II

/*

The "Teleporter Mark II" is equipped with an onboard AI construct that is entirely dedicated to the safety
of its user. With the Mark II, the user no longer needs to worry about teleporting into solid rock or a
pool of magma. With its advanced algorithms and simulated concern for your well-being, the Mark II always
ensures you arrive at your destination in one piece. Perhaps more overzealous with its safety protocols than
the designers intented, the Mark II one-button control removes all choice from its user.

*/
var dest, obj;

if (kDown) // drop item
{
    if (scrPlayerIsDucking())
    {
        holdItem.held = false;
        holdItem.resaleValue = 0;
        madeOffer = false;
        holdItem.safe = true;
        holdItem.alarm[2] = 10;

        if (facing == LEFT)
        {
            if (holdItem.heavy) holdItem.xVel = -4 + xVel;
            else holdItem.xVel = -8 + xVel;
        }
        else if (facing == RIGHT)
        {
            if (holdItem.heavy) holdItem.xVel = 4 + xVel;
            else holdItem.xVel = 8 + xVel;
        }

        holdItem.xVel *= 0.4;
        holdItem.yVel = 0.5;

        holdItem = 0;
    }
}
else // use item
{
    var sGrid, sGridPos, cx, cy, i, j, dx, dy;
    sGrid = ds_grid_create(1,2);
    ds_grid_clear(sGrid, 0);
    sGridPos = 0;
    cx = ceil(view_xview[0]/16); //*16;
    cy = ceil(view_yview[0]/16); //*16;

    for (j = 0; j < 13; j += 1)
    {
        for (i = 0; i < 18; i += 1)
        {
            dx = (cx+i)*16+8;
            dy = (cy+j)*16+8;
            if (point_distance(dx, dy, x, y) > 24 and
                !instance_position(dx, dy, oSolid) and
                collision_line(dx, dy, dx, dy+56, oSolid, 0, 0) and
                !collision_line(dx, dy+8, dx, dy+56, oSpikes, 0, 0) and
                !collision_line(dx, dy+8, dx, dy+56, oSpikesWood, 0, 0) and
                !collision_line(dx, dy, dx, dy+56, oLava, 0, 0) and
                !collision_line(dx, dy, dx-64, dy, oArrowTrapTest, 0, 0) and
                !collision_line(dx, dy, dx+64, dy, oArrowTrapTest, 0, 0) and
                !collision_line(dx, dy, dx, dy+48, oArrowTrapTest, 0, 0) and
                !collision_line(dx, dy, dx, dy-80, oThwompTrap, 0, 0) and
                !collision_line(dx, dy, dx, dy-64, oSmashTrap, 0, 0) and
                !collision_line(dx, dy, dx, dy+64, oSmashTrap, 0, 0) and
                !collision_line(dx, dy, dx-64, dy, oSmashTrap, 0, 0) and
                !collision_line(dx, dy, dx+64, dy, oSmashTrap, 0, 0) and
                !collision_line(dx, dy, dx, dy-64, oEnemy, 0, 0) and
                !collision_rectangle(dx-24, dy-24, dx+24, dy+24, oEnemy, 0, 0) and
                !collision_rectangle(dx-8, dy-8, dx+8, dy+8, oExplosion, 0, 0)
                )
            {
                if (sGridPos > 0) ds_grid_resize(sGrid, ds_grid_width(sGrid)+1, ds_grid_height(sGrid));
                ds_grid_set(sGrid, sGridPos, 0, dx);
                ds_grid_set(sGrid, sGridPos, 1, dy);
                sGridPos += 1;
            }
        }
    }

    if (ds_grid_get(sGrid, 0, 0) > 0 and not scrPlayerIsDucking())
    {
        var tx, ty, px, py;
        dest = rand(1,ds_grid_width(sGrid))-1;

        tx = ds_grid_get(sGrid, dest, 0);
        ty = ds_grid_get(sGrid, dest, 1);
        fallTimer = 0;

        px = x; // round(x/16)*16;
        py = y; // round(y/16)*16;

        repeat(3)
        {
            instance_create(x-4+rand(0,8), y-4+rand(0,8), oFlareSpark);
        }

        var sparkNum, sparkDir, sx, sy, spark, sparkSpd;
        sparkNum = round(point_distance(px, py, tx, ty)/8);
        sparkDir = point_direction(px, py, tx, ty);
        sx = px;
        sy = py;
        sparkSpd = 0.2;

        // sparkle trail!
        repeat (sparkNum)
        {
            sx += lengthdir_x(8, sparkDir);
            sy += lengthdir_y(8, sparkDir);
            spark = instance_create(sx-4+rand(0,8), sy-4+rand(0,8), oFlareSpark);
            spark.image_speed = sparkSpd;
            if (sparkSpd < 1) sparkSpd += 0.05;

        }


        if (y < 8) y = 8;
        x = tx;
        y = ty;
        invincible = 10;

        with (oBall)
        {
            x = oPlayer1.x;
            y = oPlayer1.y;
        }
        with (oChain)
        {
            x = oPlayer1.x;
            y = oPlayer1.y;
        }

        // swap positions instead of telefragging
        obj = instance_position(x, y, oEnemy);
        if (obj)
        {
            if (obj.type == "ManTrap" or
                obj.type == "Caveman" or
                obj.type == "Yeti" or
                obj.type == "Vampire" or
                obj.type == "Hawkman")
            {
                obj.status = 98;
                obj.counter = 30;
            }
            obj.x = px-8;
            obj.y = py-8;
        }

        obj = instance_place(x, y, oItem);
        if (obj)
        {
            obj.x = px;
            obj.y = py;
        }

        playSound(global.sndTeleport, /*28000*/0.63);
        with (oPlayer1) state = 16;
    }
    else // no safe destination found
    {
        playSound(global.sndTeleport, /*64000*/1.45);
    }

    ds_grid_destroy(sGrid);
}
