// scrOpenFlareCrate();
// argument0 = id of crate to open
// for use with feature that allows whip/mattock/machete to open crates

if (instance_exists(argument0)) {
  if (not collision_point(argument0.x, argument0.y, oSolid, 0, 0)) {
    with (argument0) {
      var obj, i;
      obj = 0;
      for (i = 0; i < 3; i += 1) {
        obj = instance_create(x, y, oFlare);
        obj.xVel = rand(0,3)-rand(0,3);
        obj.yVel = rand(1,3)*-1;
      }
      playSound(global.sndPickup);
      if (oPlayer1.holdItem == self.id) {
        oPlayer1.holdItem = 0;
        oPlayer1.pickupItemType = "";
      }
      oPlayer1.kAttackPressed = false;
      instance_create(x, y, oPoof);
      instance_destroy();
    }
  }
}
