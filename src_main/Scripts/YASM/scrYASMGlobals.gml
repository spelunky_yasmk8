// Called by scrClearGlobals

// starting stats for Spelunker and Damsel
//global.scumStartLife = 4;
//global.scumStartBombs = 4;
//global.scumStartRope = 4;
//global.scumStartMoney = 0;

// starting stats for Tunnel Man
//global.scumTMLife = 2;
//global.scumTMBombs = 0;
//global.scumTMRope = 0;
//global.scumTMMoney = 0;

global.heartBlink = 0; // scrDrawHUD

global.gaveSceptre = false; // prevents multiple sceptres in level 13 if there is more than 1 tomb lord
global.totalTombLords = 0; // number of tomb lords spawned so far in a level
global.scumWhipUpgrade = 0; // 0 = default, 1 = long whip, (2 = chain whip) UNFINISHED
global.scumLevelGenerated = false; // Is level from level generation test menu?
global.scumWholeLevel = false; // whole level view on screen? level gen test menu.

// Level generation test menu:
global.scumRoomGrid = false;
global.scumBlockGrid = false;
global.scumTileDebug = false;
global.scumGenCemetary = false;
global.scumGenSnakePit = false;
global.scumGenYetiLair = false;
global.scumGenMoai = false;
global.scumGenAlienCraft = false;
global.scumGenSacrificePit = false;
global.scumGenShop = false;
global.scumGenDark = false;

global.roomStyle = 0; // which templates to use for room generation (currently: 0 = original, 1 = Bizarre mode)
global.genYetiKing = false; // only used for bizarre mode
global.genAlienBoss = false; // only used for bizarre mode
global.eMult = ceil(global.enemyMult/2); // only used for bizarre mode - reduce spawn rate multiplier for bizarre mode, as more enemies spawn already due to larger variety of enemies
global.gameSaved = false; // restart music after resuming previous gameplay session

// for Level Generation test starting items. see oPlayer1>Create and oPlayer1>Alarm 6
global.spawnBow = false;
global.spawnSceptre = false;
global.spawnShotgun = false;
global.spawnTeleporter = false;
global.spawnMattock = false;

// for Editor Plus level loading - see oRandomDoor
global.secretDoor = 0;
global.doorChance = 160;
global.madeSecretDoor = false;

global.moreMinis = false; // create more mini-olmecs in tougher version of test room

// NOTE: poison effects disabled in YASM 1.7+
global.isPoisoned = false;
global.poison = 0;
global.poisonStrength = 1;
global.poisonMax = 1800;

// Disabling explosion damage and stun below allows for trick jumps by launching yourself around with bombs :)
// No longer used or accessible from config, but still functional
global.scumExplosionHurt = 1; // if 0, player is invulnerable to explosions NO LONGER FUNCTIONAL IN CODE?
global.scumExplosionStun = 1; // if 0, player does not get stunned by explosions
global.scumUnlockedEditor = 1; // unlocks all tiles in level editor

// global.throwDelay adds a delay (in seconds) until enemies can throw you again to prevent bouncing between
// a wall and being thrown endlessly. A slight random variation to the strength and angle that
// enemies throw as currently uses as an alternative solution.
// No longer used or accessible from config, but still functional
global.throwDelay = 0;

global.scumSpikeDamage = 99; // amount of damage (hearts) that spikes do
global.unarmed = false; // if true player has no default weapon (whip/mattock)
global.mattockBreak = 20; // 1 in global.mattockBreak chance of mattock item breaking each use - set to 0 for unbreakable
global.skipTransition = false; // if true, skips transition screen between custom levels (must be set to true for each level) - intended for use with MagicSigns

global.scumBallAndChain = global.initBC;

global.bulletDmg = 4;

// default damage (per step while in contact)
global.explosionDmg = 10;
global.spearDmg = 4;
global.crushDmg = 10; // smash trap and ceiling trap

global.mapSprite = sMapDefault;
global.mapIcon1 = sMapPlayer;
global.mapIcon1x = -1;
global.mapIcon1y = -1;
global.mapIcon2 = sInvisible;
global.mapIcon2x = -1;
global.mapIcon2y = -1;
global.mapIcon3 = sInvisible;
global.mapIcon3x = -1;
global.mapIcon3y = -1;
global.mapTitle = "UNKNOWN LOCATION";

global.customDir = "levels/"; // base folder to look for levels/sound/sprite resources for custom level

// Gahara
global.gahara = false; // easy check for code that only applies during Cyclone's levels/mod (custom HUD, object behaviour, etc.)
//global.musicStop = true; // if true, music plays through transitions/pause until song changes
global.shrooms = 0; // mushrooms collected
global.hpmax = 99; // maximum hp
