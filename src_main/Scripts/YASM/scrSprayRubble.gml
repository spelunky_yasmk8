// scrSprayRubble();
// argument0 = how many pieces
// argument1 = sprite 1 (ex sRubble)
// argument2 = sprite 2 (ex sRubbleSmall)

// called by solids during destroy event (oBrick etc) when destroy with force such as a boulder or explosion
// these rubble bits shower over the foreground and are not stopped by solids

var rubble;
repeat (argument0)
{
    rubble = instance_create(x+8+rand(0,8)-rand(0,8), y+8+rand(0,8)-rand(0,8), oRubblePieceNonSolid);
    if (rand(1,3) == 1) rubble.sprite_index = argument1; else rubble.sprite_index = argument2;
    rubble.image_blend = image_blend;
}
