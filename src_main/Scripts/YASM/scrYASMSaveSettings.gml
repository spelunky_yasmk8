var file;
file = file_text_open_write("yasm.cfg");
if (file) {
  file_text_write_string(file, "skipIntro=");
   if (global.skipIntro) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "useDoorWithButton=");
   if (global.useDoorWithButton) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "weaponsOpenContainers=");
   if (global.weaponsOpenContainers) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "scumClimbSpeed=");
   file_text_write_string(file, string(global.scumClimbSpeed));
   file_text_writeln(file);
  file_text_write_string(file, "scumFallDamage=");
   file_text_write_string(file, string(global.scumFallDamage));
   file_text_writeln(file);
  file_text_write_string(file, "scumSpringShoesReduceFallDamage=");
   if (global.scumSpringShoesReduceFallDamage) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "scumBallAndChain=");
   if (global.scumBallAndChain) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "scumFlipHold=");
   if (global.scumFlipHold) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "scumUnlocked=");
   if (global.scumUnlocked) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "scumDarkness=");
   file_text_write_string(file, string(global.scumDarkness));
   file_text_writeln(file);
  file_text_write_string(file, "scumGhost=");
   file_text_write_string(file, string(global.scumGhost));
   file_text_writeln(file);
  file_text_write_string(file, "ghostRandom=");
   if (global.ghostRandom) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "useFrozenRegion=");
   if (global.useFrozenRegion) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "trapMult=");
   file_text_write_string(file, string(global.trapMult));
   file_text_writeln(file);
  file_text_write_string(file, "enemyMult=");
   file_text_write_string(file, string(global.enemyMult));
   file_text_writeln(file);
  file_text_write_string(file, "scumHud=");
   if (global.scumHud) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "parallax=");
   if (global.parallax) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "scumMetric=");
   if (global.scumMetric) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "nudge=");
   if (global.nudge) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "enemyBreakWeb=");
   if (global.enemyBreakWeb) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "woodSpikes=");
   if (global.woodSpikes) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "boulderChaos=");
   if (global.boulderChaos) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "toggleRunAnywhere=");
   if (global.toggleRunAnywhere) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "naturalSwim=");
   if (global.naturalSwim) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_write_string(file, "SGAmmo=");
   if (global.optSGAmmo) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_write_string(file, "ImmTransition=");
   if (global.optImmTransition) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_write_string(file, "VSync=");
   if (global.optVSync) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_write_string(file, "throwEmptyShotgun=");
   if (global.optThrowEmptyShotgun) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_write_string(file, "startWithKapala=");
   if (global.startWithKapala) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_write_string(file, "doubleKiss=");
   if (global.optDoubleKiss) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_write_string(file, "enemyVariations=");
   if (global.optEnemyVariations) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_write_string(file, "roomStyle=");
   file_text_write_string(file, string(global.optRoomStyle));
   file_text_writeln(file);

  file_text_write_string(file, "spikeVariations=");
   if (global.optSpikeVariations) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_write_string(file, "shopkeeperIdiots=");
   if (global.optShopkeeperIdiots) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  if (global.scumStartLife != 4) {
    file_text_write_string(file, "initialHearts=");
     file_text_write_string(file, string(global.scumStartLife));
     file_text_writeln(file);
  }
  if (global.scumStartRope != 4) {
    file_text_write_string(file, "initialRopes=");
     file_text_write_string(file, string(global.scumStartRope));
     file_text_writeln(file);
  }
  if (global.scumStartBombs != 4) {
    file_text_write_string(file, "initialBombs=");
     file_text_write_string(file, string(global.scumStartBombs));
     file_text_writeln(file);
  }
  if (global.scumStartMoney != 0) {
    file_text_write_string(file, "initialMoney=");
     file_text_write_string(file, string(global.scumStartMoney));
     file_text_writeln(file);
  }
  if (global.scumTMLife != 2) {
    file_text_write_string(file, "initialHeartsTunnelMan=");
     file_text_write_string(file, string(global.scumTMLife));
     file_text_writeln(file);
  }
  if (global.scumTMRope != 0) {
    file_text_write_string(file, "initialRopesTunnelMan=");
     file_text_write_string(file, string(global.scumTMRope));
     file_text_writeln(file);
  }
  if (global.scumTMBombs != 0) {
    file_text_write_string(file, "initialBombsTunnelMan=");
     file_text_write_string(file, string(global.scumTMBombs));
     file_text_writeln(file);
  }
  if (global.scumTMMoney != 0) {
    file_text_write_string(file, "initialMoneyTunnelMan=");
     file_text_write_string(file, string(global.scumTMMoney));
     file_text_writeln(file);
  }

  file_text_write_string(file, "graphicsHigh=");
   if (global.graphicsHigh) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_write_string(file, "downToRun=");
   if (global.downToRun) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_write_string(file, "musicVol=");
   file_text_write_string(file, string(global.musicVol));
   file_text_writeln(file);

  file_text_write_string(file, "soundVol=");
   file_text_write_string(file, string(global.soundVol));
   file_text_writeln(file);

  file_text_write_string(file, "seedComputer=");
   if (global.optSeedComputer) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_write_string(file, "musicEnabled=");
   if (global.musicEnabled) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_write_string(file, "soundEnabled=");
   if (global.soundEnabled) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_close(file);
}
