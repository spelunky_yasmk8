// scrSprayGems();
// argument0,1 = x, y
// for big chest at end of game (bizarre mode)

var xx, yy, rubble;
xx = argument0;
yy = argument1;
repeat (16)
{
    rubble = instance_create(xx+rand(0,8)-rand(0,8), yy+rand(0,8)-rand(0,8), oRubblePieceNonSolid);
    rubble.sprite_index = choose(sEmerald, sRuby, sSapphire, sEmeraldBig, sRubyBig, sSapphireBig, sDiamond, sGoldChunk, sGoldNugget);
}
