// returns true if object (argument0) is within radius of light
// called by oObjectHelp to check if object can be clicked on for description

if (global.darkLevel) {
  var xx, yy;
  xx = argument0.x;
  yy = argument0.y;

  if (instance_exists(oLevel)) {
    if (isNearerThan(xx, yy, oPlayer1, 104-64*oLevel.darkness)) return true;
  }

       if (isNearerThan(xx, yy, oFlareCrate, 104)) return true;
  else if (isNearerThan(xx, yy, oFlare, 104)) return true;
  else if (isNearerThan(xx, yy, oLamp, 104)) return true;
  else if (isNearerThan(xx, yy, oLampItem, 104)) return true;
  else if (isNearerThan(xx, yy, oLampRed, 104)) return true;
  else if (isNearerThan(xx, yy, oLampRedItem, 104)) return true;
  else if (isNearerThan(xx, yy, oArrowTrapLeftLit, 40)) return true;
  else if (isNearerThan(xx, yy, oArrowTrapRightLit, 40)) return true;
  else if (isNearerThan(xx, yy, oTikiTorch, 40)) return true;
  else if (isNearerThan(xx, yy, oFireFrog, 40)) return true;
  else if (isNearerThan(xx, yy, oMagma, 40)) return true;
  else if (isNearerThan(xx, yy, oMagmaMan, 40)) return true;
  else if (isNearerThan(xx, yy, oFireball, 40)) return true;
  else if (isNearerThan(xx, yy, oSpearTrapLit, 40)) return true;
  else if (isNearerThan(xx, yy, oSmashTrapLit, 40)) return true;
  else if (isNearerThan(xx, yy, oThwompTrapLit, 40)) return true;
  else if (isNearerThan(xx, yy, oExplosion, 104)) return true;
  else if (isNearerThan(xx, yy, oLava, 40)) return true;
  else if (isNearerThan(xx, yy, oScarab, 24)) return true;
  else if (isNearerThan(xx, yy, oGhost, 72)) return true;
  return false;
}

return true;
