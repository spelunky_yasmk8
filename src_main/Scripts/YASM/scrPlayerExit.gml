// see oPlayer1 > Animation End event

if (pExit == xSTART and global.gameStart)
{
    global.gameStart = false;
    // scrClearGlobals();
    if (global.currLevel == 16) global.cityOfGold = false;

    if (global.scumLevelGenerated)
    {
        scrClearGlobals();
        room_goto(rGenerateLevel);
    }
    else if (global.testLevel != "")
    {
        if (global.gahara) stopAllMusic();
        scrClearGlobals();
        room_goto(rLevelEditor);
    }
    else if (global.customLevel and global.skipTransition) // use Magic Signs to set global.skipTransition to true
    {
        global.skipTransition = false;
        global.gameStart = true;
        room_goto(rLoadLevel);
    }
    else if (global.currLevel == 5) room_goto(rTransition1x);
    else if (global.currLevel == 9) room_goto(rTransition2x);
    else if (global.currLevel == 13) room_goto(rTransition3x);
    else if (global.levelType == 1) room_goto(rTransition2);
    else if (global.levelType == 2) room_goto(rTransition3);
    else if (global.levelType == 3) room_goto(rTransition4);
    else if (global.levelType == 4) room_goto(rTransition4);
    else room_goto(rTransition1);
}
else if (pExit == xSTART) // new game from title
{
    if (global.gameMode == 1) // bizarre mode
    {
        global.usedShortcut = false;
        global.lastShortcut = 101;
        global.gameStart = true;
        global.scoresStart = 0;
        global.bizarre = true;
        //scrClearGlobals();
        scrInitLives();
        global.currLevel = 1;
        shakeToggle = false;
        global.messageTimer = 0;
        room_goto(rLevel);
    }
    else
    {
        global.bizarre = false;
        global.usedShortcut = false;
        global.lastShortcut = 1;
        global.gameStart = true;
        scrInitLives();
        if (global.firstTime)
        {
            global.currLevel = 0;
            global.bombs = 0;
            global.rope = 2;
            global.firstTime = false;
            room_goto(rTutorial);
        }
        else if (global.levelType == 2) room_goto(rLevel2);
        else room_goto(rLevel);
    }
}
else if (pExit == xTUTORIAL)
{
    if (global.gameMode == 1) // bizarre mode
    {
        global.lastShortcut = 101;
        global.bizarre = true;
    }
    else
    {
        global.lastShortcut = 1;
        global.bizarre = false;
    }
    global.usedShortcut = false;
    global.gameStart = true;
    global.currLevel = 0;
    global.bombs = 0;
    global.rope = 2;
    room_goto(rTutorial);
}
else if (pExit == xTUTORIAL2)
{
    if (global.gameMode == 1) // bizarre mode
    {
        global.lastShortcut = 101;
        global.bizarre = true;
    }
    else
    {
        global.lastShortcut = 1;
        global.bizarre = false;
    }
    global.usedShortcut = false;
    global.gameStart = true;
    global.currLevel = 0;
    global.bombs = 0;
    global.rope = 2;
    room_goto(rTutorial2);
}
else if (pExit == xSCORES)
{
    global.gameStart = false;
    if (isRoom("rTitle"))
    {
        if (x > 496 and y < 80) global.scoresStart = 8; // test chamber
        else global.scoresStart = 0;
    }
    else if (isRoom("rSun"))
        global.scoresStart = 1;
    else if (isRoom("rMoon"))
        global.scoresStart = 2;
    else if (isRoom("rStars"))
        global.scoresStart = 3;
    else if (isRoom("rLava"))
        global.scoresStart = 4;
    else global.scoresStart = 0;
    room_goto(rHighscores);
}
else if (pExit == xTITLE)
{
    global.gameStart = false;
    if (isRoom("rHighscores"))
    {
        if (oPlayer1.x < 320) global.titleStart = 5; // test chamber door
        else global.titleStart = 1;
    }
    else if (isRoom("rTutorial") or isRoom("rTutorial2"))
    {
        scrClearGlobals();
        global.titleStart = 3;
        global.currLevel = 1;
        shakeToggle = false;
        global.darkLevel = false;
        global.snakePit = false;
        global.messageTimer = 0;
        global.mini1 = 0;
        global.mini2 = 0;
        global.mini3 = 0;
        global.hasJordans = false;
        global.arrows = 0;
    }
    room_goto(rTitle);
}
else if (pExit == xEND)
{
    global.gameStart = false;
    if (global.scumLevelGenerated)
    {
        scrClearGlobals();
        room_goto(rGenerateLevel);
    }
    else
    {
        room_goto(rEnd);
    }
}
else if (pExit == xSHORTCUT5)
{
    global.usedShortcut = true;
    global.lastShortcut = 5;
    global.currLevel = 5;
    global.gameStart = true;
    //if (isRoom("rHighscores"))
    scrInitLives();
    if (global.gameMode == 1)
    {
        global.scoresStart = 0;
        global.lastShortcut = 105;
        global.bizarre = true;
    }
    room_goto(rLevel);
}
else if (pExit == xSHORTCUT9)
{
    global.usedShortcut = true;
    global.lastShortcut = 9;
    global.currLevel = 9;
    global.gameStart = true;
    //if (isRoom("rHighscores"))
    scrInitLives();
    if (global.gameMode == 1)
    {
        global.scoresStart = 0;
        global.lastShortcut = 109;
        global.bizarre = true;
    }
    room_goto(rLevel2);
}
else if (pExit == xSHORTCUT13)
{
    global.usedShortcut = true;
    global.lastShortcut = 13;
    global.currLevel = 13;
    global.gameStart = true;
    //if (isRoom("rHighscores"))
    scrInitLives();
    if (global.gameMode == 1)
    {
        global.scoresStart = 0;
        global.lastShortcut = 113;
        global.bizarre = true;
    }
    room_goto(rLevel);
}
else if (pExit == xSUN)
{
    global.gameStart = false;
    global.lastShortcut = -1;
    scrInitLives();
    room_goto(rSun);
}
else if (pExit == xMOON)
{
    global.gameStart = false;
    global.lastShortcut = -2;
    scrInitLives();
    room_goto(rMoon);
}
else if (pExit == xSTARS)
{
    global.gameStart = false;
    global.lastShortcut = -3;
    scrInitLives();
    room_goto(rStars);
}
else if (pExit == xCHANGE)
{
    global.isDamsel = not global.isDamsel;
    active = true;
    depth = 50;
    invincible = 0;
    state = STANDING;
    facing = LEFT;
    if (global.isDamsel)
    {
        global.isTunnelMan = false;
        sprite_index = sDamselLeft;
    }
    else sprite_index = sStandLeft;
    //global.plife = 4;
    //global.bombs = 4;
    //global.rope = 4;
    //global.plife = global.scumStartLife;
    //global.bombs = global.scumStartBombs;
    //global.rope = global.scumStartRope;
    scrInitLives();

    xVel = 0;
}
else if (pExit == xCHANGE2)
{
    global.isTunnelMan = not global.isTunnelMan;
    if (global.isTunnelMan) global.isDamsel = false;
    active = true;
    depth = 50;
    invincible = 0;
    state = STANDING;
    facing = RIGHT;
    scrInitLives();
    /*
    if (global.isTunnelMan)
    {
        global.isDamsel = false;
        sprite_index = sTunnelLeft;
        global.plife = global.scumTMLife;
        global.bombs = global.scumTMBombs;
        global.rope = global.scumTMRope;
    }
    else
    {
        sprite_index = sStandLeft;
        //global.plife = 4;
        //global.bombs = 4;
        //global.rope = 4;
        global.plife = global.scumStartLife;
        global.bombs = global.scumStartBombs;
        global.rope = global.scumStartRope;
    }
    */
    xVel = 0;
    // room_goto(rStars);
}
else if (pExit == xLAVA1)
{
    global.gameStart = false;
    scrInitLives();
    room_goto(rLava);
}
else if (pExit == xTEST)
{
    global.gameStart = false;
    global.moreMinis = false;
    scrInitLives();
    room_goto(rTest);
}
else if (pExit == xTEST2)
{
    global.gameStart = false;
    global.moreMinis = true;
    scrInitLives();
    room_goto(rTest);
}
else if (pExit == xBIZARRE)
{
    global.usedShortcut = false;
    global.lastShortcut = 101;
    global.gameStart = true;
    global.scoresStart = 0;
    global.bizarre = true;
    //scrClearGlobals();
    global.currLevel = 1;
    shakeToggle = false;
    global.messageTimer = 0;
    scrInitLives();
    room_goto(rLevel);
}
else if (pExit == xDEBUG)
{
    stopAllMusic();
    global.tofu = true;
    global.currLevel = 1;
    global.gameStart = true;
    global.customLevel = false;
    global.firstCustomLevel = "";
    global.testLevel = "";
    global.previousMode = global.gameMode;
    scrInitLives();
    room_goto(rGenerateLevel);
}
else if (pExit == xMODE)
{
    global.gameStart = false;
    global.titleStart = 4;
    if (global.gameMode >= 1)
    {
        if (scrGetScore(11) > 0)
        {
            if (global.bizarrePlusTitle)
            {
                global.gameMode = 0;
                global.bizarrePlusTitle = false;
            }
            else global.bizarrePlusTitle = true;
        }
        else
        {
            global.gameMode = 0;
            global.bizarrePlusTitle = false;
        }
    }
    else
    {
        global.gameMode = 1;
        global.bizarrePlusTitle = false;
    }

    scrInitLives();
    room_goto(rTitle);
}
