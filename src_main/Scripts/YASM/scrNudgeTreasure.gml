// scrNudgeTreasure(x, y)
// Nudge oTreasure away from x,y (usually player)

// Nudge treasure with melee weapon
if (!collision_point(x, y, oSolid, 0, 0)) {
  if (global.nudge && instance_exists(oPlayer1) && !nudged) {
    if (isCollisionBottom(1) || yVel > 2) yVel -= rand(2, 4)*0.5;
    if (x < argument0) {
      if (xVel >= 0) xVel -= rand(15, 30)*0.1; // 10,15
    } else if (x > argument0) {
      if (xVel <= 0) xVel += rand(15, 30)*0.1;
    }
    nudged = true;
    alarm[9] = 10;
  }
} else if (depth < 1) {
  other.depth = 0;
}
