/*
scrScreenshot(surface_id, string_name, force_screen_save)
Saves surface to png format, using string_name as filename prefix
Function adds current date, time and png extension to filename
If no name provided, uses "screenshot"
If surface does not exist, takes screenshot using Game Maker's built-in method
If argument2 (force_screen_save) is true, uses GM's built in method instead of surface.
*/

var namePrefix, currDateTime;
if (argument1 == "") namePrefix = "Screenshot";
else namePrefix = argument1;

var timeStr;
currDateTime = date_current_datetime();
timeStr = "_"+string_get_date_value(currDateTime)+"_"+string(string_get_time_value(currDateTime));

if (not surface_exists(argument0) or argument2)
{
    screen_save(namePrefix+timeStr+".png");
    return true;
}
else if (surface_exists(argument0))
{
    surface_save(argument0, namePrefix+timeStr+".png");
    return true;
}

return false;
