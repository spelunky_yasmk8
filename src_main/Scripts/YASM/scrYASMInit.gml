// scrYASMInit
// YASM settings to be initizialize once at startup
// Called by scrInit

global.gameMode = 0; // 0 = vanilla (classic), 1 = bizarre
global.bizarre = false; // bizarre mode level gen and enemy behaviour (kind of redundant with global.gameMode now but too much code to fix)
global.bizarrePlus = false; // new game + for bizarre mode
global.bizarrePlusTitle = false; // for toggling bizarre mode / plus on title
global.yasmScore = 0; // line 11 of yasm.dat for extra data used in YASM 1.8+
global.ghostExtraTime = 30000; // default time until ghost appears after time limit is reached. can be customized by player in config.
global.testChamber = false; // if test chamber door has been revealed on title screen
global.showLevelPath = false; // show solution path from start to exit in test chamber
global.previousMode = 0; // restore game mode upon returning to title after using different game mode in test chamber
//global.musicStop = true; // if music is stopped between level transitions or plays continuously
global.toggleRunAnywhere = true; // allows switching between run/walk xVel/xFric without touching ground (more like Spelunky HD)
global.naturalSwim = true; // enhanced water movement from Spelunky Natural
global.noDolphin = true; // caps upward swimming velocity to prevent player launching out of surface of water (from Spelunky Natural)
global.optSGAmmo = true; // shotgun using ammo (and jars can drop ammo, and... you got the idea)
global.optSGAmmoOrig = global.optSGAmmo; // original value of `optSGAmmo` (Shopkeeper PC can change it)
global.optSKUnlocked = false; // Shopkeeper PC unlocked (not working yet)
global.optImmTransition = true; // second "attack" does immediate transition
global.optVSync = true;
global.optThrowEmptyShotgun = true;
global.optDoubleKiss = true; // unhurt damsel kisses twice
global.optEnemyVariations = true;
global.optRoomStyle = 0; // 1: bizarre; -1: random
global.optSpikeVariations = false;

// one-liners
global.onlinerSG = true;
global.onlinerSGText = "THIS IS MY BOOMSTICK!";

// Gahara
global.gahara = false; // easy check for code that only applies during Cyclone's levels/mod (custom HUD, object behaviour, etc.)
global.shrooms = 0; // mushrooms collected
global.hpmax = 99; // maximum hp

global.invPosBomb = 1; // DO NOT CHANGE - inventory initialization will correct these if item sort order has changed
global.invPosRope = 2;
global.invPosArrow = 5;
global.invPosCape = 10;
global.invPosJetpack = 11;

global.blockPickup = false; // destroyed blocks drop block items that can let the player place them again

global.lastExecuteStr = ""; // for debug script, see scrDebugKeys
global.devMode = true;
global.devscreenshot = false;
if (global.devMode and directory_exists("screenshots_dev")) global.devscreenshot = true;
