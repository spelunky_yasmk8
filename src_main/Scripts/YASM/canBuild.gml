// called by scrGetBuildPosition

var xx, yy, obj;
obj = argument2;

if (instance_exists(obj))
{
    xx = obj.x+argument0*16;
    yy = obj.y+argument1*16;

    if (not collision_rectangle(xx, yy, xx+15, yy+15, oSolid, 0, 0) and
        not collision_rectangle(xx, yy, xx+15, yy+15, oLadder, 0, 0) and
        not collision_rectangle(xx, yy, xx+15, yy+15, oPlatform, 0, 0) and
        not collision_rectangle(xx, yy, xx+15, yy+15, oExit, 0, 0) and
        not collision_rectangle(xx, yy, xx+15, yy+15, oXStart, 0, 0) and
        not collision_rectangle(xx+4, yy+4, xx+11, yy+11, oEnemy, 0, 0) and
        not collision_rectangle(xx+4, yy+4, xx+11, yy+11, oDamsel, 0, 0))
        return true;
}
return false;
