//Music must be manually restarted
if (global.musicEnabled) {
  if (global.levelType == 1) { playMusic(global.musLush, true); exit; }
  if (global.levelType == 2) { playMusic(global.musIce, true); exit; }
  if (global.levelType == 3) { playMusic(global.musTemple, true); exit; }
  if (isRoom("rOlmec")) { if (oPlayer1.active) playMusic(global.musBoss, true); exit; }
  if (isRoom("rTitle")) { if (oPlayer1.active) playMusic(global.musTitle, true); exit; }
  playMusic(global.musCave, true);
} else {
  //playMusic(global.musCave, true);
}
