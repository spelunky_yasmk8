// scrOpenChest();
// argument0 = instance id of chest to open
// for use with feature that allows whip/mattock/machete to open chests
var cid, obj, ctx, i;
cid = argument0;
if (instance_exists(cid)) {
  if (not collision_point(cid.x, cid.y, oSolid, 0, 0)) {
    with (cid) {
      if (sprite_index == sChest) {
        if (isRealLevel()) global.totalChests += 1;
        obj = 0;
        sprite_index = sChestOpen;
        if (variable_local_exists("contents")) ctx = contents; else ctx = "";
        /*{ // debug
          var cstr;
          cstr = "";
          if (ctx == "Bomb") {
            cstr = "Bomb";
          } else {
            for (i = 1; i <= string_length(ctx); i += 1) {
              if (cstr != "") cstr += ",";
              switch (string_char_at(ctx, i)) {
                case "e": cstr += "EM"; break;
                case "s": cstr += "SP"; break;
                case "r": cstr += "RB"; break;
                case "E": cstr += "BEM"; break;
                case "S": cstr += "BSP"; break;
                case "R": cstr += "BRB"; break;
                case "a": cstr += "S"; break;
                case "A": cstr += "SS"; break;
              }
            }
          }
          global.message = "CRATE CONTENTS";
          global.message2 = cstr;
          global.messageTimer = 80;
        }*/
        if (is_string(ctx)) {
          if (string_length(ctx) == 0) ctx = scrLevGenCreateChestContents();
        } else {
          ctx = scrLevGenCreateChestContents();
        }
        if (ctx == "Bomb") {
          obj = instance_create(x, y, oBomb);
          obj.xVel = rand(0, 3)-rand(0, 3);
          obj.yVel = -2;
          with (obj) {
            sprite_index = sBombArmed;
            alarm[1] = 40;
          }
          playSound(global.sndTrap);
        } else {
          playSound(global.sndChestOpen);
          for (i = 1; i <= string_length(ctx); i += 1) {
            obj = -1;
            switch (string_char_at(ctx, i)) {
              case "e": obj = instance_create(x, y, oEmerald); break;
              case "s": obj = instance_create(x, y, oSapphire); break;
              case "r": obj = instance_create(x, y, oRuby); break;
              case "E": obj = instance_create(x, y, oEmeraldBig); break;
              case "S": obj = instance_create(x, y, oSapphireBig); break;
              case "R": obj = instance_create(x, y, oRubyBig); break;
              case "a": if (global.optSGAmmo) obj = instance_create(x, y, oShellSingle); break;
              case "A": if (global.optSGAmmo) obj = instance_create(x, y, oShells); break;
            }
            if (/*instance_exists(obj)*/obj >= 0) {
              with (obj) {
                xVel = rand(0, 3)-rand(0, 3);
                yVel = -2;
                if (variable_local_exists("cost")) cost = 0;
                if (variable_local_exists("forSale")) forSale = 0;
              }
            }
          }
        }
        oPlayer1.kAttackPressed = false;
      } else if (sprite_index == sChestOpen && instance_exists(oPlayer1) and !nudged) {
        var xvr;
        xvr = rand(5, 8)*0.1;
        yVel -= 1;
        if (oPlayer1.x < x) xVel += xvr; else if (oPlayer1.x > x) xVel -= xvr;
        nudged = true;
        alarm[9] = 10;
      }
    }
  }
}
