if (global.devscreenshot)
{
    if (directory_exists("screenshots_dev"))
    {
        with (oScreen)
        {
            if (not paused) scrScreenshot(screen, "screenshots_dev/yasmdevscreenshot", false);
        }
    }
}
