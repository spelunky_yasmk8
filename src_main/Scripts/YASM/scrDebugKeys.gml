// called in oPlayer1.Step
// command window, "`"
if (keyboard_check_pressed(192) || (keyboard_check_pressed(vk_f12) && keyboard_check(vk_alt))) {
  var nit, los, what, dest, ashgun, executeStr;
  nit = 0;
  ashgun = false;
  //message_caption(true, "YASM Debug");
  //message_size(global.screenScale*160, global.screenScale*80);
  //message_alpha(0.8);
  //executeStr = get_string("?oObject (create in front of player)#@oObject (create at mouse cursor)#$variable (show variable value)#Text without ?/@/$ prefixes will be executed as GML##Command:", global.lastExecuteStr);
  executeStr = get_string("?oObject or @oObject (create object at mouse cursor)#$variable (show variable value)#Text without ?/@/$ prefixes will be executed as GML##Command:", global.lastExecuteStr);
  global.lastExecuteStr = executeStr;
  los = string_lower(executeStr);
  if (string_length(k8strtrim(los)) == 0) exit;
  /*
  if (los == "boulder") {
    if facing = LEFT instance_create(oPlayer1.x-40, oPlayer1.y-24, oBoulder);
    else instance_create(oPlayer1.x+40, oPlayer1.y-24, oBoulder);
  }
  */
  /*
  else if (string_pos("?", executeStr) == 1) // create object in front of player aligned to grid (ex: ?oPaste)
  {

      executeStr = string_replace_all(string_delete(executeStr, 1, 1), " ", "");
      if facing = LEFT execute_string("exObj = instance_create(oPlayer1.x-24, oPlayer1.y-8, " + executeStr + ")")
      else execute_string("exObj = instance_create(oPlayer1.x+8, oPlayer1.y-8, " + executeStr + ")");
      if instance_exists(exObj)
      {
           with (exObj)
           {
              move_snap(16, 16);
              if (variable_local_exists("cost"))
              {
                  cost = 0;
              }
          }
      }
  }
  */
  if (string_char_at(executeStr, 1) == "@" || string_char_at(executeStr, 1) == "?") {
    // as above, but create object at mouse x/y instead of at player position
    executeStr = string_delete(executeStr, 1, 1);
    execute_string("exObj = instance_create(mouse_x, mouse_y, "+executeStr+")");
    if (instance_exists(exObj)) { with (exObj) { if (variable_local_exists("cost")) cost = 0; } }
    exit;
  }
  if (string_pos("$", executeStr) == 1) {
    // show value of variable after $
    execute_string("show_message(string("+string_delete(executeStr, 1, 1)+"))");
    exit;
  }
  //execute_string(executeStr);
  if (string_pos("!", executeStr) == 1) {
    executeStr = string_delete(executeStr, 1, 1);
    execute_string(executeStr);
    exit;
  }
  if (string_pos(".", executeStr) == 1) {
    execute_string("global"+executeStr);
    exit;
    //.scumWhipUpgrade=<0|1>
  }
  // other commands
  if (los == "revive") {
    // restore player from death
    scrRevivePlayer();
    exit;
  }
  if (los == "killall") {
    with (oEnemy) hp = 0;
    playSound(global.sndPickup);
    global.message = "LETHAL COSMIC RAYS KILLED SOME MONSTERS!";
    global.message2 = "";
    global.messageTimer = 120;
    //window_set_cursor(cr_none);
    exit;
  }
  if (los == "lawabiding") {
    global.thiefLevel = 0;
    global.murderer = false;
    global.message = "YOU ARE NOT A CRIMINAL ANYMORE!";
    global.message2 = "";
    global.messageTimer = 120;
    exit;
  }
  if (los == "murderer") {
    global.murderer = true;
    global.message = "MURDERER!";
    global.message2 = "";
    global.messageTimer = 120;
    exit;
  }
  if (los == "thief") {
    global.thiefLevel += 1;
    global.message = "THIEF!";
    global.message2 = "";
    global.messageTimer = 120;
    exit;
  }
  if (k8getword(los, 0) == "test") {
    what = k8getword(los, 1);
    if (what == "stun") {
      // stun/bounce test
      global.plife = 99;
      stunned = true;
      bounced = false;
      stunTimer = rand(60, 180);
      fallTimer = rand(1, 60);
      exit;
    }
    if (what == "generator") {
      stopAllMusic();
      room_goto(rGenerateLevel);
      exit;
    }
    show_message("don't know how to test '"+what+"'");
    exit;
  }
  if (k8getword(los, 0) == "show") {
    what = k8getword(los, 1);
    if (what == "thief") {
      show_message("thief="+string(global.thiefLevel));
      exit;
    }
    show_message("don't know what '"+what+"' is");
    exit;
  }
  // to XXX
  if (k8getword(los, 0) == "to") {
    dest = k8getword(los, 1);
    if (dest == "exit" || dest == "exitl" || dest == "exitr") {
      if (not instance_exists(oPlayer1)) {
        show_message("can't teleport non-existing player");
        exit;
      }
      instance_activate_object(oExit);
      if (instance_exists(oExit)) {
        oPlayer1.x = global.exitX; //oExit.x+8;
        oPlayer1.y = global.exitY; //oExit.y+8;
        if (dest == "exitl") oPlayer1.x -= 16;
        if (dest == "exitr") oPlayer1.x += 16;
        oPlayer1.invincible = 90;
        oPlayer1.blink = 90;
      }
      exit;
    }
    if (dest == "olmec") {
      global.hasJetpack = true;
      global.bombs = 99;
      global.plife = 99;
      global.currLevel = 16;
      stopAllMusic();
      if (global.gameMode == 1 and global.bizarre) {
        room_goto(rOlmec2);
      } else {
        room_goto(rOlmec);
      }
      exit;
    }
    if (dest == "damsel") {
      if (instance_exists(oDamsel)) {
        var dms;
        dms = instance_find(oDamsel, 0);
        oPlayer1.x = dms.x;
        oPlayer1.y = dms.y;
        oPlayer1.invincible = 90;
        oPlayer1.blink = 90;
      }
      exit;
    }
    if (dest == "hiscores") {
      scrGotoHighscores();
      exit;
    }
    if (dest == "title") {
      room_goto(rTitle);
      exit;
    }
    show_message("unknown destination: '"+dest+"'");
    exit;
  }
  // give XXX
  if (k8getword(los, 0) == "give") {
    what = k8getword(los, 1);
    if (what == "jetpack") {
      global.hasJetpack = true;
      global.message = "YOU GOT JETPACK!";
      global.message2 = "";
      global.messageTimer = 120;
      exit;
    }
    if (what == "bombs") {
      global.bombs += 4;
      global.message = "YOU GOT 4 BOMBS!";
      global.message2 = "";
      global.messageTimer = 120;
      exit;
    }
    if (what == "ropes") {
      global.rope += 4;
      global.message = "YOU GOT 4 ROPES!";
      global.message2 = "";
      global.messageTimer = 120;
      exit;
    }
    if (what == "lives") {
      global.plife += 4;
      global.message = "YOU GOT 4 HEARTS!";
      global.message2 = "";
      global.messageTimer = 120;
      exit;
    }
    if (what == "money") {
      global.money += 10000;
      global.message = "YOU GOT SOME MONEY!";
      global.message2 = "";
      global.messageTimer = 120;
      exit;
    }
    // items
         if (what == "shotgun") nit = oShotgun;
    else if (what == "boomstick") { nit = oShotgun; ashgun = true; }
    else if (what == "cheatgun") nit = oCheatgun;
    else if (what == "pistol") nit = oPistol;
    else if (what == "machete") nit = oMachete;
    else if (what == "tele") nit = oTeleporter;
    else if (what == "tele2") nit = oTeleporter2;
    else if (what == "key") nit = oKey;
    else if (what == "sceptre") nit = oSceptre;
    else if (what == "block") nit = oBlockItem;
    else if (what == "rock") nit = oRock;
    else if (what == "jar") nit = oJar;
    else if (what == "skull") nit = oSkull;
    else if (what == "ball") nit = oBasketball;
    else if (what == "arrow") nit = oArrow;
    else if (what == "flare") nit = oFlare;
    else if (what == "fishbone") nit = oFishBone;
    else if (what == "mattock") nit = oMattock;
    else if (what == "mattockhead") nit = oMattockHead;
    else if (what == "webcannon") nit = oWebCannon;
    else if (what == "bow") nit = oBow;
    else if (what == "kapala") nit = oKapala;
    else if (what == "paste") nit = oPaste;
    else if (what == "compass") nit = oCompass;
    else if (what == "spring") nit = oSpringShoes;
    else if (what == "spike") nit = oSpikeShoes;
    else if (what == "spectacles") nit = oSpectacles;
    else if (what == "ankh") nit = oAnkh;
    else if (what == "gloves") nit = oGloves;
    else if (what == "mitt") nit = oMitt;
    else if (what == "cape") nit = oCapePickup;
    else if (what == "eye") nit = oUdjatEye;
    else if (what == "crown") nit = oCrown;
    else if (what == "diamond") nit = oDiamond;
    else if (what == "shell") nit = oShellSingle;
    else if (what == "shells") nit = oShells;
    else if (what == "damsel") nit = oDamsel;
    else if (what == "idol") nit = oGoldIdol;
    else if (what == "crystalidol") nit = oCrystalSkull;

    if (nit) {
      if (not instance_exists(oPlayer1)) {
        show_message("can't create item for non-existing player");
        exit;
      }
      // create item
      if (oPlayer1.facing == 18) {
        // left
        nit = instance_create(oPlayer1.x-16, oPlayer1.y-8, nit);
      } else {
        // right
        nit = instance_create(oPlayer1.x+16, oPlayer1.y-8, nit);
      }
      if (instance_exists(nit)) {
        with (nit) { if (variable_local_exists("cost")) cost = 0; }
        if (ashgun) nit.ashShotgun = true;
      }
      nit = 0;
      exit;
    }
    show_message("unknown entity: '"+what+"'");
    exit;
  }
  show_message("unknown command: '"+executeStr+"'");
  exit;
}
