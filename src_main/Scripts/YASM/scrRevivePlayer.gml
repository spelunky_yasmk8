// called by scrDebugKeys
// for playtesting - restores player from death in most cases

if (instance_exists(oPlayer1))
{
    global.plife = 8;
    if (global.bombs < 1) global.bombs = 4;
    if (global.rope < 1) global.rope = 4;

    with (oPlayer1)
    {
        dead = false;
        active = true;
        visible = true;
        depth = 50;
        stunned = false;
        bounced = false;
        fallTimer = 0;
        stunTimer = 0;
        invincible = 90;
        blink = 90;

        if (y > room_height)
        {
            if (x < 0 or x > room_width) x = 320;

            y = room_height-240;
            if (not collision_point(x, y+8, oSolid, 0, 0)) instance_create(x-8, y+8, oThinIce);
        }
        var checkLava;
        checkLava = false;

        while (collision_point(x, y, oLava, 0, 0))
        {
            obj = instance_position(x, y, oLava);
            if (instance_exists(obj))
            {
                checkLava = true;
                with (obj) instance_destroy();
                y -= 16;
            }
        }
        if (checkLava) global.checkWater = true;

        if (collision_point(x, y, oSolid, 0, 0))
        {


            if (not collision_point(x, y-24, oSolid, 0, 0)) { y -= 24; }
            else if (not collision_point(x-24, y-24, oSolid, 0, 0)) { x -= 24; y -= 24; }
            else if (not collision_point(x+24, y-24, oSolid, 0, 0)) { x += 24; y -= 24; }
            else if (instance_exists(oEnemy))
            {
                obj = instance_nearest(x, y, oEnemy);
                if (instance_exists(obj))
                {
                    x = obj.x+8;
                    y = obj.y+8;
                    with (obj) instance_destroy();
                }
            }
            else if (global.exitX > 0 and global.exitY > 0) { x = global.exitX+8; y = global.exitY+8; }
            else
            {
                instance_activate_object(oEntrance);
                if (instance_exists(oEntrance))
                {
                    x = oEntrance.x+8;
                    y = oEntrance.y+8;
                }

            }
        }
    }

}

global.drawHUD = true;
