// called by characterStepEvent
// help player jump up through one block wide gaps by nudging them to one side so they don't hit their head

var d;
d = 4; // max distance to nudge player

if (not collision_line(x, y-6, x, y-12, oSolid, 0, 0))
{
    if (collision_line(x-5, y-6, x-5, y-12, oSolid, 0, 0) and
        collision_line(x+14, y-6, x+14, y-12, oSolid, 0, 0))
    {
        while (collision_line(x-5, y-6, x-5, y-12, oSolid, 0, 0) and d > 0)
        {
            x += 1;
            d -= 1;
        }
    }
    else if (collision_line(x+5, y-6, x+5, y-12, oSolid, 0, 0) and
        collision_line(x-14, y-6, x-14, y-12, oSolid, 0, 0))
    {
        while (collision_line(x+5, y-6, x+5, y-12, oSolid, 0, 0) and d > 0)
        {
            x -= 1;
            d -= 1;
        }
    }

}
