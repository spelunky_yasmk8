// moved from oPlayer1.Step.Action so it could be shared with oAltarLeft so that traps will be triggered when the altar is destroyed without picking up the idol.
// argument0 = player triggered trap by grabbing idol (true/false)
// note: idols moved by monkeys use scrMonkeyTrigger
var trap, dist;

if (argument0) {
  global.idolsGrabbed += 1;
  if (instance_exists(holdItem)) {
    if (holdItem.type == "Gold Idol") holdItem.trigger = false;
  }
}

if (global.levelType == 0) {
  trap = instance_nearest(x, y-64, oGiantTikiHead);
  with (trap) alarm[0] = 100; //100
  scrShake(100);
} else if (global.levelType == 1) {
  if (global.cemetary and not global.ghostExists) {
    if (oPlayer1.x > room_width / 2) instance_create(view_xview[0]+view_wview[0]+8, view_yview[0]+floor(view_hview[0] / 2), oGhost);
    else instance_create(view_xview[0]-32, view_yview[0]+floor(view_hview[0] / 2), oGhost);
    global.ghostExists = true;
  }
  with (oTrapBlock) {
    dist = distance_to_object(oCharacter);
    if (dist < 90) { dying = true; /*instance_destroy();*/ }
  }
} else if (global.levelType == 3) {
  if (instance_exists(oCeilingTrap)) {
    with (oCeilingTrap) {
      status = 1;
      yVel = 0.5;
    }
    scrShake(20);
    trap = instance_nearest(x-64, y-64, oDoor);
    if (trap) { trap.status = 1; trap.yVel = 1; }
    trap = instance_nearest(x+64, y-64, oDoor);
    if (trap) { trap.status = 1; trap.yVel = 1; }
  } else {
    with (oTrapBlock) {
      dist = distance_to_object(oCharacter);
      if (dist < 90) instance_destroy();
      playSound(global.sndThump);
      scrShake(10);
    }
  }
}
