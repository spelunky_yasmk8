// Check for an item to nudge
// called by oWhip/oSlash/oMattockHit
var i, obj, objlist;

if (instance_place(x, y, oItem)) {
  objlist = instance_place_list(x, y, oItem);
  if (objlist != noone) {
    for (i = 0; i < ds_list_size(objlist); i += 1) {
      obj = ds_list_find_value(objlist, i);
      if (!obj.nudged) { with (obj) scrNudgeItem(oPlayer1.x, oPlayer1.y); }
    }
    ds_list_destroy(objlist);
  }
}

if (instance_place(x, y, oTreasure)) {
  objlist = instance_place_list(x, y, oTreasure);
  if (objlist != noone) {
    for (i = 0; i < ds_list_size(objlist); i += 1) {
      obj = ds_list_find_value(objlist, i);
      if (not obj.nudged) { with (obj) scrNudgeTreasure(oPlayer1.x, oPlayer1.y); }
    }
    ds_list_destroy(objlist);
  }
}
