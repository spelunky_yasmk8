// scrGotoHighscores
// often reused from oPlayer.Step Action event
scrGamepadReset();
scrClearGlobals();
global.lastShortcut = 0;
if (isRoom("rSun")) global.scoresStart = 1;
if (isRoom("rMoon")) global.scoresStart = 2;
if (isRoom("rStars")) global.scoresStart = 3;
room_goto(rHighscores);
