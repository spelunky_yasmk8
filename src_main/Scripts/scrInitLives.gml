if (global.isTunnelMan) {
  global.plife = global.scumTMLife; //2
  global.bombs = global.scumTMBombs; //0
  global.rope = global.scumTMRope; //0
  global.money = global.scumTMMoney;
} else {
  //global.plife = 4;
  //global.bombs = 4;
  //global.rope = 4;
  global.plife = global.scumStartLife;
  global.bombs = global.scumStartBombs;
  global.rope = global.scumStartRope;
  global.money = global.scumStartMoney;
}
