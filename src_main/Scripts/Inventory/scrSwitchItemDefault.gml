// Switch items (no inventory system)
// Called by oPlayer1.Step Action
if (holdItem) {
  // don't switch pooping monkey
  if (holdItem) {
    with (holdItem) {
      if (variable_local_exists("meGoldMonkey")) {
        if (holdItem.meGoldMonkey) exit;
      }
    }
  }
  if (holdItem.sprite_index == sBombArmed) {
      // do nothing
  } else if (holdItem.sprite_index == sBomb) {
    with (holdItem) {
      global.bombs += 1;
      instance_destroy();
    }
    if (global.rope > 0) {
      holdItem = instance_create(x, y, oRopeThrow);
      holdItem.held = true;
      global.rope -= 1;
      whoaTimer = whoaTimerMax;
    } else {
      scrHoldItem(pickupItemType);
    }
  } else if (holdItem.sprite_index == sRopeEnd) {
    with (holdItem) {
      global.rope += 1;
      instance_destroy();
    }
    scrHoldItem(pickupItemType);
  } else if (!holdItem.heavy && holdItem.cost == 0) {
    if (global.bombs > 0 || global.rope > 0) {
      pickupItemType = holdItem.type;
      if (isAshShotgun(holdItem)) pickupItemType = "Boomstick";
      if (holdItem.type == "Bow" && bowArmed) scrFireBow();
      with (holdItem) {
        breakPieces = false;
        instance_destroy();
      }
    }
    if (global.bombs > 0) {
      holdItem = instance_create(x, y, oBomb);
      if (global.hasStickyBombs && global.stickyBombsActive) holdItem.sticky = true;
      holdItem.held = true;
      global.bombs -= 1;
      whoaTimer = whoaTimerMax;
    } else if (global.rope > 0) {
      holdItem = instance_create(x, y, oRopeThrow);
      holdItem.held = true;
      global.rope -= 1;
      whoaTimer = whoaTimerMax;
    }
  }
} else {
  if (global.bombs > 0) {
    holdItem = instance_create(x, y, oBomb);
    if (global.hasStickyBombs && global.stickyBombsActive) holdItem.sticky = true;
    holdItem.held = true;
    global.bombs -= 1;
    whoaTimer = whoaTimerMax;
  } else if (global.rope > 0) {
    holdItem = instance_create(x, y, oRopeThrow);
    holdItem.held = true;
    global.rope -= 1;
    whoaTimer = whoaTimerMax;
  }
}
