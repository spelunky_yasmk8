// Player tries to sell item back to shopkeeper
// Ccalled by oPlayer1.Step Action
var merchant, disp;

merchant = instance_nearest(x, y, oShopkeeper);
if (!instance_exists(merchant)) exit;

with (holdItem) {
  if (!variable_local_exists("resaleValue")) resaleValue = 0;
}


if ((global.blackMarket and global.roomPath[scrGetRoomX(x), scrGetRoomY(y)] == 5) or (not global.blackMarket and merchant.style == "Craps")) {
  // don't allow selling to craps booth
} else if (merchant.style == "Kissing") {
  // don't allow selling to kissing booth
} else if (!madeOffer) {
  // initial offering of item, shopekeepr will respond with price or refusal
  if (holdItem.sprite_index == sCrystalSkull) {
    global.message = "A CRYSTAL SKULL!";
    global.message2 = "GET THAT THING AWAY FROM ME!";
    global.messageTimer = 120;
  } else {
    // calculate resale value of item
    // first, get original retail value
    // values that have been altered from vanilla are commented
    var retailValue;
    retailValue = 0;

    switch (holdItem.type) {
      case "Bomb": retailValue = 800; break; // bag of 3 is 2500
      case "Rope": retailValue = 800; break; // pile of 3 is 2500
      case "Machete": retailValue = 7000; break;
      case "Pistol": retailValue = 5000; break;
      case "Web Cannon": retailValue = 2000; break;
      case "Shotgun": retailValue = 15000; break;
      case "Cheatgun": retailValue = 1; break;
      case "Mattock": retailValue = 8000; break;
      case "Teleporter": retailValue = 10000; break;
      case "Teleporter II": retailValue = 15000; break;
      case "Bow": retailValue = 1000; break; // 6 arrows included with retail purchase
      case "Lamp": retailValue = 1000; break;
      case "Red Lamp": retailValue = 2000; break;
      case "Sceptre": retailValue = 50000; break; // ?
      case "Basketball": retailValue = 1000; break; // ?
      case "Arrow": retailValue = 20; break; // ?
      case "Mattock Head": retailValue = 800; break; // ?
      case "Dice": retailValue = 2000; break; // ?
      case "Jar": retailValue = 100; break; // ?
      case "Flare": retailValue = 30; break; // ?
      case "Flare Crate": retailValue = 100; break; // ?
      case "Chest": retailValue = 120; break; // could be opened or closed
      case "Locked Chest": retailValue = 5000; break; // ?
      case "Key": retailValue = 2000; break; //?
      case "Crate": retailValue = 2000; break; //?
      case "Bomb Bag": retailValue = 2500; break;
      case "Bomb Box": retailValue = 10000; break;
      case "Rope Pile": retailValue = 2500; break;
      case "Paste": retailValue = 3000; break;
      case "Parachute": retailValue = 2000; break;
      case "Compass": retailValue = 3000; break;
      case "Spring Shoes": retailValue = 5000; break;
      case "Spike Shoes": retailValue = 4000; break;
      case "Jordans": retailValue = 35000; break; // cost = 50000
      case "Spectacles": retailValue = 8000; break;
      case "Udjat Eye": retailValue = 16000; break; // cost = 0
      case "Crown": retailValue = 100000; break; // cost = 999999
      case "Kapala": retailValue = 80000; break; // cost = 999999
      case "Ankh": retailValue = 50000; break;
      case "Gloves": retailValue = 8000; break;
      case "Mitt": retailValue = 4000; break;
      case "Jetpack": retailValue = 20000; break;
      case "Cape": retailValue = 12000; break;
      case "Gold Idol": retailValue = 5000; break;
      case "Antidote": retailValue = 3200; break;
      case "Ball": retailValue = -1; break;
      case "Fire Frog Bomb": retailValue = -2; break;
      case "Rock": retailValue = -3; break;
      case "Skull": retailValue = -4; break;
      case "Fish Bone": retailValue = -5; break;
      default: retailValue = -1;
    }
    // now calculate resale value after boosting retail value based on cave level depth
    if (retailValue > 0) {
      if (holdItem.type == "Gold Idol") {
        // pay same amount as bringing idol to exit
        holdItem.resaleValue = retailValue*(global.levelType+1);
      } else {
        if (global.currLevel > 2) retailValue += (retailValue/100)*10*(global.currLevel-2);
        holdItem.resaleValue = ceil(retailValue/2);
      }
    } else {
      holdItem.resaleValue = 0;
    }

    if (holdItem.resaleValue > 0) {
      if (merchant.cash >= holdItem.resaleValue) {
        global.message = "YOU WANT TO SELL YOUR " + string_upper(holdItem.type) + "?";
        global.message2 = "I'LL GIVE YOU $" + string(holdItem.resaleValue) + ".";
        global.messageTimer = 200;
        madeOffer = true;
      } else {
        // shopkeeper can't afford item, so he tries to make a lower offer
        var rockBottom;
        rockBottom = holdItem.resaleValue-floor(holdItem.resaleValue*0.5);
        if (merchant.cash >= rockBottom) {
          var tempValue, tempDeduct;
          tempValue = holdItem.resaleValue;
          tempDeduct = 0;
          while (merchant.cash < holdItem.resaleValue) {
            tempDeduct += 0.1;
            holdItem.resaleValue = tempValue;
            holdItem.resaleValue -= floor(holdItem.resaleValue*tempDeduct);
          }
          if (holdItem.resaleValue <= 0) holdItem.resaleValue = 0;
          global.message = "GETTING RID OF YOUR OLD " +string_upper(holdItem.type) + "?";
          global.message2 = choose("I CAN GIVE YOU $" + string(holdItem.resaleValue) + ".",
              "HOW ABOUT $" + string(holdItem.resaleValue) + "?",
              "I'LL PAY $" + string(holdItem.resaleValue) + ".");
          global.messageTimer = 200;
          madeOffer = true;
        } else {
          // shopkeeper cannot afford this item
          global.message = choose("INTERESTING..", "NICE FIND..", "A FINE ITEM..", "VERY NICE..");
          global.message2 = choose(
            "BUT I CAN'T AFFORD SUCH THINGS.",
            "BUT I CAN'T PAY FOR IT RIGHT NOW.",
            "BUT I WILL HAVE TO DECLINE.",
            "BUT I CAN'T AFFORD IT!");
          global.messageTimer = 120;
          holdItem.resaleValue = 0;
          madeOffer = false;
        }
      }
    } else {
      // shopkeeper refuses worthless item
      var noStr;
      noStr = "";
      switch (holdItem.resaleValue) {
          case -1: noStr = "ARE YOU KIDDING ME?"; break;
          case -2: noStr = "GET THAT THING OUT OF HERE!"; break;
          case -3: noStr = choose("A ROCK? REALLY?", "HOW WOULD YOU LIKE TO EAT THAT ROCK?"); break;
          case -4: noStr = "HAVE SOME RESPECT FOR THE DEAD!"; break;
          case -5: noStr = choose("GROSS!", "WORTHLESS!"); break;
          default: noStr = "IS THIS A JOKE?";
       }
      global.message = noStr;
      global.message2 = "I'M NOT INTERESTED IN THAT ITEM.";
      global.messageTimer = 120;
    }
  }
} else if (holdItem.resaleValue > 0 and madeOffer) {
  // player accepts price offered by shopkeeper, complete transaction
  if (merchant.cash >= holdItem.resaleValue) {
    global.collect += holdItem.resaleValue;
    merchant.cash -= holdItem.resaleValue;
    global.collectCounter += 20;
    if (global.collectCounter > 100) global.collectCounter = 100;
    playSound(global.sndCoin);
    disp = instance_create(x, y-8, oBigCollect);
    disp.sprite_index = sBigCollectGreen;
    with (holdItem) {
      breakPieces = false;
      instance_destroy();
    }
    holdItem = 0;
    pickupItemType = "";
    madeOffer = false;
    global.message = choose(
      "A FINE TRANSACTION INDEED!",
      "PLEASE COME AGAIN!",
      "WHAT A GREAT DEAL!",
      "THANKS FOR STOPPING BY!",
      "ALL TRANSACTIONS ARE FINAL!");
    global.message2 = "";
    global.messageTimer = 120 ;
  } else {
    // error handling
    madeOffer = false;
    holdItem.resaleValue = 0;
    global.message = choose(
      "I'M AFRAID YOU'RE MISTAKEN.",
      "WHAT? OH, NEVERMIND.",
      "NO DEAL.");
    global.message2 = "";
    global.messageTimer = 80;
  }
}
