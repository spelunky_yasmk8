// isAshShotgun(inst)
var item;
item = argument0;
if (instance_exists(item)) {
  with (item) {
    if (variable_local_exists("type")) {
      if (type == "Shotgun") {
        if (variable_local_exists("ashShotgun")) return ashShotgun;
      }
    }
  }
}
return false;
