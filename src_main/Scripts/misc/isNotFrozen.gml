// must be called with active instance
if (global.useFrozenRegion) {
  var xv, yv, sw, sh;
  xv = view_xview[0];
  yv = view_yview[0];
  sw = sprite_width;
  sh = sprite_height;
  if (sw < 16) sw = 16;
  if (sh < 16) sh = 16;
  return
    /*
    x > x-16-16 && x < x+view_wview[0]+16+16 &&
    y > y-16-16 && y < y+view_hview[0]+16+16;
    */
    x > xv-sw-32 && x < xv+view_wview[0]/*+sw*/+32 &&
    y > yv-sh-32 && y < yv+view_hview[0]/*+sh*/+32;
}
return false;
