//if (argument0 == 0) return argument1+1;
//if (argument1 == 0) return ackermann(argument0-1, 1);
//return ackermann(argument0-1, ackermann(argument0, argument1-1));
var m, n;
m = argument0;
n = argument1;
if (m == 0) return n+1;
if (n == 0) return ackermann(m-1, 1);
return ackermann(m-1, ackermann(m, n-1));
