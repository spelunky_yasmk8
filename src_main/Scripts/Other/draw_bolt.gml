// Katuko

var x1, y1, x2, y2, segsize, c, seg;
x1 = argument0;
y1 = argument1;
x2 = argument2;
y2 = argument3;
segsize = argument4; // distance between segments
c = argument5; // color


var len, dir, xx, yy, prevx, prevy, i, w, d;
len = point_distance(x1, y1, x2, y2);
dir = point_direction(x1, y1, x2, y2);

prevx = x1;
prevy = y1;

seg = floor(len/segsize);

draw_set_color(c);

for (i=1; i<seg; i+=1) {
    w = 10 - irandom(20);
    d = len/seg * i;
    xx = x1 + lengthdir_x(d, dir) + lengthdir_x(w, dir+90);
    yy = y1 + lengthdir_y(d, dir) + lengthdir_y(w, dir+90);
    draw_line(prevx, prevy, xx, yy);
    prevx = xx;
    prevy = yy;
}
draw_line(prevx, prevy, x, y);
