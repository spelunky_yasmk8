// scrDrawGHUD
// Gahara HUD
// compass arrows are drawn already
var lifeX, bombX, ropeX, mushX, moneyX, life, n, m, malpha;

lifeX = 4; //8
bombX = 72; //64
ropeX = 104; //120
mushX = 136; //136
moneyX = 176;
draw_set_font(global.myFontSmall);
draw_set_color(c_white);

if (global.plife == 1)
{
    draw_sprite(sHeartSmallBlink, global.heartBlink, lifeX+2, 4);
    global.heartBlink += 0.1;
    if (global.heartBlink > 3) global.heartBlink = 0;
}
else draw_sprite(sHeartSmall,-1,lifeX+2,4);

life = global.plife;
if (life < 0) life = 0;
draw_text(lifeX+16, 8, string(life) + chr(47) + string(global.hpmax));

if (global.hasStickyBombs && global.stickyBombsActive) draw_sprite(sStickyBombIconSmall, -1,bombX+2,4);
else draw_sprite(sBombIconSmall,-1,bombX+2,4);
draw_text(bombX+16, 8, string(global.bombs));

draw_sprite(sRopeIconSmall,-1,ropeX+2,4);
draw_text(ropeX+16, 8, string(global.rope));

draw_sprite(sMushroom2Small, -1, mushX+6, 7);
draw_text(mushX+16, 8, string(min(global.shrooms, 99)));

draw_sprite(sDollarSignSmall,-1,moneyX+2,3);
draw_text(moneyX+16, 8, string(global.money));

/*
draw_sprite(sHoldItemIcon, -1, 8, 24);
if (oPlayer1.pickupItemType != "" and oPlayer1.holdItem > 0)
{
    if (oPlayer1.pickupItemType== "Rock") draw_sprite(sRock, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Jar") draw_sprite(sJar, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Skull") draw_sprite(sSkull, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Fish Bone") draw_sprite(sFishBone, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Arrow") draw_sprite(sArrowRight, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Machete") draw_sprite(sMacheteRight, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Mattock") draw_sprite(sMattockRight, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Mattock Head") draw_sprite(sMattockHead, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Pistol") draw_sprite(sPistolRight, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Web Cannon") draw_sprite(sWebCannonR, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Teleporter") draw_sprite(sTeleporter, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Shotgun") draw_sprite(sShotgunRight, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Bow")
    {
        if oPlayer1.bombArrowCounter < 80
        {
            draw_set_font(global.myFontSmall)
            draw_set_color(c_white)
            if oPlayer1.bombArrowCounter > 60 draw_text(12,28,"3")
            else if oPlayer1.bombArrowCounter > 30 and oPlayer1.bombArrowCounter <= 59 {draw_set_color(c_yellow); draw_text(12,28,"2")}
            else {draw_set_color(c_red); draw_text(12,28,"1")}
        }
        else draw_sprite(sBowDisp, -1, 8+8, 24+8);
    }
    else if (oPlayer1.pickupItemType== "Sceptre") draw_sprite(sSceptreRight, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Flare") draw_sprite(sFlare, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Key") draw_sprite(sKeyRight, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Basketball") draw_sprite(sBasketball, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Ball") draw_sprite(sBall, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Rope") draw_sprite(sRopeEnd, -1, 8+8, 24+8);
    else if (oPlayer1.pickupItemType== "Bomb" and oPlayer1.holdItem > 0)
    {
        if oPlayer1.holdItem.armed
        {
            draw_set_font(global.myFontSmall)
            draw_set_color(c_white)
            if oPlayer1.holdItem.alarm[0] > 50 draw_text(12, 28, "4")
            else if oPlayer1.holdItem.alarm[0] > 20 and oPlayer1.holdItem.alarm[0] <= 49 draw_text(12, 28, "3")
            else if oPlayer1.holdItem.alarm[0] < 21 and oPlayer1.holdItem.alarm[0] > 0 {draw_set_color(c_yellow); draw_text(12, 28, "2")}
            else if oPlayer1.holdItem.alarm[1] > 30 {draw_set_color(c_yellow); draw_text(12, 28, "2")}
            else if oPlayer1.holdItem.alarm[1] < 31 and oPlayer1.holdItem.alarm[1] > 0 {draw_set_color(c_red); draw_text(12, 28, "1")}
        }
        else draw_sprite(sBomb, -1, 8+8, 24+8)
    }
    else if (oPlayer1.pickupItemType== "Crate") draw_sprite(sCrate, -1, 8+7, 24+4);
    else if (oPlayer1.pickupItemType== "Flare Crate") draw_sprite(sFlareCrate, 0, 8+7, 24+4);
    else if (oPlayer1.pickupItemType== "Lamp") draw_sprite(sLampItem, -1, 8+8, 24+12);
    else if (oPlayer1.pickupItemType== "Red Lamp") draw_sprite(sLampRedItem, -1, 8+8, 24+12);
    else if (oPlayer1.pickupItemType== "Locked Chest") draw_sprite(sLockedChest, -1, 8+8, 24+4);
    else if (oPlayer1.pickupItemType== "Chest") draw_sprite(oPlayer1.holdItem.sprite_index, -1, 8+8, 24+4);
    else if (oPlayer1.pickupItemType== "Damsel")
    {
        if global.isDamsel draw_sprite(sDudeIconSmall, -1, 8, 24)
        else draw_sprite(sDamselIconSmall, -1, 8, 24)
    }
    else if (oPlayer1.pickupItemType== "Dice") draw_sprite(oPlayer1.holdItem.sprite_index, -1, 8+8, 24+4)
    else if (oPlayer1.pickupItemType== "Gold Idol")
    {
        if oPlayer1.holdItem > 0
        {
            if oPlayer1.holdItem.value == 15000 draw_sprite(sCrystalSkull, -1, 8+8, 24+8)
            else draw_sprite(sGoldIdol, -1, 8+8, 24+8)
        }
    }
    else if (oPlayer1.pickupItemType== "Fire Frog Bomb") draw_sprite(sFireFrogArmedL, 0, 8+8, 24+5)
    else if (oPlayer1.pickupItemType== "Caveman" or
        oPlayer1.pickupItemType== "Vampire" or
        oPlayer1.pickupItemType== "Hawkman" or
        oPlayer1.pickupItemType== "ManTrap" or
        oPlayer1.pickupItemType== "Yeti" or
        oPlayer1.pickupItemType== "Shopkeeper") draw_sprite(oPlayer1.holdItem.sprite_index, 0, 8, 21)
}
*/
n = 8; //28;
if (global.hasUdjatEye)
{
    if (global.udjatBlink) draw_sprite(sUdjatEyeIcon2, -1, n, 24);
    else draw_sprite(sUdjatEyeIcon, -1, n, 24);
    n += 20;
}
if (global.hasAnkh)
{
    draw_sprite(sAnkhIcon, -1, n, 24);
    n += 20;
}
if (global.hasCrown)
{
    draw_sprite(sCrownIcon, -1, n, 24);
    n += 20;
}
if (global.hasKapala)
{
    if (global.bloodLevel == 0) draw_sprite(sKapalaIcon, 0, n, 24);
    else if (global.bloodLevel <= 2) draw_sprite(sKapalaIcon, 1, n, 24);
    else if (global.bloodLevel <= 4) draw_sprite(sKapalaIcon, 2, n, 24);
    else if (global.bloodLevel <= 6) draw_sprite(sKapalaIcon, 3, n, 24);
    else if (global.bloodLevel <= 8) draw_sprite(sKapalaIcon, 4, n, 24);
    n += 20;
}
if (global.hasSpectacles)
{
    draw_sprite(sSpectaclesIcon, -1, n, 24);
    n += 20;
}
if (global.hasGloves)
{
    draw_sprite(sGlovesIcon, -1, n, 24);
    n += 20;
}
if (global.hasMitt)
{
    draw_sprite(sMittIcon, -1, n, 24);
    n += 20;
}
if (global.hasSpringShoes)
{
    draw_sprite(sSpringShoesIcon, -1, n, 24);
    n += 20;
}
if (global.hasJordans)
{
    draw_sprite(sJordansIcon, -1, n, 24);
    n += 20;
}
if (global.hasSpikeShoes)
{
    draw_sprite(sSpikeShoesIcon, -1, n, 24);
    n += 20;
}
if (global.hasCape)
{
    draw_sprite(sCapeIcon, -1, n, 24);
    n += 20;
}
if (global.hasJetpack)
{
    draw_sprite(sJetpackIcon, -1, n, 24);
    n += 20;
}
if (global.hasStickyBombs)
{
    draw_sprite(sPasteIcon, -1, n, 24);
    n += 20;
}
if (global.hasCompass)
{
    draw_sprite(sCompassIcon, -1, n, 24);
    n += 20;
}
if (global.hasParachute)
{
    draw_sprite(sParachuteIcon, -1, n, 24);
    n += 20;
}
if (oPlayer1.pickupItemType == "Bow" and global.arrows > 0)
{
    m = 1;
    malpha = 1;
    while (m <= global.arrows and m <= 20)
    {
        draw_sprite_ext(sArrowIcon, -1, n, 24, 1, 1, 0, c_white, malpha);
        n += 4;
        if (m > 10) malpha -= 0.1;
        m += 1;
    }

}

draw_set_font(global.myFontSmall);
draw_set_color(c_yellow);

if (global.collect > 0)
{
    draw_text(moneyX+8, 8+8, "+" + string(global.collect));
}
