// save totals
// first read and store 11 lines of data, then rewrite the file
var i, fl, vlist, word, wnum, pos, val;

fl = file_text_open_read(global.dataFile);
if (fl) {
  // skip normal data
  for (i = 0; i < 11; i += 1) {
    file_text_read_string(fl);
    file_text_readln(fl);
  }
  i = 1;
  // check data version
  vlist = scrTotalsVarList();
  wnum = 0;
  word = k8getword(vlist, wnum); wnum += 1;
  val = 0;
  while (!file_text_eof(fl)) {
    val = scrGetCrNum(file_text_read_string(fl), i); i += 1;
    file_text_readln(fl);
    if (val == 0) {
      vlist = scrTotalsVarListv0();
      val = 1;
    } else {
      val = (val == real(word));
    }
    break;
  }
  if (val) {
    //tfl = file_text_open_write("z00.log");
    // read data
    while (!file_text_eof(fl)) {
      word = k8getword(vlist, wnum); wnum += 1;
      if (string_length(word) == 0) break;
      //file_text_write_string(tfl, word); file_text_writeln(tfl);
      pos = string_pos(":", word);
      if (pos > 0) {
        // array
        var pp, n, c;
        pp = pos+1;
        n = 0;
        while (pp <= string_length(word)) {
          n = n*10+ord(string_char_at(word, pp))-ord('0');
          pp += 1;
        }
        word = string_delete(word, pos, string_length(word));
        for (c = 0; c < n; c += 1) {
          val = scrGetCrNum(file_text_read_string(fl), i); i += 1;
          file_text_readln(fl);
          variable_global_array_set(word, c, val);
        }
      } else {
        // var
        val = scrGetCrNum(file_text_read_string(fl), i); i += 1;
        file_text_readln(fl);
        variable_global_set(word, val);
      }
    }
  }
  file_text_close(fl);
  //file_text_close(tfl);
}
