// save totals
// first read and store 11 lines of data, then rewrite the file
var vlist, word, wnum, pos;

vlist = scrTotalsVarList();
wnum = 1; // skip version
while (true) {
  word = k8getword(vlist, wnum); wnum += 1;
  if (string_length(word) == 0) break;
  pos = string_pos(":", word);
  if (pos > 0) {
    // array
    var pp, n, c;
    pp = pos+1;
    n = 0;
    while (pp <= string_length(word)) {
      n = n*10+ord(string_char_at(word, pp))-ord('0');
      pp += 1;
    }
    word = string_delete(word, pos, string_length(word));
    for (c = 0; c < n; c += 1) {
      variable_global_array_set(word, c, 0);
    }
  } else {
    // var
    variable_global_set(word, 0);
  }
}
