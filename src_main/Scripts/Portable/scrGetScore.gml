// scrGetScore(entry)
// Returns a specific score value
// This replaces the highscore_value() function

/*
argument0:
1 = Money
2 = Time
3 = Kills
4 = Saves
5 = Plays
6 = Wins
7 = Deaths
8 = Tunnel 1
9 = Tunnel 2
10 = Mini games
11 = YASM 1.8 extra save data
*/
var i, idx, value, fl;
idx = argument0;

if (argument0 < 1 || argument0 > 11) return -667;
value = -1;
fl = file_text_open_read(global.dataFile);
if (fl) {
  for (i = 1; i < idx; i += 1) { file_text_read_string(fl); file_text_readln(fl); }
  if (!file_text_eof(fl)) {
    var line;
    line = file_text_read_string(fl);
    file_text_close(fl);
    value = scrGetCrNum(line, idx);
    //value = 1000+idx;
  }
}
if (value == -1) {
  // return default value
       if (idx == 8) return global.tunnel1Max+1;
  else if (idx == 9) return global.tunnel2Max+1;
  else return 0;
}
return value;
