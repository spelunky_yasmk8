// scrEncryptedScore(real, keyidx)
// takes value and returns an encrypted 32 character string to be written to score file
// argument0 = score (real)
// argument1 = slot (1-10)
// called by scrUpdateHighScores, scrResetHighScores, scrImportHighscores

// Place score value in a random position within a 32 character string of random characters to complicate decryption
var scoreStr, scoreLen, n, scorePos, preGarbage, postGarbage, garbageCharCode, postn, i;

scoreStr = "["+string(argument0)+"]";
scoreLen = string_length(scoreStr);
n = 32;
scorePos = rand(1, 32-scoreLen);
preGarbage = "";
postGarbage = "";
garbageCharCode = 0;

// generate pre-garbage
if (scorePos > 1) {
  for (i = 1; i < scorePos; i += 1) {
    while (true) {
      garbageCharCode = rand(32, 125);
      if (garbageCharCode != 91 && garbageCharCode != 93) break;
    }
    preGarbage += chr(garbageCharCode);
  }
}

for (postn = n-scoreLen-string_length(preGarbage); postn > 0; postn -= 1) {
  while (true) {
    garbageCharCode = rand(32, 125);
    if (garbageCharCode != 91 && garbageCharCode != 93) break;
  }
  postGarbage += chr(garbageCharCode);
}

return scrCipher(preGarbage+scoreStr+postGarbage, argument1, true); // encrypt
