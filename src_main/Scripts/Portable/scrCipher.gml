// scrCipher(text_string, key number, encrypt)
// argument2: true = encrypt text, false = decrypt text
// returns encrypted/decrypted string

// called by scrEncryptedScore, scrGetScore

var textStr1, keyStr1, kidx;
textStr1 = argument0;

// each score slot uses a unique encryption key (ONLY USE ASCII CHARACTERS 32-125)
if (argument1 < 1) {
  kidx = 0;
} else {
  kidx = (argument1-1) mod 10;
}
keyStr1 = ""; // linter
switch (kidx) {
  case 0: keyStr1 = "F&-RlbkW{,1]DY|)8>b|FNMh@q]nnV.3"; break;
  case 1: keyStr1 = "U=O=+>=BXNswc=%TJY;3tI^|&2kot7UG"; break;
  case 2: keyStr1 = "&kdJ(ga^\c&PTNRJ?eNlVQD|VOMiX}s+"; break;
  case 3: keyStr1 = "|!n4@OQ.<&Q/K\kEo-:o+>?qsK1t7N&O"; break;
  case 4: keyStr1 = "3rRZ!<<]8r=<X2:Q$)&=f_%o`\h-c,7i"; break;
  case 5: keyStr1 = "y{BKQJE+()<?{jmD+.Owek{Hk;BN[OP)"; break;
  case 6: keyStr1 = "v|L2GV;yl{90_I(e?cHX3^0a}I(kU(dR"; break;
  case 7: keyStr1 = "TkuG%K}?l{r_tZ=,fJ`s=r6;nm-HMLa!"; break;
  case 8: keyStr1 = "Y0?zp?B{N|4Jy<PGJ>Az%,6wvU=bW?:["; break;
  case 9: keyStr1 = "I3}-tUBiuPM`_5l&kG!|>}S@.>Ch&Q<$"; break;
}

var ch, newChar, newStr, keyChar, keyPos, i;
ch = ""; // current character in textStr1
newChar = ""; // new encrypted character
newStr = ""; // encrypted textStr1
keyChar = ""; // current character at keyPos in keyStr1
keyPos = 1; // current position in keyStr1

var charCode, keyCharCode, newCharCode;
charCode = 0; // ascii code for ch
keyCharCode = 0; // ascii code for keyChar
newCharCode = 0; // ascii code for newChar


for (i = 1; i <= string_length(textStr1); i += 1) {
  // get current character
  ch = string_char_at(textStr1, i);
  charCode = ord(ch);
  if (charCode < 32 || charCode >= 127) continue;
  charCode -= 32;

  // get current key character
  keyChar = string_char_at(keyStr1, keyPos);
  keyCharCode = ord(keyChar)-32;
  keyPos += 1;
  if (keyPos > string_length(keyStr1)) keyPos = 1; // loop key

  // cipher
  if (argument2) {
    // encrypt
    newCharCode = ((charCode+keyCharCode) mod 93)+32;
  } else {
    // decrypt
    var codeTemp;
    codeTemp = charCode-keyCharCode;
    if (codeTemp < 0) codeTemp += 93;
    newCharCode = (codeTemp mod 93)+32;
  }

  if (newCharCode < 32 || newCharCode > 125) show_error("Invalid character in encryption/decryption.", false);

  newChar = chr(newCharCode);
  newStr += newChar;
}

return newStr;
