// scrGetCrNum(str, keyidx)
var line, value;

line = scrCipher(argument0, argument1, false); // decrypt
value = -1;

// get the real value in square brackets from a string like: "laskjf982jq flkjasf[935]alskdjf8a3j88aj"
// (doesn't handle negative numbers)
var bspos, bepos;
bspos = string_pos("[", line);
bepos = string_pos("]", line);
if (bspos > 0 && bepos > bspos+1) {
  // proper format found
  var s, len;
  len = bepos-bspos-1;
  s = string_copy(line, bspos+1, len);
  s = string_digits(s);
  if (string_length(s) > 0) value = real(s);
}

return value;
