// save totals
// first read and store 11 lines of data, then rewrite the file
var i, fl, lines, vlist, word, wnum, pos;
lines = 0; // linter

// fill array in case the file is absent
for (i = 0; i < 11; i += 1) lines[i] = scrEncryptedScore(0, i+1);

if (string(argument0) != "reset highscores") {
  fl = file_text_open_read(global.dataFile);
  if (fl) {
    for (i = 0; i < 11; i += 1) {
      lines[i] = file_text_read_string(fl);
      file_text_readln(fl);
    }
    file_text_close(fl);
  }
}

/*
lines[1-1] = 0;
lines[3-1] = 0;
lines[3-1] = 0;
lines[7-1] = 0;
for (i = 0; i < 16; i += 1) lines[7-1] += variable_global_array_get("levelDeaths", i);
lines[5-1] = lines[7-1];
lines[7-1] = scrEncryptedScore(lines[7-1], 7);
lines[5-1] = scrEncryptedScore(lines[5-1], 5);
lines[4-1] = scrEncryptedScore(0, 4);
*/

fl = file_text_open_write(global.dataFile);
if (fl) {
  for (i = 0; i < 11; i += 1) {
    file_text_write_string(fl, lines[i]);
    file_text_writeln(fl);
  }
  i = 1;
  // now write data version
  vlist = scrTotalsVarList();
  wnum = 0;
  word = k8getword(vlist, wnum); wnum += 1;
  file_text_write_string(fl, scrEncryptedScore(real(word), i)); i += 1;
  file_text_writeln(fl);
  while (true) {
    word = k8getword(vlist, wnum); wnum += 1;
    if (string_length(word) == 0) break;
    pos = string_pos(":", word);
    if (pos > 0) {
      // array
      var pp, n, c;
      pp = pos+1;
      n = 0;
      while (pp <= string_length(word)) {
        n = n*10+ord(string_char_at(word, pp))-ord('0');
        pp += 1;
      }
      word = string_delete(word, pos, string_length(word));
      for (c = 0; c < n; c += 1) {
        file_text_write_string(fl, scrEncryptedScore(variable_global_array_get(word, c), i)); i += 1;
        file_text_writeln(fl);
      }
    } else {
      // var
      file_text_write_string(fl, scrEncryptedScore(variable_global_get(word), i)); i += 1;
      file_text_writeln(fl);
    }
  }
  file_text_close(fl);
}

scrStatsTextWrite();
