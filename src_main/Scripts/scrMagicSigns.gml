// Magic Signs 2.2.3 by Urza
// http://mossmouth.com/forums/index.php?topic=1540.0
var command, mycomm, target, mytarg, tcount, temp, temp2, temp3, arr, i, j;
var pos, make, obj, sprt, attr, saveobj, savespr, keep, matchmode, me;
globalvar scrMagicSigns_Result2, scrMagicSigns_Macro, scrMagicSigns_MacroCount;
globalvar scrMagicSigns_Last, scrMagicSigns_Prev;

if (is_real(argument0)) //looks like it might be an event
{
    if (event_type == ev_alarm)
    {
        if (event_number == 0) //main sign block
        {
            //Initialize
            me = self.id; //So we know who we are even in with blocks
            if (not variable_local_exists("done")) {
            //Once only (even if re-run by conditional).
                original = message;
                signmode = "";
                banner = -1;
                perm = 0;
                done = 0;
            }
            if (done == 1) exit;
            if (perm == -1) perm = 0;
            done = 1;
            message = string_replace_all(message, "&&", "╠╠");
            message = string_replace_all(message, "'(", "╚");
            message = string_replace_all(message, "')", "╩");
            while (string_pos("((", message)) {
                message = scrMagicSigns("SPLIT", message, "((");
                temp = scrMagicSigns("SPLIT", scrMagicSigns_Result2, "))");
                temp2 = scrMagicSigns_Result2;
                message += scrMagicSigns("ESC", temp, 1);
                message += temp2;
            }
            message = scrMagicSigns("ESC", message);

            if (string_pos("//", message)) { //Comment... comment handler that is.
                message = string_copy(message, 1, string_pos("//", message) - 1);
            }

            if (string_char_at(message, 1) == "%") //Conditonal
            {
                scrMagicSigns("CPREP");
                exit;
            }

            visible = 1; //Default visible, even if SPAWNed
            if (string_char_at(message, 1) == "?") { //Magic Sign
                command = string_delete(message, 1, 1);
                visible = 0;
                if (string_char_at(command, 1) == "?") { //double ? = demo (don't delete sign)
                    command = string_delete(command, 1, 1);
                    visible = 1; image_blend = $FF00FF; perm = 1;
                    if (string_char_at(command, 1) == "?") { //TRIPPLE ? = ghost sign
                        command = string_delete(command, 1, 1);
                        visible = 0; signmode = "S"; image_blend = $888888;
                    }
                }

                if (command == string_upper(command))
                    command = scrMagicSigns("S2C", command);

                if (string_char_at(command, 1) == "!") //SUPER Command
                {
                    command = string_delete(command, 1, 1);
                    xx = x; yy = y;
                    while (command != "")
                    {
                        temp = scrMagicSigns("SPLIT", command, "&", 0, 1);
                        command = scrMagicSigns_Result2;
                        temp = scrMagicSigns("MACRO", temp);
                        scrMagicSigns("SUPER", temp);
                    }
                    if (perm == 0) instance_destroy();
                    exit;
                }

                target = scrMagicSigns("SPLIT", command, "%");
                command = scrMagicSigns_Result2;

                if (string_pos("&", target)) { //Contains command separator.
                    command = target + "%" + command; //put it back to normal.
                    target = ""; //that wasn't supposed to be a replace.
                }

                target = scrMagicSigns("MACRO", target);

                pos = ""; xx = x; yy = y;
                if (string_char_at(target, 1) == "(") {
                    pos = scrMagicSigns("SPLIT", target, ")", 1);
                    target = scrMagicSigns_Result2;
                } else if (string_char_at(target, 1) == "[") {
                    pos = scrMagicSigns("SPLIT", target, "]", 1);
                    target = scrMagicSigns_Result2;
                }

                target = scrMagicSigns("SH", target, "o"); //Shorthand

                tcount = -1; matchmode = 0; //normal global replace
                if (pos != "")
                {
                    temp = scrMagicSigns("POS", pos);
                    if (temp == "" /*no match*/ and string_digits(pos) != "") {
                        tcount = real(string_digits(pos));
                    }
                }

                if (string_char_at(pos, 1) == "?")
                {
                    if (target == "") target = "all";
                    instance_activate_object(scrMagicSigns("DEREF", target, "Replace1"));
                    matchmode = 1;
                }
                else if (string_char_at(pos, 1) == "@")
                {
                    if (target == "") target = "all";
                    instance_activate_object(scrMagicSigns("DEREF", target, "Replace1.5"));
                    matchmode = 2;
                }
                else if (pos == "" or tcount > 0)
                {
                    if (target != "" and pos == "") {
                        instance_activate_object(scrMagicSigns("DEREF", target, "Replace2"));
                    }
                    if (target == "") target = "self";
                    target = scrMagicSigns("DEREF", target, "Replace3");
                }
                else
                {
                    if (target == "") target = "all";
                    target = instance_nearest(xx, yy, scrMagicSigns("DEREF", target, "Replace4"));
                }

                while (tcount != 0)
                {
                    if (matchmode == 1) {
                        temp = instance_number(scrMagicSigns("DEREF", target, "Replace5"));
                        mytarg = instance_find(scrMagicSigns("DEREF", target, "Replace6"), rand(0, temp-1));
                    } else if (matchmode == 2) {
                        mytarg = scrMagicSigns("DEREF", target, "Replace7");
                    } else if (tcount == -1) {
                        mytarg = target;
                    } else {
                        mytarg = instance_nearest(x, y, target);
                    }

                    with (mytarg)
                    {
                        if (matchmode == 2) {
                           if (max(abs(x-other.x), abs(y-other.y)) > tcount*16)
                              continue; //out of range
                        }
                        mycomm = command + "&"; pos = ""; keep = 0;
                        saveobj = object_get_name(object_index);
                        savespr = sprite_get_name(sprite_index);
                        while (mycomm != "" and mycomm != "&")
                        { //loop for &-separated commands
                            make = scrMagicSigns("SPLIT", mycomm, "&");
                            mycomm = scrMagicSigns_Result2;
                            sprt = ""; attr = ""; xx = x; yy = y;

                            make = scrMagicSigns("MACRO", make);

                            if (string_char_at(make, 1) == "(") {
                                pos = scrMagicSigns("SPLIT", make, ")", 1);
                                make = scrMagicSigns_Result2;
                            }
                            if (string_char_at(make, 1) == "[") {
                                pos = scrMagicSigns("SPLIT", make, "]", 1);
                                make = scrMagicSigns_Result2;
                            }

                            if (pos != "") {
                                scrMagicSigns("POS", pos); //adjusts xx and yy
                            }

                            if (string_char_at(make, 1) == "@")
                            {   //Offset for items.
                                make = string_delete(make, 1, 1);
                                xx += 8; yy += 11;
                            } else if (string_char_at(make, 1) == "$")
                            {   //Offset for STUCK items.
                                make = string_delete(make, 1, 1);
                                xx += 8; yy += 7;
                            } else if (string_char_at(make, 1) == ".")
                            {   //Background Tile, these work totally different.
                                scrMagicSigns("BG", make);
                                continue;
                            }

                            //Check Directives
                            if (string_length(make) == 1) {
                                x += 999; //Prevents the sign from finding itself.
                                temp = string_upper(make);
                                if (temp == "D") { //Delete
                                    with (instance_position(xx, yy, all)) {
                                        cleandeath = true; instance_destroy();
                                    }
                                    x -= 999; continue;
                                } else if (temp == "X") { //Cut
                                    with (instance_nearest(xx, yy, all)) {
                                        saveobj = object_get_name(object_index);
                                        savespr = sprite_get_name(sprite_index);
                                        cleandeath = true; instance_destroy();
                                    }
                                    x -= 999; continue;
                                } else if (temp == "C") { //Copy
                                    with (instance_nearest(xx, yy, all)) {
                                        saveobj = object_get_name(object_index);
                                        savespr = sprite_get_name(sprite_index);
                                    }
                                    x -= 999; continue;
                                } else if (temp == "W") { //Wipe Frills (Tiles)
                                    tile_layer_delete_at(3, xx, yy);
                                    x -= 999; continue;
                                } else if (temp == "M") { //Move Reference
                                    x = xx; y = yy; pos = "";
                                    /*x -= 999;*/ continue;
                                } else if (temp == "S") { //Snap to grid
                                    xx = round(xx/16)*16; yy = round(yy/16)*16;
                                    pos = string(xx) + "," + string(yy);
                                    x -= 999; continue;
                                } else if (temp == "A") { //Activate Sign
                                    temp2 = instance_nearest(xx, yy, oMsgSign);
                                    if (temp2 != noone and temp2 != me) {
                                        with (temp2) {
                                            done = 0;
                                            event_perform(ev_alarm, 0);
                                        }
                                    }
                                    x -= 999; continue;
                                } else if (temp == "N") { //New "Virtual" Sign
                                    with (other) {
                                        perm = -1; done = 0; alarm[0] = 1;
                                        message = string_copy(mycomm, 1, string_length(mycomm) - 1);
                                    }
                                    x -= 999; break;
                                }
                                x -= 999;
                            } else if (scrMagicSigns("LMATCH", make, "R?")) { //Random Chance
                                if (random(real(scrMagicSigns_Result2)) >= 1) { //Abort
                                    if (mytarg != me) keep = 1; //do not replace,
                                    break; //do not process remaining commands.
                                }
                                continue;
                            } else if (scrMagicSigns("LMATCH", make, "T$")) { //Tint
                                temp3 = scrMagicSigns("COLOR", scrMagicSigns_Result2);
                                temp = tile_layer_find(3, xx, yy);
                                if (temp) {
                                    tile_set_blend(temp, temp3);
                                    tile_set_position(temp, xx+999, yy);
                                    temp2 = tile_layer_find(3, xx, yy);
                                    if (temp2) tile_set_blend(temp2, temp3);
                                    tile_set_position(temp, xx, yy);
                                }
                                continue;
                            } else if (scrMagicSigns("LMATCH", make, "?!")) { //Super Command
                                scrMagicSigns("SUPER", scrMagicSigns_Result2);
                                continue;
                            }

                            if (string_pos("?", make))
                            {
                                make = scrMagicSigns("SPLIT", make, "?");
                                attr = scrMagicSigns_Result2;
                            }
                            if (string_pos("$", make))
                            {
                                make = scrMagicSigns("SPLIT", make, "$");
                                sprt = scrMagicSigns_Result2;
                            }

                            make = string_replace(make, "!", saveobj);
                            if (string_pos("@", make))
                            {
                                saveobj = string_copy(make, 1, string_pos("@", make) - 1);
                                make = string_replace(make, "@", "");
                            }

                            make = scrMagicSigns("SH", make, "o"); //Shorthand

                            if (string_upper(make) == "E") { //Edit
                                x += 999;
                                obj = instance_nearest(xx, yy, all);
                                if (point_distance(xx, yy, obj.x, obj.y) > 30)
                                    {sprt = ""; attr = "";}
                                x -= 999;
                            } else if (string_upper(make) == "B") { //Banner-Edit
                                x += 999;
                                obj = instance_nearest(xx, yy, oMsgSign);
                                if (point_distance(xx, yy, obj.x, obj.y) > 30)
                                    {sprt = ""; attr = "";}
                                x -= 999;
                            } else if (string_upper(make) == "K") {//Keep
                                obj = self;
                                keep = 1;
                            } else {
                                obj = instance_create(xx, yy, scrMagicSigns("DEREF", make, "Create"));
                            }

                            if (sprt != "")
                            {
                                sprt = string_replace(sprt, "!", savespr);
                                if (string_pos("@", sprt))
                                {
                                    savespr = string_copy(sprt, 1, string_pos("@", sprt) - 1);
                                    sprt = string_replace(sprt, "@", "");
                                }

                                sprt = scrMagicSigns("SH", sprt, "s"); //Shorthand

                                obj.sprite_index = scrMagicSigns("DEREF", sprt, "Sprite");
                            }
                            if (attr != "")
                                scrMagicSigns("SET", attr, obj, 1);

                            me.MSChild = obj;
                            if (variable_global_exists("scrMagicSigns_Last"))
                                scrMagicSigns_Prev = scrMagicSigns_Last;
                            scrMagicSigns_Last = obj;
                        }
                        if ((mytarg.id != me) and (not keep))
                        {
                            cleandeath = true;
                            instance_destroy();
                        }
                    }
                    if (tcount == -1 or matchmode == 2) tcount = 0; else tcount -= 1;
                }
                if (perm == 0)
                   instance_destroy();
            }
            else
            {
                signmode = "";
                message = string_replace_all(message, "╠╠", "&&");
                if (string_pos("@@", message))
                {
                    message = scrMagicSigns("SPLIT", message, "@@");
                    signmode = string_upper(scrMagicSigns_Result2);

                    if (string_pos("B", signmode)) //Banner
                    {
                        xx = 0; yy = 0; //Displacements.
                        message = string_upper(message);
                        message = string_replace_all(message, "&&", "#");
                        if (string_digits(signmode) == "")
                            banner = c_white;
                        else
                            banner = real(string_digits(signmode));
                        if (string_pos(">", signmode)) xx = 4;
                        if (string_pos("/", signmode)) yy = 4;
                        perm = 1; signmode = "S"; visible = 1; image_alpha = 0;
                    }

                    if (string_pos("I", signmode)) //Invisible
                        visible = 0;
                    if (string_pos("Z", signmode)) //Zone
                    {
                        visible = 0; x -= 16; y -= 16;
                        image_xscale = 3; image_yscale = 3;
                    }
                    //also A=Alert X=Destroy ##s=Deley
                    //Display Time: M=More L=Less N=None (Contact) V=Variable
                }
                if (banner == -1) {
                    if (string_pos("&&", message))
                    {
                        message = scrMagicSigns("SPLIT", message, "&&");
                        message2 = scrMagicSigns("MACRO", scrMagicSigns_Result2);
                    }
                    message = scrMagicSigns("MACRO", message);
                    if (string_pos("S", signmode)) //Start Message
                    {
                        visible = 0;
                        if (string_digits(signmode) == "")
                            event_perform(ev_alarm, 1);
                        else
                            alarm[1] = real(string_digits(signmode));
                    }
                }
            }
        } else if (event_number == 1) //Show Message ***********************************************************
        {
            global.message = message;
            global.message2 = message2;
            global.messageTimer = 200;

            if (string_pos("L", signmode))
                global.messageTimer = 100;
            if (string_pos("M", signmode))
                global.messageTimer = 300;
            if (string_pos("N", signmode))
                global.messageTimer = 1;
            if (string_pos("V", signmode))
                global.messageTimer = 50 + 4 * string_length(message + message2);

            if (string_pos("A", signmode))
            {
                signmode = string_replace(signmode, "A", "");
                playSound(global.sndLaser);
                playSound(global.sndPsychic);
                scrShake(5);
            }

            if (string_pos("X", signmode)) instance_destroy();
            if (string_pos("S", signmode)) instance_destroy();
        } else if (event_number == 2) //Proccess condition ********************************************************
        {
            do { //just so I can break
                if (CAsoc != self.id) {
                    instance_activate_object(CAsoc);
                    if (not instance_exists(CAsoc))
                    {
                        if (COptB & 128) { //Dead man's switch
                            done = 0;
                            event_perform(ev_alarm, 0);
                        }
                        instance_destroy();
                        exit;
                    }
                    x = CAsoc.x; y = CAsoc.y;
                }
                if (not (COptB & 1)) //Once
                    alarm[2] = CWait;
                temp3 = 0;
                if ((CTarg != 0) and not (COptB & 8192)) //not Count
                {
                    instance_activate_object(CTarg);
                    if (COptB & 32) {x+=8; y+=8;} //center
                    if (COptB & 256) temp = CTarg; //Locked on
                            else temp = instance_nearest(x, y, CTarg);
                    if (temp == noone) temp2 = 9999;
                    else {
                        temp2 = point_distance(x,y,temp.x,temp.y);
                        if (COptB & 2048) { //get var from Target
                            with (temp) temp3 = variable_local_get(other.CVari);
                        }
                    }
                    if (COptB & 32) {x-=8; y-=8;} //uncenter
                    if ((temp2 > CDist) xor (COptB & 4)) break; //Invert result of Farther is set.
                }
                if ((CVari != "") or (COptB & 8192)) //Count
                {
                    if (COptB & 8192) { //Count
                        instance_activate_object(CTarg);
                        temp3 = instance_number(CTarg);
                    } else if (COptB & 4096) { //get var from Asociated obj
                        with (CAsoc) temp3 = variable_local_get(other.CVari);
                    } else if (not (COptB & 2048)) { //not var from Target
                        if (variable_local_exists(CVari)) {
                            temp3 = variable_local_get(CVari);
                        } else {
                            temp3 = variable_global_get(CVari);
                        }
                    }
                    if ((COptB & 8) and (COptB & 16)) //not
                        {if (temp3 == CNumb) break;}
                    else if (COptB & 8) //less
                        {if (not (temp3 < CNumb)) break;}
                    else if (COptB & 16) //more
                        {if (not (temp3 > CNumb)) break;}
                    else //(Equal)
                        {if (temp3 != CNumb) break;}
                }

                //All Conditions are satisfied.
                if (not (COptB & 2)) { //not Repeat
                    alarm[2] = 0;
                    visible = true;
                    signmode = "";
                }
                done = 0; temp = COptB;     //copy options to script var
                event_perform(ev_alarm, 0); //Otherwise they could be overwritten here.
                if (temp & 1024) { //Begin again
                    message = original;
                    done = 0;
                    alarm[0] = alarm[2];
                    alarm[2] = 0;
                }
                if (temp & 16384) { //Delete
                    if (CAsoc != self.id)
                        with (CAsoc) instance_destroy();
                    instance_destroy();
                }
                exit;

            } until (true);
            //Condition failed
            if (COptB & 512) { //Start over
                message = original;
                done = 0;
                alarm[0] = alarm[2];
                alarm[2] = 0;
            }
        } else if (event_number == 3) //AntiFreeze ********************************************************
        {
            x = oPlayer1.x; y = oPlayer1.y;
            alarm[3] = 1;
        }
    } else if (event_type == ev_collision) //Colission (W/Player )************************************************
    {
        if (isRoom("rLoadLevel"))
        {
            if (variable_local_exists("signmode"))
            {
                if (!string_pos("S", signmode))
                {
                    if (string_digits(signmode) == "")
                        event_perform(ev_alarm, 1);
                    else if (alarm[1] < 1)
                        alarm[1] = real(string_digits(signmode));
                }
            }
        } else {
            global.message = message;
            global.message2 = message2;
            global.messageTimer = 200;
        }
    } else if (event_type == ev_draw) //Draw Event  *************************************************************
    {
        if (visible) draw_sprite_ext(sprite_index, -1, x, y,
                image_xscale, image_xscale, image_angle, image_blend, image_alpha);
        if (isRoom("rLevelEditor")) {
            draw_set_font(global.myFontSmall);
            if (oCursObj.x == x and oCursObj.y == y and oLevelEditor.status == 1) {
                depth = 58;
                draw_text_color(x+16, y, message, c_yellow, c_yellow, c_yellow, c_yellow, 1);
            } else {
                depth = x / 16 + 58;
                draw_text_color(x+16, y, message, c_silver, c_silver, c_silver, c_silver, 0.5);
            }
        } else if (variable_local_exists("banner")) {
            if (banner != -1) {
                draw_set_font(global.myFontSmall);
                draw_text_color(x+xx, y+yy, message, banner, banner, banner, banner, 1);
            }
        }
    } else if (event_type == ev_keypress or event_type == ev_middle_press)
    {
        if (isRoom("rLevelEditor")) {
            if (oCursObj.x == x and oCursObj.y == y and oLevelEditor.status == 1) {
                message_size(512, 128);
                message_input_color(c_black);
                message_input_font("Courier New", 12, c_white, 1);
                temp = get_string("Enter New Text for Sign: (ESC to Abort)", message);
                keyboard_check_direct(vk_escape); //Primer, seems to help.
                if (not keyboard_check_direct(vk_escape)) message = temp;
            }
        }
    }
}
else switch (argument0)
{
    //Get Index from resource name. *************************************************************************
    case "DEREF": //resource, [comment]
        if (is_string(argument2))
            temp2 = string_lettersdigits(argument2);
        else temp2 = "";

        temp = scrMagicSigns("UNESC", argument1);
        temp = string_replace_all(temp, "_", "xUND3Rx");
        temp = string_replace_all(temp, ".", "xXD0TXx");
        temp = string_replace_all(temp, "[", "xBR4Cox");
        temp = string_replace_all(temp, "]", "xoBR4Cx");

        temp = string_lettersdigits(temp);

        temp = string_replace_all(temp, "xUND3Rx", "_");
        temp = string_replace_all(temp, "xXD0TXx", ".");
        temp = string_replace_all(temp, "xBR4Cox", "[");
        temp = string_replace_all(temp, "xoBR4Cx", "]");

        if (argument1 != temp)
        {
            if (temp2 != "") temp2 = "Comment: " + temp2;
            show_message("BAD REFERENCE: (" + argument1 + ")
Name may contain letters, digits, _ and . only.
" + temp2);
            return 0;
        } else
            return execute_string("return " + argument1 + "; //" + temp2);
    break;

    //Splits a string, optionally trims being of first half. ****************************************************
    //"Terminate" means add the separator at the end (temporarily).
    case "SPLIT": //String, Separator, [Trim Count], [Terminate?]
        argument1 = string_delete(argument1, 1, argument3);
        if (argument4) temp3 = argument2; else temp3 = "";
        temp = string_pos(argument2, argument1 + temp3) - 1;
        temp2 = temp + 1 + string_length(argument2);
        scrMagicSigns_Result2 = string_delete(argument1, 1, temp2 - 1);
        return string_copy(argument1, 1, temp);
    break;

    //Checks if Needle matches the BEGINING of Haystack. ********************************************************
    //Sets Result2 to the UNMATCHED part of haystack.
    case "LMATCH": //Heystack, Needle
        temp = string_upper(string_copy(argument1, 1, string_length(argument2)));
        scrMagicSigns_Result2 = string_delete(argument1, 1, string_length(argument2));
        return (temp == string_upper(argument2));
    break;

    //Checks if Needle matches the END of Haystack. ************************************************************
    //Sets Result2 to the UNMATCHED part of haystack.
    case "RMATCH": //Heystack, Needle
        temp = string_length(argument1) - string_length(argument2);
        temp2 = string_upper(string_delete(argument1, 1, temp));
        scrMagicSigns_Result2 = string_copy(argument1, 1, temp);
        return (temp2 == string_upper(argument2));
    break;

    //Changes spaces to captials. *******************************************************************************
    case "S2C": //String
        temp = string_lower(argument1);
        temp = string_replace_all(temp, "' ", "┘");
        while (string_pos(" ", temp))
        {
            temp2 = string_copy(temp, 1, string_pos(" ", temp) - 1);
            temp2 += string_upper(string_copy(temp, string_pos(" ", temp) + 1, 1));
            temp2 += string_delete(temp, 1, string_pos(" ", temp) + 1);
            temp = temp2;
        }
        return temp;
    break;

    //Changes captialsto  spaces. *******************************************************************************
    case "C2S": //String
        temp = argument1;
        repeat (string_length(temp))
        {
            temp2 = string_char_at(temp, 1);
            temp = string_delete(temp, 1, 1);
            if (string_letters(temp2) == string_upper(temp2))
                temp += " " + temp2;
            else
                temp += string_upper(temp2);
        }
        return temp;
    break;

    //Parse POS format and change xx and yy. ******************************************************************
    case "POS": //POS String without ()
        if (string_letters(argument1) != "")
        {
            temp = string_replace_all(argument1, "!", ",");
            temp = scrMagicSigns("SPLIT", temp, ",", 0, 1);
            temp3 = scrMagicSigns_Result2;

            temp2 = string_delete(temp, 1, 1);
            temp = string_upper(string_char_at(temp, 1));

            if (temp2 == "")
                temp2 = 1;
            else
                temp2 = real(temp2);

            if (temp == "L")
                xx -= temp2 * 16;
            else if (temp == "R")
                xx += temp2 * 16;
            else if (temp == "U")
                yy -= temp2 * 16;
            else if (temp == "D")
                yy += temp2 * 16;
            else if (temp == "W")
                xx -= temp2;
            else if (temp == "E")
                xx += temp2;
            else if (temp == "N")
                yy -= temp2;
            else if (temp == "S")
                yy += temp2;

            //lets you string together two directions with a !
            if (temp3 != "") scrMagicSigns("POS", temp3);

            return "R";
        }
        else if (string_pos(".", argument1) or string_pos(",", argument1))
        {
            temp = string_replace(argument1, ".", ",");
            xx = string_copy(temp, 1, string_pos(",", temp) - 1);
            yy = string_copy(temp, string_pos(",", temp) + 1, 999);
            xx = real(xx); yy = real(yy);
            return "A";
        }
        return "";
    break;

    // Sets variable. See manual for format. *********************************************************************
    case "SET": //Expression(s), Object, [Shorthand?], [Single?], [NoUNESC?]
        argument1 += "?";
        while (argument1 != "" and argument1 != "?")
        { //loop for ?-separated attribute-value pairs
            if (argument4 == 1) {
                temp = argument1;
                argument1 = "";
            } else {
                temp = scrMagicSigns("SPLIT", argument1, "?");
                argument1 = scrMagicSigns_Result2;
            }
            temp2 = ""; temp3 = 0;

            if (string_pos("=", temp)) {
                //Do nothing.
            }
            else if (string_pos("@", temp)) //@ mean equals quoted value
            {
                temp = scrMagicSigns("SPLIT", temp, "@");
                temp2 = scrMagicSigns_Result2;
                if (string_char_at(temp2, 1) == "@") //@@ means quote and recapitalize
                {
                    temp2 = string_delete(temp2, 1, 1);
                    temp2 = scrMagicSigns("C2S", temp2);
                    temp2 = string_replace_all(temp2, "┘", "' ");
                }
                temp2 = '"' + temp2 + '"';
            }
            else
            {
                temp = string_replace(temp, "!!", "=-"); //equals minus...
                temp = string_replace(temp, "!", "="); //normal equals
            }

            if (temp2 == "")
            {
                temp = scrMagicSigns("SPLIT", temp, "=");
                temp2 = scrMagicSigns_Result2;
                if (string_char_at(temp2, 1) == "$") {
                    temp2 = string_delete(temp2, 1, 1);
                    temp2 = scrMagicSigns("COLOR", temp2);
                }
            }

            temp = string_replace_all(temp, "..", "_");
            temp = string_replace_all(temp, "(", "[");
            temp = string_replace_all(temp, ")", "]");

            if (argument3) //Special Shorthand
            {
                temp = string_replace(temp, "a.", "image_alpha");
                temp = string_replace(temp, "b.", "image_blend");
                temp = string_replace(temp, "d.", "depth");
                temp = string_replace(temp, "i.", "invincible");
                temp = string_replace(temp, "l.", "leadsTo");
                temp = string_replace(temp, "m.", "message"); //oh dear
                temp = string_replace(temp, "r.", "image_angle"); //rotation
                temp = string_replace(temp, "s.", "image_speed");
                temp = string_replace(temp, "t.", "alarm"); //timer
                temp = string_replace(temp, "v.", "visible");
                temp = string_replace(temp, "x.", "image_xscale");
                temp = string_replace(temp, "y.", "image_yscale");
            }

            if (string_char_at(temp2, 1) == '"') { //quotes, they've done their job, remove 'em
                temp2 = scrMagicSigns("SPLIT", temp2, '"', 1);
                if (argument5 == 0) temp2 = scrMagicSigns("UNESC", temp2);
            } else if(string_letters(temp2) != "") { //looks like a variable
                if(string_char_at(temp2, 1) == ".") { //prefix for global
                    temp2 = "global" + temp2;
                } else if(string_char_at(temp2, 1) == "-") { //prefix for active sign
                    temp2 = "other." + string_delete(temp2, 1, 1);
                }
                temp2 = scrMagicSigns("DEREF", temp2, "Set");
            } else { //plain old number
                temp2 = real(temp2);
            }

            if (string_pos("[", temp)) //array
            {
                temp = scrMagicSigns("SPLIT", temp, "[");
                temp3 = real(scrMagicSigns("SPLIT", scrMagicSigns_Result2, "]"));

                if (scrZMIsGlobal(argument2))
                    variable_global_array_set(temp, temp3, temp2);
                else
                    with (argument2) variable_local_array_set(temp, temp3, temp2);
            }
            else //not array
            {
                command = string_delete(temp, 1, string_length(temp) - 1); //get last char

                if (string_lettersdigits(command) == "")
                { //this might be an operator
                    temp = string_copy(temp, 1, string_length(temp) - 1);
                    if (scrZMIsGlobal(argument2) and variable_global_exists(temp)) {
                        temp3 = variable_global_get(temp);
                    } else  with (argument2) {
                        if ( variable_local_exists(temp)) {
                            temp3 = variable_local_get(temp);
                        }
                    }

                    if (is_string(temp3) and is_real(temp2))
                        temp2 = string(temp2);
                    if (is_real(temp3) and is_string(temp2))
                        temp2 = real(temp2);

                    switch (command)
                    {
                        case "$": break; //Just forces conversion.
                        case "+": temp2 = temp3 + temp2; break;
                        case "-": temp2 = temp3 - temp2; break;
                        case "*": temp2 = temp3 * temp2; break;
                        case "/": temp2 = temp3 / temp2; break;
                        default: temp += command; //wasn't an operator after all, put it back.
                    }
                }

                if (scrZMIsGlobal(argument2))
                    variable_global_set(temp, temp2);
                else
                    with (argument2) variable_local_set(temp, temp2);
            }
        }
    break;

    //Returns to numaric representation of a color. *************************************************************
    case "COLOR": //$ColorName or $HexCode
        temp = string_lower(argument1);
        switch (temp)
        {
            case "aqua": case "cyan": return c_aqua;
            case "black":  return c_black;
            case "blue":   return c_blue;
            case "dkgray": case "dgray": return c_dkgray;
            case "fuchsia": case "magenta": return c_fuchsia;
            case "gray":   return c_gray;
            case "green":  return c_green;
            case "lime":   return c_lime;
            case "ltgray": case "lgray": return c_ltgray;
            case "maroon": return c_maroon;
            case "navy":   return c_navy;
            case "olive":  return c_olive;
            case "orange": return c_orange;
            case "purple": return c_purple;
            case "red":    return c_red;
            case "silver": return c_silver;
            case "teal":   return c_teal;
            case "white":  case "clear": return c_white;
            case "yellow": return c_yellow;

            //banner colors
            case "stone": return $281E15;
            case "sign": return $5B4634;
            case "dirt": return $214478;
            case "lush": return  $216178;
            case "dark": return $684211;
            case "temple": return $5B818F;

            //material colors
            case "copper": return $1082E1;
            case "iron": return $888888;
            case "brass": return $10C8DC;
            case "jade": return $59FB8B;
            case "amethyst": return $FF91FF;
            case "clay": return $689BAD;

            default: //maybe it's hexadecimal.
            temp2 = 0;
            while (temp != "")
            {
                temp2 *= 16;
                temp3 = string_char_at(temp, 1);
                temp = string_delete(temp, 1, 1);
                if (string_digits(temp3) != "")
                    temp2 += real(temp3);
                else
                    temp2 += ord(temp3) - 87; //so a=10 b=11 etc.
            }
            return temp2;
        }
    break;

    //Trandlate shorthand, optionally add prefix. ***************************************************************
    case "SH": //String, [Prefix (e.g. "o")]
        temp = argument1;
        temp = string_replace_all(temp, "..", "_");
        temp = string_replace(temp, "a.", "Alien");
        temp = string_replace(temp, "b.", "Brick");
        temp = string_replace(temp, "c.", "Cave");
        temp = string_replace(temp, "d.", "Dark");
        temp = string_replace(temp, "e.", "Explosion");
        temp = string_replace(temp, "g.", "Gold");
        temp = string_replace(temp, "i.", "Ice");
        temp = string_replace(temp, "l.", "Lush");
        temp = string_replace(temp, "m.", "Metal");
        temp = string_replace(temp, "n.", "MsgSign");
        temp = string_replace(temp, "o.", "Block");
        temp = string_replace(temp, "p.", "PushBlock");
        temp = string_replace(temp, "r.", "Arrow");
        temp = string_replace(temp, "s.", "Spikes");
        temp = string_replace(temp, "t.", "Temple");
        temp = string_replace(temp, "u.", "Player1");
        temp = string_replace(temp, "v.", "Lava");
        temp = string_replace(temp, "w.", "WaterSwim");
        temp = string_replace(temp, "x.", "Trap");
        temp = string_replace(temp, "z.", "DrawnSprite");
        temp = string_replace(temp, "1.", "Solid");
        temp = string_replace(temp, "8.", "Top");
        temp = string_replace(temp, "4.", "Left");
        temp = string_replace(temp, "6.", "Right");
        temp = string_replace(temp, "2.", "Bottom");
        temp = string_replace(temp, "3.", "Down");
        temp = string_replace(temp, "9.", "Up"); //almost totally pontless

        if (string_char_at(temp, 1) != string_char_at(argument1, 1) and is_string(argument2))
            temp = argument2 + temp; //Used at begining, so add prefix.
        return temp;
    break;

    //Make a background tile. See manual for format. *************************************************************
    case "BG": //Background Command String.
        temp = string_delete(argument1, 1, 1);
        if (string_pos("@", temp)) //depth
        {
            arr[5] = string_copy(temp, string_pos("@", temp) + 1, 999);
            temp = string_copy(temp, 1, string_pos("@", temp) - 1);
            arr[5] = real(arr[5]);
        } else
            arr[5] = 10002;
        if (temp == "!") //Delete bg here @ this depth
            tile_layer_delete_at(real(arr[5]), xx, yy);
        else if (temp == "!!") //Delete all bg @ this depth
            tile_layer_delete(real(arr[5]));
        else if (string_char_at(temp, 1) == "$") //Main background
        {
            temp = string_replace_all(temp, ".", ",");      //for crippled typing
            if (string_pos(",", temp)) //color
            {
                temp = scrMagicSigns("SPLIT", temp, ",");
                background_blend = scrMagicSigns("COLOR", scrMagicSigns_Result2);
            } else background_blend = c_white;
            background_index = scrMagicSigns("DEREF", string_delete(temp, 1, 1), "Background1");
        } else
        {
            if (string_char_at(temp, 1) == "(") {
                temp3 = scrMagicSigns("SPLIT", temp, ")", 1);
                temp = scrMagicSigns_Result2;
            } else if (string_char_at(temp, 1) == "[") {
                temp3 = scrMagicSigns("SPLIT", temp, "]", 1);
                temp = scrMagicSigns_Result2;
            } else
                temp3 = -1;
            temp3 = real(temp3);

            if (string_pos("$", temp)) //color
            {
                temp = scrMagicSigns("SPLIT", temp, "$");
                arr[6] = scrMagicSigns("COLOR", scrMagicSigns_Result2);
            } else
            arr[6] = c_white;

            temp = string_replace_all(temp, ".", ",");      //for crippled typing
            temp = string_replace_all(temp, "!", "!,");     //count by tiles

            if (not string_pos(",", temp)) //bare name, use whole bg
            {
                temp2 = scrMagicSigns("DEREF", temp, "Background2");
                temp += ",0,0," + string(background_get_width(temp2));
                temp += "," + string(background_get_height(temp2));
            }

            temp += ","; //end with comma

            temp2 = scrMagicSigns("SPLIT", temp, ",");
            temp = scrMagicSigns_Result2;
            arr[0] = scrMagicSigns("DEREF", temp2, "Background3");

            arr[1] = 0;
            arr[2] = 0;
            arr[3] = 16;
            arr[4] = 16;
          //arr[5] = 10002; (depth, set above)
          //arr[6] = c_white; (color, set above)

            i = 1;
            while (temp != "")
            {
                temp2 = scrMagicSigns("SPLIT", temp, ",");
                temp = scrMagicSigns_Result2;
                if (temp2 != "")
                {
                    if (string_pos("!", temp2))
                        arr[i] = real(string_digits(temp2)) * 16;
                    else
                        arr[i] = real(temp2);
                }
                i += 1;
            }
            while (temp3 != 0)
            {
                if (temp3 > 0)
                {
                    xx = rand(0, 41) * 16;
                    yy = rand(0, 33) * 16;
                    temp3 -= 1;
                } else
                    temp3 = 0;

                temp = tile_add(arr[0], arr[1], arr[2], arr[3], arr[4], xx, yy, arr[5]);
                tile_set_blend(temp, arr[6]);
            }

        }
    break;

    //Execute a "Super Command". *******************************************************************************
    case "SUPER": //Command
        if (scrMagicSigns("LMATCH", argument1, "BOOTSTRAP")) {
            instance_activate_all(); //don't worry they deactivate again.
            with (oMsgSign) {
                if (not variable_local_exists("done"))
                    event_perform(ev_alarm, 0); //Once only
            }
        } else if (scrMagicSigns("LMATCH", argument1, "SET?")) {
            scrMagicSigns("SET", scrMagicSigns_Result2, " global");
        } else if (scrMagicSigns("LMATCH", argument1, "SET:")) {
            temp = scrMagicSigns("SPLIT", scrMagicSigns_Result2, "?");
            temp2 = scrMagicSigns_Result2;
            temp = scrMagicSigns("DEREF", temp);
            with (temp) scrMagicSigns("SET", temp2, self.id, 1);
        } else if (scrMagicSigns("LMATCH", argument1, "INFO")) {
            visible = 1; image_blend = $FF0000; perm = 1;
            message =  "MAGIC SIGNS V2.2.3 BY MANAUSER";
            message2 =  ""; //3rd party mod notes, if any.
        } else if (scrMagicSigns("LMATCH", argument1, "CLEANUP")) {
            tile_layer_delete(3);
            instance_activate_object(oSolid);
            instance_deactivate_object(oBlock);
            scrSetupWalls(528);
            instance_activate_object(oBlock);
        } else if (scrMagicSigns("LMATCH", argument1, "SOUND?")) {
            playSound(variable_global_get(scrMagicSigns_Result2));
        } else if (scrMagicSigns("LMATCH", argument1, "FX?")) {
            temp = scrMagicSigns("SPLIT", scrMagicSigns_Result2, "$");
            temp2 = scrMagicSigns_Result2;
            effect_create_above(real(temp), xx, yy, 0, scrMagicSigns("COLOR", temp2));
        } else if (scrMagicSigns("LMATCH", argument1, "FILL?")) {
            temp = string_replace_all(scrMagicSigns_Result2, "?", ",");
            temp = scrMagicSigns("SH", scrMagicSigns("SPLIT", temp, ","), "o");
            temp2 = real(scrMagicSigns("SPLIT", scrMagicSigns_Result2, ","));
            temp3 = real(scrMagicSigns_Result2);
            temp = scrMagicSigns("DEREF", temp, "Fill");
            for (i=0;i<temp2;i+=1) {
                for (j=0;j<temp3;j+=1) {
                    instance_create(xx+(i*16), yy+(j*16), temp);
                }
            }
        } else if (scrMagicSigns("LMATCH", argument1, "CFILL?")) {
            temp = string_replace_all(scrMagicSigns_Result2, "?", ",");
            temp = scrMagicSigns("SH", scrMagicSigns("SPLIT", temp, ","), "o");
            temp2 = real(scrMagicSigns("SPLIT", scrMagicSigns_Result2, ","));
            temp3 = real(scrMagicSigns_Result2);
            temp = instance_nearest(xx, yy, scrMagicSigns("DEREF", temp, "CFill"));
            for (i=0;i<temp2;i+=1) {
                for (j=0;j<temp3;j+=1) {
                    if (i == 0 and j == 0 and temp.x == xx and temp.y == yy)
                        continue;
                    with (temp) obj = instance_copy(false);
                    obj.x = xx+(i*16); obj.y = yy+(j*16);
                }
            }
        } else if (scrMagicSigns("LMATCH", argument1, "RESET")) {
            message = original;
        } else if (scrMagicSigns("LMATCH", argument1, "EXEC:")) {
            temp = message;
            message = scrMagicSigns("UNESC", scrMagicSigns_Result2);
            done = 0;
            event_perform(ev_alarm, 0);
            message = temp;
        } else if (scrMagicSigns("LMATCH", argument1, "MACRO")) {
            temp = ""; temp2 = scrMagicSigns_Result2;
            while (string_digits(string_char_at(temp2, 1)) != ""){
                temp += string_char_at(temp2, 1);
                temp2 = string_delete(temp2, 1, 1);
            }

            if (string_char_at(temp2, 1) == "?") {
                temp2 = "@" + string_delete(temp2, 1, 1);
            }

            temp3 = "scrMagicSigns_Macro[" + temp + "]" + temp2;
            scrMagicSigns("SET", temp3, " global", 0, 1, 1);
            if (variable_global_exists("scrMagicSigns_MacroCount")) {
                temp = real(temp);
                if (scrMagicSigns_MacroCount < temp)
                    scrMagicSigns_MacroCount = temp;
            } else scrMagicSigns_MacroCount = temp;
        } else if (scrMagicSigns("LMATCH", argument1, "BANNER!")) {
            if (scrMagicSigns_Result2 == "")
                temp = 9999; else temp = real(scrMagicSigns_Result2);
            instance_activate_object(oMsgSign);
            with (oMsgSign) {
                if (variable_local_exists("banner")) {
                    if (banner != -1) {
                        if (point_distance(x,y,other.x,other.y) <= temp)
                            instance_destroy();
                    }
                }
            }

        } else if (scrMagicSigns("LMATCH", argument1, "BANNER")) {
            temp = scrMagicSigns_Result2;
            obj = instance_create(xx, yy, oMsgSign);
            obj.message = scrMagicSigns("UNESC", scrMagicSigns_Result2);

            temp = scrMagicSigns("SPLIT", temp, "?");
            temp2 = scrMagicSigns_Result2;

            if (temp2 == string_lower(temp2)) //all lower
                temp2 = string_upper(temp2);
            else if (temp2 != string_upper(temp2)) //mixed case
                temp2 = scrMagicSigns("C2S", temp2);

            temp2 = temp2 + "@@B";
            temp2 = scrMagicSigns("MACRO", temp2);
            temp2 = scrMagicSigns("UNESC", temp2);
            temp2 = string_replace_all(temp2, "&&", "#");

            obj.message = temp2;
            obj.image_alpha = 0;
            if (string_pos("@", temp)) { //Set Depth
                temp = scrMagicSigns("SPLIT", temp, "@");
                temp2 = scrMagicSigns_Result2;
                if (string_pos("A", string_upper(temp2)) or
                        string_pos("S", string_upper(temp2))) { //Auto or Sticky
                    temp3 = instance_nearest(xx, yy, oSolid);
                    obj.depth = temp3.depth - 1;

                    if (string_pos("S", string_upper(temp2))) {
                        obj.CAsoc = temp3;
                        obj.CVari = "x"; obj.CNumb = -99;
                        obj.CWait = 1; obj.COptT = ""; obj.COptB = 0;
                        obj.CTarg = 0; obj.CDist = 0;
                        obj.alarm[2] = 1;
                    }
                }
                if (string_digits(temp2) != "") obj.depth = real(temp2);
                if (string_pos(">", temp2)) obj.message += ">";
                if (string_pos("/", temp2)) obj.message += "/";
            }
            if (scrMagicSigns("LMATCH", temp, "$"))
                obj.message += string(scrMagicSigns("COLOR", scrMagicSigns_Result2));
            with (obj) event_perform(ev_alarm, 0);

        } else if (scrMagicSigns("LMATCH", argument1, "SPAWN:")) {
            obj = instance_create(xx, yy, oMsgSign);
            obj.message = scrMagicSigns("UNESC", scrMagicSigns_Result2);
            obj.visible = false;
        } else if (scrMagicSigns("LMATCH", argument1, "DEBUG")) {
            message_size(512, 128);  message_input_color(c_black);
            message_input_font("Courier New", 12, c_white, 1);
            temp = get_string("Enter debug command...", "");
            keyboard_check_direct(vk_escape); //Primer, seems to help.
            if (not keyboard_check_direct(vk_escape))
                execute_string(temp);
        } else if (scrMagicSigns("LMATCH", argument1, "RANDOM?")) {
            temp = string_replace_all(scrMagicSigns_Result2, "?", ",");
            temp = real(scrMagicSigns("SPLIT", temp, ","));
            temp2 = real(scrMagicSigns_Result2);
            global.rnd = rand(temp, temp2);
            global.RND = global.rnd;

        } else if (scrMagicSigns("LMATCH", argument1, "INIT?")) {
            if (not variable_global_exists("oMSDS"))
            {
                //Building the data storage object for storing information across levels in a levelset
                global.oMSDS=object_add();
                object_set_persistent(global.oMSDS, true);
                object_set_parent(global.oMSDS, oGlobals); //Antifreze
                object_event_add(global.oMSDS, ev_create, 0, "gvars = 0;"); //So parent event isn't called
                object_event_add(global.oMSDS, ev_other, ev_room_start,
                "if (global.time == 0) {
                    for (i=1; i<=gvars; i+=1) {
                        variable_global_set(gvar[i], 0);
                    }
                    gvars = 0;
                }");
            }
            if (not variable_global_exists("MSDS"))
                global.MSDS=instance_create(0, 0, global.oMSDS);
            else if (not instance_exists(global.MSDS))
                global.MSDS=instance_create(0, 0, global.oMSDS);

            temp = false;
            for (i=1; i<=global.MSDS.gvars; i+=1) {
                if (global.MSDS.gvar[i] == scrMagicSigns_Result2)
                    temp = true;
            }
            if (temp == false)
            {
                global.MSDS.gvars += 1;
                global.MSDS.gvar[global.MSDS.gvars] = scrMagicSigns_Result2;
                variable_global_set(scrMagicSigns_Result2, 0);
            }
        } else {
            visible = 1; image_blend = 255; perm = 1;
            message2 = "UNKNOWN COMMAND!";
        }
    break;

    //Evaluate a condition. See manual for format. **************************************************************
    case "CPREP": //(no arguments)
        message2 = scrMagicSigns("SPLIT", message, "%", 1);
        message = scrMagicSigns_Result2;
        visible = 0; signmode = "S"; //It stands for Suspended too, honest.
        if (message2 != "")
        {
            temp = message2;
            if (temp == string_upper(temp))
                temp = scrMagicSigns("S2C", temp);
            temp = scrMagicSigns("MACRO", temp);
            temp = string_replace_all(temp, ",", "&");

            CWait = 1;
            COptT = ""; COptB = 0;
            CAsoc = self.id;
            CTarg = 0; CDist = 0;
            CVari = ""; CNumb = 1;

            while (temp != "")
            {
                command = scrMagicSigns("SPLIT", temp, "&", 0, 1);
                temp = scrMagicSigns_Result2;
                temp2 = string_upper(string_char_at(command, 1));
                temp3 = string_delete(command, 1, 1);
                if (string_pos(string_char_at(temp3, 1), "!=")) //Optional marks for readability.
                    temp3 = string_delete(temp3, 1, 1);
                switch (temp2)
                {
                    case "W": CWait = real(temp3); break;
                    case "T": CTarg = temp3;
                              CTarg = scrMagicSigns("SH", CTarg, "o");
                              CTarg = scrMagicSigns("DEREF", CTarg, "Condition1");
                    break;
                    case "A": CAsoc = temp3;
                              CAsoc = scrMagicSigns("SH", CAsoc, "o");
                              CAsoc = scrMagicSigns("DEREF", CAsoc, "Condition2");
                              if (CAsoc < 100000) {
                                  instance_activate_object(CAsoc);
                                  CAsoc = instance_nearest(x, y, CAsoc);
                              }
                    break;
                    case "D": CDist = real(temp3); break;
                    case "V": CVari = temp3; break;
                    case "N": CNumb = real(temp3); break;
                    case "O": COptT = string_upper(temp3);
                        COptB |= !!string_pos("1", COptT)*1; //once
                        COptB |= !!string_pos("R", COptT)*2; //Repeat
                        COptB |= !!string_pos("F", COptT)*4; //Farther
                        COptB |= !!string_pos("<", COptT)*8; //var less
                        COptB |= !!string_pos(">", COptT)*16; //var more
                        COptB |= !!string_pos("!", COptT)*24; //var not equal (same as <>)
                        COptB |= !!string_pos("@", COptT)*32; //Center
                        COptB |= !!string_pos("?", COptT)*64; //demo
                        COptB |= !!string_pos("D", COptT)*128; //Dead man's switch
                        COptB |= !!string_pos("L", COptT)*256; //Lock on
                        COptB |= !!string_pos("S", COptT)*512; //Start over
                        COptB |= !!string_pos("B", COptT)*1024; //Begin again
                        COptB |= !!string_pos("T", COptT)*2048; //use Target var
                        COptB |= !!string_pos("A", COptT)*4096; //use Associated var
                        COptB |= !!string_pos("C", COptT)*8192; //Count
                        COptB |= !!string_pos("X", COptT)*16384; //delete Assoc. obj.
                        COptB |= !!string_pos("Z", COptT)*32768; //Antifreeze
                    break;
    //What I did there is convert the textual options into bits.
    //My assumption is that bitwise-and will be faster than a string search.
                }
            }
            if (COptB & 2) //Repeat
                {perm = 1;}
            if (COptB & 64) //Demo
                {visible = 1; signmode = ""; image_blend = $00FF00; perm = 1;}
            if (COptB & 256) { //Lock on
                instance_activate_object(CTarg);
                CTarg = instance_nearest(x, y, CTarg);
            }
            if (COptB & 32768) //Antifreeze
                {alarm[3] = 1;}
            else
                {alarm[3] = -1;}
            alarm[2] = CWait;
        }
    break;

    //Escape characters using ' *********************************************************************************
    case "ESC": //String, [All?]
        if (argument2 == 1) {
            temp2 = "";
        } else {
            temp2 = "'";
            if (string_pos("'", argument1) == 0)
                return argument1;
        }

        temp = argument1;
        temp = string_replace_all(temp, temp2 + "'", "▓");
        temp = string_replace_all(temp, temp2 + "/", "ь");
        temp = string_replace_all(temp, temp2 + "&", "╠");
        temp = string_replace_all(temp, temp2 + "%", "В");
        temp = string_replace_all(temp, temp2 + "?", "©");
        temp = string_replace_all(temp, temp2 + "!", "║");
        temp = string_replace_all(temp, temp2 + '"', "■");
        temp = string_replace_all(temp, temp2 + "*", "∙");
        temp = string_replace_all(temp, temp2 + "@", "Е");
        temp = string_replace_all(temp, temp2 + "$", "ё");
        temp = string_replace_all(temp, temp2 + ".", "╥");
        temp = string_replace_all(temp, temp2 + "=", "╕");
        return temp;
    break;

    //Restore escape characters *********************************************************************************
    case "UNESC": //String
        temp = argument1;
        temp = string_replace_all(temp, "▓", "'");
        temp = string_replace_all(temp, "ь", "/");
        temp = string_replace_all(temp, "╠", "&");
        temp = string_replace_all(temp, "В", "%");
        temp = string_replace_all(temp, "©", "?");
        temp = string_replace_all(temp, "║", "!");
        temp = string_replace_all(temp, "■", '"');
        temp = string_replace_all(temp, "∙", "*");
        temp = string_replace_all(temp, "Е", "@");
        temp = string_replace_all(temp, "ё", "$");
        temp = string_replace_all(temp, "╥", ".");
        temp = string_replace_all(temp, "╕", "=");
        temp = string_replace_all(temp, "╚", "("); //Special
        temp = string_replace_all(temp, "╩", ")");
        temp = string_replace_all(temp, "┘", " "); //from S2C
        return temp;
    break;

    //Process macros *******************************************************************************************
    case "MACRO": //String
        if (string_pos("*", argument1) == 0)
            return argument1;
        temp = argument1;
        if (variable_global_exists("scrMagicSigns_MacroCount")) {
            for (i = scrMagicSigns_MacroCount; i > -1; i -= 1) {
                if (is_string(variable_global_array_get("scrMagicSigns_Macro", i)))
                    temp = string_replace_all(temp, "*" + string(i), string(scrMagicSigns_Macro[i]));
            }
        }

        if (variable_global_exists("scrMagicSigns_Last"))
            temp = string_replace_all(temp, "*l", string(scrMagicSigns_Last));
        if (variable_global_exists("scrMagicSigns_Prev"))
            temp = string_replace_all(temp, "*p", string(scrMagicSigns_Prev));
        with (other) {
            if (variable_local_exists("MSChild"))
                temp = string_replace_all(temp, "*c", string(MSChild));
            if (variable_local_exists("CAsoc"))
                temp = string_replace_all(temp, "*a", string(CAsoc));
            if (variable_local_exists("CTarg"))
                temp = string_replace_all(temp, "*t", string(CTarg));
        }
        return temp;
    break;

    default:
        return "";
    break;
}
