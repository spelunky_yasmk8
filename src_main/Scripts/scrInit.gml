//
// scrInit()
//
// Initialize the game (mostly controls and audio).
// called from oScreen>Create
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/
var name, value, file, str;

global.k8objTest = -1;

global.optDebugLog = false;
global.optDebugSeed = false;
global.startWithKapala = false;
global.ddamselDebug = false;
global.optSeedComputer = false;
global.seedComputerBroken = false;

global.optDoubleKiss = true;
global.optShopkeeperIdiots = false;
global.optDebugShopkeeper = false;

global.seedString = "";
global.seedMaxLength = 32;
global.seedLastRndSeed = 0;
/*
randomize();
while (true) {
  global.seedLastRndSeed = rand(1, 2147483647);
  if (global.seedLastRndSeed != 0) break;
}
*/
global.optDebugSeedLastValue = -1;

//startGame = false;
window_set_cursor(cr_none);

global.myFont = font_add_sprite(sFont, ord(' '), false, 0);
global.myFontSmall = font_add_sprite(sFontSmall, ord(' '), false, 0);
global.myFontSmall2 = font_add_sprite(sFontSmall2, ord(' '), false, 0);
global.myFontTiny = font_add_sprite(sFontTiny, ord(' '), false, 0); // 4x6
global.fullscreen = false;
global.graphicsHigh = true;
global.downToRun = true;
global.gamepadOn = false;
global.screenScale = 5;
global.musicVol = 15;
global.soundVol = 15;
global.musicEnabled = true;
global.soundEnabled = true;

global.keyUpVal = vk_up;
global.keyDownVal = vk_down;
global.keyLeftVal = vk_left;
global.keyRightVal = vk_right;
global.keyJumpVal = ord('Z');
global.keyAttackVal = ord('X');
global.keyItemVal = ord('C');
global.keyRunVal = vk_shift;
global.keyBombVal = ord('A');
global.keyRopeVal = ord('S');
global.keyPayVal = ord('P');

global.joyJumpVal = 2;
global.joyAttackVal = 1;
global.joyItemVal = 3;
global.joyRunVal = 5;
global.joyBombVal = 7;
global.joyRopeVal = 8;
global.joyFlareVal = 4;
global.joyPayVal = 6;
global.joyStartVal = 10;

// YASM 1.7 default joystick button labels (see gamepadlabels.cfg)
global.joyJumpLabel = "A";
global.joyAttackLabel = "X";
global.joyItemLabel = "LT";
global.joyRunLabel = "RT";
global.joyBombLabel = "B";
global.joyRopeLabel = "Y";
global.joyPayLabel = "RB";
global.joyStartLabel = "START";

scrInitTotals();

file = file_text_open_read("yasm_settings.cfg");
if (!file) file = file_text_open_read("settings.cfg");
if (file) {
  // read "key=value" pairs
  while (not file_text_eof(file)) {
    str = file_text_read_string(file);
    file_text_readln(file);
    name = k8strkey(str);
    value = k8strvalue(str);
         if (name == "fullscreen") { global.fullscreen = (value == "tan" or value == "true"); }
    else if (name == "graphicsHigh") { global.graphicsHigh = (value == "tan" or value == "true"); }
    else if (name == "downToRun") { global.downToRun = (value == "tan" or value == "true"); }
    else if (name == "gamepadOn") { global.gamepadOn = (value == "tan" or value == "true"); }
    else if (name == "screenScale") { global.screenScale = real(value); }
    else if (name == "musicVol") { global.musicVol = real(value); }
    else if (name == "soundVol") { global.soundVol = real(value); }
    else if (name == "debuglog") { global.optDebugLog = (value == "tan" or value == "true"); }
    else if (name == "ddamselDebug") { global.ddamselDebug = (value == "tan" or value == "true"); }
    else if (name == "debugSeed") { global.optDebugSeed = (value == "tan" or value == "true"); }
  }
  file_text_close(file);
}

if (global.musicVol > 18) global.musicVol = 18;
if (global.musicVol < 0) global.musicVol = 0;
if (global.soundVol > 18) global.soundVol = 18;
if (global.soundVol < 0) global.soundVol = 0;

file = file_text_open_read("yasm_keys.cfg");
if (!file) file = file_text_open_read("keys.cfg");
if (file) {
  // read "key=value" pairs
  while (not file_text_eof(file)) {
    str = file_text_read_string(file);
    file_text_readln(file);
    name = k8strkey(str);
    value = k8strvalue(str);
         if (name == "up") { global.keyUpVal = real(value); }
    else if (name == "down") { global.keyDownVal = real(value); }
    else if (name == "left") { global.keyLeftVal = real(value); }
    else if (name == "right") { global.keyRightVal = real(value); }
    else if (name == "jump") { global.keyJumpVal = real(value); }
    else if (name == "attack") { global.keyAttackVal = real(value); }
    else if (name == "item") { global.keyItemVal = real(value); }
    else if (name == "run") { global.keyRunVal = real(value); }
    else if (name == "bomb") { global.keyBombVal = real(value); }
    else if (name == "rope") { global.keyRopeVal = real(value); }
    else if (name == "pay") { global.keyPayVal = real(value); }
  }
  file_text_close(file);
}

file = file_text_open_read("yasm_gamepad.cfg");
if (!file) file = file_text_open_read("gamepad.cfg");
if (file) {
  global.joyJumpVal = real(file_text_read_string(file));
  file_text_readln(file);
  global.joyAttackVal = real(file_text_read_string(file));
  file_text_readln(file);
  global.joyItemVal = real(file_text_read_string(file));
  file_text_readln(file);
  global.joyRunVal = real(file_text_read_string(file));
  file_text_readln(file);
  global.joyBombVal = real(file_text_read_string(file));
  file_text_readln(file);
  global.joyRopeVal = real(file_text_read_string(file));
  file_text_readln(file);
  global.joyFlareVal = real(file_text_read_string(file));
  file_text_readln(file);
  global.joyPayVal = real(file_text_read_string(file));
  file_text_readln(file);
  global.joyStartVal = real(file_text_read_string(file));
  file_text_close(file);
}

scrGamepadLabelsRead();
scrYASMInit();
scrYASMLoadSettings(); // YASM 1.7+
global.optVSync = false; // not yet
set_synchronization(global.optVSync); //k8
scrLoadSprites(); // YASM 1.8
scrInitSound();
global.customMusicCache = ds_map_create(); // YASM 1.8 (TyrOvC)

if (!joystick_exists(1) && !joystick_exists(2)) global.gamepadOn = false;

globalvar gamepad;
gamepad = instance_create(0, 0, oGamepad);

globalvar offset;
offset = 96; // frozen region (distance from edge of screen)

//Spelunky 1.2 7-3-2012 Switch to soft fullscreen if screenScale is 5 (scale up to resolutions) and fullscreen off
global.softfullscreen = 0;
if (global.fullscreen == 0 && global.screenScale == 5) {
  global.softfullscreen = 1;
} else if (global.fullscreen == 1 && global.screenScale*240 > display_get_height()) {
  // use soft fullscreen if screenScale will not fit display - Moloch
  global.softfullscreen = 1;
  global.screenScale = 5;
}
