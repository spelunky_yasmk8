// scrUpDown(globvarname, minval, maxval, label, infotext)
var vname;

uiType[uiItemCount] = "updown";
uiText[uiItemCount] = argument3;
uiInfoText[uiItemCount] = argument4;

vname = argument0;
uiDisabled[uiItemCount] = false;
if (string_char_at(vname, 1) == "-") {
  uiDisabled[uiItemCount] = true;
  uiInfoText[uiItemCount] = "THIS OPTION IS DISABLED.";
  vname = string_delete(vname, 1, 1);
}
uiVarName[uiItemCount] = vname;

uiInfoText[uiItemCount] = scrWordWrap(uiInfoText[uiItemCount], 36, "#", true);

uiCurValue[uiItemCount] = variable_global_get(vname);
uiMinValue[uiItemCount] = argument1;
uiMaxValue[uiItemCount] = argument2;

uiItemCount += 1;
