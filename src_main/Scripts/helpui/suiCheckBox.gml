// scrOptionButton(globalvarname, label, infotext)
var vname;

uiType[uiItemCount] = "checkbox";

uiText[uiItemCount] = argument1;
uiInfoText[uiItemCount] = argument2;

vname = argument0;
uiDisabled[uiItemCount] = false;
if (string_char_at(vname, 1) == "-") {
  uiDisabled[uiItemCount] = true;
  uiInfoText[uiItemCount] = "THIS OPTION IS DISABLED.";
  vname = string_delete(vname, 1, 1);
}
uiVarName[uiItemCount] = vname;

uiInfoText[uiItemCount] = scrWordWrap(uiInfoText[uiItemCount], 36, "#", true);

uiCurValue[uiItemCount] = false;
if (variable_global_get(vname)) uiCurValue[uiItemCount] = true;

uiItemCount += 1;
