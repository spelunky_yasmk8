// scrSeedObjByName
if (is_string(argument0)) {
  switch (string_lower(argument0)) {
    case "basketball": return oBasketball;
    case "bomb bag": case "bombbag": return oBombBag;
    case "bomb box": case "bombbox": return oBombBox;
    case "bow": return oBow;
    case "cape": return oCapePickup;
    case "cheatgun": return oCheatgun;
    case "compass": return oCompass;
    case "gloves": return oGloves;
    case "jetpack": return oJetpack;
    case "jordans": return oJordans;
    case "machete": return oMachete;
    case "mattock": return oMattock;
    case "mitt": return oMitt;
    case "parachute": return oParaPickup;
    case "paste": return oPaste;
    case "pistol": return oPistol;
    case "rope pile": case "ropepile": return oRopePile;
    case "shell": return oShellSingle;
    case "shells": return oShells;
    case "shotgun": return oShotgun;
    case "spectacles": return oSpectacles;
    case "spike shoes": case "spikeshoes": return oSpikeShoes;
    case "spring shoes": case "springshoes": return oSpringShoes;
    case "teleporter": return oTeleporter;
    case "teleporter2": return oTeleporter2;
    case "web cannon": case "webcannon": return oWebCannon;
  }
}
return -1;
