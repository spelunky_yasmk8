// scrSetMattockUses
// Returns number of uses for a newly created mattock
var i, u;
i = rand(1, 20);
u = 1;
while (i > 1) {
  u += 1;
  i = rand(1, 20);
}
return u;
