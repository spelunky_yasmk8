// scrSetupSeeds(num)
/*
 * global.seed[0] will store our base seed and will be used as our
 * random_set_seed to generate the seeds for levels 1 to 16.
 * global.seed[n] will then be used to set the seed for the corresponding level
 * generation scripts:
 * random_set_seed(global.seed[global.currLevel])
 */
var i, j, ok;
if (is_string(argument0)) argument0 = 0;
if (argument0 == 0) argument0 = 666;
global.seed[0] = argument0;
random_set_seed(argument0);
for (i = 1; i <= 42; i += 1) {
  while (true) {
    ok = true;
    global.seed[i] = rand(1, 2147483647)/**choose(-1, 1)*/;
    for (j = 0; j < i; j += 1) {
      if (global.seed[i] == global.seed[j]) { ok = false; break; }
    }
    if (ok) break;
  }
}
