// scrGeneratePrize
// Select a single item for sequence of dice house prizes during level generation
if (rand(1, 40) == 1) return "Jetpack";
if (rand(1, 25) == 1) return "Cape";
if (rand(1, 20) == 1) return "Shotgun";
if (rand(1, 10) == 1) return "Gloves";
if (rand(1, 10) == 1) return "Teleporter";
if (rand(1, 8) == 1) return "Mattock";
if (rand(1, 8) == 1) return "Paste";
if (rand(1, 8) == 1) return "Spring Shoes";
if (rand(1, 8) == 1) return "Spike Shoes";
if (rand(1, 8) == 1) return "Compass";
if (rand(1, 8) == 1) return "Pistol";
if (rand(1, 8) == 1) return "Machete";
return "Bomb Box";
