// scrSeedFromString
// Generate a seed for each level using global.seedString
var n;
var str;

str = global.seedString;
global.seed = 0; // array of seeds for each level
if (str == "") {
  // random seed
  if (global.seedLastRndSeed == 0) {
    randomize();
  } else {
    random_set_seed(global.seedLastRndSeed);
  }
  n = rand(1, 2147483647);
  global.seedLastRndSeed = random_get_seed();
} else {
  n = k8str2pnum(str);
  if (n < 0) n = joaathash(str); // wow, it's not an integer, make a string hash instead
}
scrSetupSeeds(n);
