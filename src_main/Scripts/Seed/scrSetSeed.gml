// scrSetSeed
if (global.optSeedComputer && !global.seedComputerBroken) {
  if (global.scumLevelGenerated || global.currLevel < 2) {
    // regenerate seed, as it meant to be random
    scrSeedFromString();
  }

  if (global.optDebugSeed) {
    if (global.optDebugSeedLastValue != global.seed[0]) {
      global.optDebugSeedLastValue = global.seed[0];
      var file, i;
      file = file_text_open_read("zseed");
      if (file) {
        scrSetupSeeds(real(file_text_read_string(file)));
      } else {
        file = file_text_open_append("z_yasm_seed");
        if (file) {
          file_text_write_string(file, string(global.seed[0]));
          file_text_writeln(file);
          for (i = 1; i < 16; i += 1) {
            file_text_write_string(file, "  ");
            file_text_write_string(file, string(global.seed[i]));
            file_text_writeln(file);
          }
        }
      }
      if (file) file_text_close(file);
    }
  }

  random_set_seed(global.seed[global.currLevel]);
}
