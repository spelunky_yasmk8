// scrCreatePrize(x, y, name)
// Create the next prize in dice house.
// Called by oShopkeeper.Step
var obj;
obj = scrSeedObjByName(argument2);
if (obj < 0) obj = oBombBox;
return instance_create(argument0, argument1, obj);
