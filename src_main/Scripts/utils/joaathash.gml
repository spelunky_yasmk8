// joaathash(str)
// Bob Jenkins' One-At-A-Time hash function
var i, hash;

hash = 0;
for (i = 1; i <= string_length(argument0); i += 1) {
  hash += ord(string_char_at(argument0, i));
  hash &= $ffffffff;
  hash += (hash<<10);
  hash &= $ffffffff;
  hash ^= (hash>>6);
}
hash += (hash<<3);
hash &= $ffffffff;
hash ^= (hash>>11);
hash += (hash<<15);
hash &= $ffffffff;
return hash;
