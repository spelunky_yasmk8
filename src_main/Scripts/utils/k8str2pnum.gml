// k8str2pnum(str)
// convert string to POSITIVE number
// return -1 on error
var n, i, str;
str = argument0;
if (string_length(str) < 1) return -1;
n = 0;
for (i = 1; i <= string_length(str); i += 1) {
  d = ord(string_char_at(str, i))-48;
  if (d < 0 || d > 9) return -1;
  n = n*10+d;
}
if (i <= string_length(str)) return -1;
return n;
