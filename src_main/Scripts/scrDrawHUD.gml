//
// scrDrawHUD()
//
// Draw the HUD.
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/
if (instance_exists(oPlayer1)) {
  if (global.drawHUD) {
    if (global.hasCompass) scrDrawCompass();
    if (global.gahara) {
      scrDrawGHUD();
    } else {
      var n, lifeX, bombX, ropeX, moneyX, ammoX, life, hhup, ity;

      lifeX = 4; // 8
      /*
      bombX = 64;
      ropeX = 120;
      ammoX = 176;
      moneyX = 232;
      */
      bombX = 56;
      ropeX = 104;
      ammoX = 152;
      moneyX = 200;

      if (!global.optSGAmmo) moneyX = ammoX;

      if (global.scumHud) {
        draw_set_font(global.myFontSmall);
        hhup = 6;
      } else {
        draw_set_font(global.myFont);
        hhup = 0;
      }
      draw_set_color(c_white);

      // hearts
      if (global.scumHud) {
        if (global.plife == 1) {
          draw_sprite(sHeartSmallBlink, global.heartBlink, lifeX+2, 4-hhup);
          global.heartBlink += 0.1;
          if (global.heartBlink > 3) global.heartBlink = 0;
        } else {
          draw_sprite(sHeartSmall, -1, lifeX+2, 4-hhup);
        }
      } else {
        if (global.plife == 1) {
          draw_sprite(sHeartBlink, round(global.heartBlink), lifeX, 8-hhup);
          global.heartBlink += 0.1;
          if (global.heartBlink > 3) global.heartBlink = 0;
        } else {
          draw_sprite(sHeart, -1, lifeX, 8-hhup);
        }
      }
      life = global.plife;
      if (life < 0) life = 0;
      if (!global.scumHud && life > 99) life = 99;
      draw_text(lifeX+16, 8-hhup, string(life));

      // bombs
      if (global.hasStickyBombs && global.stickyBombsActive) {
        if (global.scumHud) draw_sprite(sStickyBombIconSmall, -1, bombX+2, 4-hhup); else draw_sprite(sStickyBombIcon, -1, bombX, 8-hhup);
      } else {
        if (global.scumHud) draw_sprite(sBombIconSmall, -1, bombX+2, 4-hhup); else draw_sprite(sBombIcon, -1, bombX, 8-hhup);
      }
      n = global.bombs;
      if (n < 0) n = 0; else if (!global.scumHud && n > 99) n = 99;
      draw_text(bombX+16, 8-hhup, n);

      // ropes
      if (global.scumHud) draw_sprite(sRopeIconSmall, -1, ropeX+2, 4-hhup); else draw_sprite(sRopeIcon, -1, ropeX, 8-hhup);
      n = global.rope;
      if (n < 0) n = 0; else if (!global.scumHud && n > 99) n = 99;
      draw_text(ropeX+16, 8-hhup, n);

      // shotgun shells
      if (global.optSGAmmo) {
        if (global.scumHud) draw_sprite(sShellsIcon, -1, ammoX+6, 8-hhup); else draw_sprite(sShellsIcon, -1, ammoX+7, 12-hhup);
        n = global.sgammo;
        if (n < 0) n = 0; else if (!global.scumHud && n > 99) n = 99;
        draw_text(ammoX+16, 8-hhup, n);
      }

      // money
      if (global.scumHud) draw_sprite(sDollarSignSmall, -1, moneyX+2, 3-hhup); else draw_sprite(sDollarSign, -1, moneyX, 8-hhup);
      draw_text(moneyX+16, 8-hhup, global.money);

      if (global.scumHud) ity = 18-hhup; else ity = 24-hhup;

      n = 8; //28;
      if (global.hasUdjatEye) {
        if (global.udjatBlink) draw_sprite(sUdjatEyeIcon2, -1, n, ity); else draw_sprite(sUdjatEyeIcon, -1, n, ity);
        n += 20;
      }
      if (global.hasAnkh) { draw_sprite(sAnkhIcon, -1, n, ity); n += 20; }
      if (global.hasCrown) { draw_sprite(sCrownIcon, -1, n, ity); n += 20; }
      if (global.hasKapala) {
             if (global.bloodLevel == 0) draw_sprite(sKapalaIcon, 0, n, ity);
        else if (global.bloodLevel <= 2) draw_sprite(sKapalaIcon, 1, n, ity);
        else if (global.bloodLevel <= 4) draw_sprite(sKapalaIcon, 2, n, ity);
        else if (global.bloodLevel <= 6) draw_sprite(sKapalaIcon, 3, n, ity);
        else if (global.bloodLevel <= 8) draw_sprite(sKapalaIcon, 4, n, ity);
        n += 20;
      }
      if (global.hasSpectacles) { draw_sprite(sSpectaclesIcon, -1, n, ity); n += 20; }
      if (global.hasGloves) { draw_sprite(sGlovesIcon, -1, n, ity); n += 20; }
      if (global.hasMitt) { draw_sprite(sMittIcon, -1, n, ity); n += 20; }
      if (global.hasSpringShoes) { draw_sprite(sSpringShoesIcon, -1, n, ity); n += 20; }
      if (global.hasJordans) { draw_sprite(sJordansIcon, -1, n, ity); n += 20; }
      if (global.hasSpikeShoes) { draw_sprite(sSpikeShoesIcon, -1, n, ity); n += 20; }
      if (global.hasCape) { draw_sprite(sCapeIcon, -1, n, ity); n += 20; }
      if (global.hasJetpack) { draw_sprite(sJetpackIcon, -1, n, ity); n += 20; }
      if (global.hasStickyBombs) { draw_sprite(sPasteIcon, -1, n, ity); n += 20; }
      if (global.hasCompass) { draw_sprite(sCompassIcon, -1, n, ity); n += 20; }
      if (global.hasParachute) { draw_sprite(sParachuteIcon, -1, n, ity); n += 20; }
      if (oPlayer1.pickupItemType == "Bow" && global.arrows > 0) {
        var m, malpha;
        m = 1;
        malpha = 1;
        while (m <= global.arrows && m <= 20) {
          draw_sprite_ext(sArrowIcon, -1, n, ity, 1, 1, 0, c_white, malpha);
          n += 4;
          if (m > 10) malpha -= 0.1; //and global.arrows > 20 malpha -= 0.1
          m += 1;
        }
      }

      draw_set_font(global.myFontSmall);
      draw_set_color(c_yellow);
      if (global.collect > 0) {
        if (global.scumHud) draw_text(moneyX+8, 8+8-hhup, "+"+string(global.collect)); else draw_text(moneyX, 8+16-hhup, "+"+string(global.collect));
      }
    }
  }
}
