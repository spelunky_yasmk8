caster_stop(global.musTitle);
caster_stop(global.musCave);
caster_stop(global.musLush);
caster_stop(global.musIce);
caster_stop(global.musTemple);
caster_stop(global.musBoss);
caster_stop(global.musVictory);
caster_stop(global.musCredits);

// Cycle through and stop any custom level music that's been loaded
// YASM 1.7.3 (TyrOvC)
var tempKey, i;
tempKey = ds_map_find_first(global.customMusicCache);
for (i = 0; i < ds_map_size(global.customMusicCache); i+= 1) {
  caster_stop(ds_map_find_value(global.customMusicCache, tempKey));
  tempKey = ds_map_find_next(global.customMusicCache, tempKey);
}
