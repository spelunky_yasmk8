// startMusic(dontStopSound=false)
if (!argument0) sndStopAll();
//caster_stop(all);
//caster_free(all);

if (instance_exists(oMusic)) {
  oMusic.currMusic = "";
  oMusic.customID = -1;
  oMusic.currFreq = -1;
}

if (global.musicEnabled) {
  if (instance_exists(oLoadLevel)) {
    var mid;
    mid = sndMusGetCached(oLoadLevel.music);
    if (mid != -1) playMusic(mid);
    exit;
  }
  if (isLevel()) {
    if (isRoom("rOlmec")) { if (oPlayer1.active) playMusic(global.musBoss); exit; }
    if (global.levelType == 1) { playMusic(global.musLush); exit; }
    if (global.levelType == 2) { playMusic(global.musIce); exit; }
    if (global.levelType == 3) { playMusic(global.musTemple); exit; }
    playMusic(global.musCave);
    exit;
  }
  if (isRoom("rTitle")) { playMusic(global.musTitle); exit; }
  if (isRoom("rSun") || isRoom("rMoon") || isRoom("rStars") || isRoom("rLava") || isRoom("rTest")) { playMusic(global.musBoss); exit; }
}

