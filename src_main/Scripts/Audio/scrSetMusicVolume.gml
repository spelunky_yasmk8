// argument0 = 0 to 18 (18 is maximum volume), or < 0 to reduce default volume (for pause)
var vol;
vol = 0;

if (argument0 < 0) {
  // reduce volume slightly for pause screen
  vol = max(0, (global.musicVol-2)/18);
} else if (argument0 > 0) {
  vol = min(argument0, 18)/18;
}

if (caster_is_playing(global.musTitle)) caster_set_volume(global.musTitle, vol);
if (caster_is_playing(global.musCave)) caster_set_volume(global.musCave, vol);
if (caster_is_playing(global.musLush)) caster_set_volume(global.musLush, vol);
if (caster_is_playing(global.musIce)) caster_set_volume(global.musIce, vol);
if (caster_is_playing(global.musTemple)) caster_set_volume(global.musTemple, vol);
if (caster_is_playing(global.musBoss)) caster_set_volume(global.musBoss, vol);

if (instance_exists(oLoadLevel)) {
  var musicSuperSoundID;
  if (ds_map_exists(global.customMusicCache, oLoadLevel.music)) {
    musicSuperSoundID = ds_map_find_value(global.customMusicCache, oLoadLevel.music);
    if (caster_is_playing(musicSuperSoundID)) caster_set_volume(musicSuperSoundID, vol);
  }
}
