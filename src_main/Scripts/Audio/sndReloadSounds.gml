// sndReloadSounds(showmessage)
if (argument0) {
  var msg;
  msg = "INITIALIZING...";
  surface_reset_target();
  draw_set_blend_mode(bm_normal);
  //screen_redraw();
  draw_set_font(global.myFont);
  draw_set_color(c_yellow);
  var msgx, msgy;
  msgx = (320*global.screenScale-string_length(msg)*16) div 2;
  msgy = (240*global.screenScale-16) div 2;
  draw_text(msgx, msgy, msg);
  screen_refresh();
}
scrLoadSound();
/*
if (argument0) {
  surface_reset_target();
  draw_set_blend_mode(bm_normal);
  screen_redraw();
  screen_refresh();
}
*/
