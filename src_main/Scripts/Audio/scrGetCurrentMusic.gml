// returns ID of music currently playing
// returns -1 if none/error

if (caster_is_playing(global.musCave)) return global.musCave;
if (caster_is_playing(global.musLush)) return global.musLush;
if (caster_is_playing(global.musIce)) return global.musIce;
if (caster_is_playing(global.musTemple)) return global.musTemple;
if (caster_is_playing(global.musBoss)) return global.musBoss;
if (caster_is_playing(global.musVictory)) return global.musVictory;
if (caster_is_playing(global.musCredits)) return global.musCredits;
if (caster_is_playing(global.musTitle)) return global.musTitle;
if (instance_exists(oLoadLevel)) return sndMusGetCached(oLoadLevel.music);

return -1;
