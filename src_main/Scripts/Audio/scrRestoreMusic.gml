// scrRestorMusic
// called by startMusic
// restores music playback frequency if a save file was just loaded
if (instance_exists(oMusic)) {
  if (oMusic.currMusic != "") {
    var musStr, musID;
    musStr = argument0; // "LUSH"
    musID = argument1; // global.musLush
    if (oMusic.currMusic == musStr) {
      if (oMusic.currFreq > 0) sndSetFreq(musID, oMusic.currFreq);
    }
  }
}
