// playMusic(sampleId[, once]])
if (global.musicEnabled) {
  var smpid, once;
  smpid = argument0;

  if (variable_global_exists("optDebugLog")) {
    if (global.optDebugLog) {
      k8dbglog("=== playMusic called ===");
      if (caster_is_playing(global.musCave)) k8dbglog("current: CAVE");
      if (caster_is_playing(global.musLush)) k8dbglog("current: LUSH");
      if (caster_is_playing(global.musIce)) k8dbglog("current: ICE");
      if (caster_is_playing(global.musTemple)) k8dbglog("current: TEMPLE");
      if (caster_is_playing(global.musBoss)) k8dbglog("current: BOSS");
      if (caster_is_playing(global.musVictory)) k8dbglog("current: VICTORY");
      if (caster_is_playing(global.musCredits)) k8dbglog("current: CREDITS");
      if (caster_is_playing(global.musTitle)) k8dbglog("current: TITLE");

           if (smpid == global.musCave) k8dbglog("new: CAVE");
      else if (smpid == global.musLush) k8dbglog("new: LUSH");
      else if (smpid == global.musIce) k8dbglog("new: ICE");
      else if (smpid == global.musTemple) k8dbglog("new: TEMPLE");
      else if (smpid == global.musBoss) k8dbglog("new: BOSS");
      else if (smpid == global.musVictory) k8dbglog("new: VICTORY");
      else if (smpid == global.musCredits) k8dbglog("new: CREDITS");
      else if (smpid == global.musTitle) k8dbglog("new: TITLE");
      else k8dbglog("new: <UNKNOWN>");
    }
  }

  // reload sounds when we returned to title (casterfuck)
  /*
  if (smpid == global.musTitle) {
    scrLoadSound();
    smpid = global.musTitle;
  }
  */

  once = argument1;
  if (!caster_is_playing(smpid)) {
    var mid;
    stopAllMusic();
    if (once) {
      mid = caster_play(smpid, global.musicVol/18, 1);
    } else {
      mid = caster_loop(smpid, global.musicVol/18, 1);
    }
  } else {
    mid = smpid;
    caster_set_volume(mid, global.musicVol/18);
  }
  if (caster_is_playing(mid)) {
    caster_set_pitch(mid, 1);
    caster_set_panning(mid, 0);
  }
  return mid;
}
return -1;
