// playSound(sampleId[, pitch[, pan]])
var sid;
var pitch, pan;
pitch = argument1;
pan = argument2;
if (pitch <= 0) pitch = 1;

if (variable_global_exists("optDebugLog")) {
  if (global.optDebugLog) {
    var name;
    k8dbglog("=== playSound called ===");
    name = sndGetSampleName(argument0);
    if (caster_is_playing(argument0)) {
      k8dbglog("sound (playing): "+name);
    } else {
      k8dbglog("sound: "+name);
    }
  }
}

if (caster_is_playing(argument0)) caster_stop(argument0);
if (global.soundEnabled) {
  sid = caster_play(argument0, global.soundVol/18, pitch);
  if (caster_is_playing(sid)) {
    caster_set_panning(sid, pan);
  } else {
    sid = -1;
  }
} else {
  sid = -1;
}
return sid;
