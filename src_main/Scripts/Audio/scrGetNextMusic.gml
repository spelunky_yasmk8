// returns ID of music that should be playing in this level
// returns -1 if none/error

if (instance_exists(oLoadLevel)) return sndMusGetCached(oLoadLevel.music);

if (isLevel()) {
  if (isRoom("rOlmec")) return global.musBoss;
  if (global.levelType == 1) return global.musLush;
  if (global.levelType == 2) return global.musIce;
  if (global.levelType == 3) return global.musTemple;
  return global.musCave;
}

if (isRoom("rTitle")) return global.musTitle;

if (isRoom("rSun") || isRoom("rMoon") || isRoom("rStars") || isRoom("rLava") || isRoom("rTest")) return global.musBoss;

return -1;
