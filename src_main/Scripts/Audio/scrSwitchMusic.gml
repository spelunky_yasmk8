// Gahara
// Stop currently playing song and start a new one. If new song is already playing, ignore.
// argument0 = string of next song to use "CAVE" or custom song "MYSONG" for mysong.ogg, or "" to
// let script choose song based on current level.

var currSong, nextSong;
currSong = scrGetCurrentMusic();
nextSong = -1;

if (string(argument0) == "" || string(argument0) == "0") {
  nextSong = currSong;
} else {
  nextSong = sndMusGetCached(argument0);
}

//show_message("current:"+string(currSong)+"#next:"+string(nextSong));

if (!compare(currSong, nextSong)) {
  stopAllMusic();
  if (nextSong != -1) playMusic(nextSong);
}
