if (isLevel()) {
  var musPlaying;
  musPlaying = scrGetCurrentMusic();
  if (musPlaying != -1) {
    if (isRoom("rOlmec")) {
      if (oPlayer1.active) caster_set_pitch(musPlaying, caster_get_pitch(musPlaying)-0.00226);
    } else {
      caster_set_pitch(musPlaying, caster_get_pitch(musPlaying)-0.00226);
    }
  }
}
