// sndMusGetCached(name)
// returns ID of music or -1 if none/error
var musname;
musname = string_upper(argument0);

if (musname == "CAVE") return global.musCave;
if (musname == "LUSH") return global.musLush;
if (musname == "ICE") return global.musIce;
if (musname == "TEMPLE") return global.musTemple;
if (musname == "BOSS") return global.musBoss;
if (musname == "CREDITS") return global.musCredits;
if (musname == "TITLE") return global.musTitle;
if (musname == "VICTORY") return global.musVictory;

if (musname != "" && musname != "NONE") {
  var musicSuperSoundID;
  if (ds_map_exists(global.customMusicCache, musname)) {
    musicSuperSoundID = ds_map_find_value(global.customMusicCache, musname);
    return musicSuperSoundID;
  }
  if (file_exists("levels/"+musname+".ogg")) {
    musicSuperSoundID = caster_load("levels/"+musname+".ogg");
    ds_map_add(global.customMusicCache, musname, musicSuperSoundID);
    return musicSuperSoundID;
  }
  if (file_exists("sound/"+musname+".ogg")) {
    musicSuperSoundID = caster_load("sound/"+musname+".ogg");
    ds_map_add(global.customMusicCache, musname, musicSuperSoundID);
    return musicSuperSoundID;
  }
}
return -1;
