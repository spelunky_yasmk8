var name, sid, i;
sid = -1; // linter
name = argument0;
// sometimes reloading first ogg failing, so...
for (i = 3; i > 0; i -= 1) {
  sid = caster_load(name);
  if (caster_error_occurred()) {
    k8dbglog("sound '"+name+"' loading error: "+caster_error_message());
    sid = -1;
  } else {
    break;
  }
}
return sid;
