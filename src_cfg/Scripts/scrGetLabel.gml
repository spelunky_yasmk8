// scrGetLabel
switch (argument0) {
  case 1: return global.joyAttackLabel;
  case 2: return global.joyItemLabel;
  case 3: return global.joyRunLabel;
  case 4: return global.joyBombLabel;
  case 5: return global.joyRopeLabel;
  case 6: return global.joyPayLabel;
  case 7: return global.joyStartLabel;
  default: return global.joyJumpLabel;
}
