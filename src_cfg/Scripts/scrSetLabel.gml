// scrSetLabel
switch(argument0)
{
    case 1: {global.joyAttackLabel = labelText; break;}
    case 2: {global.joyItemLabel = labelText; break;}
    case 3: {global.joyRunLabel = labelText; break;}
    case 4: {global.joyBombLabel = labelText; break;}
    case 5: {global.joyRopeLabel = labelText; break;}
    case 6: {global.joyPayLabel = labelText; break;}
    case 7: {global.joyStartLabel = labelText; break;}
    default: {global.joyJumpLabel = labelText; break;}
}
