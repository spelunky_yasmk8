// k8dbglog(str[, filename])
//if (!variable_global_exists("optDebugLog")) global.optDebugLog = true;

if (variable_global_exists("optDebugLog")) {
  if (global.optDebugLog) {
    var str, fname, file;
    str = string(argument0);
    fname = string(argument1);
    if (fname == "" || fname == "0") fname = "zlog.log";
    file = file_text_open_append(fname);
    if (file) {
      file_text_write_string(file, str);
      file_text_writeln(file);
      file_text_close(file);
    }
  }
}
