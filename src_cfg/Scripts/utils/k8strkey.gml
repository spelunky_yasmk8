// get key from "a=b" string
var pos, res;
pos = string_pos("=", argument0);
if (pos < 2) return "";
res = string_copy(argument0, 1, pos-1);
return k8strtrim(res);
