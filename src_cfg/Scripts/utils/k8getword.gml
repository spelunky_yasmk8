// get nth word [0..] from space-delimited string
// k8getword(str, nth)
str = argument0;
idx = argument1;
while (true) {
  // skip spaces
  pos = 1;
  while (pos <= string_length(str) && ord(string_char_at(str, pos)) <= 32) pos += 1;
  if (pos > 1) str = string_delete(str, 1, pos-1);
  if (string_length(str) < 1) return ""; // no more words
  // skip non-spaces
  pos = 1;
  while (pos <= string_length(str) && ord(string_char_at(str, pos)) > 32) pos += 1;
  // got a word
  idx -= 1;
  if (idx < 0) return string_copy(str, 1, pos-1);
  // delete this word
  str = string_delete(str, 1, pos);
}
