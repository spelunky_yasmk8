// return random number in interval [lo..hi]
// rand (lo, hi)
randNum = argument1-argument0+1;
return floor(random(randNum))+argument0;
