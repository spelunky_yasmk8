// get value from "a=b" string
var pos, res;
pos = string_pos("=", argument0);
if (pos < 1 or pos >= string_length(argument0)) return "";
res = string_copy(argument0, pos+1, string_length(argument0)-pos);
return k8strtrim(res);
