var str;
str = argument0;
while (string_length(str) > 0 && ord(string_char_at(str, 1)) <= 32) str = string_delete(str, 1, 1);
while (string_length(str) > 0 && ord(string_char_at(str, string_length(str))) <= 32) str = string_delete(str, string_length(str), 1);
return str;
