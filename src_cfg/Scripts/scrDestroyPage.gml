// scrDestroyPage(dir)
// should be called in `oYASMConfig` object
// `dir` is -1 for prev page, 1 for next page or 666 for something else
// no need to destroy "next" and "prev" buttons here

// destroy controls
with (oCheckBoxUI) instance_destroy();
with (oUpDownButton) instance_destroy();

// process page transitions
switch (argument0) {
  case -1: if (page > 1) page -= 1; break;
  case  1: if (page < 3) page += 1; break;
}
