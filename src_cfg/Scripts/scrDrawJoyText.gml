// scrDrawJoyText(status, joy_text, button_val)
// from oJoyConfig > Draw

draw_set_font(global.myFontSmall);
draw_set_color(c_white);
draw_text(xx, 192, "PRESS ESCAPE TO KEEP SAME OR");
draw_text(xx, 200, "MOUSE WHEEL TO MOVE UP/DOWN.");

if (argument0 == status)
{
    draw_set_color(c_yellow);
    draw_set_alpha(glowAlpha);
    draw_text(xx, 160, "PRESS BUTTON FOR " + argument1);
    draw_set_alpha(1);

    // selection highlight
    draw_set_color(c_silver);
    draw_rectangle(xx - 2, yy - 2, xx + 272, yy + 8, true);
}

draw_text(xx, yy, argument1);

if (argument0 == status)
{
    draw_set_color(glowCol);
    draw_set_alpha(glowAlpha);
}

switch (argument2)
{
    case 1: { draw_text(xx + xg, yy, "B1"); break; }
    case 2: { draw_text(xx + xg, yy, "B2"); break; }
    case 3: { draw_text(xx + xg, yy, "B3"); break; }
    case 4: { draw_text(xx + xg, yy, "B4"); break; }
    case 5: { draw_text(xx + xg, yy, "B5"); break; }
    case 6: { draw_text(xx + xg, yy, "B6"); break; }
    case 7: { draw_text(xx + xg, yy, "B7"); break; }
    case 8: { draw_text(xx + xg, yy, "B8"); break; }
    case 9: { draw_text(xx + xg, yy, "B9"); break; }
    case 10: { draw_text(xx + xg, yy, "B10"); break; }
    case -1: { draw_text(xx + xg, yy, "LT (XB)"); break; }
    case -2: { draw_text(xx + xg, yy, "RT (XB)"); break; }
    default: { draw_text(xx + xg, yy, "B" + string(argument2)); break; }
}

//if (argument0 == status) draw_text(xx + xg*2, yy, argument3 + textCursor);
//else draw_text(xx + xg*2, yy, argument3);
draw_text(xx + xg*2, yy, argument3);
if (argument0 == status and textCursor)
{
    draw_rectangle(xx + xg*2 + string_length(argument3)*8 + 1, yy + 6, xx + xg*2 + string_length(argument3)*8 + 7, yy + 6, false);
}

if (textCursorTimer <= 0)
{
    textCursor = !textCursor;
    textCursorTimer = 90;
}
else
{
    textCursorTimer -= 1;
}

draw_set_alpha(1);

yy += 12;
