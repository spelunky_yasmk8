// scrDrawKbText(status, key_text, button_val)
// from oKeyConfig > Draw

draw_set_font(global.myFontSmall);
draw_set_color(c_white);
draw_text(xx, 208, "PRESS ESCAPE TO KEEP SAME OR");
draw_text(xx, 216, "MOUSE WHEEL TO MOVE UP/DOWN");

if (argument0 == status)
{
    draw_set_color(c_yellow);
    draw_set_alpha(glowAlpha);
    draw_text(xx, 200, "PRESS KEY FOR " + argument1);
    draw_set_alpha(1);

    // selection highlight
    draw_set_color(c_silver);
    draw_rectangle(xx - 2, yy - 2, xx + 248, yy + 8, true);
}

draw_set_color(c_white);
draw_text(xx, yy, argument1);

if (argument0 == status)
{
    draw_set_color(glowCol);
    draw_set_alpha(glowAlpha);
}

var keyname;
keyname = scrGetKey(argument2);
draw_text(xx+xg, yy, keyname);
/*
switch (argument2)
{
    case vk_up: { draw_text(xx + xg, yy, "UP ARROW"); break; }
    case vk_down: { draw_text(xx + xg, yy, "DOWN ARROW"); break; }
    case vk_left: { draw_text(xx + xg, yy, "LEFT ARROW"); break; }
    case vk_right: { draw_text(xx + xg, yy, "RIGHT ARROW"); break; }
    case vk_shift: { draw_text(xx + xg, yy, "SHIFT"); break; }
    case vk_control: { draw_text(xx + xg, yy, "CTRL"); break; }
    case vk_alt: { draw_text(xx + xg, yy, "ALT"); break; }
    case vk_space: { draw_text(xx + xg, yy, "SPACE"); break; }
    case vk_enter: { draw_text(xx + xg, yy, "ENTER"); break; }
    case ord("A"): { draw_text(xx + xg, yy, "A"); break; }
    case ord("B"): { draw_text(xx + xg, yy, "B"); break; }
    case ord("C"): { draw_text(xx + xg, yy, "C"); break; }
    case ord("D"): { draw_text(xx + xg, yy, "D"); break; }
    case ord("E"): { draw_text(xx + xg, yy, "E"); break; }
    case ord("F"): { draw_text(xx + xg, yy, "F"); break; }
    case ord("G"): { draw_text(xx + xg, yy, "G"); break; }
    case ord("H"): { draw_text(xx + xg, yy, "H"); break; }
    case ord("I"): { draw_text(xx + xg, yy, "I"); break; }
    case ord("J"): { draw_text(xx + xg, yy, "J"); break; }
    case ord("K"): { draw_text(xx + xg, yy, "K"); break; }
    case ord("L"): { draw_text(xx + xg, yy, "L"); break; }
    case ord("M"): { draw_text(xx + xg, yy, "M"); break; }
    case ord("N"): { draw_text(xx + xg, yy, "N"); break; }
    case ord("O"): { draw_text(xx + xg, yy, "O"); break; }
    case ord("P"): { draw_text(xx + xg, yy, "P"); break; }
    case ord("Q"): { draw_text(xx + xg, yy, "Q"); break; }
    case ord("R"): { draw_text(xx + xg, yy, "R"); break; }
    case ord("S"): { draw_text(xx + xg, yy, "S"); break; }
    case ord("T"): { draw_text(xx + xg, yy, "T"); break; }
    case ord("U"): { draw_text(xx + xg, yy, "U"); break; }
    case ord("V"): { draw_text(xx + xg, yy, "V"); break; }
    case ord("W"): { draw_text(xx + xg, yy, "W"); break; }
    case ord("X"): { draw_text(xx + xg, yy, "X"); break; }
    case ord("Y"): { draw_text(xx + xg, yy, "Y"); break; }
    case ord("Z"): { draw_text(xx + xg, yy, "Z"); break; }
    default: { draw_text(xx + xg, yy, "KEY " + string(argument2)); break; }
}
*/

draw_set_alpha(1);
yy += 12;
