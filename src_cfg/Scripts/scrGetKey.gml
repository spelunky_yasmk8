//
// scrGetKey(key)
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/

switch (argument0) {
  case vk_up: return "UP ARR";
  case vk_down: return "DOWN ARR";
  case vk_left: return "LEFT ARR";
  case vk_right: return "RIGHT ARR";
  case vk_shift: return "SHIFT";
  case vk_control: return "CTRL";
  case vk_alt: return "ALT";
  case vk_space: return "SPACE";
  case vk_enter: return "ENTER";
  case ord("A"): return "A";
  case ord("B"): return "B";
  case ord("C"): return "C";
  case ord("D"): return "D";
  case ord("E"): return "E";
  case ord("F"): return "F";
  case ord("G"): return "G";
  case ord("H"): return "H";
  case ord("I"): return "I";
  case ord("J"): return "J";
  case ord("K"): return "K";
  case ord("L"): return "L";
  case ord("M"): return "M";
  case ord("N"): return "N";
  case ord("O"): return "O";
  case ord("P"): return "P";
  case ord("Q"): return "Q";
  case ord("R"): return "R";
  case ord("S"): return "S";
  case ord("T"): return "T";
  case ord("U"): return "U";
  case ord("V"): return "V";
  case ord("W"): return "W";
  case ord("X"): return "X";
  case ord("Y"): return "Y";
  case ord("Z"): return "Z";
  case ord("0"): return "0";
  case ord("1"): return "1";
  case ord("2"): return "2";
  case ord("3"): return "3";
  case ord("4"): return "4";
  case ord("5"): return "5";
  case ord("6"): return "6";
  case ord("7"): return "7";
  case ord("8"): return "8";
  case ord("9"): return "9";
  case vk_numpad0: return "KPINS";
  case vk_numpad1: return "KP1";
  case vk_numpad2: return "KP2";
  case vk_numpad3: return "KP3";
  case vk_numpad4: return "KP4";
  case vk_numpad5: return "KP5";
  case vk_numpad6: return "KP6";
  case vk_numpad7: return "KP7";
  case vk_numpad8: return "KP8";
  case vk_numpad9: return "KP9";
  case vk_escape: return "ESC";
  case vk_backspace: return "BACKSPACE";
  case vk_tab: return "TAB";
  case vk_home: return "HOME";
  case vk_end: return "END";
  case vk_delete: return "DELETE";
  case vk_insert: return "INSERT";
  case vk_pageup: return "PAGEUP";
  case vk_pagedown: return "PAGEDOWN";
  case vk_pause: return "PAUSE";
  case vk_printscreen: return "PRTSCR";
  case vk_f1: return "F1";
  case vk_f2: return "F2";
  case vk_f3: return "F3";
  case vk_f4: return "F4";
  case vk_f5: return "F5";
  case vk_f6: return "F6";
  case vk_f7: return "F7";
  case vk_f8: return "F8";
  case vk_f9: return "F9";
  case vk_f10: return "F10";
  case vk_f11: return "F11";
  case vk_f12: return "F12";
  case vk_multiply: return "KP*";
  case vk_divide: return "KP/";
  case vk_add: return "KP+";
  case vk_subtract: return "KP-";
  case vk_decimal: return "KPDEL";
  default: return "KEY"+string(argument0);
}
