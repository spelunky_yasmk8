// scrUpDown(globvarname, minval, maxval, label, infotext)
// `varname` starts with '!': special
// `varname` may and with `:width`
var objlx, objrx, objy, res, vname, spec, v, pos;

objlx = 40+8+string_length(argument3)*8;
objrx = objlx+4+8;
v = argument2;
while (true) {
  objrx += 8;
  v = v div 10;
  if (v <= 0) break;
}
objy = uiypos;
uiypos += 8;

vname = argument0;
spec = false;
if (string_char_at(vname, 1) == "!") {
  spec = true;
  vname = string_delete(vname, 1, 1);
}

pos = string_pos(":", vname);
if (pos > 0) {
  var pp;
  pp = pos;
  v = 0;
  pos += 1;
  while (pos <= string_length(vname)) {
    v = v*10+ord(string_char_at(vname, pos))-ord('0');
    pos += 1;
  }
  vname = string_delete(vname, pp, string_length(vname));
  objrx = objlx+4+8+v*8;
}

uiText[uiinum] = argument3;
uiInfoText[uiinum] = argument4;
uiNumericX[uiinum] = objrx;
uiNumericVar[uiinum] = vname;
uiinum += 1;

res = instance_create(objlx, objy, oUpDownButton);
res.varname = vname;
res.varmin = argument1;
res.varmax = argument2;
res.asminus = true;
res.varspecial = spec;

res = instance_create(objrx, objy, oUpDownButton);
res.varname = vname;
res.varmin = argument1;
res.varmax = argument2;
res.asminus = false;
res.varspecial = spec;
