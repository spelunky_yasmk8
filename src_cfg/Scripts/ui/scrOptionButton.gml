// scrOptionButton(globalvarname, label, infotext)
var objx, objy, res, vname, dis;

objx = 24;
objy = uiypos;
uiypos += 8;

vname = argument0;
dis = false;
if (string_char_at(vname, 1) == "-") {
  dis = true;
  vname = string_delete(vname, 1, 1);
}

uiText[uiinum] = argument1;
if (dis) {
  uiInfoText[uiinum] = "THIS OPTION IS DISABLED.";
} else {
  uiInfoText[uiinum] = argument2;
}
uiNumericX[uiinum] = -1;
uiNumericVar[uiinum] = "";
uiinum += 1;

if (!dis) {
  res = instance_create(objx, objy, oCheckBoxUI);
  res.varname = vname;
  if (variable_global_get(vname)) {
    res.on = true;
    res.sprite_index = sBoxChecked;
  }
}
