// scrUpDownPressed(varname, dir)
// called from oUpDownButton
if (argument0 == "scumGhost") {
  var dir, g;
  dir = argument1; // -1 or 1
  g = global.scumGhost;
  if (dir < 0) {
    if (g != 666) {
           if (g < 1) g = 1200;
      else if (g > 0 and g <= 10) g = 666;
      else if (g <= 30) g = 10;
      else if (g <= 60) g = 30;
      else if (g <= 90) g = 60;
      else if (g <= 120) g = 90;
      else if (g <= 180) g = 120;
      else if (g <= 300) g = 180;
      else if (g <= 600) g = 300;
      else if (g <= 900) g = 600;
      else if (g > 900) g = 900;
    }
  } else {
    if (g != 666) {
           if (g >= 1200) g = 0;
      else if (g >= 900) g = 1200;
      else if (g >= 600) g = 900;
      else if (g >= 300) g = 600;
      else if (g >= 180) g = 300;
      else if (g >= 120) g = 180;
      else if (g >= 90) g = 120;
      else if (g >= 60) g = 90;
      else if (g >= 30) g = 60;
      else if (g >= 1) g = 30;
    } else {
      g = 10;
    }
  }
  global.scumGhost = g;
}
