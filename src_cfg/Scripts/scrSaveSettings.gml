//
// scrSaveSettings()
//

/**********************************************************************************
    Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC

    This file is part of Spelunky.

    You can redistribute and/or modify Spelunky, including its source code, under
    the terms of the Spelunky User License.

    Spelunky is distributed in the hope that it will be entertaining and useful,
    but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.

    The Spelunky User License should be available in "Game Information", which
    can be found in the Resource Explorer, or as an external file called COPYING.
    If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>

***********************************************************************************/

file = file_text_open_write("yasm_settings.cfg");
if (file) {
  file_text_write_string(file, "fullscreen=");
   if (global.fullscreen) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "graphicsHigh=");
   if (global.graphicsHigh) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "downToRun=");
   if (global.downToRun) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "gamepadOn=");
   if (global.gamepadOn) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);
  file_text_write_string(file, "screenScale=");
   file_text_write_string(file, string(global.screenScale));
   file_text_writeln(file);
  file_text_write_string(file, "musicVol=");
   file_text_write_string(file, string(global.musicVol));
   file_text_writeln(file);
  file_text_write_string(file, "soundVol=");
   file_text_write_string(file, string(global.soundVol));
   file_text_writeln(file);
  file_text_write_string(file, "debuglog=");
   if (global.optDebugLog) file_text_write_string(file, "tan"); else file_text_write_string(file, "ona");
   file_text_writeln(file);

  file_text_close(file);
}
