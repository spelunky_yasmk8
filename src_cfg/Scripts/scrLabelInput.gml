// scrLabelInput
// any_key input
// argument0 = mouse wheel up/down
labelText = scrGetLabel(status);
var updated;
updated = false;

if (keyboard_key == vk_enter or keyboard_key == vk_down or argument0 == 180) {
  scrSetLabel(status);
  status += 1;
  if (status > 7) status = 0;
  scrGetLabel(status);
  updated = true;
} else if (keyboard_key == vk_up or argument0 == 90) {
  scrSetLabel(status);
  status -= 1;
  if (status < 0) status = 7;
  scrGetLabel(status);
  updated = true;
} else if (keyboard_key == vk_backspace or keyboard_key == vk_delete) {
  labelText = string_delete(labelText, string_length(labelText), 1);
} else {
  if (string_length(labelText) < 10) labelText += scrGetChar(keyboard_key);
}

if (not updated) scrSetLabel(status);
