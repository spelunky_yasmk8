{\rtf1\ansi\ansicpg1251\uc1\deff0\deflang1033\deflangfe1033{\fonttbl{\f0\fcharset0 Courier New;}{\f1 Arial;}}
\f0{\colortbl;\red4\green18\blue81;\red0\green0\blue0;}{\*\generator Wine Riched20 2.0.????;}\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\b\cf1\fs40\ulnone Spelunky User License}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\cf1\fs24\ulnone Copyright }{\cf1\fs24\ulnone (}{\cf1\fs24\ulnone c}{\cf1\fs24\ulnone ) }{\cf1\fs24\ulnone 2008}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone 2009 }{\cf1\fs24\ulnone Derek Yu and Mossmouth}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone LLC}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\b\cf1\fs24\ulnone 1}{\b\cf1\fs24\ulnone . }{\b\cf1\fs24\ulnone Preamble}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\cf1\fs24\ulnone As of December }{\cf1\fs24\ulnone 25}{\cf1\fs24\ulnone th}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone 2009}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone I}{\cf1\fs24\ulnone '}{\cf1\fs24\ulnone m releasing the }{\cf1\fs24\ulnone .}{\cf1\fs24\ulnone gmk files for Spelunky and the Spelunky}{\cf1\fs24\ulnone  }{\cf1\fs24\ulnone configuration program to the public}{\cf1\fs24\ulnone .  }{\cf1\fs24\ulnone The game and the source code are being provided to you}{\cf1\fs24\ulnone  }{\cf1\fs24\ulnone for the purpose of learning}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone entertainment}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone and sharing with the Spelunky community}{\cf1\fs24\ulnone .}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\cf1\fs24\ulnone I}{\cf1\fs24\ulnone '}{\cf1\fs24\ulnone m interested in seeing what you guys create}{\cf1\fs24\ulnone !  }{\cf1\fs24\ulnone Happy Spelunky}{\cf1\fs24\ulnone -}{\cf1\fs24\ulnone ing}{\cf1\fs24\ulnone !}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\b\cf1\fs24\ulnone 2}{\b\cf1\fs24\ulnone . }{\b\cf1\fs24\ulnone Definitions}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\cf1\fs24\ulnone "}{\cf1\fs24\ulnone Mossmouth}{\cf1\fs24\ulnone " }{\cf1\fs24\ulnone is defined as Derek Yu and Mossmouth}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone LLC}{\cf1\fs24\ulnone .}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\cf1\fs24\ulnone The }{\cf1\fs24\ulnone "}{\cf1\fs24\ulnone game}{\cf1\fs24\ulnone " }{\cf1\fs24\ulnone is defined as the main Spelunky program and all related files}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone including}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone but not}{\cf1\fs24\ulnone  }{\cf1\fs24\ulnone limited to}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone the Spelunky configuration program}{\cf1\fs24\ulnone .}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\cf1\fs24\ulnone The }{\cf1\fs24\ulnone "}{\cf1\fs24\ulnone source code}{\cf1\fs24\ulnone " }{\cf1\fs24\ulnone is defined as the }{\cf1\fs24\ulnone .}{\cf1\fs24\ulnone gmk files for Spelunky and the Spelunky configuration}{\cf1\fs24\ulnone  }{\cf1\fs24\ulnone program}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone and all of the wobjects}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone scripts}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone sprites}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone and tiles contained within those files}{\cf1\fs24\ulnone .}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\b\cf1\fs24\ulnone 3}{\b\cf1\fs24\ulnone . }{\b\cf1\fs24\ulnone Rights Granted}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\cf1\fs24\ulnone The rights granted under this license are granted for the duration of the copyright}{\cf1\fs24\ulnone .}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\cf1\fs24\ulnone You may modify and redistribute the game and its source code under the following conditions}{\cf1\fs24\ulnone :}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\cf1\fs24\ulnone   (}{\cf1\fs24\ulnone 1}{\cf1\fs24\ulnone ) }{\cf1\fs24\ulnone This license and any appropriate copyright notices in the source code are kept intact}{\cf1\fs24\ulnone .}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\cf1\fs24\ulnone   (}{\cf1\fs24\ulnone 2}{\cf1\fs24\ulnone ) }{\cf1\fs24\ulnone You may not sell the source code}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone the game}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone or anything derived from the source code}{\cf1\fs24\ulnone  }{\cf1\fs24\ulnone or the game}{\cf1\fs24\ulnone .}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\cf1\fs24\ulnone   (}{\cf1\fs24\ulnone 3}{\cf1\fs24\ulnone ) }{\cf1\fs24\ulnone You must display an appropriate copyright notice somewhere in the beginning of the}{\cf1\fs24\ulnone  }{\cf1\fs24\ulnone game and in documentation accompanying the game}{\cf1\fs24\ulnone . }
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\cf1\fs24\ulnone By accepting this license you are agreeing not to restrict or prevent Mossmouth from}{\cf1\fs24\ulnone  }{\cf1\fs24\ulnone modifying or adding to Spelunky in any way}{\cf1\fs24\ulnone .}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\b\cf1\fs24\ulnone 4}{\b\cf1\fs24\ulnone . }{\b\cf1\fs24\ulnone Disclaimer of Warranty}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\cf1\fs24\ulnone The game and its source code are provided WITHOUT WARRANTY}{\cf1\fs24\ulnone ; }{\cf1\fs24\ulnone without even the implied}{\cf1\fs24\ulnone  }{\cf1\fs24\ulnone warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE}{\cf1\fs24\ulnone .}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\b\cf1\fs24\ulnone 5}{\b\cf1\fs24\ulnone . }{\b\cf1\fs24\ulnone Limitation of Liability}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0{\cf1\fs24\ulnone In no event}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone unless required by applicable law or agreed to in writing will any copyright}{\cf1\fs24\ulnone  }{\cf1\fs24\ulnone holders be liable to you for damages}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone including any general}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone special}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone incidental}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone or}{\cf1\fs24\ulnone  }{\cf1\fs24\ulnone consequential damages arising out of the use or inability to use the game}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone the source code}{\cf1\fs24\ulnone ,}{\cf1\fs24\ulnone  }{\cf1\fs24\ulnone or modified versions of the game or the source code}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone even if such holder has been advised of}{\cf1\fs24\ulnone  }{\cf1\fs24\ulnone the possibility of such damages}{\cf1\fs24\ulnone .  }{\cf1\fs24\ulnone Such damages include}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone but are not limited to}{\cf1\fs24\ulnone , }{\cf1\fs24\ulnone loss of data}{\cf1\fs24\ulnone  }{\cf1\fs24\ulnone or data being rendered inaccurate}{\cf1\fs24\ulnone .}
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par\pard\sl-240\slmult1\li0\fi0\ri0\sa0\sb0\s-1\cfpat0\cbpat0
\par}
