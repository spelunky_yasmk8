import arsd.dom;

string[uint] idmap;

int main (string[] args) {
  foreach (string fname; args[1..$]) {
    import std.file : readText;
    auto doc = new Document(readText(fname));
    auto obj = doc.querySelector("object[id]");
    if (obj) {
      import std.conv : to;
      auto id = to!uint(obj.getAttribute("id"));
      if (auto pname = id in idmap) {
        import std.stdio;
        writeln("DUPLICATE ID ", id, " for object '", fname, "' (previous: '", *pname, "'");
        //return -1;
      } else {
        idmap[id] = fname;
      }
    }
  }
  {
    import std.algorithm : sort;
    import std.stdio;
    uint[] ids;
    foreach (uint id; idmap.byKey) ids ~= id;
    ids.sort;
    //foreach (uint id; ids) writeln(id);
    writeln("minimal: ", ids[0]);
    writeln("maximal: ", ids[$-1]);
  }
  return 0;
}
