module yasmdatdecode is aliced;

import std.stdio;

string scrCipher (string textStr1, usize key, bool encrypt=false) {
  //while (textStr1.length > 0 && textStr1[0] <= ' ') textStr1 = textStr1[1..$];
  //while (textStr1.length > 0 && textStr1[$-1] <= ' ') textStr1 = textStr1[0..$-1];
  string keyStr1;
  // each score slot uses a unique encryption key (ONLY USE ASCII CHARACTERS 32-125)
  final switch (key%10) {
    case 0: keyStr1 = r"F&-RlbkW{,1]DY|)8>b|FNMh@q]nnV.3"; break; //"
    case 1: keyStr1 = r"U=O=+>=BXNswc=%TJY;3tI^|&2kot7UG"; break; //"
    case 2: keyStr1 = r"&kdJ(ga^\c&PTNRJ?eNlVQD|VOMiX}s+"; break; //"
    case 3: keyStr1 = r"|!n4@OQ.<&Q/K\kEo-:o+>?qsK1t7N&O"; break; //"
    case 4: keyStr1 = r"3rRZ!<<]8r=<X2:Q$)&=f_%o`\h-c,7i"; break; //`
    case 5: keyStr1 = r"y{BKQJE+()<?{jmD+.Owek{Hk;BN[OP)"; break; //"
    case 6: keyStr1 = r"v|L2GV;yl{90_I(e?cHX3^0a}I(kU(dR"; break; //"
    case 7: keyStr1 = r"TkuG%K}?l{r_tZ=,fJ`s=r6;nm-HMLa!"; break; //`
    case 8: keyStr1 = r"Y0?zp?B{N|4Jy<PGJ>Az%,6wvU=bW?:["; break; //"
    case 9: keyStr1 = r"I3}-tUBiuPM`_5l&kG!|>}S@.>Ch&Q<$"; break; //`
  }

  int newCharCode = 0; // ascii code for newChar
  int keyPos = 0;
  string res;
  foreach (char ch; textStr1) {
    if (ch < ' ' || ch >= '\x7f') continue;
    // get current character
    int charCode = ch-32;
    // get current key character
    int keyCharCode = keyStr1[keyPos++]-32;
    if (keyPos >= keyStr1.length) keyPos = 0; // loop key
    // cipher
    if (encrypt) {
      // encrypt
      newCharCode = ((charCode+keyCharCode)%93)+32;
    } else {
      // decrypt
      newCharCode = charCode-keyCharCode;
      if (newCharCode < 0) newCharCode += 93;
      newCharCode = (newCharCode%93)+32;
    }
    if (newCharCode < 32 || newCharCode > 125) assert(0, "Invalid character in encryption/decryption.");
    res ~= cast(char)newCharCode;
  }
  return res;
}


static immutable string[114] VarNames = [
  "Money", // 0
  "Time", // 1
  "Kills", // 2
  "Saves", // 3
  "Plays", // 4
  "Wins", // 5
  "Deaths", // 6
  "Tunnel 1", // 7 // This is how much you 'owe' the tunnel man before he builds the first shortcut
  "Tunnel 2", // 8
  "Mini games", // 9
  "YASM 1.8 extra save data", // 10
  //
  "TOTALS VERSION",
  //
  "CRATES OPENED",
  "CHESTS OPENED",
  "IDOLS GRABBED",
  "IDOLS CONVERTED",
  "DAMSELS GRABBED",
  "KISSES BOUGHT",
  "DAMSELS BOUGHT",
  "DAMSELS SAVED",
  "DAMSELS KILLED",
  "ITEMS BOUGHT",
  "ITEMS STOLEN",
  "MONEY EARNED",
  "MONEY SPENT",
  "DICE GAMES PLAYED",
  "DICE GAMES WON",
  "DICE GAMES LOST",
  "SACRIFICES",
  "DEATHS ON LEVEL 1",
  "DEATHS ON LEVEL 2",
  "DEATHS ON LEVEL 3",
  "DEATHS ON LEVEL 4",
  "DEATHS ON LEVEL 5",
  "DEATHS ON LEVEL 6",
  "DEATHS ON LEVEL 7",
  "DEATHS ON LEVEL 8",
  "DEATHS ON LEVEL 9",
  "DEATHS ON LEVEL 10",
  "DEATHS ON LEVEL 11",
  "DEATHS ON LEVEL 12",
  "DEATHS ON LEVEL 13",
  "DEATHS ON LEVEL 14",
  "DEATHS ON LEVEL 15",
  "DEATHS ON LEVEL 16",

  "DEATHES FROM BATS",
  "DEATHES FROM SNAKES",
  "DEATHES FROM SPIDERS",
  "DEATHES FROM G. SPIDERS",
  "DEATHES FROM CAVEMANS",
  "DEATHES FROM SKELETONS",
  "DEATHES FROM ZOMBIES",
  "DEATHES FROM VAMPIRES",
  "DEATHES FROM FROGS",
  "DEATHES FROM FIRE FROGS",
  "DEATHES FROM MANTRAPS",
  "DEATHES FROM PIRANHAS",
  "DEATHES FROM MEGAMOUTHS",
  "DEATHES FROM YETIS",
  "DEATHES FROM YETI KINGS",
  "DEATHES FROM ALIENS",
  "DEATHES FROM UFOS",
  "DEATHES FROM ALIEN BOSSS",
  "DEATHES FROM HAWKMANS",
  "DEATHES FROM SHOPKEEPERS",
  "DEATHES FROM TOMB LORDS",
  "DEATHES FROM MAGMA MANS",
  "DEATHES FROM OLMECS",
  "DEATHES FROM GHOST",
  "1:DEATHES FROM COBRAS",
  "1:DEATHES FROM GREEN SPIDERS",
  "1:DEATHES FROM GREEN FROGS",

  "DEATHES BY ROCKS",
  "DEATHES BY EXPLOSIONS",
  "DEATHES BY CRUSHEDS",
  "DEATHES BY LONG FALLS",
  "DEATHES BY SPIKESS",
  "DEATHES BY BOULDERS",
  "DEATHES BY ARROW TRAPS",
  "DEATHES BY SPEAR TRAPS",
  "DEATHES BY SMASH TRAPS",
  "DEATHES BY CEILING TRAPS",
  "DEATHES BY PITS",
  "DEATHES BY LAVAS",
  "DEATHES BY SUICIDES",
  "DEATHES BY OTHERS",

  "BATS KILLED",
  "SNAKES KILLED",
  "SPIDERS KILLED",
  "G. SPIDERS KILLED",
  "CAVEMANS KILLED",
  "SKELETONS KILLED",
  "ZOMBIES KILLED",
  "VAMPIRES KILLED",
  "FROGS KILLED",
  "FIRE FROGS KILLED",
  "MANTRAPS KILLED",
  "PIRANHAS KILLED",
  "MEGAMOUTHS KILLED",
  "YETIS KILLED",
  "YETI KINGS KILLED",
  "ALIENS KILLED",
  "UFOS KILLED",
  "ALIEN BOSS' KILLED",
  "HAWKMANS KILLED",
  "SHOPKEEPERS KILLED",
  "TOMB LORDS KILLED",
  "OLMECS KILLED",
  "OTHERS KILLED",
  "1:COBRAS KILLED",
  "1:GREEN SPIDERS KILLED",
  "1:GREEN FROGS KILLED",
  "MONKEYS KILLED",
];


void main (string[] args) {
  auto fl = File(args.length > 1 ? args[1] : "yasm.dat");
  usize idx = 0, lineidx = 0, lnum = 0;
  uint ver = 0;
  foreach (string s; fl.byLineCopy) {
    if (ver == 0) {
      // skip v1 fields
      import std.algorithm : startsWith;
      while (idx < VarNames.length && VarNames[idx].startsWith("1:")) {
        //writeln("SKIPPED: ", VarNames[idx][2..$]);
        ++idx;
      }
    }
    s = scrCipher(s, (lineidx <= 10 ? lineidx : lineidx-11));
    ++lineidx;
    //{ ++lnum; writeln(lnum, ": < ", s, " >"); }
    // get the real value in square brackets from a string like: "laskjf982jq flkjasf[935]alskdjf8a3j88aj"
    bool wasValue = false;
    auto sorig = s;
    while (s.length > 0) {
      import std.string : indexOf;
      auto p0 = s.indexOf('[');
      if (p0 < 0) break;
      auto p1 = s.indexOf(']', p0+1);
      if (p1 < 0) break;
      auto v = s[p0+1..p1];
      s = s[p1+1..$];
      if (v.length == 0) continue;
      usize pos = 0;
      if (v[0] == '-' || v[0] == '+') pos = 1;
      if (pos >= v.length) continue;
      while (pos < v.length && v[pos] >= '0' && v[pos] <= '9') ++pos;
      if (pos < v.length) continue;
      import std.conv : to;
      auto val = to!int(v);
      wasValue = true;
      if (idx == 9) {
        // decode minigames
        int stars = val%100; val = val/100;
        int moon = val%100; val = val/100;
        int sun = val;
        writeln("StarsRoom = ", stars);
        writeln("MoonRoom = ", moon);
        writeln("SunRoom = ", sun);
      } else if (idx < VarNames.length) {
        import std.algorithm : startsWith;
        string n = VarNames[idx];
        if (n == "TOTALS VERSION") ver = val;
        if (n.startsWith("1:")) n = n[2..$];
        writeln(n, " = ", val);
      } else {
        writeln("DUNNO #", idx, " = ", val);
      }
      break;
    }
    if (!wasValue) writeln("FUCKED STTRING: [", sorig, "]");
    ++idx;
  }
}
